<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
//Broadcast::channel('pool.{poolId}', function ($user, $poolId) {
//    $pool = Pool::find($poolId);
//    return $user->id == $pool->user_id;
//});

//Broadcast::channel('items.{item}', function ($user, 1) {
//    return $user->id === $item->user_id;
//});
