<?php
use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware('api_user')->group(function () {

///////////////////////////////---DOCTOR PANEL---//////////////////
Route::get('/view-doctor-all-profile-info/{id}', 'DoctorApi\doctor\DoctorApiController@getDoctorInfo');
Route::post('/doctor-sign-up', 'DoctorApi\doctor\DoctorApiController@signup');
Route::post('/doctor-sign-in', 'DoctorApi\doctor\DoctorApiController@signin');
Route::get('/view-doctor-profile/{id}', 'DoctorApi\doctor\DoctorApiController@viewDoctorProfile');
Route::post('/update-Doctor-Profile','DoctorApi\doctor\DoctorApiController@updateDoctorProfile');
Route::post('/doctor-forgot-password','DoctorApi\doctor\DoctorApiController@doctorUpdatePass');

//////////PMDC
Route::get('/doctor-get-pmdc/{id}', 'DoctorApi\doctor\DoctorApiController@getDoctorPmdc');
Route::post('/doctor-update-pmdc', 'DoctorApi\doctor\DoctorApiController@updateDoctorPmdc');
Route::get('/submit-for-review/{id}', 'DoctorApi\doctor\DoctorApiController@submitProProfile');

/////////NOTIFICATION
Route::get('doctor-get-all-notification/{doctor_id}', 'DoctorApi\doctor\DoctorApiController@getNotificationList');
Route::get('doctor-get-all-unread-notification/{doctor_id}', 'DoctorApi\doctor\DoctorApiController@getuUreadNotificationList');
Route::get('doctor-update-read-notification/{id}', 'DoctorApi\doctor\DoctorApiController@readNotification');
///////////////chat
Route::post('/doctor-get-all-message', 'DoctorApi\Chat\DoctorChatApiController@getMessageFromDoctor');
Route::post('/doctor-send-text', 'DoctorApi\Chat\DoctorChatApiController@sendMessage');
Route::post('/doctor-send-file', 'DoctorApi\Chat\DoctorChatApiController@sendFileToDoctor');
Route::post('/doctor-send-voice', 'DoctorApi\Chat\DoctorChatApiController@sendVoiceToDoctor');



/////EDUCATION
Route::post('/doctor-add-edu','DoctorApi\doctor\DoctorApiEducationController@addeducation');
Route::get('/doctor-view-all-edu/{id}','DoctorApi\doctor\DoctorApiEducationController@viewAllEducation');
Route::get('/doctor-get-edu/{id}','DoctorApi\doctor\DoctorApiEducationController@getEducation');
Route::post('/doctor-update-edu','DoctorApi\doctor\DoctorApiEducationController@updateEducation');
Route::get('/doctor-delete-edu/{id}','DoctorApi\doctor\DoctorApiEducationController@deleteEducation');
//////Experiance
Route::post('/doctor-add-experiance','DoctorApi\doctor\DoctorApiExperianceController@addExperiance');
Route::get('/doctor-view-all-experiance/{id}','DoctorApi\doctor\DoctorApiExperianceController@viewAllExperiance');
Route::get('/doctor-get-experiance/{id}','DoctorApi\doctor\DoctorApiExperianceController@getExperiance');
Route::post('/doctor-update-experiance','DoctorApi\doctor\DoctorApiExperianceController@updateExperiance');
Route::get('/doctor-delete-experiance/{id}','DoctorApi\doctor\DoctorApiExperianceController@deleteExperiance');
///////Service
Route::get('/doctor-get-all-service/{id}','DoctorApi\doctor\DoctorApiServiceController@getAllServices');
Route::post('/doctor-add-service','DoctorApi\doctor\DoctorApiServiceController@addService');
Route::get('/doctor-my-service/{id}','DoctorApi\doctor\DoctorApiServiceController@viewMyService');
Route::get('/doctor-delete-service/{id}','DoctorApi\doctor\DoctorApiServiceController@deleteService');
////////spaciality
Route::get('/get-all-pro-profile-data/{doctor_id}','DoctorApi\doctor\DoctorApiSpacialityController@get_all_p_p');
Route::get('/doctor-all-Primary-and-Secondary-Specialization','DoctorApi\doctor\DoctorApiSpacialityController@getAllPrimaryAndSecondarySpecialization');
Route::post('/doctor-add-primary-spaciality','DoctorApi\doctor\DoctorApiSpacialityController@addPrimarySpeciality');
Route::get('/doctor-view-my-primary-spaciality/{id}','DoctorApi\doctor\DoctorApiSpacialityController@viewMyPrimarySpeciality');

Route::post('/doctor-add-spaciality','DoctorApi\doctor\DoctorApiSpacialityController@addSpeciality');
Route::get('/doctor-view-all-spaciality/{id}','DoctorApi\doctor\DoctorApiSpacialityController@viewAllSpeciality');
Route::post('/doctor-delete-spaciality','DoctorApi\doctor\DoctorApiSpacialityController@deleteSpeciality');

///////Timetable
Route::post('/doctor-create-timetable','DoctorApi\doctor\DoctorApiTimeTableController@createVisitTime');
Route::post('/doctor-view-timetable','DoctorApi\doctor\DoctorApiTimeTableController@viewTimetable');
Route::post('/doctor-change-day-status','DoctorApi\doctor\DoctorApiTimeTableController@dayStatus');
Route::post('/doctor-change-time-status','DoctorApi\doctor\DoctorApiTimeTableController@timeStatus');
Route::post('/doctor-delete-timetable','DoctorApi\doctor\DoctorApiTimeTableController@deleteTimatable');

/////////clinic
Route::get('/doctor-only-my-created-clinic/{id}','DoctorApi\doctor\DoctorApiClinicController@doctor_only_my_created_clinic');
Route::get('/doctor-only-my-editable-clinic/{id}','DoctorApi\doctor\DoctorApiClinicController@doctor_only_my_editable_clinic');
Route::post('/doctor-add-clinic','DoctorApi\doctor\DoctorApiClinicController@addClinic');
Route::get('/doctor-get-all-clinic','DoctorApi\doctor\DoctorApiClinicController@viewAllClinic');
Route::get('/doctor-get-my-clinic/{id}','DoctorApi\doctor\DoctorApiClinicController@viewMyClinic');
Route::post('/doctor-update-clinic','DoctorApi\doctor\DoctorApiClinicController@updateClinic');
//Route::get('/doctor-delete-clinic/{id}','DoctorApi\doctor\DoctorApiClinicController@deleteClinic');
Route::post('/doctor-save-clinic','DoctorApi\doctor\DoctorApiClinicController@saveClinic');
Route::get('/doctor-view-save-clinic/{id}','DoctorApi\doctor\DoctorApiClinicController@viewSaveClinic');
//Route::get('/doctor-remove-save-clinic/{id}','DoctorApi\doctor\DoctorApiClinicController@removeSaveClinic');
Route::post('/doctor-delete-clinic','DoctorApi\doctor\DoctorApiClinicController@removeSaveClinic');

////////Appointment-Patients Profile
Route::post('/doctor-view-all-appointment','DoctorApi\doctor\DoctorApiAppointmentController@allAppointmentList');
Route::get('/doctor-cancel-appointment/{id}','DoctorApi\doctor\DoctorApiAppointmentController@cancelAppointment');
Route::get('/doctor-view-all-patient-list/{id}','DoctorApi\doctor\DoctorApiAppointmentController@allMyPatientList');
Route::post('/doctor-view-patients-profile','DoctorApi\doctor\DoctorApiAppointmentController@viewPatientsProfile');
Route::get('/get-all-user-text-doctor/{id}','DoctorApi\doctor\DoctorApiAppointmentController@allMyPatientListSendDoctorText');

////////////condition
Route::get('/doctor-get-all-condition/{id}','DoctorApi\doctor\DoctorApiConditionController@getAllCondition');
Route::get('/doctor-delete-condition/{id}','DoctorApi\doctor\DoctorApiConditionController@deleteCondition');
Route::post('/doctor-add-condition','DoctorApi\doctor\DoctorApiConditionController@addCondition');
Route::get('/doctor-view-my-condition/{id}','DoctorApi\doctor\DoctorApiConditionController@viewMyCondition');




///////////////////////////////---public user---//////////////////
Route::post('/change-lang', 'DoctorApi\MyapiController@changeLanguageOfApp');
Route::post('/login', 'DoctorApi\MyapiController@loginUser');
Route::post('/send-number', 'DoctorApi\MyapiController@getPhoneNumber');
Route::post('/check-verification-code', 'DoctorApi\MyapiController@check_verification_code');
Route::post('/user-update-pass', 'DoctorApi\MyapiController@userUpdatePassword');
Route::post('/user-register', 'DoctorApi\MyapiController@userRgister');
Route::get('/get-All-Spaciality/{lang}', 'DoctorApi\MyapiController@getAllSpaciality');
Route::get('/get-All-Spaciality-and-doctors/{lang}', 'DoctorApi\MyapiController@getAllSpacialityAndDoctors');
//Route::post('/get-Spac-star-avail-of-doctor-by-id', 'DoctorApi\MyapiController@getSpacialityStarByDoctorId');arif

Route::get('/get-All-District/{lang}', 'DoctorApi\MyapiController@getAllDistrict');
//Route::get('/get-All-Doctors', 'DoctorApi\MyapiController@getAllDoctor');Arif
Route::get('/get-doctor-info/{id}', 'DoctorApi\MyapiController@getDoctorInfo');
//Route::post('/join-Doctor-Request', 'DoctorApi\MyapiController@joinDoctorRequest');Arif
//Route::post('/search-Doctor-by-spaciality-and-district', 'DoctorApi\MyapiController@searchDoctorBySpacialityAndDistrict');Arif
//Route::post('/search-Doctor-By-names-And-District', 'DoctorApi\MyapiController@searchDoctorByNameAndDistrict');Arif
Route::post('/search-by-filter', 'DoctorApi\MyapiController@filterDoctorList');

Route::post('/book-Appointment', 'DoctorApi\MyapiController@bookAppointment');
Route::post('/store-Appointment', 'DoctorApi\MyapiController@storeAppointment');

///////////////////////////////---blogs---//////////////////
Route::get('/get-All-Blogs', 'DoctorApi\MyapiController@getAllBlogs');
Route::post('/view-blog-post', 'DoctorApi\MyapiController@viewBlogs');

///////////////////////////////---Patients Portal---//////////////////
Route::get('my-doctor-related-appointment/{user_id}', 'DoctorApi\PatientApiController@getTheAppointmentDoctorsList');
Route::post('/allow-sharing-record', 'DoctorApi\PatientApiController@allowSharingAllRecord');
Route::post('/allow-sharing-the-record', 'DoctorApi\PatientApiController@allowSharingRecord');

Route::post('/pending-feedback', 'DoctorApi\PatientApiController@pendingFeedback');
Route::post('/store-feedback', 'DoctorApi\PatientApiController@storeFeedback');
Route::post('/patient-profile', 'DoctorApi\PatientApiController@patientProfile');
Route::post('/update-patient-profile', 'DoctorApi\PatientApiController@updatePatient');
Route::post('/my-doctor', 'DoctorApi\PatientApiController@myDoctor');
Route::post('/bookmark-doctor', 'DoctorApi\PatientApiController@bookmarkDoctor');
Route::post('/cancel-bookmark-doctor', 'DoctorApi\PatientApiController@cancelBookmarkDoctor');

Route::post('/add-family-member', 'DoctorApi\PatientApiController@addFamilyMember');
Route::post('/view-all-family-member', 'DoctorApi\PatientApiController@viewAllFamilyMember');
Route::post('/update-family-member', 'DoctorApi\PatientApiController@updateFamilyMember');
Route::post('/delete-family-member', 'DoctorApi\PatientApiController@deleteFamilyMember');

Route::get('/list-family-member-get-appointment/{userid}', 'DoctorApi\PatientApiController@view_All_Family_who_get_appointment');

///////////////chat
Route::post('/user-get-all-message', 'DoctorApi\Chat\UserChatApiController@getMessageFromDoctor');
Route::post('/user-send-text', 'DoctorApi\Chat\UserChatApiController@sendMessage');
Route::post('/user-send-file', 'DoctorApi\Chat\UserChatApiController@sendFileToDoctor');
Route::post('/user-send-voice', 'DoctorApi\Chat\UserChatApiController@sendVoiceToDoctor');
Route::get('/user-delete-text-chat/{id}', 'DoctorApi\Chat\UserChatApiController@deleteText');

Route::post('/forgot-password', 'DoctorApi\PatientApiController@forgetPassword');
Route::post('/my-appointment-list', 'DoctorApi\PatientApiController@myAppointmentList');
Route::post('/all-appointment-list', 'DoctorApi\PatientApiController@allAppointmentList');
Route::post('/family-appointment-list', 'DoctorApi\PatientApiController@familyAppointmentList');
Route::post('/cancel-appointment', 'DoctorApi\PatientApiController@cancelAppointment');
Route::post('/update-appointment-user', 'DoctorApi\PatientApiController@updateAppointment');
Route::post('/get-appointment', 'DoctorApi\PatientApiController@getAppointment');

Route::post('/add-medical-records', 'DoctorApi\PatientApiController@addMedicalRecords');
Route::post('/view-all-medical-records', 'DoctorApi\PatientApiController@viewMedicalRecords');
Route::post('/get-medical-records', 'DoctorApi\PatientApiController@getRecordsById');
Route::post('/delete-medical-records', 'DoctorApi\PatientApiController@deleteRecords');
Route::post('/update-medical-records', 'DoctorApi\PatientApiController@updateRecords');
///inside records we have the files related APi'S
Route::post('user-delete-files-of-record', 'DoctorApi\PatientApiController@deleteFile');
Route::post('view-files-of-record', 'DoctorApi\PatientApiController@viewRecords');
Route::post('add-new-files-to-records', 'DoctorApi\PatientApiController@addNewFilesToRecord');
/////////NOTIFICATION
Route::get('user-get-all-notification/{user_id}', 'DoctorApi\PatientApiController@getNotificationList');
Route::get('user-get-all-unread-notification/{user_id}', 'DoctorApi\PatientApiController@getuUreadNotificationList');
Route::get('user-update-read-notification/{id}', 'DoctorApi\PatientApiController@readNotification');


});




