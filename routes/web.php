<?php

Auth::routes();

//Route::get('/', function () {
//    return redirect(app()->getLocale());
//});

if (app()->getLocale() == 'en') {
    Route::redirect('/', '/en');
}

if (app()->getLocale() == 'fa') {
    Route::redirect('/', '/fa');
}

if (app()->getLocale() == 'pa') {
    Route::redirect('/', '/pa');
}

Route::get('locale/{locale}', function ($locale) {

    Session::put('language', $locale);
    // dd($a);
    return redirect()->back();
})->name('locale');

Route::group(['prefix' => '{language}'], function() {

    Route::get('/terms-and-conditions', function () {
        return view('public/termscondition');
    })->name('terms.conditions');

    Route::get('policy', function () {
        return view('public/policy');
    })->name('doctor.policy');


    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/login-page', 'Auth\LoginController@loginPage')->name('login.page.user');
    Route::get('/register-page', 'Auth\LoginController@register_page')->name('register.page');
    Route::get('/join-as-doctor', 'Auth\LoginController@join_as_doctor')->name('join.as.doctor');
    Route::get('/forget-password', 'Auth\LoginController@forget_password')->name('forget.password');
    Route::get('/view-doctor-profile/{id}', 'HomeController@viewDoctorProfile')->name('view.doctor.profile');
    Route::post('/register/user/page', 'Admin\AdminAuthController@registerUser')->name('user.register.them');
    Route::post('/doctor-join', 'DoctorController@signupDoctor')->name('doctor.join.as');
    Route::post('/userlogin', 'Auth\LoginController@loginUser')->name('login.user');
    Route::get('/user-logout', 'Auth\LoginController@logout')->name('logout.user');
    Route::get('/Join-As-Doctor', 'HomeController@joinDoctor')->name('Join.As.Doctor');
    Route::post('/search-district-Spaciality', 'HomeController@searchDoctorByDistrictAndSpaciality')->name('search.district.Spaciality');

    Route::post('/filter-doctor', 'HomeController@filterDoctorList')->name('use.filter.doctor');
    Route::get('/doctors-list/{id}', 'HomeController@searchDoctorBySpaciality')->name('search.doctor.Spaciality');
    Route::get('/all-doctors-list', 'HomeController@allDoctorList')->name('all.list.doctor');

    Route::post('/store-appointment', 'AppoinmentController@storeAppointment')->name('store.appointment');
    Route::get('/success-appointment', 'AppoinmentController@SuccessAppointmentView')->name('success.appointment.page');

    Route::get('/email-active/{doctorid}/{token}', 'HomeController@emailActivationDoctor')->name('email.active');
    
    Route::get('/email-reset/{doctorid}/{token}', 'HomeController@emailResetDoctor')->name('email.reset');
    Route::post('/doctor-reset-pass', 'HomeController@passwordResetDoctor')->name('doctor.password.updatereset');

    Route::get('send-number-to-server', 'HomeController@getPhoneNumber');
    Route::get('get-doctor-clinic-ajax', 'HomeController@getDoctorAvailableClinic');
    Route::get('find/{q}', 'HomeController@find')->name('spa.find');
    Route::get('finddoctor/{q}', 'HomeController@finddoctor')->name('doctor.find');
    Route::get('getalldistrict/{district}', 'HomeController@getalldistrict')->name('get.all.district');
    Route::get('/get/doctor/by/class/spaciality/{id}', 'HomeController@searchDoctorByClassSpaciality')->name('get.doctor.by.class.spaciality');
    Route::get('ajax-get-time', 'AppoinmentController@edit')->name('ajaxs.get.time');

    /////blog
    Route::get('blogs', 'Admin\BlogController@publicBlog')->name('public.blog.list');
    Route::get('public/view/blog/{id}', 'Admin\BlogController@publicblogView')->name('public.view.blog');
    Route::get('public/post/view/{id}', 'Admin\BlogController@publicBlogFilterCategory')->name('public.post.view');
    Route::get('/book/appointment/{id}', 'AppoinmentController@index')->name('Book.Appointment');
    Route::post('Comments-add', 'Admin\BlogController@CommentAdd')->name('comment.add');


///////////////////////////////////////////////////
///////////////for Patient portal//////////////////
///////////////////////////////////////////////////
////Bookmark-doctor
  //  Route::domain('pt.doctorplus.io')->group(function ($router) {
        Route::get('/pateint-home', 'PateintController@home')->name('pateint.home')->middleware('checkpateint');
        Route::get('bookmark/doctor/{id}', 'PateintController@bookmarkDoctor')->name('bookmark.doctor')->middleware('checkpateint');
        Route::get('cancel/bookmark/doctor/{id}', 'PateintController@cancelBookmarkDoctor')->name('cancel.bookmark.doctor')->middleware('checkpateint');

/////myprofile


        Route::get('My-profile', 'PateintController@myProfile')->name('my.profile')->middleware('checkpateint');
        Route::post('user-update-profile', 'PateintController@updateProfile')->name('pateint.updateprofile')->middleware('checkpateint');
        Route::get('/logout', 'Auth\LoginController@logout')->name('pateint.logout')->middleware('checkpateint');
        Route::get('my-doctors', 'PateintController@getMyDoctors')->name('get.my.doctors')->middleware('checkpateint');
        Route::post('password', 'PateintController@savePassword')->name('password.save')->middleware('checkpateint');

///feedback
        Route::post('store-feedback', 'FeedbackController@store')->name('feedback.store')->middleware('checkpateint');
        Route::get('/user-feedback', 'FeedbackController@index')->name('pateint.feedback')->middleware('checkpateint');


////chat
        Route::get('private-chat-with-doctor', 'ChatController@userChatView')->name('user.Private.Chat')->middleware('checkpateint');
        Route::get('get-chat/{id}', 'ChatController@getMessageFromDoctor')->name('user.get.Chat')->middleware('checkpateint');
        Route::post('sendmessage', 'ChatController@sendMessage')->name('user.send.message')->middleware('checkpateint');
        Route::post('uploadFileToDoctor', 'ChatController@uploadFileToDoctor')->name('upload.File.To.Doctor');

/////appointment

        Route::post('/search-appointment-list', 'PateintController@searchAppointmentbyType')->name('search.appointment.list')->middleware('checkpateint');
        Route::get('/pateint-appointment-list', 'PateintController@appointment')->name('pateint.appointment')->middleware('checkpateint');
        Route::get('pateint/cancel/appointment/{app_id}/', 'PateintController@cancelmyAppointment')->name('pateint.cancel.appointment')->middleware('checkpateint');
        Route::get('ajax-unread-appointment-notification', 'PateintController@ajaxUnreadNotificationAppointment')->name('ajax.unread.appointment.notification')->middleware('checkpateint');

/////familymember
        Route::get('pateint/view/familymember/{pateint_id}/', 'PateintController@familyMemberView')->name('pateint.familymamber.View')->middleware('checkpateint');
        Route::get('pateint/edit/editFamilyMember/{pateint_id}/', 'PateintController@editFamilyMember')->name('pateint.familymamber.Edit')->middleware('checkpateint');
        Route::get('pateint/delete/familymember/{pateint_id}/', 'PateintController@deleteFamilyMember')->name('pateint.familymamber.Delete')->middleware('checkpateint');
        Route::get('/pateint-family-member', 'PateintController@familyMember')->name('pateint.familymamber')->middleware('checkpateint');
        Route::post('pateint-update-family', 'PateintController@updateFamilyMember')->name('pateint.updatefamilymember')->middleware('checkpateint');
        Route::get('pateint-add-family-member', 'PateintController@addFamilyMemberView')->name('pateint.addFamilyMemberView')->middleware('checkpateint');
        Route::post('pateint-add-family', 'PateintController@addFamilyMember')->name('pateint.addfamilymember')->middleware('checkpateint');
        /////medical-records
        Route::get('multiple-file-upload', 'MultipleUploadController@index')->name('multiple.file.upload')->middleware('checkpateint');
        Route::get('view-my-records/{doc_id}', 'MultipleUploadController@viewRecords')->name('view.my.records')->middleware('checkpateint');
        Route::get('delete-my-records/{record_id}', 'MultipleUploadController@deleteRecords')->name('delete.my.records')->middleware('checkpateint');
        Route::get('allow-share-records/{record_id}', 'MultipleUploadController@allowSharingRecord')->name('allow.share.records')->middleware('checkpateint');

        Route::post('multiple-file-upload/upload', 'MultipleUploadController@upload')->name('upload.multi.file')->middleware('checkpateint');
        Route::get('pateints-medical-records', 'PateintController@viewMedicalRecords')->name('pateint.medical.records')->middleware('checkpateint');
        Route::get('user/delete/doc/{doc_id}/', 'PateintController@deleteDocument')->name('user.delete.doc')->middleware('checkpateint');
        Route::get('upload/document/{doc_id}/', 'FileUploadController@uploadDocView')->name('upload.doc')->middleware('checkpateint');

//////notification

        Route::get('user-noti-list/{userid}', 'PateintController@get_Notification_List')->name('user.noti.list');

        Route::get('change-password', function () {
            return view('pateint/changepassword');
        })->name('user.change.pass');
   // });

///////////////////////////////////////////////
////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
///////////////////////////////////////////////
/////////////////////////////////////////////////////
///////////////////For Doctor Panel/////////////////
///////////////////////////////////////////////////
   // Route::domain('dr.doctorplus.io')->group(function ($router) {
/////////////////////Speciality
        Route::post('add/primary/Speciality/', 'DoctorProProController@addPrimarySpeciality')->name('add.primary.Speciality')->middleware('checkdoctor'); //
        Route::post('add/secondry/Speciality/', 'DoctorProProController@addSecondrySpeciality')->name('add.secondry.Speciality')->middleware('checkdoctor'); //
        Route::get('delete/Speciality/{id}/', 'DoctorProProController@deleteSpeciality')->name('delete.Speciality')->middleware('checkdoctor');//
//doctor-service
        Route::post('add/service/doctor/', 'DoctorProProController@addService')->name('add.doctor.service')->middleware('checkdoctor'); //
        Route::get('delete/service/doctor/{id}/', 'DoctorProProController@deleteService')->name('delete.service.doctor')->middleware('checkdoctor'); //
//doctor-condition
        Route::post('add/condition/doctor/', 'DoctorProProController@addCondition')->name('add.doctor.condition')->middleware('checkdoctor'); //
        Route::get('delete/condition/doctor/{id}/', 'DoctorProProController@deleteCondition')->name('delete.condition.doctor')->middleware('checkdoctor'); //
/////////doctor-clinic
        Route::post('add/clinic/new/', 'DoctorProProController@addNewClinic')->name('add.new.clinic')->middleware('checkdoctor'); //
        Route::get('save/clinic/doctor/{id}/', 'DoctorProProController@saveClinic')->name('save.clinic.doctor')->middleware('checkdoctor'); //
        Route::get('remove/clinic/doctor/{id}/', 'DoctorProProController@removeSaveClinic')->name('remove.clinic.doctor')->middleware('checkdoctor'); //
////////profile
        Route::get('/doctor-profile', 'DoctorController@profile')->name('doctor.profile')->middleware('checkdoctor'); //
        Route::post('/doctor-profile-update', 'DoctorController@updateDoctorProfile')->name('doctor.update.profile')->middleware('checkdoctor'); //
//time-table
        Route::post('/create/time/slot', 'DoctorController@createVisitTime')->name('doctor.create.time')->middleware('checkdoctor'); //
        Route::get('/scheduling', 'DoctorController@scheduling')->name('doctor.scheduling')->middleware('checkdoctor');//
        Route::get('doctor/time/change/{vtid}/', 'DoctorController@timeChange')->name('doctor.time.change')->middleware('checkdoctor'); //
        Route::get('doctor/day/status/{visitid}', 'DoctorController@dayStatus')->name('doctor.day.change')->middleware('checkdoctor'); //
//Route::get('doctor/time/edit/{doctorid}/{day}', 'DoctorController@timeEdit');
        Route::get('/delete-timetable-doctor/{id}', 'DoctorController@deleteTimatable')->name('doctor.delete.timatabel')->middleware('checkdoctor'); //

        Route::get('/my-patient', 'DoctorController@patientsProfile')->name('doctor.view.patient')->middleware('checkdoctor'); //
        Route::get('/doctor-dashboard', 'DoctorController@home')->name('doctor.dashboard')->middleware('checkdoctor'); //
        Route::get('/doctor-feedback', 'FeedbackController@doctorFeedback')->name('doctor.feedback')->middleware('checkdoctor'); //
        Route::get('ajax-unread-appoinment', 'DoctorController@ajaxnotificationAppointment')->middleware('checkdoctor'); //not-clear
////////social-media and password
        Route::post('save-password', 'DoctorController@savePassword')->name('doctor.password.save')->middleware('checkdoctor'); //
        Route::get('my-social-media', 'DoctorController@socialMedia')->name('social.media')->middleware('checkdoctor'); //
        Route::post('save-social-media', 'DoctorController@saveSocialMedia')->name('save.social.media')->middleware('checkdoctor'); //
//notification
        Route::get('doctor-notification', 'DoctorController@getNotification')->name('doctor.Notification')->middleware('checkdoctor'); //
        Route::get('doctor/delete/notification/{id}', 'DoctorController@deleteNotification')->name('doctor.delete.noti')->middleware('checkdoctor'); //
        Route::get('doctor/read/notification/{id}', 'DoctorController@readNotification')->name('doctor.read.notification')->middleware('checkdoctor'); //

        Route::get('doctor-change-password', function () {
            return view('doctor/changepassword');
        })->name('doctor.change.password')->middleware('checkdoctor'); //
        Route::get('doctor-unread-appointment-notification', 'DoctorController@UnreadNotificationAppointment')->name('doctor.unread.appointment.notification')->middleware('checkdoctor');


//appointment
//Route::get('accept-appointment/{appid}/', 'DoctorController@acceptAppointment');
        Route::get('cancel-appointment/{appid}/', 'DoctorController@cancelAppointment')->name('cancel.app.by.doctor')->middleware('checkdoctor'); //
//Route::get('view-the-patient-profile/{pid}/', 'DoctorController@viewPatientProfile');
/////chat-with-user
        Route::get('private-chat-with-user', 'ChatController@doctorChatView')->name('doctor.Private.Chat')->middleware('checkdoctor'); //
        Route::get('get-chat-u/{receiver_id}', 'ChatController@getMessageFromUser')->name('doctor.Get.Chat')->middleware('checkdoctor');
        Route::post('sendmessage-u', 'ChatController@sendMessageToUser')->name('send.message.u')->middleware('checkdoctor');
        Route::post('uploadFileToUser', 'ChatController@uploadFileToUser')->name('upload.File.To.User')->middleware('checkdoctor');
        ;

//doctor-education
        Route::post('add/education/', 'Admin\DoctorsController@addeducation')->name('add.education')->middleware('checkdoctor'); //
        Route::post('update/education/', 'Admin\DoctorsController@updateeducation')->name('update.education')->middleware('checkdoctor'); //
        Route::get('delete/education/{id}/', 'Admin\DoctorsController@deleteeducation')->name('delete.education')->middleware('checkdoctor'); //
  //  });

/////////////////////////////////////////
///////////////ADMIN/////////////////////
/////////////////////////////////////////
//    Route::get('/doctorplus-admin', function () {
//        return view('admin/login');
//    })->name('admin.login');
//    Route::get('/doctorplus-blogger-page', function () {
//        return view('admin/bloglogin');
//    });
//notification
    Route::get('admin/delete/notification/{id}', 'Admin\AdminAuthController@deleteNotification')->name('admin.delete.notification'); //
    Route::get('admin/read/notification/{id}', 'Admin\AdminAuthController@readNotification')->name('admin.read.notification'); //
    Route::get('admin/list/notification/{id}', 'Admin\AdminAuthController@notification');
    Route::get('ajax-unread-notif', 'Admin\AdminAuthController@ajaxnotification');
    Route::get('admin/list/notification', 'Admin\AdminAuthController@notification')->name('admin.notification'); //side
    Route::get('all-appointment-list', 'Admin\PateintsController@getAllAppointmentList')->name('admin.appointment.list'); //side

    Route::get('admin/dashboard', 'Admin\AdminAuthController@home')->name('admin.dashboard'); //side
    Route::post('admin-home', 'Admin\AdminAuthController@login')->name('admin.home');
    Route::get('admin-profile', 'Admin\AdminAuthController@adminProfile')->name('admin.view.profile');
    Route::get('admin-profile-edit', 'Admin\AdminAuthController@adminEditProfile')->name('admin.edit.profile');
    Route::post('admin-profile-update', 'Admin\AdminAuthController@adminUpdateProfile')->name('admin.updateprofile');


/////Admin-Manage-Speciality
    Route::get('specialities-list', 'Admin\SpecialityController@specialities')->name('admin.specialities.list'); //side
    Route::post('specialities-add', 'Admin\SpecialityController@specialitiesAdd')->name('admin.specialities.add'); //
//Route::get('admin/edit/specialities/{id}', 'Admin\SpecialityController@specialitiesEdit')->name('admin.specialities.add');
//    Route::get('admin/delete/specialities/{id}', 'Admin\SpecialityController@specialitiesDelete');
//pateint-admin

    Route::get('pateints-list', 'Admin\PateintsController@index')->name('admin.pateints.index'); //side
    Route::get('users-list', 'Admin\PateintsController@userList')->name('admin.user.list');
    Route::get('pateint/view/{id}/', 'Admin\PateintsController@checkFamilyMemberView')->name('admin.view.pateint');
    Route::get('admin/delete/user/{id}', 'Admin\PateintsController@destroy');

//doctor-admin
    Route::get('admin-doctor-confirm/{doctorid}', 'Admin\DoctorsController@adminConfirmdoctor')->name('confirm.doctor.status'); 
    Route::get('admin-doctor-cancel/{doctorid}', 'Admin\DoctorsController@adminCanceldoctor')->name('cancel.doctor.status'); 
    Route::get('admin-doctors', 'Admin\DoctorsController@index')->name('doctor.index'); //side
    Route::get('doctor/{id}/', 'Admin\DoctorsController@show')->name('admin.doctor.view');
    Route::get('doctor/edit/{id}/', 'Admin\DoctorsController@editDoctor')->name('admin.doctor.edit'); //
    Route::get('admin/doctor/delete/{id}/', 'Admin\DoctorsController@deleteDoctor')->name('admin.doctor.delete');
    Route::get('create/doctors', 'Admin\DoctorsController@create')->name('create.doctor');
    Route::post('admin-store-doctor', 'Admin\DoctorsController@storeDoctor')->name('store.doctors');
    Route::post('admin-update-doctor', 'Admin\DoctorsController@doctorUpdate')->name('doctor.update');

//doctor-education
    Route::post('admin/add/education/', 'Admin\DoctorsController@addeducation')->name('admin.add.education'); //
    Route::post('admin/update/education/', 'Admin\DoctorsController@updateeducation')->name('admin.update.education'); //
    Route::get('admin/delete/education/{id}/', 'Admin\DoctorsController@deleteeducation')->name('admin.delete.education'); //
//doctor-Speciality
    Route::post('add/Speciality/', 'Admin\DoctorsController@addSpeciality')->name('add.Speciality'); //
//doctor-service
    Route::post('add/service/', 'Admin\DoctorsController@addService')->name('add.service'); //
    Route::post('update/service/', 'Admin\DoctorsController@updateService')->name('update.service'); //
    Route::get('delete/service/{id}/', 'Admin\DoctorsController@deleteService')->name('admin.delete.service'); //
//doctor-experiance

    Route::post('add/experiance/', 'Admin\DoctorsController@addSexperiance')->name('add.experiance'); //
    Route::post('update/experiance/', 'Admin\DoctorsController@updateexperiance')->name('update.experiance'); //
    Route::get('admin/delete/experiance/{id}/', 'Admin\DoctorsController@deleteexperiance')->name('admin.delete.experiance');

//Blog
//Route::post('add/education/', 'Admin\DoctorsController@addeducation')->name('add.education');
//Route::post('update/education/', 'Admin\DoctorsController@updateeducation')->name('update.education');
//Route::get('admin/post/view/{id}', 'Admin\BlogController@adminBlogFilterCategory');

    Route::get('/admin-logout', 'Auth\LoginController@logout')->name('logout.admin'); //
    Route::post('blogger-home', 'Admin\AdminAuthController@bloglogin')->name('blogger.login'); //

    Route::get('blog-list', 'Admin\BlogController@index')->name('blog.list'); //
    Route::get('admin/view/blog/{id}', 'Admin\BlogController@blogView')->name('admin.view.blog'); //
    Route::get('blog-view-add', 'Admin\BlogController@blogViewAdd')->name('blog.view.add'); //
    Route::get('admin/edit/blog/{id}', 'Admin\BlogController@blogViewEdit')->name('admin.edit.blog'); //
    Route::get('admin/delete/post/{id}', 'Admin\BlogController@deleteBlog')->name('admin.delete.post'); //
    Route::post('blog-store', 'Admin\BlogController@store')->name('store.blog'); //
    Route::post('blog-update', 'Admin\BlogController@update')->name('edit.blog'); //
//categories
    Route::get('Categories', 'Admin\BlogController@category')->name('Categories'); //
    Route::post('add-category', 'Admin\BlogController@addCategory')->name('add.category'); //
    Route::post('edit-category', 'Admin\BlogController@editCategory')->name('edit.category'); //
    Route::get('admin/delete/category/{id}', 'Admin\BlogController@deleteCategory')->name('admin.delete.category'); //
//Comments
    Route::get('Comments-list', 'Admin\BlogController@Comments')->name('Comments'); //admin
    Route::get('admin/delete/comment/{id}', 'Admin\BlogController@deleteComment')->name('admin.delete.comment'); //admin
    //user
//Clear route cache:
//  Route::get('/route-cache', function() {
//      $exitCode = Artisan::call('route:cache');
//      return 'Routes cache cleared';
//  });
//Clear config cache:
//    Route::get('/config-cache', function() {
//        $exitCode = Artisan::call('config:cache');
//        return 'Config cache cleared';
//    });
// Clear application cache:
//    Route::get('/clear-cache', function() {
//        $exitCode = Artisan::call('cache:clear');
//        return 'Application cache cleared';
//    });
//    Route::get('/config-clear', function() {
//        $exitCode = Artisan::call('config:clear');
//        return 'View cache cleared';
//    });
});
