<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctorspeciality extends Model {

    protected $table = "doctorspeciality";
    public $timestamps = false;

}
