<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pateint extends Model {

    protected $table = "pateint";
    public $timestamps = false;
    protected $fillable = [
        'user_ID',
        'p_name',
        'p_photo',
        'p_fathername',
        'p_phoneNo',
        'age',
        'gender',
    ];

}
