<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorNotification extends Model {

    protected $table = "doctor_notification";
    public $timestamps = false;

}
