<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinic_address extends Model
{
   protected $table = "address_doctor_view";
    public $timestamps = false;
}
