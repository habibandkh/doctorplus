<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Socialmedia extends Model {

    protected $table = "socialmedia";
    public $timestamps = false;

}
