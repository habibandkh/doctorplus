<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorServiceView extends Model
{
    protected $table = "doctor_services_view";
    public $timestamps = false;
}
