<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor_condition extends Model
{
    protected $table = "doctor_condition";
    public $timestamps = false;
}
