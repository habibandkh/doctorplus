<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Speciality extends Model {

    protected $table = "speciality";
    public $timestamps = false;
    protected $fillable = [
        'specialityID ',
        'specialityName',
        'farsi_name',
        'pashto_name',
        'photo',
    ];

    public static function getSpaciality($doctorID) {

        $doctorspeciality = DB::table('doctor')
                ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                ->select('speciality.specialityName')
                ->where('doctor.doctorID', $doctorID)
                ->get();
        // dd($doctorspeciality);
        return $doctorspeciality;
    }

}
