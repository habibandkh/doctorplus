<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteDoctor extends Model
{
    protected $table = "favorite_doctor";
    public $timestamps = false;
}
