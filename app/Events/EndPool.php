<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EndPool implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

   public $text;

   public function __construct($text)
   {
      // dd($text);
       $this->text = $text;
   }

     public function broadcastOn() {
        return new Channel('isAppointment-channel');
    }



    public function broadcastAs() {
        return 'isAppointment-event';
    }
}