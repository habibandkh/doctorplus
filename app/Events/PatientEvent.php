<?php
namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PatientEvent implements ShouldBroadcast{

    use Dispatchable,
        InteractsWithSockets,
        SerializesModels;

    public $appid;
    public $doctorid;
    public $patientsid;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($appid,$doctorid,$patientsid) {
        //dd($text);
        $this->appid = $appid;
        $this->doctorid = $doctorid;
        $this->patientsid = $patientsid;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn() {
        return new Channel('isAppointment-channel');
    }

    public function broadcastAs() {
        return 'isAppointment-event';
    }

}
