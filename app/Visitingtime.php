<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitingtime extends Model {

    protected $table = "visitingtime";
    public $timestamps = false;
  
    protected $fillable = [
        'doctorID',
        'day',
        'visit_time',
        
        'doctor_active',
        'time_off_on',
    ];
}
