<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor_condition_view extends Model
{
     protected $table = "doctor_condition_view";
    public $timestamps = false;
}
