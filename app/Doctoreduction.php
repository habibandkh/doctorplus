<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctoreduction extends Model {

    protected $table = "doctor_education";
    public $timestamps = false;

}
