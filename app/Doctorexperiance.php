<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctorexperiance extends Model {

    protected $table = "doctor_experience";
    public $timestamps = false;

}
