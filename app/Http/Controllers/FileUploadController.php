<?php

namespace App\Http\Controllers;

use DB;
use App\Document;
use Illuminate\Http\Request;
use Validator;

class FileUploadController extends Controller {

    public function uploadDocView() {
        return view('pateint.uploaddoc');
    }

    function upload(Request $request) {
        $rules = array(
            'file' => 'mimes:jpeg,bmp,png,gif,svg,pdf|max:2048'
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $image = $request->file('file');

        $new_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('document/pateint'), $new_name);



        $doc = new Document();
        $doc->pateint_id = $request->p_id;

        $doc->details = $request->details;
        $doc->document = $new_name;
        $doc->save();

        // $extension = $request->file->extension();
        $output = array(
            'success' => 'File uploaded successfully',
            'pdf' => ' <iframe src="/document/pateint/' . $new_name . '" type="application"  frameborder="1"></iframe>'
        );




        return response()->json($output);
    }

}

?>