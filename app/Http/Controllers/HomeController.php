<?php

namespace App\Http\Controllers;

use App\Events\NewComment;
use App\Message;
use DB;
use Illuminate\Http\Request;
use CMText\TextClient;
use function back;
use function event;
use function now;
use function redirect;
use function request;
use function response;
use function session;
use function view;

class HomeController extends Controller {

    public function index() {
//       $m_send =new CMTextController();
//       $Message='hi habib';
//       $mobile='0093705931096';
//       $From='doctorplus';
//      $res=$m_send->index('hi hi',[$mobile],'doctorPlus');
    ////////////use this only for now
         //$textClient = new TextClient('24DA2FC6-CDBD-4F61-ACE7-953DA97333EE');
        // $message = $textClient->SendMessage('this is phd testing APIs','DoctorPlus', ['0093749999016']);
       // dd($message);

      
        
        $doctors = DB::table('doctor')->limit(4)->get();
        foreach ($doctors as $doctor) {

            $id = $doctor->doctorID;

            $speciality = $this->get_speciality($id, 'p');
            $available = $this->get_availablity($id);
            $address = $this->get_address($id);
            $stars = $this->get_stars($id);
            $total_stars = $this->get_total_stars($id);

            $doctor->Address = $address;
            $doctor->stars = $stars;
            $doctor->available = $available;
            $doctor->speciality = $speciality;
            $doctor->total_stars = $total_stars;
        }

        //$district = DB::table('district')->get();
        $value = session()->get('language');
        //  dd($value);
        if ($value == 'en' || $value == null) {
            $specialitis = DB::table('speciality_class')->get();
        } else {
            $specialitis = DB::table('speciality_class')->select('id', 'farsi_name as Name', 'photo')->get();
        }

        return view('public.homepage', compact('doctors', 'specialitis'));
    }

    public function getDoctorAvailableClinic() {

        if (request()->ajax()) {
            $id = request()->doctor_id;
            $address = DB::table('address')
                    ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                    ->join('visitingtime', 'address_doctor_view.address_id', '=', 'visitingtime.clinic_address_id')
                    ->where('visitingtime.doctorID', $id)
                    ->select('address_doctor_view.id', 'name', 'address', 'address_id', 'clinic_address_id', 'address_doctor_view.doctor_id')
                    ->groupby('visitingtime.clinic_address_id')
                    ->get();

            foreach ($address as $addr) {
                $from = DB::table('visitingtime')
                        ->select('visit_time')
                        ->where('doctorID', $id)
                        ->where('clinic_address_id', $addr->clinic_address_id)
                        ->min('visit_time');
                $to = DB::table('visitingtime')
                        ->select('visit_time')
                        ->where('doctorID', $id)
                        ->where('clinic_address_id', $addr->clinic_address_id)
                        ->max('visit_time');
                $addr->From = $from;
                $addr->To = $to;
            }
            return response()->json(['success' => $address]);
        }
    }

    function get_address($id) {

        $address = DB::table('address')
                ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                ->join('visitingtime', 'address_doctor_view.address_id', '=', 'visitingtime.clinic_address_id')
                ->where('visitingtime.doctorID', $id)
                ->select('address_doctor_view.id', 'name', 'address', 'address_id', 'clinic_address_id')
                ->groupby('visitingtime.clinic_address_id')
                ->get();

        foreach ($address as $addr) {
            $from = DB::table('visitingtime')
                    ->select('visit_time')
                    ->where('doctorID', $id)
                    ->where('clinic_address_id', $addr->clinic_address_id)
                    ->min('visit_time');

            $to = DB::table('visitingtime')
                    ->select('visit_time')
                    ->where('doctorID', $id)
                    ->where('clinic_address_id', $addr->clinic_address_id)
                    ->max('visit_time');
            $addr->From = $from;
            $addr->To = $to;
        }
        return $address;
    }

    function get_all_clinic($id) {
        $address = DB::table('address')->get();
        return $address;
    }

    function get_my_clinic($id) {

        $address = DB::table('address')
                ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                ->where('address_doctor_view.doctor_id', $id)
                ->get();
        return $address;
    }

    function get_stars($id) {
        
        $total = DB::table('feedback')->where('feedback_doctor_id', $id)->count();
        if ($total != 0) {
            $overall_experience_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('overall_experience', 1)->count();
            $doctor_checkup_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('doctor_checkup', 1)->count();
            $staff_behavior_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('staff_behavior', 1)->count();
            $clinic_environment_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('clinic_environment', 1)->count();

            $star = ($overall_experience_1 + $overall_experience_1 + $doctor_checkup_1 + $staff_behavior_1 + $clinic_environment_1) / $total;
            $stars = number_format($star, 1);
        } else {
            $stars = number_format(5, 1);
        }
        return $stars;
    }

    function get_total_stars($id) {
        $total = DB::table('feedback')->where('feedback_doctor_id', $id)->count();
        return $total;
    }

    function get_availablity($id) {
        $year = now()->format('Y');
        $month = now()->format('m');
        $day = now()->format('l');
        $dayone = now()->addDays(1)->format('l');
        $daytwo = now()->addDays(2)->format('l');
        $daythree = now()->addDays(3)->format('l');


        $getday = DB::table('visitingtime')->select('day')->where('doctorID', $id)->where('doctor_active', 1)->where('day', $day)->first();

        $getday1 = DB::table('visitingtime')->select('day')->where('doctorID', $id)->where('doctor_active', 1)->where('day', $dayone)->first();

        $getday2 = DB::table('visitingtime')->select('day')->where('doctorID', $id)->where('doctor_active', 1)->where('day', $daytwo)->first();

        $getday3 = DB::table('visitingtime')->select('day')->where('doctorID', $id)->where('doctor_active', 1)->where('day', $daythree)->first();

        if ($getday != null && $getday->day == $day) {
            $available = 'Available on ' . now()->format('l d F');
        } elseif ($getday1 != null && $getday1->day == $dayone) {
            $available = 'Available on ' . now()->addDays(1)->format('l d F');
        } elseif ($getday2 != null && $getday2->day == $daytwo) {
            $available = now()->addDays(2)->format('l d F');
        } elseif ($getday3 != null && $getday3->day == $daythree) {
            $available = now()->addDays(3)->format('l d F');
        } else {
            $available = 'not Available for next 3 day';
        }
        return $available;
    }

    function get_speciality($id, $type) {
        if ($type == 'all') {
            $speciality = DB::table('doctor')
                    ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                    ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                    ->select('id_s_d', 'specialityName', 'photo', 'specialityID')
                    ->where('doctor.doctorID', $id)
                    ->get();
        }
        if ($type == 'S') {
            $speciality = DB::table('doctor')
                    ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                    ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                    ->select('id_s_d', 'specialityName', 'photo', 'specialityID')
                    ->where('doctor.doctorID', $id)
                    ->where('doctorspeciality.type', 'S')
                    ->get();
        } else {
            $speciality = DB::table('doctor')
                    ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                    ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                    ->select('id_s_d', 'specialityName', 'photo', 'specialityID')
                    ->where('doctor.doctorID', $id)
                    ->where('doctorspeciality.type', 'P')
                    ->get();
        }
        return $speciality;
    }

    function get_count_patients($id) {
        $appointments_count = DB::table('appointment')->where('doctor_id', $id)->groupBy('pateint_id', 'user_id')->count();
        return $appointments_count;
    }

    function get_count_account_view($id) {
        $count_view_account = DB::table('doctor_view')->where('doctor_id', $id)->count();
        return $count_view_account;
    }

//my get condition
    function get_condition_link($doctor_id) {
        $Condition = DB::table('doctor_condition')
                ->join('doctor_condition_view', 'doctor_condition.id', '=', 'doctor_condition_view.condition_id')
                ->where('doctor_condition_view.doctor_id', $doctor_id)
                ->select('doctor_condition.id', 'name', 'doctor_condition_view.id AS fkid')
                ->groupBy('name')
                ->get();
        return $Condition;
    }

    //get my services
    function get_services_link($doctor_id) {
        $services = DB::table('doctor_services')
                ->join('doctor_services_view', 'doctor_services.services_id', '=', 'doctor_services_view.services_id')
                ->where('doctor_services_view.doctor_id', $doctor_id)
                ->select('doctor_services.services_id', 'service_name', 'doctor_services_view.id')
                ->groupBy('service_name')
                ->get();
        return $services;
    }

    function get_all_condition($doctor_id) {
        $Condition = DB::table('speciality')
                ->join('doctor_condition', 'speciality.specialityID', '=', 'doctor_condition.specialty_id')
                ->join('doctorspeciality', 'speciality.specialityID', '=', 'doctorspeciality.F_specialityID')
                ->where('doctorspeciality.F_doctorID', $doctor_id)
                ->select('doctor_condition.id', 'name')
                ->get();
        return $Condition;
    }

    function get_all_services($doctor_id) {

        $services = DB::table('speciality')
                ->join('doctor_services', 'speciality.specialityID', '=', 'doctor_services.specialty_id')
                ->join('doctorspeciality', 'speciality.specialityID', '=', 'doctorspeciality.F_specialityID')
                ->where('doctorspeciality.F_doctorID', $doctor_id)
                ->select('services_id', 'service_name')
                ->get();
        return $services;
    }

    function get_doctor_my_clinic($id) {

        $address = DB::table('address')
                ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                ->where('address_doctor_view.doctor_id', $id)
                ->select('address_id', 'name', 'address')
                ->groupby('name')
                ->get();
        return $address;
    }

    public function viewDoctorProfile(Request $request) {
        $id = $request->id;

        $year = now()->format('Y');
        $month = now()->format('m');

        DB::table('doctor_view')->insert(['doctor_id' => $id, 'year' => $year, 'month' => $month]);

        $doctors = DB::table('doctor')->where('doctorID', $id)->get();

        foreach ($doctors as $doctor) {

            $id = $doctor->doctorID;
            $speciality = $this->get_speciality($id, 'p');
            $doctor->speciality = $speciality;

            $available = $this->get_availablity($id);
            $doctor->available = $available;

            $address = $this->get_address($id);
            $doctor->Address = $address;

            $stars = $this->get_stars($id);
            $doctor->stars = $stars;

            $total_stars = $this->get_total_stars($id);
            $doctor->total_stars = $total_stars;

            $count_patient = $this->get_count_patients($id);
            $doctor->count_patient = $count_patient;

            $account_view = $this->get_count_account_view($id);
            $doctor->account_view = $account_view;

            /////second section
            $userid = session('id_pateint');
            $bookmarkdoctor = DB::table('favorite_doctor')->where('doctor_id', $id)->where('user_id', $userid)->first();
            $doctor->doctor_bookmark = $bookmarkdoctor;

            $doctor_experience = DB::table('doctor_experience')->where('doctor_FID', $id)->get();
            $doctor->doctor_experience = $doctor_experience;

            $doctor_education = DB::table('doctor_education')->where('D_FID', $id)->get();
            $doctor->doctor_education = $doctor_education;

            $speciality_all = $this->get_speciality($id, 'all');
            $doctor->speciality_all = $speciality_all;

            $my_Condition = $this->get_condition_link($id);
            $doctor->my_Condition = $my_Condition;

            $my_services = $this->get_services_link($id);
            $doctor->my_services = $my_services;
        }

        return view('public.doctorProfile', compact("doctors"));
    }

    function emailActivationDoctor(Request $request) {
        $doctorId = $request->doctorid;
        $tokenId = $request->token;

        $status_review = DB::table('doctor')->where('doctorID', $doctorId)->where('email_verification_token', $tokenId)->first();
        if ($status_review != null) {

            DB::table('doctor')->where('doctorID', $doctorId)->update(['email_verification' => 1]);
            return view('public.Email.emailsuccess');
        } else {

            return view('public.Email.emailerror');
        }
    }

      function emailResetDoctor(Request $request) {
        $doctorId = $request->doctorid;
        $tokenId = $request->token;

        $status_review = DB::table('doctor')->where('doctorID', $doctorId)->where('email_verification_token', $tokenId)->first();
        if ($status_review != null) {

            DB::table('doctor')->where('doctorID', $doctorId)->update(['email_verification' => 1]);
            return view('public.ResetPassword.emailsuccess');
        } else {

            return view('public.ResetPassword.emailerror');
        }
    }
    
     function passwordResetDoctor(Request $request) {
        
          $id=$request->doctorid;
          $pass=$request->pass;
          $conf_pass=$request->conf_pass;
          if($pass==$conf_pass){
            
             DB::table('doctor')->where('doctorID', $id)->update(['doctor_password' => $pass]);
          
             \Session::flash('success', 'your password update successfully!');
            return view('public.ResetPassword.emailsuccess');
          }else{
             \Session::flash('message', 'password not match');
            return view('public.ResetPassword.emailsuccess'); 
          }
            
    }
    
    public function getPhoneNumber() {
        
        if (request()->ajax()) {

            $phonenumber = request()->Phone_number;

            $is_phone_d = DB::table('doctor')->where('doctor_phoneNo', $phonenumber)->first();
            $is_phone_u = DB::table('users')->select('phone')->where('phone', $phonenumber)->first();

            if ($is_phone_d != null || $is_phone_u != null) {
                return response()->json(['is_phone' => 'yesitis']);
            } else {
                $code = mt_rand(100000, 999999);


        //       $phonex = ltrim($phonenumber, '0');
        //      // dd($phonenumber);
        //       $phone = '0093' . $phonex;
            
        //  $mytext = 'This is your phone number verification code ' . $code;
        //  $textClient = new TextClient('24DA2FC6-CDBD-4F61-ACE7-953DA97333EE');
        //  $textClient->SendMessage($mytext,'DoctorPlus', [$phone]);


        return response()->json(['code' => $code, 'phonenumber' => $phonenumber]);
            }
        }
    }

    public function joinDoctor() {

        return view('public.joinAsDoctor');
    }

    public function joinDoctorAdd(Request $request) {
        $cat = new Message();
        $cat->name = $request->name;
        $cat->phone = $request->phone;
        $cat->message = $request->message;
        $cat->save();
        event(new NewComment($request->name));
        return back()->with('message', 'your message send successfully');
    }

    public function getalldistrict(Request $request) {
        $value = session()->get('language');

        if ($value == 'en' || $value == null) {
            return DB::table('city')->select('*')->where('e_name', 'LIKE', "%{$request->district}%")->get();
        } else {
            return DB::table('city')->select('id', 'f_name as e_name')->where('f_name', 'LIKE', "%{$request->district}%")->get();
        }
    }

    public function find(Request $request) {
        return DB::table('speciality')->select('*')->where('specialityName', 'LIKE', "%{$request->q}%")->get();
    }

    public function finddoctor(Request $request) {
        return DB::table('doctor')->select('*')->where('doctor.under_review', 2)->where('doctor_name', 'LIKE', "%{$request->q}%")->get();
    }

    public function searchDoctorByDistrictAndSpaciality(Request $request) {

        if ($request->district == 'بدخشان') {
            $District_name = 'badakhshan';
        } elseif ($request->district == 'بادغیس') {
            $District_name = 'badghiz';
        } elseif ($request->district == 'بغلان') {
            $District_name = 'baghlan';
        } elseif ($request->district == 'بلخ') {
            $District_name = 'balkh';
        } elseif ($request->district == 'بامیان') {
            $District_name = 'bamiyan';
        } elseif ($request->district == 'دایکندی') {
            $District_name = 'daikundi';
        } elseif ($request->district == 'دایکندي') {
            $District_name = 'daikundi';
        } elseif ($request->district == 'فاریاب') {
            $District_name = 'faryab';
        } elseif ($request->district == 'غزني') {
            $District_name = 'ghazni';
        } elseif ($request->district == 'غزنی') {
            $District_name = 'ghazni';
        } elseif ($request->district == 'غور') {
            $District_name = 'ghor';
        } elseif ($request->district == 'هیلمند') {
            $District_name = 'helmand';
        } elseif ($request->district == 'هرات') {
            $District_name = 'herat';
        } elseif ($request->district == 'جوزجان') {
            $District_name = 'jowzjan';
        } elseif ($request->district == 'کابل') {
            $District_name = 'kabul';
        } elseif ($request->district == 'کندهار') {
            $District_name = 'kandahar';
        } elseif ($request->district == 'کاپیسا') {
            $District_name = 'kapisa';
        } elseif ($request->district == 'خوست') {
            $District_name = 'khost';
        } elseif ($request->district == 'کونر') {
            $District_name = 'kunar';
        } elseif ($request->district == 'کندوز') {
            $District_name = 'kunduz';
        } elseif ($request->district == 'لغمان') {
            $District_name = 'laghman';
        } elseif ($request->district == 'لوګر') {
            $District_name = 'logar';
        } elseif ($request->district == 'لوگر') {
            $District_name = 'logar';
        } elseif ($request->district == 'ننګرهار') {
            $District_name = 'nangarhar';
        } elseif ($request->district == 'ننگرهار') {
            $District_name = 'nangarhar';
        } elseif ($request->district == 'پکټیا') {
            $District_name = 'paktia';
        } elseif ($request->district == 'پکتیا') {
            $District_name = 'paktia';
        } elseif ($request->district == 'میدان وردک') {
            $District_name = 'maidan wardak';
        } elseif ($request->district == 'میډن ورډک') {
            $District_name = 'maidan wardak';
        } elseif ($request->district == 'پکتيکا') {
            $District_name = 'paktika';
        } elseif ($request->district == 'پنجشیر') {
            $District_name = 'panjshir';
        } elseif ($request->district == 'parwan') {
            $District_name = 'پروان';
        } elseif ($request->district == 'سمنګان') {
            $District_name = 'samangaan';
        } elseif ($request->district == 'سمنگان') {
            $District_name = 'samangaan';
        } elseif ($request->district == 'سرپل') {
            $District_name = 'sar-e-pul';
        } elseif ($request->district == 'تخار') {
            $District_name = 'takhar';
        } elseif ($request->district == 'زابل') {
            $District_name = 'zabul';
        } elseif ($request->district == 'نیمروز') {
            $District_name = 'nimroz';
        } elseif ($request->district == 'فراه') {
            $District_name = 'farah';
        } elseif ($request->district == 'نورستان') {
            $District_name = 'nuristan';
        } elseif ($request->district == 'اروزگان') {
            $District_name = 'uruzgan';
        } else {
            $District_name = $request->district;
        }

        $spaciality = $request->q;

        $spa = DB::table('speciality')->where('specialityName', $spaciality)->first();
        // dd($spa);
        if ($spa != null) {
            $request->session()->flash('spaciality_class_id', $spa->class_spa_id);
            $request->session()->flash('District_name', $District_name);


            if ($District_name != null) {
                $doctors = DB::table('doctor')
                        ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                        ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                        ->where('speciality.specialityName', $spaciality)
                        ->where('doctor.city', $District_name)
                        ->where('doctor.under_review', 2)
                        ->paginate(20);
            } else {

                $doctors = DB::table('doctor')
                        ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                        ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                        ->where('speciality.specialityName', $spaciality)
                         ->where('doctor.under_review', 2)
                        ->paginate(20);
            }
            foreach ($doctors as $doctor) {

                $id = $doctor->doctorID;

                $speciality = $this->get_speciality($id, 'p');
                $available = $this->get_availablity($id);
                $address = $this->get_address($id);
                $stars = $this->get_stars($id);
                $total_stars = $this->get_total_stars($id);

                $doctor->Address = $address;
                $doctor->stars = $stars;
                $doctor->available = $available;
                $doctor->speciality = $speciality;
                $doctor->total_stars = $total_stars;
                $count_patient = $this->get_count_patients($id);
                $doctor->count_patient = $count_patient;

                $account_view = $this->get_count_account_view($id);
                $doctor->account_view = $account_view;
            }

            return view('public.gridlist', compact('doctors'));
        } else {
            $doctors = DB::table('speciality')->where('speciality.specialityName', 'phd')->paginate(20);
            return view('public.gridlist', compact('doctors'));
        }
    }

    public function searchDoctorByClassSpaciality(Request $request) {
        $spaciality = $request->id;
        session()->flash('spaciality_class_id', $spaciality);
        $doctors = $this->callThisClassAllDoctor($spaciality);

        foreach ($doctors as $doctor) {

            $id = $doctor->doctorID;

            $speciality = $this->get_speciality($id, 'p');
            $available = $this->get_availablity($id);
            $address = $this->get_address($id);
            $stars = $this->get_stars($id);
            $total_stars = $this->get_total_stars($id);

            $doctor->Address = $address;
            $doctor->stars = $stars;
            $doctor->available = $available;
            $doctor->speciality = $speciality;
            $doctor->total_stars = $total_stars;
            $count_patient = $this->get_count_patients($id);
            $doctor->count_patient = $count_patient;

            $account_view = $this->get_count_account_view($id);
            $doctor->account_view = $account_view;
        }
        // dd($doctors);
        return view('public.gridlist', compact('doctors'));
    }

    function callThisClassAllDoctor($spaciality) {


        $specialitys_list = DB::table('speciality_class')
                ->join('speciality', 'speciality_class.id', '=', 'speciality.class_spa_id')
                ->where('speciality.class_spa_id', $spaciality)
                ->get();
        $query = DB::table('doctor')
                ->join('visitingtime', 'doctor.doctorID', '=', 'visitingtime.doctorID')
                ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                ->select('doctor.doctorID'
                , 'doctor.title'
                , 'doctor.doctor_name'
                , 'doctor.doctor_email'
                , 'doctor.doctor_phoneNo'
                , 'doctor.experience'
                , 'doctor.doctor_fee'
                , 'doctor.doctor_picture')->where('doctor.under_review', 2);

        foreach ($specialitys_list as $spc) {
            $spa_id[] = $spc->specialityID;
        }

        $query->whereIn('speciality.specialityID', $spa_id);
        $query->groupBy('doctor.doctorID');
        $Doctors = $query->paginate(20);

        return $Doctors;
    }

    public function allDoctorList(Request $req) {
        $doctors = DB::table('doctor')->where('doctor.under_review', 2)->paginate(20);
        foreach ($doctors as $doctor) {

            $id = $doctor->doctorID;

            $speciality = $this->get_speciality($id, 'p');
            $available = $this->get_availablity($id);
            $address = $this->get_address($id);
            $stars = $this->get_stars($id);
            $total_stars = $this->get_total_stars($id);

            $doctor->Address = $address;
            $doctor->stars = $stars;
            $doctor->available = $available;
            $doctor->speciality = $speciality;
            $doctor->total_stars = $total_stars;
            $count_patient = $this->get_count_patients($id);
            $doctor->count_patient = $count_patient;

            $account_view = $this->get_count_account_view($id);
            $doctor->account_view = $account_view;
        }


        return view('public.gridlist', compact('doctors'));
    }

    public function searchDoctor(Request $req) {
        $special_id = $req->specializations;
        $gender = $req->gender;

        if ($gender == 'any') {
            $Doctors = DB::table('doctor')
                    ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                    ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                    ->where('speciality.specialityID', $special_id)
                     ->where('doctor.under_review', 2)
                    ->get();
        } else {
            $Doctors = DB::table('doctor')
                    ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                    ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                    ->where('speciality.specialityID', $special_id)
                    ->where('doctor.doctor_gender', $gender)
                     ->where('doctor.under_review', 2)
                    ->get();
        }
        $doctorspeciality = DB::table('doctor')
                ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                ->select('speciality.specialityName')
                ->where('doctorspeciality.F_specialityID', 'doctorspeciality.F_doctorID')
                ->get();
        return view('public.gridlist', compact('Doctors', 'doctorspeciality'));
    }

    public function edit($id) {

        if (request()->ajax()) {

            $day = request()->dayselected;
            $date_selected = request()->DATE;
            $data1 = DB::table('appointment')->where('doctor_id', $id)->where('appointment_date', $date_selected)->get();
            $data = DB::table('visitingtime')
                    ->where('doctorID', $id)
                    ->where('time_off_on', 1)
                    ->where('doctor_active', 1)
                    ->where('day', $day)
                    ->get();
            return response()->json(['success' => $data, 'used' => $data1]);
        }
    }

    public function filterDoctorList(Request $request) {

        $year = now()->format('Y');
        $month = now()->format('m');

        $day = now()->format('l');
        $dayone = now()->addDays(1)->format('l');
        $daytwo = now()->addDays(2)->format('l');
        $daythree = now()->addDays(3)->format('l');

        $available = $request->available;
        $gender = $request->gender;
        $District_name = $request->District_name;
        $special_id = $request->special_id;

        $request->session()->flash('spaciality_class_id', $special_id);
        $request->session()->flash('District_name', $District_name);

        if ($available == 'today') {

            $doctors = $this->getTodayDoctorAvaiable($day, $gender, $District_name, $special_id);
            foreach ($doctors as $doctor) {

                $id = $doctor->doctorID;

                $speciality = $this->get_speciality($id, 'p');
                $available = $this->get_availablity($id);
                $address = $this->get_address($id);
                $stars = $this->get_stars($id);
                $total_stars = $this->get_total_stars($id);

                $doctor->Address = $address;
                $doctor->stars = $stars;
                $doctor->available = $available;
                $doctor->speciality = $speciality;
                $doctor->total_stars = $total_stars;
                $count_patient = $this->get_count_patients($id);
                $doctor->count_patient = $count_patient;

                $account_view = $this->get_count_account_view($id);
                $doctor->account_view = $account_view;
            }

            return view('public.gridlist', compact('doctors'));
        }
        if ($available == 'next_days') {

            $doctors = $this->getNextdaysDoctorAvaiable($dayone, $daytwo, $daythree, $gender, $District_name, $special_id);
            foreach ($doctors as $doctor) {

                $id = $doctor->doctorID;

                $speciality = $this->get_speciality($id, 'p');
                $available = $this->get_availablity($id);
                $address = $this->get_address($id);
                $stars = $this->get_stars($id);
                $total_stars = $this->get_total_stars($id);

                $doctor->Address = $address;
                $doctor->stars = $stars;
                $doctor->available = $available;
                $doctor->speciality = $speciality;
                $doctor->total_stars = $total_stars;
                $count_patient = $this->get_count_patients($id);
                $doctor->count_patient = $count_patient;

                $account_view = $this->get_count_account_view($id);
                $doctor->account_view = $account_view;
            }

            return view('public.gridlist', compact('doctors'));
        } else {
            $doctors = $this->getAlldaysDoctorAvaiable($gender, $District_name, $special_id);
            foreach ($doctors as $doctor) {

                $id = $doctor->doctorID;

                $speciality = $this->get_speciality($id, 'p');
                $available = $this->get_availablity($id);
                $address = $this->get_address($id);
                $stars = $this->get_stars($id);
                $total_stars = $this->get_total_stars($id);

                $doctor->Address = $address;
                $doctor->stars = $stars;
                $doctor->available = $available;
                $doctor->speciality = $speciality;
                $doctor->total_stars = $total_stars;
                $count_patient = $this->get_count_patients($id);
                $doctor->count_patient = $count_patient;

                $account_view = $this->get_count_account_view($id);
                $doctor->account_view = $account_view;
            }

            return view('public.gridlist', compact('doctors'));
        }
    }

    function getTodayDoctorAvaiable($day, $gender, $District_name, $special_id) {
        $specialitys_list = DB::table('speciality_class')
                ->join('speciality', 'speciality_class.id', '=', 'speciality.class_spa_id')
                ->where('speciality.class_spa_id', $special_id)
                ->get();

        $query = DB::table('doctor')
                ->join('visitingtime', 'doctor.doctorID', '=', 'visitingtime.doctorID')
                ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                ->select('doctor.doctorID'
                        , 'doctor.title'
                        , 'doctor.doctor_name'
                        , 'doctor.doctor_email'
                        , 'doctor.doctor_phoneNo'
                        , 'doctor.experience'
                        , 'doctor.doctor_fee'
                        , 'doctor.doctor_picture')
                ->where('visitingtime.doctor_active', 1)
                ->where('visitingtime.day', $day);

        foreach ($specialitys_list as $spc) {
            $spa_id[] = $spc->specialityID;
        }

        $query->whereIn('speciality.specialityID', $spa_id);


        if ($gender != 'both') {
            $query->where('doctor.doctor_gender', $gender);
        }

        if ($District_name != 'null') {
            $query->where('doctor.district', $District_name);
        }

        $query->groupBy('doctor.doctorID');


        $Doctors = $query->paginate(20);
        return $Doctors;
    }

    function getNextdaysDoctorAvaiable($dayone, $daytwo, $daythree, $gender, $District_name, $special_id) {
        $specialitys_list = DB::table('speciality_class')
                ->join('speciality', 'speciality_class.id', '=', 'speciality.class_spa_id')
                ->where('speciality.class_spa_id', $special_id)
                ->get();

        $query = DB::table('doctor')
                ->join('visitingtime', 'doctor.doctorID', '=', 'visitingtime.doctorID')
                ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                ->select('doctor.doctorID'
                        , 'doctor.title'
                        , 'doctor.doctor_name'
                        , 'doctor.doctor_email'
                        , 'doctor.doctor_phoneNo'
                        , 'doctor.experience'
                        , 'doctor.doctor_fee'
                        , 'doctor.doctor_picture')
                ->where('visitingtime.doctor_active', 1)
                ->whereIn('visitingtime.day', [$dayone, $daytwo, $daythree]);

        foreach ($specialitys_list as $spc) {
            $spa_id[] = $spc->specialityID;
        }

        $query->whereIn('speciality.specialityID', $spa_id);

        if ($gender != 'both') {
            $query->where('doctor.doctor_gender', $gender);
        }

        if ($District_name != 'null') {
            $query->where('doctor.district', $District_name);
        }
        $query->groupBy('doctor.doctorID');
        $Doctors = $query->paginate(20);
        return $Doctors;
    }

    function getAlldaysDoctorAvaiable($gender, $District_name, $special_id) {


        $specialitys_list = DB::table('speciality_class')
                ->join('speciality', 'speciality_class.id', '=', 'speciality.class_spa_id')
                ->where('speciality.class_spa_id', $special_id)
                ->get();
        $query = DB::table('doctor')
                ->join('visitingtime', 'doctor.doctorID', '=', 'visitingtime.doctorID')
                ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                ->select('doctor.doctorID'
                , 'doctor.title'
                , 'doctor.doctor_name'
                , 'doctor.doctor_email'
                , 'doctor.doctor_phoneNo'
                , 'doctor.experience'
                , 'doctor.doctor_fee'
                , 'doctor.doctor_picture');

        foreach ($specialitys_list as $spc) {
            $spa_id[] = $spc->specialityID;
        }

        $query->whereIn('speciality.specialityID', $spa_id);
        if ($gender != 'both') {
            $query->where('doctor.doctor_gender', $gender);
        }

        if ($District_name != 'null') {
            $query->where('doctor.district', $District_name);
        }

        $query->groupBy('doctor.doctorID');
        $Doctors = $query->paginate(20);
        return $Doctors;
    }

}
