<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\UserNotification;
use App\DoctorNotification;
use DB;
use Illuminate\Http\Request;

class NotificationController extends Controller {

    function send_message_tag($title, $message, $id, $type) {
        $heading = array(
            "en" => "$title"
        );

        $content = array(
            "en" => "$message"
        );
        if ($type == 'user') {
            $fields = array(
                'app_id' => "da97d6fc-33e6-4cbe-99f0-2e31637eb9bc",
                'filters' => array(array("field" => "tag", "key" => "user_id", "relation" => "=", "value" => $id)),
                'headings' => $heading,
                'data' => array("is_chat" => "NO"),
                'contents' => $content
            );
        } else {
            $fields = array(
                'app_id' => "da97d6fc-33e6-4cbe-99f0-2e31637eb9bc",
                'filters' => array(array("field" => "tag", "key" => "doctor_id", "relation" => "=", "value" => $id)),
                'headings' => $heading,
                'data' => array("is_chat" => "NO"),
                'contents' => $content
            );
        }


        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NDljYjQ0MWQtMzczYi00ZTYwLTg0ZWItYjE1YjQ0NjcxY2Ux'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    function send_message_tag_chat($title, $message, $userid, $doctorid, $type, $pateint_id) {
        $heading = array(
            "en" => "$title"
        );

        $content = array(
            "en" => "$message"
        );
        if ($type == 'user') {
            $fields = array(
                'app_id' => "da97d6fc-33e6-4cbe-99f0-2e31637eb9bc",
                'filters' => array(array("field" => "tag", "key" => "user_id", "relation" => "=", "value" => $userid)),
                'headings' => $heading,
                'data' => array("is_chat" => "YES", "user_id" => $userid, "doctor_id" => $doctorid, "patient_id" => $pateint_id),
                'contents' => $content
            );
        } else {
            $fields = array(
                'app_id' => "da97d6fc-33e6-4cbe-99f0-2e31637eb9bc",
                'filters' => array(array("field" => "tag", "key" => "doctor_id", "relation" => "=", "value" => $doctorid)),
                'headings' => $heading,
                'data' => array("is_chat" => "YES", "user_id" => $userid, "doctor_id" => $doctorid, "patient_id" => $pateint_id),
                'contents' => $content
            );
        }


        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NDljYjQ0MWQtMzczYi00ZTYwLTg0ZWItYjE1YjQ0NjcxY2Ux'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    function appCancelNoti_cancel_by_doctor($app) {
        $id_user = $app->user_id;
        $id_doctor = $app->doctor_id;
        $pateint_id = $app->pateint_id;
        $DateOfAppointment = $app->appointment_date;
        $TimeOfAppointment = $app->appointment_time;

        $appdate = date("D d M Y", strtotime($DateOfAppointment));
        $apptime = date("g:i A", strtotime($TimeOfAppointment));

        $user = DB::table('users')->select('id', 'name', 'gender')->where('id', $id_user)->first();

        $doctor = DB::table('doctor')->select('doctorID', 'title', 'doctor_name')->where('doctorID', $id_doctor)->first();
        if ($user->gender == 1) {
            $G = 'Mr';
        } else {
            $G = 'Miss';
        }

        if ($pateint_id == null) {

            $message = $G . ' ' . $user->name . ' your Appointment with ' . $doctor->title . '. ' . $doctor->doctor_name . ' On ' . $appdate . ' at ' . $apptime . ' is cancelled by doctor.';

            $notification = new UserNotification();
            $notification->user_id = $id_user;
            $notification->doctor_id = $id_doctor;
            $notification->patient_id = 0;
            $notification->text = $message;
            $notification->status = 0;
            $notification->date = $date = now();
            $notification->action = 'cancel';
            $notification->save();
        } else {
            $patient = DB::table('pateint')->select('patientID', 'p_name', 'gender', 'p_Relation')->where('patientID', $pateint_id)->first();

            if ($patient->gender == 1) {
                $p = 'Mr';
            } else {
                $p = 'Miss';
            }

            $name = $patient->p_name;
            $p_Relation = $patient->p_Relation;

            $message = $G . ' ' . $user->name . ' your Appointment for your ' . $p_Relation . ' ' . $p . ' ' . $name . ' with  ' . $doctor->title . '. ' . $doctor->doctor_name . ' On ' . $appdate . ' at ' . $apptime . ' is cancelled by doctor.';

            $notification = new UserNotification();
            $notification->user_id = $id_user;
            $notification->doctor_id = $id_doctor;
            $notification->patient_id = $pateint_id;
            $notification->text = $message;
            $notification->status = 0;
            $notification->date = $date = now();
            $notification->action = 'cancel';
            $notification->save();
        }

        $title = 'Appointment cancelled !';
        $user_id = $id_user;
        $type = 'user';
        $this->send_message_tag($title, $message, $user_id, $type);
        return 0;
    }

    function appCancelNoti_cancel_by_user($app) {
        $id_user = $app->user_id;
        $id_doctor = $app->doctor_id;
        $pateint_id = $app->pateint_id;

        $DateOfAppointment = $app->appointment_date;
        $TimeOfAppointment = $app->appointment_time;

        $appdate = date("D d M Y", strtotime($DateOfAppointment));
        $apptime = date("g:i A", strtotime($TimeOfAppointment));

        $user = DB::table('users')->select('id', 'name', 'gender')->where('id', $id_user)->first();

        $doctor = DB::table('doctor')->select('doctorID', 'title', 'doctor_name')->where('doctorID', $id_doctor)->first();
        if ($user->gender == 1) {
            $G = 'Mr';
        } else {
            $G = 'Miss';
        }

        $message = '' . $doctor->title . '. ' . $doctor->doctor_name . ' ,your patient ' . $G . ' ' . $user->name . ' cancel his appoitnemt which was schedule On ' . $appdate . ' at ' . $apptime . '';

        $notification = new DoctorNotification();
        $notification->user_id = $id_user;
        $notification->doctor_id = $id_doctor;

        $notification->text = $message;
        $notification->status = 0;
        $notification->date = $date = now();
        $notification->action = 'cancel';
        $notification->save();


        $title = 'Appointment cancelled !';
        $doctor_id = $id_doctor;
        $type = 'doctor';
        $this->send_message_tag($title, $message, $doctor_id, $type);
        return 0;
    }

    function appConfirmNotification_user($user, $doctor, $DateOfAppointment, $TimeOfAppointment) {

        if ($user->gender == 1) {
            $G = 'Mr';
        } else {
            $G = 'Miss';
        }

        $appdate = date("D d M Y", strtotime($DateOfAppointment));
        $apptime = date("g:i A", strtotime($TimeOfAppointment));

        $id_doctor = $doctor->doctorID;
        $id_user = $user->id;


        $message = $G . ' ' . $user->name . ' your Appointment with ' . $doctor->title . '. ' . $doctor->doctor_name . ' On ' . $appdate . ' at ' . $apptime . ' is confirmd.';

        $notification = new UserNotification();
        $notification->user_id = $id_user;
        $notification->doctor_id = $id_doctor;
        $notification->patient_id = 0;
        $notification->text = $message;
        $notification->status = 0;
        $notification->date = $date = now();
        $notification->action = 'confirmd';
        $notification->save();

        $title = 'Appointment Confirmd!';
        $user_id = $id_user;
        $type = 'user';
        $this->send_message_tag($title, $message, $user_id, $type);
        return 0;
    }

    function appConfirmNotification_family($user, $patient, $doctor, $DateOfAppointment, $TimeOfAppointment) {

        if ($user->gender == 1) {
            $G = 'Mr';
        } else {
            $G = 'Miss';
        }
        if ($patient->gender == 1) {
            $P = 'Mr';
        } else {
            $P = 'Miss';
        }
        $appdate = date("D d M Y", strtotime($DateOfAppointment));
        $apptime = date("g:i A", strtotime($TimeOfAppointment));

        $id_doctor = $doctor->doctorID;
        $id_user = $user->id;
        $id_patient = $patient->patientID;
        $Relation = $patient->p_Relation;
        $p_name = $patient->p_name;

        $message = $G . ' ' . $user->name . ' your Appointment for your ' . $Relation . '  ' . $P . ' ' . $p_name . ' with ' . $doctor->title . '. ' . $doctor->doctor_name . ' On ' . $appdate . ' at ' . $apptime . ' is confirmed.';

        $notification = new UserNotification();
        $notification->user_id = $id_user;
        $notification->doctor_id = $id_doctor;
        $notification->patient_id = $id_patient;
        $notification->text = $message;
        $notification->status = 0;
        $notification->date = $date = now();
        $notification->action = 'confirmed';
        $notification->save();


        $title = 'Appointment Confirmed!';
        $user_id = $id_user;
        $type = 'user';
        $this->send_message_tag($title, $message, $user_id, $type);
        return 0;
    }

    function appConfirmNotification_doctor($user, $doctor, $DateOfAppointment, $TimeOfAppointment) {

        if ($user->gender == 1) {
            $G = 'Mr';
        } else {
            $G = 'Miss';
        }

        $appdate = date("D d M Y", strtotime($DateOfAppointment));
        $apptime = date("g:i A", strtotime($TimeOfAppointment));

        $id_doctor = $doctor->doctorID;
        $id_user = $user->id;
        //   dd('h');

        $message = $G . ' ' . $user->name . ' Booked an  Appointment  with you On ' . $appdate . ' at ' . $apptime . ' .';

        $notification = new DoctorNotification();
        $notification->user_id = $id_user;
        $notification->doctor_id = $id_doctor;

        $notification->text = $message;
        $notification->status = 0;
        $notification->date = $date = now();
        $notification->action = 'confirmd';
        $notification->save();

        $title = 'Appointment Confirmd!';
        $id = $id_doctor;
        $type = 'doctor';
        $this->send_message_tag($title, $message, $id, $type);
        return 0;
    }

    function sendMessageToDoctor($userid, $doctorid, $text, $mess_type, $pateint_id) {

        $user_info = DB::table('users')->select('id', 'name', 'gender')->where('id', $userid)->first();

        if ($mess_type == 'm') {
            $message = $text;
        }
        if ($mess_type == 'f') {
            $message = 'send a file';
        }
        if ($mess_type == 'v') {
            $message = 'send a voice';
        }

          if ($pateint_id == 0) {
            if ($user_info->gender == 1) {
                $G = 'Mr.';
            }
            if ($user_info->gender == 0) {
                $G = 'Miss.';
            }
            $title = '' . $G . '' . $user_info->name . '';
        }
        if ($pateint_id != 0) {
            $pateint_info = DB::table('pateint')->select('patientID','p_name', 'gender')->where('patientID', $pateint_id)->first();
            if ($pateint_info->gender == 1) {
                $pa = 'Mr.';
            }
            if ($pateint_info->gender == 0) {
                $pa = 'Miss.';
            }
            $title = '' . $pa . '' . $pateint_info->p_name . '';
        }


        $notification = new DoctorNotification();
        $notification->user_id = $userid;
        $notification->doctor_id = $doctorid;
        $notification->patient_id = $pateint_id;
        $notification->is_chat = 'YES';
        $notification->text = $message;
        $notification->status = 0;
        $notification->date = $date = now();
        $notification->action = 'confirmd';
        $notification->save();

       
        $type = 'doctor';
        $this->send_message_tag_chat($title, $message, $userid, $doctorid, $type, $pateint_id);
        return 0;
    }

    function sendMessageToUser($doctorid, $userid, $text, $mess_type, $pateint_id) {

        $doctor_info = DB::table('doctor')->select('doctorID', 'title', 'doctor_name')->where('doctorID', $doctorid)->first();

        if ($mess_type == 'm') {
            $message = $text;
        }
        if ($mess_type == 'f') {
            $message = 'send a file';
        }
        if ($mess_type == 'v') {
            $message = 'send a voice';
        }
        $notification = new UserNotification();
        $notification->user_id = $userid;
        $notification->doctor_id = $doctorid;
        $notification->patient_id = $pateint_id;
        $notification->is_chat = 'YES';
        $notification->text = $message;
        $notification->status = 0;
        $notification->date = $date = now();
        $notification->action = 'confirmd';
        $notification->save();
        $title = '' . $doctor_info->title . '' . $doctor_info->doctor_name . '';
        $type = 'user';
        $this->send_message_tag_chat($title, $message, $userid, $doctorid, $type, $pateint_id);
        return 0;
    }

}
