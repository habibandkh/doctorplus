<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Chat;
use Pusher\Pusher;
use Validator;

class ChatController extends Controller {

    public function userChatView() {
        $id_user = session('id_pateint');
        $doctors = DB::table('users')
                        ->join('favorite_doctor', 'users.id', '=', 'favorite_doctor.user_id')
                        ->join('doctor', 'favorite_doctor.doctor_id', '=', 'doctor.doctorID')
                        ->select('doctor.doctorID', 'doctor.doctor_name', 'doctor.doctor_picture')
                        ->where('users.id', $id_user)->get();
        $userInfos = DB::table('users')->where('id', $id_user)->first();

        return view('pateint.chat', compact('doctors', 'userInfos'));
    }

    public function getMessageFromDoctor(Request $request) {
        $doctor_id = $request->id;
        $my_id = session('id_pateint');
        //  return $doctor_id;
        // Make read all unread message
        //  Message::where(['from' => $doctor_id, 'to' => $my_id])->update(['is_read' => 1]);
        // Get all message from selected user
        $messages = \App\Chat::where(function ($query) use ($doctor_id, $my_id) {
                    $query->where('from', $doctor_id)->where('to', $my_id);
                })->oRwhere(function ($query) use ($doctor_id, $my_id) {
                    $query->where('from', $my_id)->where('to', $doctor_id);
                })->get();

        return view('pateint.message.index', compact('messages'));
    }

    public function sendMessage(Request $request) {
        $from = session('id_pateint');
        $to = $request->receiver_id;
        $message = $request->message;


        $data = new Chat();
        $data->from = $from;
        $data->user_id = $from;
        $data->to = $to;
        $data->doctor_id = $to;
        $data->message = $message;
        $data->type = 'text';
        $data->is_read = 0; // message will be unread when sending message
        $data->save();
        // pusher
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => true
        );
        $pusher = new Pusher(
                env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), $options
        );

        $data1 = ['from' => $from, 'to' => $to]; // sending from and to user id when pressed enter
        $pusher->trigger('my-channel', 'my-event', $data1);
    }

    public function uploadFileToDoctor(Request $request) {

        $from = session('id_pateint');
        $to = $request->receiver_id;
        $message = $request->message;

        $validator = Validator::make($request->all(), [
                    'file' => 'required|mimes:doc,docx,pdf,zip,jpg,jpeg,png,gef|max:30720',
        ]);

        if ($validator->passes()) {
            $image = $request->file('file');

            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('chat/file'), $new_name);

            $store = new Chat();
            $store->from = $from;
            $store->user_id = $from;
            $store->to = $to;
            $store->doctor_id = $to;
            $store->message = $message;
            $store->file = $new_name;
            $store->type = 'file';
            $store->is_read = 0;
            $store->save();

            // pusher
            $options = array(
                'cluster' => 'ap2',
                'useTLS' => true
            );

            $pusher = new Pusher(
                    env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), $options
            );

            $data = ['from' => $from, 'to' => $to]; // sending from and to user id when pressed enter
            $pusher->trigger('my-channel', 'my-event', $data);
            return response()->json(['success' => 'Added new records.']);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }

    // related to doctors chating 

    public function doctorChatView() {

        $id_doctor = session('id_doctor');

        $patients = DB::table('doctor')
                        ->join('favorite_doctor', 'doctor.doctorID', '=', 'favorite_doctor.doctor_id')
                        ->join('users', 'favorite_doctor.user_id', '=', 'users.id')
                        ->select('users.id', 'users.name', 'users.pic')
                        ->where('doctor.doctorID', $id_doctor)->get();

        $doctorInfos = DB::table('doctor')->where('doctorID', $id_doctor)->first();

//  dd($patients);

        return view('doctor.chat', compact('patients', 'doctorInfos'));
    }

    public function getMessageFromUser(Request $request) {
        $user_id = $request->receiver_id;
        $my_id = session('id_doctor');
        //  return $doctor_id;
        // Make read all unread message
        //  Message::where(['from' => $doctor_id, 'to' => $my_id])->update(['is_read' => 1]);
        // Get all message from selected user
        $messages = \App\Chat::where(function ($query) use ($user_id, $my_id) {
                    $query->where('from', $user_id)->where('to', $my_id);
                })->oRwhere(function ($query) use ($user_id, $my_id) {
                    $query->where('from', $my_id)->where('to', $user_id);
                })->get();
        // dd($messages);
        return view('doctor.message.index', compact('messages'));
    }

    public function sendMessageToUser(Request $request) {
        $from = session('id_doctor');
        $to = $request->receiver_id;
        $message = $request->message;

        $data = new Chat();
        $data->from = $from;
        $data->doctor_id = $from;
        $data->to = $to;
        $data->user_id = $to;
        $data->message = $message;
        $data->type = 'text';
        $data->is_read = 0; // message will be unread when sending message
        $data->save();


        // pusher
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => true
        );

        $pusher = new Pusher(
                env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), $options
        );

        $data = ['from' => $from, 'to' => $to]; // sending from and to user id when pressed enter
        $pusher->trigger('my-channel', 'my-event', $data);
    }

    public function uploadFileToUser(Request $request) {

        $from = session('id_doctor');
        $to = $request->receiver_id;
        $message = $request->message;

        $validator = Validator::make($request->all(), [
                    'file' => 'required|mimes:doc,docx,pdf,zip,jpg,jpeg,png,gef|max:30720',
        ]);
        if ($validator->passes()) {
            $image = $request->file('file');

            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('chat/file'), $new_name);

            $store = new Chat();
            $store->from = $from;
            $store->doctor_id = $from;
            $store->to = $to;
            $store->user_id = $to;
            $store->message = $message;
            $store->file = $new_name;
            $store->type = 'file';
            $store->is_read = 0;
            $store->save();


            // pusher
            $options = array(
                'cluster' => 'ap2',
                'useTLS' => true
            );

            $pusher = new Pusher(
                    env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), $options
            );

            $data = ['from' => $from, 'to' => $to]; // sending from and to user id when pressed enter
            $pusher->trigger('my-channel', 'my-event', $data);
            return response()->json(['success' => 'Added new records.']);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }

}
