<?php

namespace App\Http\Controllers;

use App\FavoriteDoctor;
use App\Events\EndPool;
use App\Http\Controllers\NotificationController;
use App\Pateint;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Image;

class PateintController extends Controller {

    function bookmarkDoctor(Request $qre) {
        $doctorid = $qre->id;
        $userid = session('id_pateint');
        $check = DB::table('favorite_doctor')->where('doctor_id', $doctorid)->where('user_id', $userid)->first();

        if ($check == null) {

            $pateint = new FavoriteDoctor();
            $pateint->user_id = $userid;
            $pateint->doctor_id = $doctorid;
            $pateint->save();
            return back()->with('message', 'doctor bookmark successfully!');
        } else {
            return back()->with('u-message', 'the doctor is already bookmarked!');
        }
    }

    function cancelBookmarkDoctor(Request $qre) {
        $doctorid = $qre->id;
        $userid = session('id_pateint');

        $check = DB::table('favorite_doctor')->where('doctor_id', $doctorid)->where('user_id', $userid)->first();

        if ($check == null) {
            return back()->with('u-message', 'the id of doctor is not bookmarked!');
        } else {

            DB::table('favorite_doctor')->where('doctor_id', $doctorid)->where('user_id', $userid)->delete();
            return back()->with('message', 'doctor un-bookmark successfully!');
        }
    }

    function get_Notification_List(Request $re) {
        $user_id = $re->userid;


//        $count = DB::table('user_notification')
//                ->where('user_id', $user_id)
//                ->select('id', 'doctor_id', 'text', 'status', 'date')
//                ->orderBy('id', 'DESC')
//                ->count();
        DB::table('user_notification')->where('status', 0)->where('user_id', $user_id)->update(['status' => 1]);

        $notification = DB::table('user_notification')->where('user_id', $user_id)->select('id', 'doctor_id', 'user_id', 'patient_id', 'text', 'status', 'date', 'is_chat')->orderBy('id', 'DESC')->paginate(10);
        foreach ($notification as $noti) {

            $doctor = DB::table('doctor')->where('doctorID', $noti->doctor_id)->first();
            $noti->name = $doctor->doctor_name;
            $noti->title = $doctor->title;
        }
        return view('pateint.notification', compact('notification'));
        // return response()->json($notification, 201);
    }

    public function ajaxUnreadNotificationAppointment(Request $re) {
        if (request()->ajax()) {
            $data = DB::table('user_notification')->where('status', 0)->where('user_id', session('id_pateint'))->limit(3)->get();

            return response()->json(['success' => $data]);
        }
    }

    public function myProfile() {
        $user_id = session('id_pateint');
        $pateintEdit = DB::table('users')->where('id', $user_id)->first();
        return view('pateint.profile', compact('pateintEdit'));
    }

    public function getMyDoctors() {
        $user_id = session('id_pateint');



        $mydoctors = DB::table('favorite_doctor')
                ->join('doctor', 'favorite_doctor.doctor_id', '=', 'doctor.doctorID')
                ->select('doctor.doctorID'
                        , 'doctor.title'
                        , 'doctor.doctor_name'
                        , 'doctor.doctor_email'
                        , 'doctor.doctor_phoneNo'
                        , 'doctor.doctor_picture'
                        , 'doctor.doctor_clinicAddress'
                        , 'doctor.Expert')
                ->where('favorite_doctor.user_id', $user_id)
                ->groupBy('doctor.doctorID')
                ->paginate(20);

        return view('pateint.mydoctors', compact('mydoctors'));
    }

    public function viewMedicalRecords() {
        $user_id = session('id_pateint');
        $document = DB::table('document')->where('user_id', $user_id)->paginate(5);
        return view('pateint.medicalrecords', compact('document'));
    }

    public function home() {

        $now = Carbon::now();
        $future = $now->addDays(1);
        $tomorrow = $future->toDateString();
        $userid = session('id_pateint');

        $myappointmentlist = DB::table('appointment')
                ->join('users', 'appointment.user_id', '=', 'users.id')
                ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                ->where('appointment.user_id', $userid)
                ->where('appointment.pateint_id', null)
                ->select('doctorID', 'doctor_picture', 'pic', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
                ->orderByRaw("Status ASC , appointment_date ASC")
                ->paginate(5);
        $myfamilyappointmentlist = DB::table('appointment')
                ->join('users', 'appointment.user_id', '=', 'users.id')
                ->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')
                ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                ->where('appointment.user_id', $userid)
                ->select('doctorID', 'doctor_picture', 'p_photo', 'appID', 'patientID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
                ->orderByRaw("Status ASC , appointment_date ASC")
                ->paginate(5);


        $feedback = DB::table('appointment')
                ->join('doctor', 'doctor.doctorID', '=', 'appointment.doctor_id')
                ->where('user_id', $userid)
                ->where('Status', 1)
                ->where('appointment_date', '<', $tomorrow)
                ->where('feedback', 0)
                ->get();

        $pateint = DB::table('users')->join('pateint', 'pateint.user_ID', '=', 'users.id')
                        ->where('pateint.user_ID', $userid)->get();

        return view('pateint.home', compact('myappointmentlist', 'myfamilyappointmentlist', 'feedback', 'pateint'));
    }

    public function searchAppointmentbyType(Request $request) {
        $get_user_type = $request->get_user_type;
        $get_app_type = $request->get_app_type;
        //  dd($get_user_type);
        $now = Carbon::now();
        $today = $now->toDateString();
        $future = $now->addDays(1);
        $tomorrow = $future->toDateString();


        $userid = session('id_pateint');

        $feedback = DB::table('appointment')
                ->join('doctor', 'doctor.doctorID', '=', 'appointment.doctor_id')
                ->where('user_id', $userid)
                ->where('Status', 1)
                ->where('appointment_date', '<', $tomorrow)
                ->where('feedback', 0)
                ->get();

        $pateint = DB::table('users')->join('pateint', 'pateint.user_ID', '=', 'users.id')
                        ->where('pateint.user_ID', $userid)->get();

        if ($get_app_type == 'null') {
            if ($get_user_type == 0) {
                $myappointmentlist = DB::table('appointment')
                        ->join('users', 'appointment.user_id', '=', 'users.id')
                        ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                        ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                        ->where('appointment.user_id', $userid)
                        ->where('appointment.pateint_id', null)
                        ->select('doctorID', 'doctor_picture', 'pic', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
                        ->orderByRaw("Status ASC , appointment_date ASC")
                        ->paginate(20);
                $myfamilyappointmentlist = DB::table('appointment')->where('appointment.user_id', 00)->paginate(5);
                return view('pateint.home', compact('myappointmentlist', 'myfamilyappointmentlist', 'feedback', 'pateint'));
            } else {
                $myfamilyappointmentlist = DB::table('appointment')
                        ->join('users', 'appointment.user_id', '=', 'users.id')
                        ->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')
                        ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                        ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                        ->where('appointment.pateint_id', $get_user_type)
                        ->select('doctorID', 'doctor_picture', 'p_photo', 'appID', 'patientID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
                        ->orderByRaw("Status ASC , appointment_date ASC")
                        ->paginate(20);
                $myappointmentlist = DB::table('appointment')->where('appointment.user_id', 00)->paginate(5);
                return view('pateint.home', compact('myappointmentlist', 'myfamilyappointmentlist', 'feedback', 'pateint'));
            }
        } else {
            if ($get_user_type == 0) {
                $query = DB::table('appointment')
                        ->join('users', 'appointment.user_id', '=', 'users.id')
                        ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                        ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                        ->where('appointment.user_id', $userid)
                        ->where('appointment.pateint_id', null);
                if ($get_app_type == 'current') {
                    $query->where('appointment.Status', 0);
                    $query->whereDate('appointment.appointment_date', '=', $today);
                }
                if ($get_app_type == 'upcoming') {
                    $query->where('appointment.Status', 0);
                    $query->whereDate('appointment.appointment_date', '>', $today);
                }
                if ($get_app_type == 'cancel') {
                    $query->where('appointment.Status', 1);
                    $query->whereDate('appointment.appointment_date', '>', $today);
                }
                if ($get_app_type == 'expired') {
                    $query->whereDate('appointment.appointment_date', '<', $today);
                }
                $query->select('doctorID', 'doctor_picture', 'pic', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address');
                $query->orderByRaw("Status ASC , appointment_date ASC");
                $myappointmentlist = $query->paginate(20);

                $myfamilyappointmentlist = DB::table('appointment')->where('appointment.user_id', 00)->paginate(5);
                return view('pateint.home', compact('myappointmentlist', 'myfamilyappointmentlist', 'feedback', 'pateint'));
            } if ($get_user_type > 0) {
                $query = DB::table('appointment')
                        ->join('users', 'appointment.user_id', '=', 'users.id')
                        ->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')
                        ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                        ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                        ->where('appointment.pateint_id', $get_user_type);
                if ($get_app_type == 'current') {
                    $query->where('appointment.Status', 0);
                    $query->whereDate('appointment.appointment_date', '=', $today);
                }
                if ($get_app_type == 'upcoming') {
                    $query->where('appointment.Status', 0);
                    $query->whereDate('appointment.appointment_date', '>', $today);
                }
                if ($get_app_type == 'cancel') {
                    $query->where('appointment.Status', 1);
                    $query->whereDate('appointment.appointment_date', '>', $today);
                }
                if ($get_app_type == 'expired') {
                    $query->whereDate('appointment.appointment_date', '<', $today);
                }
                $query->select('doctorID', 'doctor_picture', 'p_photo', 'appID', 'patientID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address');
                $query->orderByRaw("Status ASC , appointment_date ASC");
                $myfamilyappointmentlist = $query->paginate(20);
                $myappointmentlist = DB::table('appointment')->where('appointment.user_id', 00)->paginate(5);
                return view('pateint.home', compact('myappointmentlist', 'myfamilyappointmentlist', 'feedback', 'pateint'));
            } else {

                $query = DB::table('appointment')
                        ->join('users', 'appointment.user_id', '=', 'users.id')
                        ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                        ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                        ->where('appointment.user_id', $userid)
                        ->where('appointment.pateint_id', null);
                if ($get_app_type == 'current') {
                    $query->where('appointment.Status', 0);
                    $query->whereDate('appointment.appointment_date', '=', $today);
                }
                if ($get_app_type == 'upcoming') {
                    $query->where('appointment.Status', 0);
                    $query->whereDate('appointment.appointment_date', '>', $today);
                }
                if ($get_app_type == 'cancel') {
                    $query->where('appointment.Status', 1);
                    $query->whereDate('appointment.appointment_date', '>', $today);
                }
                if ($get_app_type == 'expired') {
                    $query->whereDate('appointment.appointment_date', '<', $today);
                }
                $query->select('doctorID', 'doctor_picture', 'pic', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address');
                $query->orderByRaw("Status ASC , appointment_date ASC");
                $myappointmentlist = $query->paginate(20);


                $q = DB::table('appointment')
                        ->join('users', 'appointment.user_id', '=', 'users.id')
                        ->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')
                        ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                        ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                        ->where('appointment.user_id', $userid);
                if ($get_app_type == 'current') {
                    $q->where('appointment.Status', 0);
                    $q->whereDate('appointment.appointment_date', '=', $today);
                }
                if ($get_app_type == 'upcoming') {
                    $q->where('appointment.Status', 0);
                    $q->whereDate('appointment.appointment_date', '>', $today);
                }
                if ($get_app_type == 'cancel') {
                    $q->where('appointment.Status', 1);
                    $q->whereDate('appointment.appointment_date', '>', $today);
                }
                if ($get_app_type == 'expired') {
                    $q->whereDate('appointment.appointment_date', '<', $today);
                }
                $q->select('doctorID', 'doctor_picture', 'p_photo', 'appID', 'patientID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address');
                $q->orderByRaw("Status ASC , appointment_date ASC");
                $myfamilyappointmentlist = $q->paginate(5);

                return view('pateint.home', compact('myappointmentlist', 'myfamilyappointmentlist', 'feedback', 'pateint'));
            }
        }
    }

    public function cancelmyAppointment(Request $request) {

        $app_id = $request->app_id;
        $cancel = DB::table('appointment')->where('appID', $app_id)->update(['Status' => 1, 'cancel_by_user' => 1,]);
        $app = DB::table('appointment')->where('appID', $app_id)->first();
        $noti = new NotificationController();
        $res = $noti->appCancelNoti_cancel_by_user($app);
        event(new EndPool('1'));
        return back()->with('success', 'appointment cancel successful');
    }

    public function editFamilyMember(Request $request) {

        $pateint_id = $request->pateint_id;
        $pateintEdit = DB::table('pateint')->where('patientID', $pateint_id)->first();
        return view('pateint.editPateint', compact('pateintEdit'));
    }

    public function familyMember() {
        $pateint_id = session('id_pateint');
        $pateint = DB::table('pateint')->where('user_ID', $pateint_id)->paginate(5);
        return view('pateint.familyMember', compact('pateint'));
    }

    public function familyMemberView(Request $request) {

        $pateint_id = $request->pateint_id;
        $document = DB::table('document')->where('pateint_id', $pateint_id)->get();

        // dd($document);

        foreach ($document as $do) {
            $file = DB::table('file')->where('doc_id', $do->docID)->get();
            $do->file = $file;
        }
        //dd($document);

        $pateintview = DB::table('pateint')->where('patientID', $pateint_id)->first();
        return view('pateint.viewpateint', compact('pateintview', 'document'));
    }

    public function addFamilyMemberView() {
        return view('pateint.addPateint');
    }

    public function deleteFamilyMember(Request $request) {

        $pateint_id = $request->pateint_id;
        DB::table('appointment')->where('pateint_id', $pateint_id)->delete();
        DB::table('pateint')->where('patientID', $pateint_id)->delete();
        return back()->with('success', 'family member add successful');
    }

    public function deleteDocument(Request $request) {

        $doc_id = $request->doc_id;
        //  dd($doc_id);
        DB::table('file')->where('doc_id', $doc_id)->delete();

        return back()->with('success', 'file deleted successful');
    }

    public function addFamilyMember(Request $request) {

        $this->validate($request, [
            'name' => 'required',
            'Relation' => 'required',
            'Birthday' => 'required',
            'gender' => 'required'
        ]);
        if ($request->hasFile('photo')) {
            $this->validate($request, [
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            $image = $request->file('photo');
            $image_name = 'pateint-' . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/pateint');
            $resize_image = Image::make($image->getRealPath());
            $resize_image->resize(150, 150, function($constraint) {
                //  $constraint->aspectRatio();
            })->save($destinationPath . '/' . $image_name);
            // $image->move($destinationPath, $image_name);

            $pateint_id = session('id_pateint');
            $pateint = new Pateint();
            $pateint->p_photo = $image_name;
            $pateint->user_ID = $pateint_id;
            $pateint->p_name = $request->name;
            $pateint->p_Relation = $request->Relation;
            $pateint->p_phoneNo = $request->phone;
            $pateint->birthday = $request->Birthday;
            $pateint->gender = $request->gender;
            // $pateint->bloodgroup = $request->blood;
            $pateint->save();
        } else {
            $pateint_id = session('id_pateint');
            $pateint = new Pateint();
            $pateint->user_ID = $pateint_id;
            $pateint->p_name = $request->name;
            if ($request->gender == 1)
                $pateint->p_photo = 'male.png';
            else
                $pateint->p_photo = 'female.png';
            $pateint->p_Relation = $request->Relation;
            $pateint->p_phoneNo = $request->phone;
            $pateint->birthday = $request->Birthday;
            $pateint->gender = $request->gender;
            //$pateint->bloodgroup = $request->blood;
            $pateint->save();
        }
        return back()->with('success', 'family member add successful');
    }

    public function updateFamilyMember(Request $request) {
        $patient_ID = $request->patientID;

        $pateint_id = session('id_pateint');
        $this->validate($request, [
            'name' => 'required',
            'Relation' => 'required',
            'Birthday' => 'required',
            'gender' => 'required'
        ]);
        if ($request->hasFile('photo')) {
            $this->validate($request, [
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $image = $request->file('photo');
            $image_name = 'pateint-' . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/pateint');
            $resize_image = Image::make($image->getRealPath());
            $resize_image->resize(150, 150, function($constraint) {
                //  $constraint->aspectRatio();
            })->save($destinationPath . '/' . $image_name);

            DB::table('pateint')
                    ->where('patientID', $patient_ID)
                    ->update([
                        'p_photo' => $image_name,
                        'user_ID' => $pateint_id,
                        'p_name' => $request->name,
                        'p_Relation' => $request->Relation,
                        'p_phoneNo' => $request->phone,
                        'birthday' => $request->Birthday,
                        'gender' => $request->gender,
                            //'bloodgroup' => $request->blood,
            ]);
        } else {
            DB::table('pateint')
                    ->where('patientID', $patient_ID)
                    ->update([
                        'user_ID' => $pateint_id,
                        'p_name' => $request->name,
                        'p_Relation' => $request->Relation,
                        'p_phoneNo' => $request->phone,
                        'birthday' => $request->Birthday,
                        'gender' => $request->gender,
                            // 'bloodgroup' => $request->blood,
            ]);
        }
        return back()->with('success', 'family member update successful');
    }

    public function updateProfile(Request $request) {

        $pateint_id = session('id_pateint');

        $this->validate($request, [
            'name' => 'required',
            'father_name' => 'required',
            'birthday' => 'required',
            'phone' => 'required',
            'gender' => 'required'
        ]);

        if ($request->hasFile('photo')) {

            $this->validate($request, [
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            $image = $request->file('photo');
            $image_name = 'pateint-' . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/user');
            $resize_image = Image::make($image->getRealPath());
            $resize_image->resize(150, 150, function($constraint) {
                // $constraint->aspectRatio();
            })->save($destinationPath . '/' . $image_name);


            DB::table('users')
                    ->where('id', $pateint_id)
                    ->update([
                        'pic' => $image_name,
                        'name' => $request->name,
                        'f_name' => $request->father_name,
                        'phone' => $request->phone,
                        'email' => $request->email,
                        'birthday' => $request->birthday,
                        'gender' => $request->gender,
            ]);
        } else {
            DB::table('users')
                    ->where('id', $pateint_id)
                    ->update([
                        'name' => $request->name,
                        'f_name' => $request->father_name,
                        'phone' => $request->phone,
                        'email' => $request->email,
                        'birthday' => $request->birthday,
                        'gender' => $request->gender,
            ]);
        }
        return back()->with('success', 'data updated successfully');
    }

    public function appointment() {

        $pateint_id = session('id_pateint');

        $appointmentlist = DB::table('appointment')
                ->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')
                ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                ->where('appointment.user_id', $pateint_id)
                ->paginate(10);

        return view('pateint.appointmentlist', compact('appointmentlist'));
    }

    public function savePassword(Request $request) {

        $user_id = session('id_pateint');

        $oldpass = $request->old_pass;
        $newpass = $request->new_pass;

        $is_pass = DB::table('users')->where('id', '=', $user_id)->where('password', '=', $oldpass)->first();

        if ($is_pass) {
            DB::table('users')
                    ->where('id', $user_id)
                    ->update([
                        'password' => $newpass,
            ]);
            return back()->with('success', 'password changed successfully');
        } else {
            return back()->with('danger', 'the old password is wrong');
        }
    }

}
