<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Doctorspeciality;
use App\DoctorServiceView;
use App\Doctor_condition_view;
use App\Clinic_address;
use App\Clinic;
use Carbon\Carbon;

class DoctorProProController extends Controller {

    function addPrimarySpeciality(Request $request) {

        $doctor_id = session()->get('id_doctor');
        $spaciality_id = $request->spaciality_id;

        $doctorPrimary = DB::table('doctorspeciality')->where('F_doctorID', $doctor_id)->where('type', 'P')->first();
        if ($doctorPrimary != null) {
            $ser = DB::table('doctorspeciality')->where('id_s_d', $doctorPrimary->id_s_d)->delete();
        }

        $doctor = DB::table('doctor')->where('doctorID', $doctor_id)->first();
        $speciality = DB::table('speciality')->where('specialityID', $spaciality_id)->first();

        if ($doctor != null && $speciality != null) {
            $insert = new Doctorspeciality();
            $insert->F_doctorID = $doctor_id;
            $insert->F_specialityID = $spaciality_id;
            $insert->type = 'P';
            $insert->save();

            DB::table('doctor')->where('doctorID', $doctor_id)->update(['Expert' => $speciality->specialityName]);
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    function addSecondrySpeciality(Request $request) {

        $doctor_id = session()->get('id_doctor');

        $add_spaciality_id = $request->add_spaciality_id;
        if ($add_spaciality_id[0] != null) {
            $insert = new Doctorspeciality();
            $insert->F_doctorID = $doctor_id;
            $insert->F_specialityID = $add_spaciality_id;
            $insert->type = 'S';
            $insert->save();
        }

        return redirect()->back();
    }

    function deleteSpeciality(Request $request) {
        $id = $request->id;
        DB::table('doctorspeciality')->where('id_s_d', $id)->delete();
        return redirect()->back();
    }

////////////////////Doctors Services

    function addService(Request $request) {

        $doctor_id = session()->get('id_doctor');
        $doctor_id_service = $request->service_id;

        $ds = new DoctorServiceView();
        $ds->doctor_id = $doctor_id;
        $ds->services_id = $doctor_id_service;
        $ds->save();

        return redirect()->back();
    }

    function deleteService(Request $request) {
        $id = $request->id;
        DB::table('doctor_services_view')->where('id', $id)->delete();
        return redirect()->back();
    }

    function addCondition(Request $request) {

        $doctor_id = session()->get('id_doctor');
        $doctor_condition_id = $request->condition_id;

        $ds = new Doctor_condition_view();
        $ds->doctor_id = $doctor_id;
        $ds->condition_id = $doctor_condition_id;
        $ds->save();
        return redirect()->back();
    }

    function deleteCondition(Request $request) {

        $id = $request->id;
        DB::table('doctor_condition_view')->where('id', $id)->delete();
        return redirect()->back();
    }

    function saveClinic(Request $request) {
        $clincid = $request->id;
        $doctorid = session()->get('id_doctor');


        //$doctor = DB::table('doctor')->where('doctorID', $doctorid)->first();
        //$my_clinic = DB::table('address')->where('id', $clincid)->first();
        $count_clinic = DB::table('address_doctor_view')->where('doctor_id', $doctorid)->count();
        //  dd($count_clinic);
        if (!($count_clinic > 3)) {

            $add = new Clinic_address();
            $add->address_id = $clincid;
            $add->doctor_id = $doctorid;
            $add->save();

            return redirect()->back()->with('message', "clinic add successfuly");
        } else {
            // return response()->json(['errors' => " you can't add more then 3clinic"], 400);
            return redirect()->back()->with('message', "you can't add more then 3 clinic");
        }
    }

    function addNewClinic(Request $request) {

        $doctorid = session()->get('id_doctor');
        $count_clinic = DB::table('address_doctor_view')->where('doctor_id', $doctorid)->count();

        if (!($count_clinic > 3)) {
            $clinic = new Clinic();
            $clinic->name = $request->name;
            $clinic->address = $request->address;
            $clinic->city = $request->city;
            $clinic->doctor_id = $doctorid;
            $clinic->save();

            $address_id = DB::table('address')->where('doctor_id', $doctorid)->orderBy('id', 'DESC')->first();

            $add = new Clinic_address();
            $add->address_id = $address_id->id;
            $add->doctor_id = $doctorid;
            $add->save();

            return redirect()->back()->with('message', "clinic add successfuly");
        } else {
            return redirect()->back()->with('message', "you can't add more then 3 clinic");
        }
    }

    function removeSaveClinic(Request $request) {

        $clincid = $request->id;
        $doctorid = session()->get('id_doctor');

        // dd($clincid);
        $edu = DB::table('address_doctor_view')->where('address_id', $clincid)->where('doctor_id', $doctorid)->first();
        if ($edu != null) {
            $res = $this->call_to_remove_or_delete_clinic($doctorid, $clincid);

            return redirect()->back()->with('message', "clinic Removed!");
        } else {
            return redirect()->back()->with('message', "invalid data!");
        }
    }

    function call_to_remove_or_delete_clinic($doctorid, $clincid) {

        $edu = DB::table('address_doctor_view')->where('address_id', $clincid)->count();
        if ($edu == 1) {
            $res = $this->call_to_delete_clinic($doctorid, $clincid);
            return 0;
        } else {

            $res = $this->call_to_remove_clinic($doctorid, $clincid);
            return 0;
        }
    }

    function call_to_delete_clinic($doctorid, $clincid) {

        $vis_del = DB::table('visitingtime')->where('doctorID', $doctorid)->where('clinic_address_id', $clincid)->delete();
        $res = $this->callToRemoveAppAndSendNoti($doctorid, $clincid);
        $edu1 = DB::table('address_doctor_view')->where('doctor_id', $doctorid)->where('address_id', $clincid)->delete();
        $edu = DB::table('address')->where('id', $clincid)->delete();

        return 0;
    }

    function call_to_remove_clinic($doctorid, $clincid) {

        $vis_del = DB::table('visitingtime')->where('doctorID', $doctorid)->where('clinic_address_id', $clincid)->delete();
        $res = $this->callToRemoveAppAndSendNoti($doctorid, $clincid);
        $edu = DB::table('address_doctor_view')->where('doctor_id', $doctorid)->where('address_id', $clincid)->delete();

        return 0;
    }

    function callToRemoveAppAndSendNoti($doctorid, $clincid) {
        $now = Carbon::now();
        $today = $now->toDateString();
        $appointments = DB::table('appointment')
                ->where('doctor_id', $doctorid)
                ->where('clinic_address_id', $clincid)
                ->where('cancel_by_user', 0)
                ->where('cancel_by_doctor', 0)
                ->where('appointment_date', '>=', $today)
                ->get();
        foreach ($appointments as $appt) {

            $cancel = DB::table('appointment')
                    ->where('appID', $appt->appID)
                    ->update([
                'Status' => 1,
                'cancel_by_doctor' => 1,
            ]);

            //store notification
            // send notification to user
        }
        return 0;
    }

}
