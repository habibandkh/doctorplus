<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Image;
use Symfony\Component\HttpFoundation\Session\Session;

class LoginController extends Controller {

    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function loginPage() {
        return view('public/login');
    }

    public function loginUser(Request $request) {

        $number = $request->user_phone;
        $pass = $request->user_pass;


        $is_pateint = DB::table('users')->where('phone', '=', $number)->where('password', '=', $pass)->first();
        $is_doctor = DB::table('doctor')->where('doctor_email', '=', $number)->where('doctor_password', '=', $pass)->first();
        $is_Admin = DB::table('admins')->where('email', '=', $number)->where('password', '=', $pass)->first();
        $is_Blogger = DB::table('admins')->where('email', '=', $number)->where('password', '=', $pass)->first();

        if ($is_pateint) {

            $id_pateint = $is_pateint->id;
            $name_pateint = $is_pateint->name;
            $phone_pateint = $is_pateint->phone;
            $pic_pateint = $is_pateint->pic;
            $birthday = $is_pateint->birthday;

            session(['name_pateint' => $name_pateint]);
            session(['id_pateint' => $id_pateint]);
            session(['phone_pateint' => $phone_pateint]);
            session(['pic_pateint' => $pic_pateint]);
            session(['birthday' => $birthday]);
            session(['is_pateint' => 'pateint']);

            return redirect()->action('PateintController@home');
            // $value = session('phone_pateint');
            //dd($value);
        }

        if ($is_doctor) {
            $id_doctor = $is_doctor->doctorID;
            $name_doctor = $is_doctor->doctor_name;
            $pic_doctor = $is_doctor->doctor_picture;
            $birthday = $is_doctor->doctor_birthday;
            session(['id_doctor' => $id_doctor]);
            session(['name_doctor' => $name_doctor]);
            session(['pic_doctor' => $pic_doctor]);
            session(['birthday' => $birthday]);
            session(['is_doctor' => 'doctor']);

            return redirect()->action('DoctorController@home');
            // $value = session('name_pateint');
            //dd($value);
        }

        if ($is_Admin) {

            $id_admin = $is_Admin->id;
            $name_admin = $is_Admin->name;
            $email_admin = $is_Admin->email;
            $pic_admin = $is_Admin->pic;
            $type_admin = $is_Admin->type;

            session(['name_admin' => $name_admin]);
            session(['id_admin' => $id_admin]);
            session(['email_admin' => $email_admin]);
            session(['pic_admin' => $pic_admin]);
            session(['type_admin' => $type_admin]);
            session(['is_admin' => 'Phd_Admin']);

             return redirect()->action('Admin\AdminAuthController@home');
        } if ($is_Blogger) {
            $id_admin = $is_Admin->id;
            $name_admin = $is_Admin->name;
            $email_admin = $is_Admin->email;
            $pic_admin = $is_Admin->pic;
            $type_admin = $is_Admin->type;

            session(['name_admin' => $name_admin]);
            session(['id_admin' => $id_admin]);
            session(['email_admin' => $email_admin]);
            session(['pic_admin' => $pic_admin]);
            session(['type_admin' => $type_admin]);
            session(['is_blogger' => 'Phd_Blogger']);

            $posts = DB::table('post')->get();
            return view('admin.blog.blog', compact('posts'));
        } else {
            \Session::flash('message', 'incorrect username or password');
            return redirect()->back()->withInput($request->only('user_phone'));
        }
    }

    public function logout(Request $req) {
        $req->session()->flush();
        return redirect()->route('home');
    }

    public function registerUser(Request $request) {

        if ($request->hasFile('photo')) {
            $this->validate($request, [
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
            ]);
            $image = $request->file('photo');

            $image_name = 'user-' . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/user');
            $resize_image = Image::make($image->getRealPath());
            $resize_image->resize(150, 150, function($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $image_name);

            $image->move($destinationPath, $image_name);

            $doctor = new User();
            $doctor->pic = $image_name;
            $doctor->name = $request->name;
            $doctor->f_name = $request->f_name;
            $doctor->age = $request->age;
            $doctor->gender = $request->gender;
            $doctor->phone = $request->phone;
            $doctor->password = $request->pass;

            $doctor->save();
        } else {
            $doctor = new User();
            $doctor->name = $request->name;
            $doctor->f_name = $request->f_name;
            $doctor->age = $request->age;
            $doctor->gender = $request->gender;
            $doctor->phone = $request->phone;
            $doctor->password = $request->pass;
            $doctor->save();
        }

        return redirect()->back()->with('message', 'Doctor account created successfully');
    }

    public function register_page() {
        return view('public/userRegister');
    }

    public function forget_password() {
        return view('public/password');
    }

    public function join_as_doctor() {
        return view('public/joinAsDoctor');
    }

}
