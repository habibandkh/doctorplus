<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;
use DB;
use App\FileModel;
use File;

class MultipleUploadController extends Controller {

    function allowSharingRecord(Request $request) {

        $docID = $request->record_id;
        $allow = DB::table('document')->select('allow_share_records')->where('docID', $docID)->first();


        if ($allow->allow_share_records == 0) {

            DB::table('document')->where('docID', $docID)->update(['allow_share_records' => 1]);
            return redirect()->back();
        } else {

            DB::table('document')->where('docID', $docID)->update(['allow_share_records' => 0]);
            return redirect()->back();
        }
    }

    public function deleteRecords(Request $request) {

        $doc_id = $request->record_id;
        // dd($doc_id);
        $files = DB::table('file')->where('doc_id', $doc_id)->get();
        $destinationPath = '/images/medicalrecords/';
        foreach ($files as $file) {

            File::delete($destinationPath . $file->document);

            DB::table('file')->where('doc_id', $doc_id)->delete();
        }

        $res = DB::table('document')->where('docID', $doc_id)->delete();
        if ($res) {
            $output = array('success' => 'Recored deleted successfully');
            return redirect()->back();
        } else {
            $output = array('failed' => 'Recored id not found');
            return redirect()->back();
        }
    }

    function index() {

        $user_id = session('id_pateint');
        $patient_name = DB::table('pateint')
                ->join('appointment', 'pateint.patientID', '=', 'appointment.pateint_id')
                ->select('patientID', 'pateint.user_ID', 'p_name')
                ->where('pateint.user_ID', $user_id)
                ->groupBy('patientID')
                ->get();
        $doctor_name = DB::table('doctor')->get();

        return view('pateint.multiple_file_upload', compact('patient_name', 'doctor_name'));
    }

    public function upload(Request $request) {
        
        $validator = \Validator::make($request->all(), [
                    'doctor_who' => 'required',
                    'related_to_who' => 'required',
                    'records_type' => 'required',
                    'file' => 'required',
                    'file.*' => 'mimes:doc,pdf,docx,txt,zip,jpeg,jpg,png,gif|max:30720'
        ]);
   
        if ($validator->passes()) {
            
            $user_id = session('id_pateint');
            $images = $request->file('file');
            $related = $request->related_to_who;

            $records_type = $request->records_type;
            $doctor_id = $request->doctor_who;
            $doctorx = DB::table('doctor')->select('doctor_name')->where('doctorID', $doctor_id)->first();
            $doctor_name = $doctorx->doctor_name;
            if ($related == 0) {

                $users = DB::table('users')->select('name')->where('id', $user_id)->first();
                $user_name = $users->name;
                $id = $this->storeDocForUser($doctor_id, $doctor_name, $user_id, $user_name, $records_type, $related);
            } else {
                $pateint_id = $related;
                $pateint = DB::table('pateint')->select('p_name')->where('patientID', $pateint_id)->first();
                $pateint_name = $pateint->p_name;
                $id = $this->storeDocForUser($doctor_id, $doctor_name, $user_id, $pateint_name, $records_type, $related);
            }

            $record = $this->uploadMultiFile($id, $images);
            return response()->json(['success' => 'Added new records.']);
        } else {
            return response()->json(['error' => $validator->errors()->all()]);
        }
    }

    function storeDocForUser($doctor_id, $doctor_name, $user_id, $name, $records_type, $related) {

        $doc = new Document();
        $doc->user_id = $user_id;
        $doc->pateint_id = $related;
        $doc->pateint_name = $name;
        $doc->doctor_name = $doctor_name;
        $doc->doctor_id = $doctor_id;
        $doc->record_type = $records_type;
        $doc->save();

        $userregiter = DB::table('document')->select('docID')->orderBy('docID', 'DESC')->first();

        return $userregiter->docID;
    }

    function uploadMultiFile($id, $images) {

        foreach ($images as $image) {

            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/images/medicalrecords/'), $new_name);

            $doc = new FileModel();
            $doc->doc_id = $id;
            $doc->document = $new_name;
            $doc->save();
        }

        return 0;
    }

    function viewRecords(Request $request) {
        $doc_id = $request->doc_id;

        $document = DB::table('file')->where('doc_id', $doc_id)->paginate(5);
        // dd($doc_id);
        return view('pateint.viewrecords', compact('document'));
    }

    function updateRecords(Request $request) {
        $user_id = $request->user_id;
        $related = $request->related_to_who;
        $doctor_name = $request->doctor_name;
        $records_type = $request->records_type;
        $record_id = $request->medical_records_id;
        $doctor_id = $request->doctor_id;

        if ($related == 0) {
            $users = DB::table('users')->select('name')->where('id', $user_id)->first();
            $user_name = $users->name;
            $id = $this->updateDocForUser($doctor_id, $doctor_name, $user_name, $records_type, $record_id, $related);
        } else {
            $pateint_id = $related;
            $pateint = DB::table('pateint')->select('p_name')->where('patientID', $pateint_id)->first();
            $pateint_name = $pateint->p_name;
            $id = $this->updateDocForUser($doctor_id, $doctor_name, $pateint_name, $records_type, $record_id, $related);
        }


        $output = array(
            'success' => 'Record Updated successfully',
        );

        return response()->json($output, 201);
    }

    function updateDocForUser($doctor_id, $doctor_name, $pateint_name, $records_type, $record_id, $related) {
        $update = DB::table('document')
                ->where('docID', $record_id)
                ->update([
            'pateint_id' => $related,
            'pateint_name' => $pateint_name,
            'doctor_name' => $doctor_name,
            'doctor_id' => $doctor_id,
            'record_type' => $records_type
        ]);

        return $update;
    }

}
