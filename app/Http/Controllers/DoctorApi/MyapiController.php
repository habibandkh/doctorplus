<?php

namespace App\Http\Controllers\DoctorApi;

use App\Appointment;
use App\District;
use App\Events\NewComment;
use App\Http\Controllers\Controller;
use App\Message;
use App\PhoneVerification;
use App\User;
use App\Pateint;
use DB;
use App\Http\Controllers\NotificationController;
use Illuminate\Http\Request;
use CMText\TextClient;

class MyapiController extends Controller {

    public function changeLanguageOfApp(Request $request) {
        $id = $request->id;
        $usertype = $request->user_type;
        $lang = $request->lang_type;

        if ($usertype == 'doctor') {
            $update = DB::table('doctor')->where('doctorID', $id)->update(['lang' => $lang]);
            return response()->json('language changed', 200);
        } if ($usertype == 'user') {
            $update = DB::table('users')->where('id', $id)->update(['lang' => $lang]);
            return response()->json('language changed', 200);
        } else {

            return response()->json('invalid user type!', 400);
        }
    }

    public function loginUser(Request $request) {
        //max 9 digit
        //first digit must not be zero
        $number = $request->user_phone;
        $pass = $request->user_pass;
        // $mynumber = '93' . $number;
        
        $is_pateint = DB::table('users')->where('phone', '=', $number)->where('password', '=', $pass)->first();
        if ($is_pateint) {

            $pic = $is_pateint->pic;
            $url = asset('images/user/');
            $embed_pic = $url . $pic;
            return response()->json(['Patient_info' => $is_pateint, 'embed_pic' => $embed_pic], 200);
        } else {
            return response()->json('Incorrect Phone or Password', 400);
        }
    }

    public function getPhoneNumber(Request $request) {


        $var = $request->Phone_number;


        DB::table('phone_verification')->where('p_number', $var)->delete();
        $is_phone_d = DB::table('doctor')->where('doctor_phoneNo', $var)->first();
        $is_phone_u = DB::table('users')->select('phone')->where('phone', $var)->first();

        if ($is_phone_d != null || $is_phone_u != null) {
            $mess = 'the phone number ' . $var . ' already registerd to system!';
            return response()->json($mess, 400);
        } else {
            $code = mt_rand(100000, 999999);

            $veri = new PhoneVerification();
            $veri->p_number = $var;
            $veri->v_code = $code;
            $veri->save();

            $phonenumber = ltrim($var, '0');
            // dd($phonenumber);
            $phone = '0093' . $phonenumber;

            $mytext = 'This is your phone number verification code ' . $code;
            $textClient = new TextClient('24DA2FC6-CDBD-4F61-ACE7-953DA97333EE');
            $message = $textClient->SendMessage($mytext, 'DoctorPlus', [$phone]);


//            $basic = new \Nexmo\Client\Credentials\Basic('fe075704', 'TpOoj1FvPft7aDZW');
//            $client = new \Nexmo\Client($basic);
//
//            $message = $client->message()->send([
//                'to' => '93705931096',
//                'from' => 'Find Doctor',
//                'text' => 'This is your phone number verification code ' . $code
//            ]);

            return response()->json(['verification_code:' => $code]);
        }
    }
 public function check_verification_code(Request $request) {
        $phonenumber = $request->phone_number;
        $vcode = $request->v_code;
        $verfication = DB::table('phone_verification')->where('v_code', $vcode)->where('p_number', $phonenumber)->first();

        if ($verfication != null) {
            $code = $verfication->v_code;
            if ($code == $vcode) {
                return response()->json('verification code is valid', 200);
            } else {
                return response()->json('invalid code', 400);
            }
        } else {
            return response()->json('the code or mobile number is invalid ' . $request->phone_number, 400);
        }
    }
       public function userUpdatePassword(Request $req) {

        $phone = $req->phone_number;
        $pass = $req->new_password;

        $is_pateint = DB::table('users')->where('phone', $phone)->first();

        if ($is_pateint != null) {

            DB::table('users')->where('phone', $phone)->update(['password' => $pass,]);
            $mess = 'the password update successfully';
            return response()->json($mess, 201);
        } else {
             $mess = 'invalid phone number';
            return response()->json($mess, 400);
        }
    }
    public function userRgister(Request $request) {

        $mynumber = $request->mobile_number;
        $password = $request->password;

        $is_phone_d = DB::table('doctor')->where('doctor_phoneNo', $mynumber)->first();
        $is_phone_u = DB::table('users')->select('phone')->where('phone', $mynumber)->first();

        if ($is_phone_d != null || $is_phone_u != null) {
            $mess = 'the phone number ' . $mynumber . ' already registerd to system!';
            return response()->json($mess, 400);
        } else {

            $user = new User();
            $user->phone = $mynumber;
            $user->password = $password;
            $user->save();
            $userregiter = DB::table('users')->orderBy('id', 'DESC')->first();
            return response()->json($userregiter, 201);
        }
    }

    public function getAllSpaciality($lang) {
        if ($lang == 'en') {
            $specialitys = DB::table('speciality_class')->select('id AS specialityID', 'Name AS specialityName', 'photo')->get();
        }
        if ($lang == 'fa') {
            $specialitys = DB::table('speciality_class')->select('id AS specialityID', 'farsi_name AS specialityName', 'photo')->get();
        }
        if ($lang == 'pa') {
            $specialitys = DB::table('speciality_class')->select('id AS specialityID', 'pashto_name AS specialityName', 'photo')->get();
        }
        $url = asset('images/specialities/');
        return response()->json(['specialitys' => $specialitys, 'embed_pic' => $url], 200);
    }

    public function getAllSpacialityAndDoctors($lang) {

        if ($lang == 'en') {
            $specialitys = DB::table('speciality_class')->select('id AS specialityID', 'Name AS specialityName', 'photo')->get();
            $district = DB::table('city')->select('id', 'e_name As district_name')->get();
        }

        if ($lang == 'fa') {
            $specialitys = DB::table('speciality_class')->select('id AS specialityID', 'farsi_name AS specialityName', 'photo')->get();
            $district = DB::table('city')->select('id', 'f_name AS district_name')->get();
        }

        if ($lang == 'pa') {
            $specialitys = DB::table('speciality_class')->select('id AS specialityID', 'pashto_name AS specialityName', 'photo')->get();
            $district = DB::table('city')->select('id', 'p_name AS district_name')->get();
        }


        $doctors = DB::table('doctor')->select('doctorID', 'doctor_name', 'doctor_picture', 'Expert')->where('doctor.under_review', 2)->get();
        $allclinic = DB::table('address')->get();
        $speciality_pic = asset('images/specialities/');
        $doctor_pic = asset('images/doctor/');

        $Doctors_clinics = DB::table('doctor')->select('doctorID','title','city','doctor_name','doctor_picture','lang','under_review','professional_profile_status','experience','Expert','doctor_Short_Biography','doctor_last_name','doctor_gender','doctor_fee','doctor_regDate','doctor_status','district','doctor_clinicAddress')->get();

        foreach ($Doctors_clinics as $Doctor) {

            $address = DB::table('address')
                    ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                    ->join('visitingtime', 'address_doctor_view.address_id', '=', 'visitingtime.clinic_address_id')
                    ->where('visitingtime.doctorID', $Doctor->doctorID)
                    ->select('address_doctor_view.id', 'name', 'address', 'address_id', 'clinic_address_id')
                    ->groupby('visitingtime.clinic_address_id')
                    ->get();

            foreach ($address as $addr) {

                $from = DB::table('visitingtime')
                        ->select('visit_time')
                        ->where('doctorID', $Doctor->doctorID)
                        ->where('clinic_address_id', $addr->clinic_address_id)
                        ->min('visit_time');

                $to = DB::table('visitingtime')
                        ->select('visit_time')
                        ->where('doctorID', $Doctor->doctorID)
                        ->where('clinic_address_id', $addr->clinic_address_id)
                        ->max('visit_time');

                $addr->From = $from;
                $addr->To = $to;
            }
            $Doctor->clinic = $address;
        }

        return response()->json(['City' => $district, 'doctor_clinic' => $Doctors_clinics, 'all_clinic_hospital' => $allclinic, 'Doctors' => $doctors, 'Specialitys' => $specialitys, 'speciality_pic' => $speciality_pic, 'doctor_pic' => $doctor_pic], 200);
    }

    // public function getAllSpacialityAndDoctors($lang) {
    //     if ($lang == 'en') {
    //         $specialitys = DB::table('speciality_class')->select('id AS specialityID', 'Name AS specialityName', 'photo')->get();
    //     }
    //     if ($lang == 'fa') {
    //         $specialitys = DB::table('speciality_class')->select('id AS specialityID', 'farsi_name AS specialityName', 'photo')->get();
    //     }
    //     if ($lang == 'pa') {
    //         $specialitys = DB::table('speciality_class')->select('id AS specialityID', 'pashto_name AS specialityName', 'photo')->get();
    //     }
    //      $doctors = DB::table('doctor')->select('doctorID', 'doctor_name', 'doctor_picture', 'Expert')->get();
    //     $allclinic = DB::table('address')->get();
    //     $speciality_pic = asset('images/specialities/');
    //     $doctor_pic = asset('images/doctor/');
    //     return response()->json(['all_clinic_hospital' => $allclinic,'Doctors' => $doctors, 'Specialitys' => $specialitys, 'speciality_pic' => $speciality_pic, 'doctor_pic' => $doctor_pic], 200);
    // }
    // public function getSpacialityStarByDoctorId(Request $request) {
    //     $id = $request->doctor_id;
    //     $day = now()->format('l');
    //     $total = DB::table('feedback')->where('feedback_doctor_id', $id)->count();
    //     if ($total != 0) {
    //         $overall_experience_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('overall_experience', 1)->count();
    //         $overall_experience = ($overall_experience_1 / $total) * 100;
    //         $doctor_checkup_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('doctor_checkup', 1)->count();
    //         $doctor_checkup = ($doctor_checkup_1 / $total) * 100;
    //         $staff_behavior_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('staff_behavior', 1)->count();
    //         $staff_behavior = ($staff_behavior_1 / $total) * 100;
    //         $clinic_environment_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('clinic_environment', 1)->count();
    //         $clinic_environment = ($clinic_environment_1 / $total) * 100;
    //         $star = ($overall_experience_1 + $overall_experience_1 + $doctor_checkup_1 + $staff_behavior_1 + $clinic_environment_1) / $total;
    //         $overall_experiences = number_format($overall_experience, 1);
    //         $doctor_checkups = number_format($doctor_checkup, 1);
    //         $staff_behaviors = number_format($staff_behavior, 1);
    //         $clinic_environments = number_format($clinic_environment, 1);
    //         $stars = number_format($star, 1);
    //         $feedback_Collection = collect([['Stars' => $stars, 'overall_experience' => $overall_experiences, 'doctor_checkup' => $doctor_checkups, 'staff_behavior' => $staff_behaviors, 'clinic_environment' => $clinic_environments]]);
    //     } else {
    //         $feedback_Collection = collect([['Stars' => 5, 'overall_experience' => 100, 'doctor_checkup' => 100, 'staff_behavior' => 100, 'clinic_environment' => 100]]);
    //     }
    //     $doctorspeciality = DB::table('doctor')->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')->select('speciality.specialityName', 'speciality.specialityID')->where('doctor.doctorID', $id)->get();
    //     $available_Today = DB::table('visitingtime')->select('day')->where('doctorID', $id)->where('doctor_active', 1)->where('day', $day)->first();
    //     // dd($available_Today);
    //     $doctor_available_this_weak = DB::table('visitingtime')
    //             ->select('day')
    //             ->where('doctorID', $id)
    //             ->where('doctor_active', 1)
    //             ->groupBy('day')
    //             ->get();
    //     $doctor_not_available_this_weak = DB::table('visitingtime')
    //             ->select('day')->where('doctorID', $id)
    //             ->where('doctor_active', 0)
    //             ->groupBy('day')
    //             ->get();
    //     foreach ($doctor_available_this_weak as $doctor_available) {
    //         $is_this_weak[] = $doctor_available->day;
    //     }
    //     foreach ($doctor_not_available_this_weak as $doctor_not_available) {
    //         $is_this_weak_not[] = $doctor_not_available->day;
    //     }
    //     $doctor_is_available = collect([[
    //     'doctor_available_today' => $available_Today->day
    //     , 'doctor_available_this_weak' => $doctor_available_this_weak
    //     , 'doctor_not_available_this_weak' => $doctor_not_available_this_weak
    //     ]]);
    //     //   dd($is_this_weak_not);
    //     return response()->json([
    //                 'doctor_Specialitys' => $doctorspeciality
    //                 , 'doctor_feedback' => $feedback_Collection
    //                 , 'doctor_available' => $doctor_is_available
    //     ]);
    // }

    public function getAllDistrict($lang) {
        if ($lang == 'en') {
            $district = DB::table('city')->select('id', 'e_name As district_name')->get();
        } if ($lang == 'fa') {
            $district = DB::table('city')->select('id', 'f_name AS district_name')->get();
        }if ($lang == 'pa') {
            $district = DB::table('city')->select('id', 'p_name AS district_name')->get();
        }
        return response()->json($district, 200);
    }

    public function getAllDoctor() {
        $Doctors = DB::table('doctor')->where('doctor.under_review', 2)->paginate(20);
        return response()->json($Doctors, 200);
    }

    public function getDoctorInfo($id) {

        $year = now()->format('Y');
        $month = now()->format('m');
        $day = now()->format('l');
        $dayone = now()->addDays(1)->format('l');
        $daytwo = now()->addDays(2)->format('l');
        $daythree = now()->addDays(3)->format('l');

        DB::table('doctor_view')->insert(['doctor_id' => $id, 'year' => $year, 'month' => $month]);
        $appointments_count = DB::table('appointment')->where('doctor_id', $id)->count();
        $count_view_account = DB::table('doctor_view')->where('doctor_id', $id)->count();
        $total = DB::table('feedback')->where('feedback_doctor_id', $id)->count();
        if ($total != 0) {
            $overall_experience_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('overall_experience', 1)->count();
            $overall_experience = ($overall_experience_1 / $total) * 100;

            $doctor_checkup_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('doctor_checkup', 1)->count();
            $doctor_checkup = ($doctor_checkup_1 / $total) * 100;

            $staff_behavior_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('staff_behavior', 1)->count();
            $staff_behavior = ($staff_behavior_1 / $total) * 100;

            $clinic_environment_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('clinic_environment', 1)->count();
            $clinic_environment = ($clinic_environment_1 / $total) * 100;

            $star = ($overall_experience_1 + $overall_experience_1 + $doctor_checkup_1 + $staff_behavior_1 + $clinic_environment_1) / $total;

            $overall_experiences = number_format($overall_experience, 1);
            $doctor_checkups = number_format($doctor_checkup, 1);
            $staff_behaviors = number_format($staff_behavior, 1);
            $clinic_environments = number_format($clinic_environment, 1);
            $stars = number_format($star, 1);

            $feedback_Collection = collect([['view_account' => (string) $count_view_account, 'total_patient' => (string) $appointments_count, 'total_review' => (string) $total, 'Stars' => $stars, 'overall_experience' => $overall_experiences, 'doctor_checkup' => $doctor_checkups, 'staff_behavior' => $staff_behaviors, 'clinic_environment' => $clinic_environments]]);
        } else {
            $feedback_Collection = collect([['view_account' => (string) $count_view_account, 'total_patient' => (string) $appointments_count, 'total_review' => 1, 'Stars' => 5, 'overall_experience' => 100, 'doctor_checkup' => 100, 'staff_behavior' => 100, 'clinic_environment' => 100]]);
        }
        $doctorspeciality = DB::table('doctor')->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')->select('speciality.specialityName', 'speciality.specialityID')->where('doctor.doctorID', $id)->get();
        $doctor_experience = DB::table('doctor_experience')->where('doctor_FID', $id)->get();
        $doctor_education = DB::table('doctor_education')->where('D_FID', $id)->get();
        //  $doctor_services = DB::table('doctor_services')->where('doctor_s_id', $id)->get();
        $doctor_services = DB::table('doctor_services')
                ->join('doctor_services_view', 'doctor_services.services_id', '=', 'doctor_services_view.services_id')
                ->where('doctor_services_view.doctor_id', $id)
                ->select('service_name', 'doctor_services_view.id')
                ->groupBy('service_name')
                ->get();


        $address = DB::table('address')
                ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                ->join('visitingtime', 'address_doctor_view.address_id', '=', 'visitingtime.clinic_address_id')
                // ->where('address_doctor_view.doctor_id', $id)
                ->where('visitingtime.doctorID', $id)
                ->select('address_doctor_view.id', 'name', 'address', 'address_id', 'clinic_address_id')
                ->groupby('visitingtime.clinic_address_id')
                ->get();
        foreach ($address as $addr) {
            $from = DB::table('visitingtime')
                    ->select('visit_time')
                    ->where('doctorID', $id)
                    ->where('clinic_address_id', $addr->clinic_address_id)
                    ->min('visit_time');
            $to = DB::table('visitingtime')
                    ->select('visit_time')
                    ->where('doctorID', $id)
                    ->where('clinic_address_id', $addr->clinic_address_id)
                    ->max('visit_time');


            $addr->From = $from;
            $addr->To = $to;
        }

        // dd($address);

        $Doctors = DB::table('doctor')->where('doctorID', $id)->select('doctorID', 'title', 'city', 'doctor_name', 'doctor_picture', 'lang', 'under_review', 'professional_profile_status', 'experience', 'Expert', 'doctor_Short_Biography', 'doctor_last_name', 'doctor_gender', 'doctor_fee', 'doctor_regDate', 'doctor_status', 'district', 'doctor_clinicAddress')->get();


        $getday = DB::table('visitingtime')->select('day')->where('doctorID', $id)->where('doctor_active', 1)->where('day', $day)->first();

        $getday1 = DB::table('visitingtime')->select('day')->where('doctorID', $id)->where('doctor_active', 1)->where('day', $dayone)->first();

        $getday2 = DB::table('visitingtime')->select('day')->where('doctorID', $id)->where('doctor_active', 1)->where('day', $daytwo)->first();

        $getday3 = DB::table('visitingtime')->select('day')->where('doctorID', $id)->where('doctor_active', 1)->where('day', $daythree)->first();

        if ($getday != null && $getday->day == $day) {
            $available = 'Today ' . now()->format('l d F');
        } elseif ($getday1 != null && $getday1->day == $dayone) {
            $available = 'Tomorrow ' . now()->addDays(1)->format('l d F');
        } elseif ($getday2 != null && $getday2->day == $daytwo) {
            $available = now()->addDays(2)->format('l d F');
        } elseif ($getday3 != null && $getday3->day == $daythree) {
            $available = now()->addDays(3)->format('l d F');
        } else {
            $available = 'not Available for next 3 day';
        }
        return response()->json(['clinic_address' => $address, 'doctor_feedback' => $feedback_Collection, 'doctor_available' => $available, 'Doctors' => $Doctors, 'doctorspeciality' => $doctorspeciality, 'doctor_experience' => $doctor_experience, 'doctor_education' => $doctor_education, 'doctor_services' => $doctor_services]);
    }

    public function joinDoctorRequest(Request $request) {

        $mess = new Message();
        $mess->name = $request->name;
        $mess->phone = $request->phone;
        $mess->message = $request->message;
        $mess->save();

        event(new NewComment($request->name));
        return response()->json('done!', 200);
    }

    public function searchDoctorBySpacialityAndDistrict(Request $request) {
        $District_name = $request->District_name;
        $special_id = $request->special_id;
        $url = asset('images/doctor/');

        if ($District_name) {
            $Doctors = DB::table('doctor')
                    ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                    ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                    ->where('speciality.specialityID', $special_id)
                    ->where('doctor.city', $District_name)
                    ->where('doctor.under_review', 2)
                    ->select('doctorID','title','city','doctor_name','doctor_picture','lang','under_review','professional_profile_status','experience','Expert','doctor_Short_Biography','doctor_last_name','doctor_gender','doctor_fee','doctor_regDate','doctor_status','district','doctor_clinicAddress','id_s_d','F_specialityID','F_doctorID','type','specialityID','class_spa_id','specialityName','farsi_name','pashto_name','photo')
                    ->paginate(20);
        } else {
            $Doctors = DB::table('doctor')
                    ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                    ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                    ->where('speciality.specialityID', $special_id)
                    ->where('doctor.under_review', 2)
                    ->select('doctorID','title','city','doctor_name','doctor_picture','lang','under_review','professional_profile_status','experience','Expert','doctor_Short_Biography','doctor_last_name','doctor_gender','doctor_fee','doctor_regDate','doctor_status','district','doctor_clinicAddress','id_s_d','F_specialityID','F_doctorID','type','specialityID','class_spa_id','specialityName','farsi_name','pashto_name','photo')
                    ->paginate(20);
        }
        return response()->json(['doctor' => $Doctors, 'embed_pic' => $url], 200);
    }

    public function searchDoctorByNameAndDistrict(Request $request) {
        $District_name = $request->District_name;
        $doctor_id = $request->doctor_id;
        $url = asset('images/doctor/');
        if ($District_name) {
            $Doctors = DB::table('doctor')
                    ->where('doctor.doctorID', $doctor_id)
                    ->where('doctor.district', $District_name)
                    ->where('doctor.under_review', 2)
                    ->get();
        } 
        else {
            $Doctors = DB::table('doctor')->where('doctor.doctorID', $doctor_id)->where('doctor.under_review', 2)->get();
        }
        
        return response()->json(['doctor' => $Doctors, 'embed_pic' => $url], 200);
    }

    public function bookAppointment(Request $request) {

        $day = $request->dayselected; //Monday
        $date_selected = $request->date;
        $doctor_id = $request->doctor_id;
        $clinic_id = $request->clinic_id;

        $appointment = DB::table('appointment') //->select('appointment_time')
                // ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                ->where('appointment.doctor_id', $doctor_id)
                ->where('appointment_date', $date_selected)
                ->where('cancel_by_user', 0)
                ->get();

        $visit_time = DB::table('visitingtime') //->select('visit_time')
                ->where('doctorID', $doctor_id)
                ->where('clinic_address_id', $clinic_id)
                ->where('time_off_on', 1)
                ->where('doctor_active', 1)
                ->where('day', $day)
                ->get();

        return response()->json(['appointment' => $appointment, 'visit_time' => $visit_time], 200);
    }

    public function storeAppointment(Request $store) {

        $noti = new NotificationController();

        $id_user = $store->user_id;
        $id_doctor = $store->DocotsID;
        $familyMemberId = $store->family_member_id;
        $clinic_id = $store->clinic_id;
        $DateOfAppointment = $store->date_of_appointment;
        $TimeOfAppointment = $store->time_of_appointment;
        $Name = $store->name;
        $Age = $store->age;
        $Gender = $store->gender;
        $relation_ship = $store->relation_ship;
        $Token = $store->token;
        //get day from date
        $Timestamp = strtotime($DateOfAppointment);
        $The_day = date('l', $Timestamp);

        $user = DB::table('users')->select('id', 'name', 'gender')->where('id', $id_user)->first();

        $doctor = DB::table('doctor')->select('doctorID', 'title', 'doctor_name')->where('doctorID', $id_doctor)->first();


        if ($Token == 'self') {
            ///not: a user or patient can't make more then one appointment per day with a Doctor.
            $appointment_count = DB::table('appointment')
                    ->where('doctor_id', $id_doctor)
                    ->where('user_id', $id_user)
                    ->where('pateint_id', null)
                    ->where('appointment_date', $DateOfAppointment)
                    // ->where('Status', 0)
                    ->count();
            if ($appointment_count > 0) {

                return response()->json(['appointment_status' => 2, 'message' => "a user or patient can't make more then one appointment per day with a Doctor."], 200);
            } else {
                $appointment = new Appointment();
                $appointment->user_id = $id_user;
                $appointment->doctor_id = $id_doctor;
                $appointment->clinic_address_id = $clinic_id;
                $appointment->appointment_date = $DateOfAppointment;
                $appointment->appointment_time = $TimeOfAppointment;
                $appointment->feedback = '0';
                $appointment->Status = '0';
                $appointment->day = $The_day;
                $appointment->save();

                DB::table('users')->where('id', $id_user)->update(['name' => $Name, 'birthday' => $Age, 'gender' => $Gender]);
                // call for notification
                $doc_noti = $noti->appConfirmNotification_doctor($user, $doctor, $DateOfAppointment, $TimeOfAppointment);

                $res = $noti->appConfirmNotification_user($user, $doctor, $DateOfAppointment, $TimeOfAppointment);
                return response()->json(['appointment_status' => 1, 'message' => "Your Appointment  booked Successfully"], 200);
            }
        }
        if ($Token == 'other') {

            if ($familyMemberId == 0) {
                $pateint = new Pateint();
                $pateint->user_ID = $id_user;
                $pateint->p_name = $Name;
                $pateint->p_Relation = $relation_ship;
                $pateint->birthday = $Age;
                $pateint->gender = $Gender;
                if ($Gender == 1)
                    $pateint->p_photo = 'male.png';
                else
                    $pateint->p_photo = 'female.png';
                $pateint->save();


                $patient = DB::table('pateint')->orderBy('patientID', 'DESC')->first();
                $appointment = new Appointment();
                $appointment->user_id = $id_user;
                $appointment->doctor_id = $id_doctor;
                $appointment->pateint_id = $patient->patientID;
                $appointment->clinic_address_id = $clinic_id;
                $appointment->appointment_date = $DateOfAppointment;
                $appointment->appointment_time = $TimeOfAppointment;
                $appointment->feedback = '0';
                $appointment->Status = '0';
                $appointment->day = $The_day;
                $appointment->save();
            } else {
                $appointment_count_1 = DB::table('appointment')
                        ->where('doctor_id', $id_doctor)
                        ->where('user_id', $id_user)
                        ->where('pateint_id', $familyMemberId)
                        ->where('appointment_date', $DateOfAppointment)
                        // ->where('Status', 0)
                        ->count();
                if ($appointment_count_1 > 0) {
                    return response()->json(['appointment_status' => 2, 'message' => "a user or patient can't make more then one appointment per day with a Doctor."], 200);
                } else {
                    $appointment = new Appointment();
                    $appointment->user_id = $id_user;
                    $appointment->doctor_id = $id_doctor;
                    $appointment->pateint_id = $familyMemberId;
                    $appointment->clinic_address_id = $clinic_id;
                    $appointment->appointment_date = $DateOfAppointment;
                    $appointment->appointment_time = $TimeOfAppointment;
                    $appointment->feedback = '0';
                    $appointment->Status = '0';
                    $appointment->day = $The_day;
                    $appointment->save();

                    $pateint = DB::table('pateint')
                            ->where('patientID', $familyMemberId)
                            ->update([
                        'p_name' => $Name,
                        'p_Relation' => $relation_ship,
                        'birthday' => $Age,
                        'gender' => $Gender,
                    ]);
                    $patient = DB::table('pateint')->where('patientID', $familyMemberId)->first();
                }
            }
            $doc_noti = $noti->appConfirmNotification_doctor($user, $doctor, $DateOfAppointment, $TimeOfAppointment);

            $res = $noti->appConfirmNotification_family($user, $patient, $doctor, $DateOfAppointment, $TimeOfAppointment);

            return response()->json(['appointment_status' => 1, 'message' => "Your Appointment  booked Successfully"], 200);
        }
        //  event(new EndPool($id_doctor));
    }

    public function getAllBlogs() {
        $posts = DB::table('post')->orderBy('post_id', 'desc')->paginate(20);
        return response()->json($posts, 200);
    }

    public function viewBlogs(Request $request) {

        $postid = $request->postid;
        $post = DB::table('post')->where('post_id', $postid)->get();
        return response()->json($post);
    }
public function filterDoctorList(Request $request) {

        $year = now()->format('Y');
        $month = now()->format('m');

        $day = now()->format('l');
        $dayone = now()->addDays(1)->format('l');
        $daytwo = now()->addDays(2)->format('l');
        $daythree = now()->addDays(3)->format('l');

        $available = $request->available;
        $gender = $request->gender;
        $District_name = $request->District_name;
        $special_id = $request->special_id;
        $clinic_id = $request->clinic_id;

        if ($available == 'today') {

            $Doctors = $this->getTodayDoctorAvaiable($day, $gender, $District_name, $special_id);

            $array = $Doctors->items();
            if ($array == []) {
                return response()->json(['doctor_available_today' => 'false', 'doctor_available_next_3_days' => 'false', 'doctor' => 'no doctor found!'], 200);
            } else {
                foreach ($Doctors as $Doctor) {
                    $doctor_is_available = $this->get_Doctor_Availabilty($Doctor->doctorID);
                    $feedback_Collection = $this->get_Doctor_Feedback($Doctor->doctorID);

                    $doctorspeciality = DB::table('doctor')
                            ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                            ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                            ->select('speciality.specialityName')
                            ->where('doctor.doctorID', $Doctor->doctorID)
                            // ->where('speciality.specialityID', $special_id)
                            ->get();

                    $address = DB::table('address')
                            ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                            ->where('address_doctor_view.doctor_id', $Doctor->doctorID)
                            ->select('name', 'address')
                            ->get();

                    $Doctor->speciality = $doctorspeciality;
                    $Doctor->address = $address;
                    $Doctor->doctor_feedback = $feedback_Collection;
                    $Doctor->doctor_available = $doctor_is_available;
                }
                return response()->json(['doctor_available_today' => 'true', 'doctor_available_next_3_days' => 'false', 'doctor' => $Doctors], 200);
            }
        }
        if ($available == 'next_days') {

            $Doctors = $this->getNextdaysDoctorAvaiable($dayone, $daytwo, $daythree, $gender, $District_name, $special_id);
            $arrays = $Doctors->items();

            if ($arrays == []) {
                return response()->json(['doctor_available_today' => 'false', 'doctor_available_next_3_days' => 'false', 'doctor' => 'no doctor found!'], 200);
            } else {
                foreach ($Doctors as $Doctor) {
                    $doctor_is_available = $this->get_Doctor_Availabilty($Doctor->doctorID);
                    $feedback_Collection = $this->get_Doctor_Feedback($Doctor->doctorID);

                    $doctorspeciality = DB::table('doctor')
                            ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                            ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                            ->select('speciality.specialityName')
                            ->where('doctor.doctorID', $Doctor->doctorID)
                            // ->where('speciality.specialityID', $special_id)
                            ->get();

                    $address = DB::table('address')
                            ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                            ->where('address_doctor_view.doctor_id', $Doctor->doctorID)
                            ->select('name', 'address')
                            ->get();

                    $Doctor->speciality = $doctorspeciality;
                    $Doctor->address = $address;
                    $Doctor->doctor_feedback = $feedback_Collection;
                    $Doctor->doctor_available = $doctor_is_available;
                }
                return response()->json(['doctor_available_today' => 'false', 'doctor_available_next_3_days' => 'true', 'doctor' => $Doctors], 200);
            }
        } else {
            $Doctors = $this->getAlldaysDoctorAvaiable($gender, $District_name, $special_id, $clinic_id);
            $arrays = $Doctors->items();
            //dd($arrays);
            if ($arrays == []) {
                return response()->json(['doctor_available_today' => 'false', 'doctor_available_next_3_days' => 'false', 'doctor' => 'no doctor found!'], 200);
            } else {
                foreach ($Doctors as $Doctor) {
                    $doctor_is_available = $this->get_Doctor_Availabilty($Doctor->doctorID);
                    $feedback_Collection = $this->get_Doctor_Feedback($Doctor->doctorID);
                    //  dd($doctor_is_available);
                    $doctorspeciality = DB::table('doctor')
                            ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                            ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                            ->select('speciality.specialityName')
                            ->where('doctor.doctorID', $Doctor->doctorID)
                            //   ->where('speciality.specialityID', $special_id)
                            ->get();

                    $address = DB::table('address')
                            ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                            ->where('address_doctor_view.doctor_id', $Doctor->doctorID)
                            ->select('name', 'address')
                            ->get();

                    $Doctor->speciality = $doctorspeciality;
                    $Doctor->address = $address;
                    $Doctor->doctor_feedback = $feedback_Collection;
                    $Doctor->doctor_available = $doctor_is_available;
                }
                return response()->json(['doctor_available_today' => 'true', 'doctor_available_next_3_days' => 'true', 'doctor' => $Doctors], 200);
            }
        }
    }

    function getTodayDoctorAvaiable($day, $gender, $District_name, $special_id) {
        $specialitys_list = DB::table('speciality_class')
                ->join('speciality', 'speciality_class.id', '=', 'speciality.class_spa_id')
                ->where('speciality.class_spa_id', $special_id)
                ->get();

        $query = DB::table('doctor')
                ->join('visitingtime', 'doctor.doctorID', '=', 'visitingtime.doctorID')
                ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                ->select('doctor.doctorID'
                        , 'doctor.title'
                        , 'doctor.doctor_name'
                        , 'doctor.doctor_email'
                        , 'doctor.doctor_phoneNo'
                        , 'doctor.experience'
                        , 'doctor.doctor_fee'
                        , 'doctor.doctor_picture')
                ->where('visitingtime.doctor_active', 1)
                ->orderBy('doctor.doctor_fee', 'asc')
                ->orderBy('doctor.experience', 'DESC')
                ->where('visitingtime.day', $day);

        foreach ($specialitys_list as $spc) {
            $spa_id[] = $spc->specialityID;
        }

        $query->whereIn('speciality.specialityID', $spa_id);


        if ($gender != 'both') {
            $query->where('doctor.doctor_gender', $gender);
        }

        if ($District_name != null) {
            $query->where('doctor.city', $District_name);
        }

        $query->groupBy('doctor.doctorID');


        $Doctors = $query->paginate(20);
        return $Doctors;
    }

    function getNextdaysDoctorAvaiable($dayone, $daytwo, $daythree, $gender, $District_name, $special_id) {
        $specialitys_list = DB::table('speciality_class')
                ->join('speciality', 'speciality_class.id', '=', 'speciality.class_spa_id')
                ->where('speciality.class_spa_id', $special_id)
                ->get();

        $query = DB::table('doctor')
                ->join('visitingtime', 'doctor.doctorID', '=', 'visitingtime.doctorID')
                ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                ->select('doctor.doctorID'
                        , 'doctor.title'
                        , 'doctor.doctor_name'
                        , 'doctor.doctor_email'
                        , 'doctor.doctor_phoneNo'
                        , 'doctor.experience'
                        , 'doctor.doctor_fee'
                        , 'doctor.doctor_picture')
                ->where('visitingtime.doctor_active', 1)
                ->orderBy('doctor.doctor_fee', 'asc')
                ->orderBy('doctor.experience', 'DESC')
                ->whereIn('visitingtime.day', [$dayone, $daytwo, $daythree]);

        foreach ($specialitys_list as $spc) {
            $spa_id[] = $spc->specialityID;
        }

        $query->whereIn('speciality.specialityID', $spa_id);

        if ($gender != 'both') {
            $query->where('doctor.doctor_gender', $gender);
        }

        if ($District_name != null) {
            $query->where('doctor.city', $District_name);
        }
        $query->groupBy('doctor.doctorID');
        $Doctors = $query->paginate(20);
        return $Doctors;
    }

    function getAlldaysDoctorAvaiable($gender, $District_name, $special_id, $clinic_id) {

        if ($special_id != 'null') {

            $specialitys_list = DB::table('speciality_class')
                    ->join('speciality', 'speciality_class.id', '=', 'speciality.class_spa_id')
                    ->where('speciality.class_spa_id', $special_id)
                    ->get();
            // dd($specialitys_list);
            $query = DB::table('doctor')
                    ->join('visitingtime', 'doctor.doctorID', '=', 'visitingtime.doctorID')
                    ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                    ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                    ->select('doctor.doctorID'
                    , 'doctor.title'
                    , 'doctor.doctor_name'
                    , 'doctor.doctor_email'
                    , 'doctor.doctor_phoneNo'
                    , 'doctor.experience'
                    , 'doctor.doctor_gender'
                    , 'doctor.city'
                    , 'doctor.doctor_fee'
                    , 'doctor.doctor_picture')
                  ->orderBy('doctor.doctor_fee', 'asc')
                    ->orderBy('doctor.experience', 'DESC');

            foreach ($specialitys_list as $spc) {
                $spa_id[] = $spc->specialityID;
            }

            $query->whereIn('speciality.specialityID', $spa_id);
            if ($gender != 'both') {
                $query->where('doctor.doctor_gender', $gender);
            }
            if ($District_name != null) {
                $query->where('doctor.city', $District_name);
            }
            $query->groupBy('doctor.doctorID');
            $Doctors = $query->paginate(20);
            // dd($Doctors);
            return $Doctors;
        } else {

            $query = DB::table('doctor')
                    ->join('visitingtime', 'doctor.doctorID', '=', 'visitingtime.doctorID')
                    ->join('address_doctor_view', 'doctor.doctorID', '=', 'address_doctor_view.doctor_id')
                    ->select('doctor.doctorID'
                            , 'doctor.title'
                            , 'doctor.doctor_name'
                            , 'doctor.doctor_email'
                            , 'doctor.doctor_phoneNo'
                            , 'doctor.experience'
                            , 'doctor.doctor_gender'
                            , 'doctor.city'
                            , 'doctor.doctor_fee'
                            , 'doctor.doctor_picture')
                    ->where('address_doctor_view.address_id', $clinic_id)
                    ->orderBy('doctor.doctor_fee', 'asc')
                    ->orderBy('doctor.experience', 'DESC');
            if ($gender != 'both') {
                $query->where('doctor.doctor_gender', $gender);
            }
            if ($District_name != null) {
                $query->where('doctor.city', $District_name);
            }
            $query->groupBy('doctor.doctorID');

            $Doctors = $query->paginate(20);

            return $Doctors;
        }
    }

    public function get_Doctor_Feedback($id) {

        $total = DB::table('feedback')->where('feedback_doctor_id', $id)->count();
        if ($total != 0) {
            $overall_experience_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('overall_experience', 1)->count();
            $overall_experience = ($overall_experience_1 / $total) * 100;

            $doctor_checkup_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('doctor_checkup', 1)->count();
            $doctor_checkup = ($doctor_checkup_1 / $total) * 100;

            $staff_behavior_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('staff_behavior', 1)->count();
            $staff_behavior = ($staff_behavior_1 / $total) * 100;

            $clinic_environment_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('clinic_environment', 1)->count();
            $clinic_environment = ($clinic_environment_1 / $total) * 100;

            $star = ($overall_experience_1 + $overall_experience_1 + $doctor_checkup_1 + $staff_behavior_1 + $clinic_environment_1) / $total;

            $overall_experiences = number_format($overall_experience, 1);
            $doctor_checkups = number_format($doctor_checkup, 1);
            $staff_behaviors = number_format($staff_behavior, 1);
            $clinic_environments = number_format($clinic_environment, 1);
            $stars = number_format($star, 1);

            $feedback_Collection = collect([['Stars' => $stars, 'overall_experience' => $overall_experiences, 'doctor_checkup' => $doctor_checkups, 'staff_behavior' => $staff_behaviors, 'clinic_environment' => $clinic_environments]]);
        } else {
            $feedback_Collection = collect([['Stars' => 5, 'overall_experience' => 100, 'doctor_checkup' => 100, 'staff_behavior' => 100, 'clinic_environment' => 100]]);
        }
        return $feedback_Collection;
    }

    public function get_Doctor_Availabilty($id) {
        $day = now()->format('l');
        $available_Today = DB::table('visitingtime')->select('day')->where('doctorID', $id)->where('doctor_active', 1)->where('day', $day)->first();

        $doctor_available_this_weak = DB::table('visitingtime')
                ->select('day')
                ->where('doctorID', $id)
                ->where('doctor_active', 1)
                ->groupBy('day')
                ->get();

        $doctor_not_available_this_weak = DB::table('visitingtime')
                ->select('day')->where('doctorID', $id)
                ->where('doctor_active', 0)
                ->groupBy('day')
                ->get();

        foreach ($doctor_available_this_weak as $doctor_available) {
            $is_this_weak[] = $doctor_available->day;
        }

        foreach ($doctor_not_available_this_weak as $doctor_not_available) {
            $is_this_weak_not[] = $doctor_not_available->day;
        }

       ////very importent
        if ($available_Today == null) {
            $availableToday = "not available today";
        } else {
            $availableToday = $available_Today->day;
        }
        $doctor_is_available = collect([[
        'doctor_available_today' => $availableToday
        , 'doctor_available_this_weak' => $doctor_available_this_weak
        , 'doctor_not_available_this_weak' => $doctor_not_available_this_weak
        ]]);
        return $doctor_is_available;
    }

}
