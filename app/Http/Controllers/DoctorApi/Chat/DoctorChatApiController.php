<?php

namespace App\Http\Controllers\DoctorApi\Chat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Chat;
use File;
use Pusher\Pusher;
use App\Http\Controllers\NotificationController;

class DoctorChatApiController extends Controller {

    public function getMessageFromDoctor(Request $request) {
        $my_id = $request->doctor_id;
        $user_id = $request->user_id;
        $pateintid = $request->pateint_id;

        if ($pateintid == 0) {
            $messages = \App\Chat::where(function ($query) use ($user_id, $my_id) {
                        $query->where('from', $user_id)->where('to', $my_id)->where('pateint_id', 0);
                    })->oRwhere(function ($query) use ($user_id, $my_id) {
                        $query->where('from', $my_id)->where('to', $user_id)->where('pateint_id', 0);
                    })->orderBy('chatID', 'DESC')->get();
        }
        if ($pateintid != 0) {
            $messages = \App\Chat::where(function ($query) use ($user_id, $my_id, $pateintid) {
                        $query->where('from', $user_id)->where('to', $my_id)->where('pateint_id', $pateintid);
                    })->oRwhere(function ($query) use ($user_id, $my_id, $pateintid) {
                        $query->where('from', $my_id)->where('to', $user_id)->where('pateint_id', $pateintid);
                    })->orderBy('chatID', 'DESC')->get();
        }
        return response()->json($messages, 201);
    }

    public function sendMessage(Request $request) {
        $date = now(); //->format('yyyy-m-d h:i:s');
        $to = $request->user_id;
        $from = $request->doctor_id;
        $pateint_id = $request->pateint_id;
        $message = $request->message;

        $data = new Chat();
        $data->from = $from;
        $data->doctor_id = $from;
        $data->pateint_id = $pateint_id;
        $data->to = $to;
        $data->user_id = $to;
        $data->message = $message;
        $data->type = 'text';
        $data->who_send = 'doctor';
        $data->is_read = 0;
        $data->date = $date;
        $data->save();



        $chat = DB::table('chat')->select('*')->where('user_id', $to)->where('doctor_id', $from)->where('type', 'text')->orderBy('chatID', 'DESC')->first();
        // pusher
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => true
        );

        $pusher = new Pusher(
                env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), $options
        );

        $data = ['date' => $date, 'who_send' => 'doctor', 'chatID' => $chat->chatID, 'pateint_id' => $pateint_id, 'from' => $from, 'to' => $to, 'doctor_id' => $from, 'user_id' => $to, 'message' => $message, 'type' => 'text', 'is_read' => 0];
        $pusher->trigger('my-channel', 'my-event', $data);
        $noti = new NotificationController();
        $res = $noti->sendMessageToUser($from, $to, $message, 'm',$pateint_id);
        return response()->json($chat, 201);
    }

    public function sendFileToDoctor(Request $request) {
        $date = now(); //->format('yy-m-d h:i:s');
        $from = $request->doctor_id;
        $to = $request->user_id;
        $pateint_id = $request->pateint_id;
        $message = $request->caption;

        $image = $request->file('file');

        $new_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('chat/file'), $new_name);

        $store = new Chat();
        $store->from = $from;
        $store->to = $to;
        $store->pateint_id = $pateint_id;
        $store->doctor_id = $from;
        $store->user_id = $to;
        $store->message = $message;
        $store->file = $new_name;
        $store->type = 'file';
        $store->who_send = 'doctor';
        $store->is_read = 0;
        $store->date = $date;
        $store->save();
        $chat = DB::table('chat')->where('user_id', $to)->where('doctor_id', $from)->where('type', 'file')->orderBy('chatID', 'DESC')->first();


        // pusher
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => true
        );

        $pusher = new Pusher(
                env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), $options
        );

        // $file= public_path('chat/file/'.$new_name);
        $file = $new_name;
        $data = ['date' => $date, 'who_send' => 'doctor', 'chatID' => $chat->chatID, 'pateint_id' => $pateint_id, 'from' => $from, 'to' => $to, 'doctor_id' => $from, 'user_id' => $to, 'message' => $message, 'file' => $file, 'type' => 'file', 'is_read' => 0]; // sending from and to user id when pressed enter

        $pusher->trigger('my-channel', 'my-event', $data);

        $noti = new NotificationController();
        $mess = 'this is a file';
        $res = $noti->sendMessageToUser($from, $to, $mess, 'f',$pateint_id);
        return response()->json($chat, 201);
    }

    public function sendVoiceToDoctor(Request $request) {
        
        $date = now(); //->format('yy-m-d h:i:s');
        
        $to = $request->user_id;
        $from = $request->doctor_id;
        $pateint_id = $request->pateint_id;
        // $message = $request->caption;

        $voice = $request->file('voice');

        $new_name = rand() . '.' . $voice->getClientOriginalExtension();
        $voice->move(public_path('chat/voice'), $new_name);

        $store = new Chat();
        $store->doctor_id = $from;
        $store->user_id = $to;
        $store->pateint_id = $pateint_id;
        $store->from = $from;
        $store->to = $to;
        $store->voice = $new_name;
        $store->type = 'voice';
        $store->who_send = 'doctor';
        $store->is_read = 0;
        $store->date = $date;
        $store->save();

        $chat = DB::table('chat')->where('user_id', $to)->where('doctor_id', $from)->where('type', 'voice')->orderBy('chatID', 'DESC')->first();

        // pusher
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => true
        );

        $pusher = new Pusher(
                env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), $options
        );

        //   $file= public_path('chat/file/'.$new_name);
        $file = $new_name;
        $data = ['date' => $date, 'who_send' => 'doctor', 'chatID' => $chat->chatID, 'pateint_id' => $pateint_id, 'from' => $from, 'to' => $to, 'doctor_id' => $from, 'user_id' => $to, 'voice' => $file, 'type' => 'voice', 'is_read' => 0]; // sending from and to user id when pressed enter
        $pusher->trigger('my-channel', 'my-event', $data);
        $noti = new NotificationController();
        $mess = 'this is a voice';
        $res = $noti->sendMessageToUser($from, $to, $mess, 'v',$pateint_id);
        return response()->json($chat, 201);
    }

}
