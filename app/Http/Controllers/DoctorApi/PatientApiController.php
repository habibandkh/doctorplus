<?php

namespace App\Http\Controllers\DoctorApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\NewComment;
use App\Message;
use DB;
use Carbon\Carbon;
use Image;
use File;
use App\User;
use App\Speciality;
use App\District;
use App\Doctor;
use App\Document;
use App\Pateint;
use App\FileModel;
use App\FavoriteDoctor;
use App\Rating;
use App\UserNotification;
use App\PhoneVerification;
use CMText\TextClient;
use App\Http\Controllers\NotificationController;

class PatientApiController extends Controller {

    // function getNotificationList($user_id) {
    //     $noti = DB::table('user_notification')
    //             ->where('user_id', $user_id)
    //             ->select('id', 'user_id', 'text', 'status', 'date')
    //             ->orderBy('id', 'DESC')
    //             ->get();
    //     return response()->json($noti, 201);
    // }
  function getNotificationList($user_id) {

        $count = DB::table('user_notification')
                ->where('user_id', $user_id)
                ->select('id', 'doctor_id', 'text', 'status', 'date')
                ->orderBy('id', 'DESC')
                ->count();

        $notification = DB::table('user_notification')->where('user_id', $user_id)->select('id', 'doctor_id', 'user_id', 'patient_id', 'text', 'status', 'date', 'is_chat')->orderBy('id', 'DESC')->paginate(20);
        foreach ($notification as $noti) {
          
                $doctor = DB::table('doctor')->where('doctorID', $noti->doctor_id)->first();
                $noti->name = $doctor->doctor_name;
                $noti->title = $doctor->title;
            
        }
        return response()->json($notification, 201);
    }

    function getuUreadNotificationList($user_id) {
        $noti = DB::table('user_notification')
                ->where('user_id', $user_id)
                ->where('status', 0)
                 ->select('id', 'doctor_id','user_id','patient_id', 'text', 'status', 'date')
                ->orderBy('id', 'DESC')
                ->get();
        return response()->json($noti, 201);
    }

    function readNotification($noti_id) {
        $update = DB::table('user_notification')->where('id', $noti_id)->update(['status' => 1]);
        return response()->json('done!', 201);
    }

    public function pendingFeedback(Request $request) {

        $userid = $request->user_id;
        $now = Carbon::now();
        //dd($now);
       // $future = $now->addDays(0);
        $today = $now->toDateString();
//dd($tomorrow);
        $feedback = DB::table('appointment')
                ->join('doctor', 'doctor.doctorID', '=', 'appointment.doctor_id')
                ->where('user_id', $userid)
                ->where('Status', 0)
                ->where('cancel_by_user', 0)
                ->where('appointment_date', '<', $today)
                ->where('feedback', 0)
                ->get();
        return response()->json($feedback, 201);
    }

    public function storeFeedback(Request $request) {

        $ap_id = $request->appointment_id;
        $doctorid = $request->doctor_id;
        $userid = $request->user_id;
        $patientid = $request->patient_id;
        $overall_experience = $request->overall_experience;
        $doctor_checkup = $request->doctor_checkup;
        $staff_behavior = $request->staff_behavior;
        $clinic_environment = $request->clinic_environment;
        $comment = $request->comment;

        $user = new Rating();
        $user->feedback_doctor_id = $doctorid;
        $user->feedback_user_id = $userid;
        $user->feedback_patient_id = $patientid;
        $user->overall_experience = $overall_experience;
        $user->doctor_checkup = $doctor_checkup;
        $user->staff_behavior = $staff_behavior;
        $user->clinic_environment = $clinic_environment;
        $user->comment = $comment;
        $user->save();
        DB::table('appointment')->where('appID', $ap_id)->update(['feedback' => 1]);
        return response()->json('feedback store successfuly!', 201);
    }

    // function forgetPassword(Request $req) {
    //     $phone = $req->phone_number;
    //     return response()->json(['we send the password to your phone number'], 201);
    // }
 function forgetPassword(Request $req) {

        $phonenumber = $req->phone_number;

        $is_phone_u = DB::table('users')->select('phone', 'password')->where('phone', $phonenumber)->first();

        if ($is_phone_u != null) {


            $code = mt_rand(100000, 999999);

            $veri = new PhoneVerification();
            $veri->p_number = $phonenumber;
            $veri->v_code = $code;
            $veri->save();

            $phonenumber1 = ltrim($phonenumber, '0');
            $phone = '0093' . $phonenumber1;
            $mytext = 'phd ' . $code;

              $mytext = 'This is your phone number verification code ' . $code;
            $textClient = new TextClient('24DA2FC6-CDBD-4F61-ACE7-953DA97333EE');
            $message = $textClient->SendMessage($mytext, 'DoctorPlus', [$phone]);

            return response()->json(['we send a token to your mobile number'], 201);
            
        } else {
            $mess = 'the phone number ' . $phonenumber . ' Not registerd to system!';
            return response()->json($mess, 400);
        }
    }
    // public function patientProfile(Request $req) {
    //     $userid = $req->user_id;
    //     $patient_profile = DB::table('users')->where('id', $userid)->first();
    //     return response()->json($patient_profile, 201);
    // }

  public function patientProfile(Request $req) {
        $userid = $req->user_id;
        
        $pateint = DB::table('pateint')->where('user_ID', $userid)->get();
        $patient_profile = DB::table('users')->where('id', $userid)->first();
        
        return response()->json(['patient_profile'=>$patient_profile,'family_member'=>$pateint], 201);
    }


    public function changepassword($new_pass, $old_pass, $userid) {

        $is_pateint = DB::table('users')->where('id', $userid)->where('password', $old_pass)->first();

        if ($is_pateint != null) {
          
            DB::table('users')->where('id', $userid)->update(['password' => $new_pass,]);
            return 'yes';
        } else {
            return 'no';
        }
    }

    public function updatePatient(Request $request) {
        $userid = $request->user_id;
        //  dd($userid);
        $old_pass = $request->old_password;
        $new_pass = $request->new_password;
        if ($old_pass != '') {
            $check = $this->changepassword($new_pass, $old_pass, $userid);

            if ($check == 'no') {
                return response()->json('the old password is incorrect!', 400);
            }
        }
        if ($request->hasFile('photo')) {

            $image = $request->file('photo');
            $image_name = 'pateint-' . time() . '.' . $image->getClientOriginalExtension();

            $image->move(public_path('/images/user/'), $image_name);

            $update = DB::table('users')
                    ->where('id', $userid)
                    ->update([
                'name' => $request->name,
                'birthday' => $request->birthday,
                'gender' => $request->gender,
                'pic' => $image_name,
            ]);
        } else {

            $update = DB::table('users')
                    ->where('id', $userid)
                    ->update([
                'name' => $request->name,
                'birthday' => $request->birthday,
                'gender' => $request->gender,
            ]);
        }
        return response()->json('user account updated successfully', 201);
    }

    public function myDoctor(Request $request) {
        $userid = $request->user_id;

        $mydoctors = DB::table('favorite_doctor')
                ->join('doctor', 'favorite_doctor.doctor_id', '=', 'doctor.doctorID')
                ->select('doctor.doctorID'
                        , 'doctor.title'
                        , 'doctor.doctor_name'
                        , 'doctor.doctor_email'
                        , 'doctor.doctor_phoneNo'
                        , 'doctor.doctor_picture'
                        , 'doctor.doctor_clinicAddress'
                        , 'doctor.Expert')
                ->where('favorite_doctor.user_id', $userid)
                ->groupBy('doctor.doctorID')
                ->paginate(20);

        return response()->json($mydoctors, 201);
    }

    function bookmarkDoctor(Request $request) {
        $userid = $request->user_id;
        $doctorid = $request->doctor_id;
        $check = DB::table('favorite_doctor')->where('doctor_id', $doctorid)->where('user_id', $userid)->first();
        if ($check == null) {
            $pateint = new FavoriteDoctor();
            $pateint->user_id = $userid;
            $pateint->doctor_id = $doctorid;
            $pateint->save();
            return response()->json('doctor bookmark successfully', 201);
        } else {
            return response()->json('the doctor is already bookmarked', 400);
        }
    }

    function cancelBookmarkDoctor(Request $request) {
        $userid = $request->user_id;
        $doctorid = $request->doctor_id;
        $check = DB::table('favorite_doctor')->where('doctor_id', $doctorid)->where('user_id', $userid)->first();
        if ($check == null) {
            return response()->json(['error' => 'the id of doctor is not bookmarked'], 400);
        } else {
            DB::table('favorite_doctor')->where('doctor_id', $doctorid)->where('user_id', $userid)->delete();
            return response()->json(['success' => 'doctor un-bookmark successfully'], 201);
        }
    }

    public function addFamilyMember(Request $request) {
        
        $userid = $request->user_id;
        
        if ($request->hasFile('photo')) {
            
            $this->validate($request, [
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            
            $image = $request->file('photo');
            
            $image_name = 'pateint-' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/images/pateint/'), $image_name);
            $pateint = new Pateint();
            $pateint->p_photo = $image_name;
            $pateint->user_ID = $userid;
            $pateint->p_name = $request->name;
            $pateint->p_Relation = $request->relation;
            $pateint->p_phoneNo = $request->phone;
            $pateint->birthday = $request->birthday;
            $pateint->gender = $request->gender;
            $pateint->save();
            
        } else {

            $pateint = new Pateint();
            $pateint->user_ID = $userid;
            $pateint->p_name = $request->name;
            if ($request->gender == 1)
                $pateint->p_photo = 'male.png';
            else
                $pateint->p_photo = 'female.png';
            $pateint->p_Relation = $request->relation;
            $pateint->p_phoneNo = $request->phone;
            $pateint->birthday = $request->birthday;
            $pateint->gender = $request->gender;
            $pateint->save();
        }
        return response()->json($pateint, 201);
    }

    public function updateFamilyMember(Request $request) {
        $patient_ID = $request->family_member_id;
        if ($request->hasFile('photo')) {
            $this->validate($request, [
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            $image = $request->file('photo');
            $image_name = 'pateint-' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/images/pateint/'), $image_name);


            $pateint = DB::table('pateint')
                    ->where('patientID', $patient_ID)
                    ->update([
                'p_photo' => $image_name,
                'p_name' => $request->name,
                'p_Relation' => $request->relation,
                'p_phoneNo' => $request->phone,
                'birthday' => $request->birthday,
                'gender' => $request->gender,
            ]);
        } else {

            $pateint = DB::table('pateint')
                    ->where('patientID', $patient_ID)
                    ->update([
                'p_name' => $request->name,
                'p_Relation' => $request->relation,
                'p_phoneNo' => $request->phone,
                'birthday' => $request->birthday,
                'gender' => $request->gender,
            ]);
        }

        return response()->json($pateint, 201);
    }

    public function viewAllFamilyMember(Request $request) {
        $userid = $request->user_id;
        $pateint = DB::table('pateint')->where('user_ID', $userid)->get();
        return response()->json(['family_member'=>$pateint], 200);
    }
    
        public function view_All_Family_who_get_appointment($userid) {
        
        $pateint = DB::table('pateint')
                ->join('appointment','pateint.patientID', '=', 'appointment.pateint_id')
                ->select('patientID','pateint.user_ID','p_name','p_photo','p_Relation','p_phoneNo','gender','birthday')
                ->where('pateint.user_ID',$userid)
                ->groupBy('patientID')
                ->get();
        
        return response()->json($pateint, 200);
    } 

//    public function allAppointmentList(Request $request) {
//
//        $now = Carbon::now();
//        $today = $now->toDateString();
//        $userid = $request->user_id;
//        // dd($today);
//        //list of current appointment
//        $current_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.user_id', $userid)->where('appointment.pateint_id', null)->select('doctorID', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 0)
//                ->whereDate('appointment.appointment_date', '=', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        //list of upcoming appointment
//        $upcoming_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.user_id', $userid)->where('appointment.pateint_id', null)->select('doctorID', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
////                ->where('appointment.cancel_by_doctor', 0)
////                ->where('appointment.cancel_by_user', 0)
//                ->where('appointment.Status', 0)
//                ->whereDate('appointment.appointment_date', '>', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        //list of cancel appointment
//
//        $cancel_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.user_id', $userid)->where('appointment.pateint_id', null)->select('doctorID', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 1)
//                ->whereDate('appointment.appointment_date', '>', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        //list of expired appointment
//        $expired_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.user_id', $userid)->where('appointment.pateint_id', null)->select('doctorID', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 1)
//                ->whereDate('appointment.appointment_date', '<', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//
//
//        $family_current_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.user_id', $userid)->select('doctorID', 'appID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 0)
//                ->whereDate('appointment.appointment_date', '=', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        $family_upcoming_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.user_id', $userid)->select('doctorID', 'appID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 0)
//                ->whereDate('appointment.appointment_date', '>', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        $family_cancel_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.user_id', $userid)->select('doctorID', 'appID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 1)
//                ->whereDate('appointment.appointment_date', '>', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        $family_expired_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.user_id', $userid)->select('doctorID', 'appID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 1)
//                ->whereDate('appointment.appointment_date', '<', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        $user_app_collection = collect(['current_app' => $current_app, 'upcoming_app' => $upcoming_app, 'cancel_app' => $cancel_app, 'expired_app' => $expired_app]);
//        $family_app_collection = collect(['family_current_app' => $family_current_app, 'family_upcoming_app' => $family_upcoming_app, 'family_cancel_app' => $family_cancel_app, 'family_expired_app' => $family_expired_app]);
//
//        return response()->json(['user_appointment' => $user_app_collection, 'family_appointment' => $family_app_collection], 200);
//    }
//
//    public function myAppointmentList(Request $request) {
//        $now = Carbon::now();
//        $today = $now->toDateString();
//        $userid = $request->user_id;
//        $current_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.user_id', $userid)->where('appointment.pateint_id', null)->select('doctorID', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 0)
//                ->whereDate('appointment.appointment_date', '=', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        //list of upcoming appointment
//        $upcoming_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.user_id', $userid)->where('appointment.pateint_id', null)->select('doctorID', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
////                ->where('appointment.cancel_by_doctor', 0)
////                ->where('appointment.cancel_by_user', 0)
//                ->where('appointment.Status', 0)
//                ->whereDate('appointment.appointment_date', '>', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        //list of cancel appointment
//
//        $cancel_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.user_id', $userid)->where('appointment.pateint_id', null)->select('doctorID', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 1)
//                ->whereDate('appointment.appointment_date', '>', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        //list of expired appointment
//        $expired_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.user_id', $userid)->where('appointment.pateint_id', null)->select('doctorID', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 1)
//                ->whereDate('appointment.appointment_date', '<', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        $user_app_collection = collect(['current_app' => $current_app, 'upcoming_app' => $upcoming_app, 'cancel_app' => $cancel_app, 'expired_app' => $expired_app]);
//
//        return response()->json($user_app_collection, 200);
//    }
//
//    public function familyAppointmentList(Request $request) {
//        $now = Carbon::now();
//        $today = $now->toDateString();
//        $patientid = $request->family_member_id;
//
//        $family_current_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.pateint_id', $patientid)->select('doctorID', 'appID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 0)
//                ->whereDate('appointment.appointment_date', '=', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        $family_upcoming_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.pateint_id', $patientid)->select('doctorID', 'appID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 0)
//                ->whereDate('appointment.appointment_date', '>', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        $family_cancel_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.pateint_id', $patientid)->select('doctorID', 'appID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 1)
//                ->whereDate('appointment.appointment_date', '>', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//
//        $family_expired_app = DB::table('appointment')->join('users', 'appointment.user_id', '=', 'users.id')->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')->join('address', 'appointment.clinic_address_id', '=', 'address.id')->where('appointment.pateint_id', $patientid)->select('doctorID', 'appID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
//                ->where('appointment.Status', 1)
//                ->whereDate('appointment.appointment_date', '<', $today)
//                ->orderBy('appID', 'desc')
//                ->paginate(20);
//        $family_app_collection = collect(['family_current_app' => $family_current_app, 'family_upcoming_app' => $family_upcoming_app, 'family_cancel_app' => $family_cancel_app, 'family_expired_app' => $family_expired_app]);
//
//        return response()->json($family_app_collection, 200);
//    }

    public function allAppointmentList(Request $request) {
        $now = Carbon::now();
        $today = $now->toDateString();
        $userid = $request->user_id;

       $pateint = DB::table('pateint')->where('user_ID', $userid)->get();
        $myappointmentlist = DB::table('appointment')
                ->join('users', 'appointment.user_id', '=', 'users.id')
                ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                ->where('appointment.user_id', $userid)
                ->where('appointment.pateint_id', null)
                //->where('appointment.cancel_by_user', 0)
                //->where('appointment.cancel_by_doctor', 0)
                //->where('appointment.appointment_date', '>=', $today)
                ->select('doctorID', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
                ->orderByRaw("Status ASC, appointment_date ASC")
                ->paginate(10);
                
                foreach ($myappointmentlist as $my_app) {
                    
                 $is_feedback = DB::table('feedback')
                    ->where('feedback_user_id', $userid)
                    ->where('feedback_doctor_id', $my_app->doctorID)
                     ->where('feedback_patient_id', '0')
                    ->first();

            if ($is_feedback == null) {
                $my_app->chat_icon = 'off';
            } else {
                $my_app->chat_icon = 'on';
            }
            
        }
                

        $myfamilyappointmentlist = DB::table('appointment')
                ->join('users', 'appointment.user_id', '=', 'users.id')
                ->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')
                ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                ->where('appointment.user_id', $userid)
                //   ->where('appointment.cancel_by_user', 0)
                //  ->where('appointment.cancel_by_doctor', 0)
                // ->where('appointment.appointment_date', '>=', $today)
                ->select('doctorID', 'appID', 'patientID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
                ->orderByRaw("Status ASC, appointment_date ASC")
                ->paginate(10);
                
                foreach ($myfamilyappointmentlist as $my_app) {
                    
                $is_feedback = DB::table('feedback')
                    ->where('feedback_user_id', $userid)
                    ->where('feedback_doctor_id', $my_app->doctorID)
                    ->where('feedback_patient_id','!=','0')
                     ->where('feedback_patient_id', $my_app->patientID)
                    ->first();

                if ($is_feedback == null) {
                $my_app->chat_icon = 'off';
                } else {
                $my_app->chat_icon = 'on';
            }
        }

        return response()->json(['user_appointment' => $myappointmentlist, 'family_appointment' => $myfamilyappointmentlist,'family_member'=>$pateint], 200);
    }

    public function myAppointmentList(Request $request) {
        $now = Carbon::now();
        $today = $now->toDateString();
        $userid = $request->user_id;

        $myappointmentlist = DB::table('appointment')
                ->join('users', 'appointment.user_id', '=', 'users.id')
                ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                ->where('appointment.user_id', $userid)
                ->where('appointment.pateint_id', null)
                //   ->where('appointment.cancel_by_user', 0)
                //  ->where('appointment.cancel_by_doctor', 0)
                //  ->where('appointment.appointment_date', '>=', $today)
                ->select('doctorID', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
                ->orderByRaw("Status ASC, appointment_date ASC")
                ->paginate(10);

        return response()->json($myappointmentlist, 200);
    }

    public function familyAppointmentList(Request $request) {
        $now = Carbon::now();
        $today = $now->toDateString();
        $patientid = $request->family_member_id;
        $myfamilyappointmentlist = DB::table('appointment')
                ->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')
                ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                ->where('appointment.pateint_id', $patientid)
                //  ->where('appointment.cancel_by_user', 0)
                //  ->where('appointment.cancel_by_doctor', 0)
                //  ->where('appointment.appointment_date', '>=', $today)
                ->select('doctorID', 'appID', 'patientID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
                ->orderByRaw("Status ASC, appointment_date ASC")
                ->paginate(10);

        return response()->json($myfamilyappointmentlist, 200);
    }

    public function updateAppointment(Request $store) {
        
        $id_user = $store->user_id;
        $id_doctor = $store->DocotsID;
        $familyMemberId = $store->family_member_id;
        $DateOfAppointment = $store->date_of_appointment;
        $TimeOfAppointment = $store->time_of_appointment;
        $Name = $store->name;
        $Age = $store->age;
        $Gender = $store->gender;
        $relation_ship = $store->relation_ship;
        $Token = $store->token;
        $app_id = $store->appointment_id;


        if ($Token == 'self') {


            $update_appointment = DB::table('appointment')
                    ->where('appID', $app_id)
                    ->update([
                'user_id' => $id_user,
                'doctor_id' => $id_doctor,
                'appointment_date' => $DateOfAppointment,
                'appointment_time' => $TimeOfAppointment,
                'feedback' => '0',
                'Status' => '0'
            ]);

            $update = DB::table('users')
                    ->where('id', $id_user)
                    ->update([
                'name' => $Name,
                'birthday' => $Age,
                'gender' => $Gender,
            ]);

            return response()->json(["appointment_status"=>1,"message"=>"Your Appointment  updated Successfully"], 201);
        }
        if ($Token == 'other') {
            if ($familyMemberId == 0) {
                $pateint = new Pateint();
                $pateint->user_ID = $id_user;
                $pateint->p_name = $Name;
                $pateint->p_Relation = $relation_ship;
                $pateint->birthday = $Age;
                $pateint->gender = $Gender;
                if ($Gender == 1)
                    $pateint->p_photo = 'male.png';
                else
                    $pateint->p_photo = 'female.png';
                $pateint->save();


                $data = DB::table('pateint')->orderBy('patientID', 'DESC')->first();
                $update_appointment = DB::table('appointment')
                        ->where('appID', $app_id)
                        ->update([
                    'user_id' => $id_user,
                    'doctor_id' => $id_doctor,
                    'pateint_id' => $data->patientID,
                    'appointment_date' => $DateOfAppointment,
                    'appointment_time' => $TimeOfAppointment,
                    'feedback' => '0',
                    'Status' => '0'
                ]);
            } else {
                $update_appointment = DB::table('appointment')
                        ->where('appID', $app_id)
                        ->update([
                    'user_id' => $id_user,
                    'doctor_id' => $id_doctor,
                    'pateint_id' => $familyMemberId,
                    'appointment_date' => $DateOfAppointment,
                    'appointment_time' => $TimeOfAppointment,
                    'feedback' => '0',
                    'Status' => '0'
                ]);

                $pateint = DB::table('pateint')
                        ->where('patientID', $familyMemberId)
                        ->update([
                    'p_name' => $Name,
                    'p_Relation' => $relation_ship,
                    'birthday' => $Age,
                    'gender' => $Gender,
                ]);
            }

            return response()->json(["appointment_status"=>1,"message"=>"Your Appointment  updated Successfully"], 201);
        }
        // event(new EndPool($id_doctor));
    }

    // function getAppointment(Request $request) {
    //     $app_id = $request->appointment_id;
    //     $appointment = DB::table('appointment')->where('appID', $app_id)->get();
    //     return response()->json(['appointment' => $appointment], 200);
    // }
    function getAppointment(Request $request) {
        $app_id = $request->appointment_id;
        $appointment = DB::table('appointment')->where('appID', $app_id)->get();

        foreach ($appointment as $app) {
            $address = DB::table('address')
                    ->where('id', $app->clinic_address_id)
                    ->select('id', 'name', 'address')
                    ->get();
            $app->address = $address;
        }
        return response()->json(['appointment' => $appointment], 200);
    }

    public function cancelAppointment(Request $request) {
        $Appointmentid = $request->appointment_id;
        $cancel = DB::table('appointment')
                ->where('appID', $Appointmentid)
                ->update([
            'Status' => 1,
            'cancel_by_user' => 1,
        ]);
        $app = DB::table('appointment')->where('appID', $Appointmentid)->first();

        $noti = new NotificationController();
        $res = $noti->appCancelNoti_cancel_by_user($app);


        return response()->json($cancel, 201);
    }

    // function viewMedicalRecords(Request $request) {
    //     $user_id = $request->user_id;
    //     $document = DB::table('document')
    //             ->where('document.user_id', $user_id)
    //             ->orderby('document.docID', 'DESC')
    //             ->paginate(10);

    //     return response()->json($document, 200);
    // }

 function viewMedicalRecords(Request $request) {
        $user_id = $request->user_id;
          
       // $pateint = DB::table('pateint')->where('user_ID', $user_id)->paginate(100);
       $pateint = DB::table('pateint')
                ->join('appointment', 'pateint.patientID', '=', 'appointment.pateint_id')
                ->select('patientID', 'pateint.user_ID', 'p_name', 'p_photo', 'p_Relation', 'p_phoneNo', 'gender', 'birthday')
                ->where('pateint.user_ID', $user_id)
                ->groupBy('patientID')
                ->get();
        
        $document = DB::table('document')
                ->where('document.user_id', $user_id)
                ->orderby('document.docID', 'DESC')
                ->paginate(10);

        return response()->json(['document'=>$document,'family_member'=>$pateint], 200);
    }

    function addMedicalRecords(Request $request) {
        $user_id = $request->user_id;
        $images = $request->file('file');
        $related = $request->related_to_who;
        $doctor_name = $request->doctor_name;
        $records_type = $request->records_type;
        $doctor_id = $request->doctor_id;
        if ($related == 0) {

            $users = DB::table('users')->select('name')->where('id', $user_id)->first();
            $user_name = $users->name;
            $id = $this->storeDocForUser($doctor_id, $doctor_name, $user_id, $user_name, $records_type, $related);
        } else {
            $pateint_id = $related;
            $pateint = DB::table('pateint')->select('p_name')->where('patientID', $pateint_id)->first();
            $pateint_name = $pateint->p_name;
            $id = $this->storeDocForUser($doctor_id, $doctor_name, $user_id, $pateint_name, $records_type, $related);
        }

        $record = $this->uploadMultiFile($id, $images);

        $output = array(
            'success' => 'Recored created successfully',
        );

        return response()->json($output, 201);
    }

    function storeDocForUser($doctor_id, $doctor_name, $user_id, $name, $records_type, $related) {

        $doc = new Document();
        $doc->user_id = $user_id;
        $doc->pateint_id = $related;
        $doc->pateint_name = $name;
        $doc->doctor_name = $doctor_name;
        $doc->doctor_id = $doctor_id;
        $doc->record_type = $records_type;
        $doc->date = $date = now();
        $doc->save();

        $userregiter = DB::table('document')->select('docID')->orderBy('docID', 'DESC')->first();

        return $userregiter->docID;
    }

    function uploadMultiFile($id, $images) {
        foreach ($images as $image) {

            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/images/medicalrecords/'), $new_name);

            $doc = new FileModel();
            $doc->doc_id = $id;
            $doc->document = $new_name;
            $doc->save();
        }

        return 0;
    }

    function getRecordsById(Request $request) {
        $doc_id = $request->medical_records_id;
        $document = DB::table('document')->where('docID', $doc_id)->get();
        return response()->json(['Record' => $document], 200);
    }

    function updateRecords(Request $request) {
        $user_id = $request->user_id;
        $related = $request->related_to_who;
        $doctor_name = $request->doctor_name;
        $records_type = $request->records_type;
        $record_id = $request->medical_records_id;
        $doctor_id = $request->doctor_id;

        if ($related == 0) {
            $users = DB::table('users')->select('name')->where('id', $user_id)->first();
            $user_name = $users->name;
            $id = $this->updateDocForUser($doctor_id, $doctor_name, $user_name, $records_type, $record_id, $related);
        } else {
            $pateint_id = $related;
            $pateint = DB::table('pateint')->select('p_name')->where('patientID', $pateint_id)->first();
            $pateint_name = $pateint->p_name;
            $id = $this->updateDocForUser($doctor_id, $doctor_name, $pateint_name, $records_type, $record_id, $related);
        }


        $output = array(
            'success' => 'Recored Updated successfully',
        );

        return response()->json($output, 201);
    }

    function updateDocForUser($doctor_id, $doctor_name, $pateint_name, $records_type, $record_id, $related) {
          $date = now();
        $update = DB::table('document')
                ->where('docID', $record_id)
                ->update([
            'pateint_id' => $related,
            'pateint_name' => $pateint_name,
            'doctor_name' => $doctor_name,
            'doctor_id' => $doctor_id,
            'date' =>$date,
            'record_type' => $records_type
        ]);

        return $update;
    }

    function allowSharingRecord(Request $request) {
        $docID = $request->document_id;

        $allow = DB::table('document')->select('allow_share_records')->where('docID', $docID)->first();


        if ($allow->allow_share_records == 0) {

            DB::table('document')->where('docID', $docID)->update(['allow_share_records' => 1]);
            return response()->json('record is now visible to doctors', 200);
        } else {

            DB::table('document')->where('docID', $docID)->update(['allow_share_records' => 0]);
            return response()->json('record is now invisible to doctors', 200);
        }
    }

    function allowSharingAllRecord(Request $request) {

        $userid = $request->user_id;
        $family_mamber_id = $request->family_mamber_id;
        $allow = DB::table('document')->select('allow_share_all_records', 'allow_share_records')
                        ->where('user_id', $userid)
                        ->where('pateint_id', $family_mamber_id)->first();

        if ($allow != null) {
            if ($family_mamber_id == 0) {

                if ($allow->allow_share_all_records == 0) {
                    DB::table('document')->where('user_id', $userid)->where('pateint_id', 0)->update(['allow_share_all_records' => 1]);
                    return response()->json('records are now visible to doctors', 200);
                } else {
                    DB::table('document')->where('user_id', $userid)->where('pateint_id', 0)->update(['allow_share_all_records' => 0]);
                    return response()->json('records are now invisible to doctors', 200);
                }
            } else {
                if ($allow->allow_share_all_records == 1) {
                    DB::table('document')->where('user_id', $userid)->where('pateint_id', $family_mamber_id)->update(['allow_share_all_records' => 0]);
                    return response()->json('records are now invisible to doctors', 200);
                } else {
                    DB::table('document')->where('user_id', $userid)->where('pateint_id', $family_mamber_id)->update(['allow_share_all_records' => 1]);
                    return response()->json('records are now visible to doctors', 200);
                }
            }
        } else {
            return response()->json('no data found', 400);
        }
    }

    public function deleteRecords(Request $request) {
        $doc_id = $request->medical_records_id;

        $files = DB::table('file')->where('doc_id', $doc_id)->get();
        $destinationPath = '/images/medicalrecords/';
        foreach ($files as $file) {

            File::delete($destinationPath . $file->document);

            DB::table('file')->where('doc_id', $doc_id)->delete();
        }

        $res = DB::table('document')->where('docID', $doc_id)->delete();
        if ($res) {
            $output = array('success' => 'Recored deleted successfully');
            return response()->json($output, 200);
        } else {
            $output = array('failed' => 'Recored id not found');
            return response()->json($output, 400);
        }
    }

    function viewRecords(Request $request) {
        $doc_id = $request->medical_records_id;

        $document = DB::table('file')
                ->join('document', 'file.doc_id', '=', 'document.docID')
                ->where('file.doc_id', $doc_id)
                ->get();
        if ($document != '[]') {

            return response()->json($document, 200);
        } else {
            $output = array('failed' => 'no file found for this id');
            return response()->json($output, 400);
        }
    }

    function addNewFilesToRecord(Request $request) {

        $images = $request->file('file');
        $id = $request->medical_records_id;
        foreach ($images as $image) {

            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/images/medicalrecords/'), $new_name);

            $doc = new FileModel();
            $doc->doc_id = $id;
            $doc->document = $new_name;
            $doc->save();
        }
        if ($doc) {

            return response()->json($doc, 200);
        } else {

            return response()->json('no file found for this id', 400);
        }
    }

    public function deleteFile(Request $request) {
        $file_id = $request->file_id;
        $name = DB::table('file')->select('document')->where('file_id', $file_id)->first();
        if ($name != null) {
            $file = $name->document;
            $destinationPath = 'images/medicalrecords/';
            File::delete($destinationPath . $file);
            DB::table('file')->where('file_id', $file_id)->delete();
            return response()->json('file deleted successfully', 200);
        } else {
            return response()->json('no file found for this id', 400);
        }
    }

    // public function deleteFamilyMember(Request $request) {

    //     $family_id = $request->family_member_id;
    //     DB::table('appointment')->where('pateint_id', $family_id)->delete();
    //     $status = DB::table('pateint')->where('patientID', $family_id)->delete();
    //     if ($status) {
    //         return response()->json('user deleted successfully', 200);
    //     } else {
    //         return response()->json('user not deleted ', 400);
    //     }
    // }

      public function deleteFamilyMember(Request $request) {

        $family_id = $request->family_member_id;
        DB::table('appointment')->where('pateint_id', $family_id)->delete();
        DB::table('chat')->where('pateint_id', $family_id)->delete();
        DB::table('document')->where('pateint_id', $family_id)->delete();
        $status = DB::table('pateint')->where('patientID', $family_id)->delete();
        if ($status) {
            return response()->json('user deleted successfully', 200);
        } else {
            return response()->json('user not deleted ', 400);
        }
    }
    
    
    function getTheAppointmentDoctorsList($user_id) {
       $pateint = DB::table('pateint')
                ->join('appointment','pateint.patientID', '=', 'appointment.pateint_id')
                ->select('patientID','pateint.user_ID','p_name','p_photo','p_Relation','p_phoneNo','gender','birthday')
                ->where('pateint.user_ID',$user_id)
                ->groupBy('patientID')
                ->get();
                
        $doctorlist = DB::table('appointment')
                ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                ->where('appointment.user_id', $user_id)
                ->select('doctorID', 'title', 'doctor_name')
                ->orderBy('appID', 'desc')
                ->groupby('doctorID')
                ->get();

        return response()->json(['doctor_list' => $doctorlist, 'family_member' => $pateint], 200);
    }
    // function getTheAppointmentDoctorsList($user_id) {

    //     $doctorlist = DB::table('appointment')
    //             ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
    //             ->where('appointment.user_id', $user_id)
    //             ->select('doctorID', 'title', 'doctor_name')
    //             ->orderBy('appID', 'desc')
    //             ->groupby('doctorID')
    //             ->get();

    //     return response()->json($doctorlist, 200);
    // }

}
