<?php

namespace App\Http\Controllers\DoctorApi\doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use App\Chat;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\NotificationController;
class DoctorApiAppointmentController extends Controller {

    // public function allAppointmentList($id) {
    //     $now = Carbon::now();
    //     $today = $now->toDateString();
    //     $appointments_count = DB::table('appointment')->where('doctor_id', $id)
    //             ->where('cancel_by_user', 0)
    //             ->where('cancel_by_doctor', 0)
    //             ->where('appointment_date', '>=', $today)
    //             ->orderBy('appointment_time', 'asc')
    //             ->count();
    //     if ($appointments_count != 0) {
    //         $appointments = DB::table('appointment')->where('doctor_id', $id)
    //                 ->where('cancel_by_user', 0)
    //                 ->where('cancel_by_doctor', 0)
    //                 ->where('appointment_date', '>=', $today)
    //                 ->orderBy('appointment_time', 'asc')
    //                 ->get();
    //         foreach ($appointments as $appt) {

    //             $collection = collect(['appID' => $appt->appID, 'user_id' => $appt->user_id, 'pateint_id' => $appt->pateint_id, 'appointment_date' => $appt->appointment_date, 'appointment_time' => $appt->appointment_time]);

    //             if ($appt->pateint_id == null) {
    //                 $user_name = DB::table('users')->select('name','phone')->where('id', $appt->user_id)->first();

    //                 $collection->put('name', $user_name->name);
    //                 $collection->put('phone', $user_name->phone);
    //             } else {
    //                 $p_name = DB::table('pateint')->select('p_name','p_phoneNo')->where('patientID', $appt->pateint_id)->first();

    //                 $collection->put('name', $p_name->p_name);
    //                 $collection->put('phone', $p_name->p_phoneNo);
    //             }
    //             $all[] = $collection->all();
    //         }
    //         return response()->json($all, 200);
    //     } else {
    //         return response()->json('no Appointment found!', 200);
    //     }
    // }
  public function allAppointmentList(Request $req) {
      //  $now = Carbon::now();
       // $today = $now->toDateString();
        $id=$req->doctor_id;
        $today=$req->date;
        $appointments_count = DB::table('appointment')->where('doctor_id', $id)
//                ->where('cancel_by_user', 0)
//                ->where('cancel_by_doctor', 0)
                ->where('appointment_date', '=', $today)
                ->orderBy('appointment_time', 'asc')
                ->count();
        if ($appointments_count != 0) {
            $appointments = DB::table('appointment')->where('doctor_id', $id)
//                    ->where('cancel_by_user', 0)
//                    ->where('cancel_by_doctor', 0)
                    ->where('appointment_date', '=', $today)
                    ->orderBy('appointment_time', 'asc')
                    ->get();
            foreach ($appointments as $appt) {

                $collection = collect(['appID' => $appt->appID, 'user_id' => $appt->user_id, 'pateint_id' => $appt->pateint_id, 'appointment_date' => $appt->appointment_date, 'appointment_time' => $appt->appointment_time,'Status' => $appt->Status]);

                if ($appt->pateint_id == null) {
                    $user_name = DB::table('users')->select('name','phone')->where('id', $appt->user_id)->first();

                    $collection->put('name', $user_name->name);
                    $collection->put('phone', $user_name->phone);
                } else {
                    $p_name = DB::table('pateint')->select('p_name','p_phoneNo')->where('patientID', $appt->pateint_id)->first();

                    $collection->put('name', $p_name->p_name);
                    $collection->put('phone', $p_name->p_phoneNo);
                }
                $all[] = $collection->all();
            }
            return response()->json($all, 200);
        } else {
            return response()->json('no Appointment found!', 200);
        }
    }

  public function cancelAppointment($app_id) {

        $cancel = DB::table('appointment')->where('appID', $app_id)->update(['Status' => 1,'cancel_by_doctor' => 1]);
     
         $app = DB::table('appointment')->where('appID', $app_id)->first();
           
         $noti= new NotificationController();
         $res = $noti->appCancelNoti_cancel_by_doctor($app);

        
        return response()->json($cancel, 201);
    }

    public function allMyPatientList($id) {

        $appointments_count = DB::table('appointment')->where('doctor_id', $id)->groupBy('pateint_id', 'user_id')->count();
        if ($appointments_count != 0) {
            $appointments = DB::table('appointment')->where('doctor_id', $id)->groupBy('pateint_id', 'user_id')->get();

            foreach ($appointments as $appt) {

                $collection = collect(['user_id' => $appt->user_id
                    , 'appID' => $appt->appID
                    , 'family_member_id' => $appt->pateint_id
                    , 'appointment_time' => $appt->appointment_time]);

                if ($appt->pateint_id == null) {

                    $user = DB::table('users')->where('id', $appt->user_id)->first();
                    $collection->put('name', $user->name);
                    $collection->put('pic', $user->pic);
                    $collection->put('phone', $user->phone);
                    $collection->put('birthday', $user->birthday);
                    $collection->put('gender', $user->gender);
                } else {

                    $user = DB::table('pateint')->where('patientID', $appt->pateint_id)->first();
                    $collection->put('name', $user->p_name);
                    $collection->put('pic', $user->p_photo);
                    $collection->put('phone', $user->p_phoneNo);
                    $collection->put('birthday', $user->birthday);
                    $collection->put('gender', $user->gender);
                }
                $all[] = $collection->all();
            }
            return response()->json($all, 200);
        } else {
            return response()->json('no Patient found!', 200);
        }
    }
 public function allMyPatientListSendDoctorText($id) {

        $appointments_count = DB::table('chat')->where('doctor_id', $id)->groupBy('pateint_id', 'user_id')->count();
        if ($appointments_count != 0) {
                 $appointments = Chat::select('user_id','pateint_id',DB::raw('MAX(date) AS max_date'))
                    ->where('doctor_id', $id)
                   // ->where('who_send', 'user')
                    ->orderBy('max_date', 'DESC')
                    ->groupBy('pateint_id', 'user_id')
                    ->get();
//dd($appointments);
             foreach ($appointments as $appt) {

                   if ($appt->pateint_id == 0) {
            $user = DB::table('users')->where('id', $appt->user_id)->first();
                    $appt->name = $user->name;
                    $appt->pic = $user->pic;
                    $appt->phone = $user->phone;
                    $appt->birthday = $user->birthday;
                    $appt->gender = $user->gender;
                } else {
                   
              $patient = DB::table('pateint')->where('patientID', $appt->pateint_id)->first();
                    
                    
                    $appt->name = $patient->p_name;
                    $appt->pic = $patient->p_photo;
                    $appt->phone = $patient->p_phoneNo;
                    $appt->birthday = $patient->birthday;
                    $appt->gender = $patient->gender;
                }
            }
           // dd($appointments);
            return response()->json($appointments, 200);
        } else {
            return response()->json('no Patient found!', 200);
        }
    }

    //  public function allMyPatientListSendDoctorText($id) {

    //     $appointments_count = DB::table('chat')->where('doctor_id', $id)->groupBy('user_id', 'pateint_id')->count();
    //     if ($appointments_count != 0) {
    //         $appointments = DB::table('chat')->where('doctor_id', $id)->where('who_send','user')->groupBy('pateint_id', 'user_id')->orderby('date', 'desc')->get();
          
    //      $collection = collect();
    //         foreach ($appointments as $appt) {

               

    //             if ($appt->pateint_id == 0) {

    //               //  $user = DB::table('users')->where('id', $appt->user_id)->first();
                    // $user = DB::table('users')
                    //         ->join('chat', 'users.id', '=', 'chat.user_id')
                    //         ->where('id', $appt->user_id)
                    //         ->orderby('date', 'desc')
                    //         ->first();
    //                 if ($user != null) {
    //                     $collection->put('user_id', $appt->user_id);
    //                     $collection->put('name', $user->name);
    //                     $collection->put('pic', $user->pic);
    //                     $collection->put('phone', $user->phone);
    //                     $collection->put('birthday', $user->birthday);
    //                     $collection->put('gender', $user->gender);
    //                 }
    //             } else {

    //              //   $user = DB::table('pateint')->where('patientID', $appt->pateint_id)->first();
                //  $user = DB::table('pateint')
                //             ->join('chat', 'pateint.patientID', '=', 'chat.pateint_id')
                //             ->where('patientID', $appt->pateint_id)
                //             ->orderby('date', 'desc')
                //             ->first();
    //                 if ($user != null) {
    //                     $collection->put('user_id',$appt->user_id);
    //                     $collection->put('patientID', $appt->pateint_id);
    //                     $collection->put('name', $user->p_name);
    //                     $collection->put('pic', $user->p_photo);
    //                     $collection->put('phone', $user->p_phoneNo);
    //                     $collection->put('birthday', $user->birthday);
    //                     $collection->put('gender', $user->gender);
    //                 }
    //             }
    //             $all[] = $collection->all();
    //         }
    //         return response()->json($all, 200);
    //     } else {
    //         return response()->json('no Patient found!', 200);
    //     }
    // }
    public function viewPatientsProfile(Request $req) {
        $userid = $req->user_id;
        $family_member_id = $req->family_member_id;
        if ($family_member_id == 0) {
            $pateint = DB::table('users')->where('id', $userid)->first();

            $documents = DB::table('document')
                    ->where('user_id', $userid)
                    ->where('document.pateint_id', 0)
                    ->where('document.allow_share_records', 1)
                    ->orderby('docID', 'desc')
                    ->get();

            foreach ($documents as $doc) {

                $file = DB::table('file')
                        ->where('doc_id', $doc->docID)
                        ->orderby('doc_id', 'desc')
                        ->get();
                $doc->file = $file;
            }
            //dd($documents);
            return response()->json(['patient_info' => $pateint, 'document' => $documents], 201);
        } else {
            $pateint = DB::table('pateint')->where('patientID', $family_member_id)->first();
            $documents = DB::table('document')
                    ->where('pateint_id', $family_member_id)
                    ->orderby('docID', 'desc')
                    ->get();
            foreach ($documents as $doc) {

                $file = DB::table('file')
                        ->where('doc_id', $doc->docID)
                        ->orderby('doc_id', 'desc')
                        ->get();
                $doc->file = $file;
            }


            return response()->json(['patient_info' => $pateint, 'document' => $documents], 201);
        }
    }

    function viewMedicalRecords(Request $request) {
        $user_id = $request->user_id;
        $document = DB::table('document')->where('document.user_id', $user_id)->orderby('document.docID', 'desc')->paginate(10);

        return response()->json($document, 200);
    }

    function viewRecords(Request $request) {
        $doc_id = $request->medical_records_id;

        $document = DB::table('file')
                ->join('document', 'file.doc_id', '=', 'document.docID')
                ->where('file.doc_id', $doc_id)
                ->get();
        if ($document != '[]') {

            return response()->json($document, 200);
        } else {
            $output = array('failed' => 'no file found for this id');
            return response()->json($output, 400);
        }
    }

}
