<?php

namespace App\Http\Controllers\DoctorApi\doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Doctorexperiance;


class DoctorApiExperianceController extends Controller
{
  
    function addExperiance(Request $request) {
        $data = ['doctor_id' => 'required|integer', 'experiance_name' => 'required', 'start_date' => 'required|integer', 'end_date' => 'required|integer'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $doctorid = $request->doctor_id;
            $doctor = DB::table('doctor')->where('doctorID', $doctorid)->first();
            if ($doctor != null) {
                $edu = new Doctorexperiance();
                $edu->doctor_FID = $request->doctor_id;
                $edu->experience_name = $request->experiance_name;
                $edu->start = $request->start_date;
                $edu->end = $request->end_date;
                $edu->save();
              $experience = DB::table('doctor_experience')->where('doctor_FID', $doctorid)->get();
                return response()->json($experience, 201);
            } else {
                return response()->json(['errors' => 'doctor id is invalid'], 400);
            }
        }
    }

    function viewAllExperiance($id) {
        $doctorid = $id;
        $doctor = DB::table('doctor')->where('doctorID', $doctorid)->first();
        if ($doctor != null) {
            $experience = DB::table('doctor_experience')->where('doctor_FID', $doctorid)->get();
            return response()->json($experience, 201);
        } else {
            return response()->json(['errors' => 'doctor id is invalid'], 400);
        }
    }

    function getExperiance($id) {
        $experience = DB::table('doctor_experience')->where('experience_id', $id)->first();
        if ($experience != null) {
            return response()->json($experience, 201);
        } else {
            return response()->json(['errors' => 'edu id is invalid'], 400);
        }
    }

    function updateExperiance(Request $request) {

        $data = ['experience_id' => 'required|integer', 'doctor_id' => 'required|integer', 'experiance_name' => 'required', 'start_date' => 'required|integer', 'end_date' => 'required|integer'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $doctorid = $request->doctor_id;
            $experience_id = $request->experience_id;
            $doctor = DB::table('doctor')->where('doctorID', $doctorid)->first();
            $experience = DB::table('doctor_experience')->where('experience_id', $experience_id)->first();
            if ($doctor != null && $experience != null) {
                DB::table('doctor_experience')->where('experience_id', $experience_id)
                        ->update(['experience_name' => $request->experiance_name,
                            'doctor_FID' => $doctorid,
                            'start' => $request->start_date,
                            'end' => $request->end_date]);
                return response()->json(['success' => 'experience updated!'], 201);
            } else {
                return response()->json(['errors' => 'doctor id or experience id is invalid'], 400);
            }
        }
    }

    function deleteExperiance($id) {

        $edu = DB::table('doctor_experience')->where('experience_id', $id)->first();
        if ($edu != null) {
            $edu = DB::table('doctor_experience')->where('experience_id', $id)->delete();
            return response()->json(['success' => 'experience deleted!'], 201);
        } else {
            return response()->json(['errors' => 'experience id is invalid'], 400);
        }
    }
}
