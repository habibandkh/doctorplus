<?php

namespace App\Http\Controllers\DoctorApi\doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Doctoreduction;

class DoctorApiEducationController extends Controller
{
  function addeducation(Request $request) {
        $data = ['doctor_id' => 'required|integer', 'School_name' => 'required', 'Study_title' => 'required', 'start_date' => 'required|integer', 'end_date' => 'required|integer'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $doctorid = $request->doctor_id;
            $doctor = DB::table('doctor')->where('doctorID', $doctorid)->first();
            if ($doctor != null) {
                $edu = new Doctoreduction();
                $edu->D_FID = $request->doctor_id;
                $edu->school_name = $request->School_name;
                $edu->title_of_study = $request->Study_title;
                $edu->start_Date = $request->start_date;
                $edu->end_Date = $request->end_date;
                $edu->save();
                $eduction = DB::table('doctor_education')->where('D_FID', $doctorid)->get();
                return response()->json($eduction, 201);
            } else {
                return response()->json(['errors' => 'doctor id is invalid'], 400);
            }
        }
    }

    function viewAllEducation($id) {
        $doctorid = $id;
        $doctor = DB::table('doctor')->where('doctorID', $doctorid)->first();
        if ($doctor != null) {
            $edu = DB::table('doctor_education')->where('D_FID', $doctorid)->get();
            return response()->json($edu, 201);
        } else {
            return response()->json(['errors' => 'doctor id is invalid'], 400);
        }
    }

    function getEducation($id) {
        $edu = DB::table('doctor_education')->where('D_E_ID', $id)->first();
        if ($edu != null) {
            return response()->json($edu, 201);
        } else {
            return response()->json(['errors' => 'edu id is invalid'], 400);
        }
    }

    function updateEducation(Request $request) {

        $data = ['eduction_id' => 'required|integer', 'doctor_id' => 'required|integer', 'School_name' => 'required', 'Study_title' => 'required', 'start_date' => 'required|integer', 'end_date' => 'required|integer'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $doctorid = $request->doctor_id;
            $eduid = $request->eduction_id;
            $doctor = DB::table('doctor')->where('doctorID', $doctorid)->first();
            $edu = DB::table('doctor_education')->where('D_E_ID', $eduid)->first();
            if ($doctor != null && $edu != null) {
                DB::table('doctor_education')->where('D_E_ID', $eduid)
                        ->update(['school_name' => $request->School_name,
                            'D_FID' => $doctorid,
                            'title_of_study' => $request->Study_title,
                            'start_Date' => $request->start_date,
                            'end_Date' => $request->end_date]);
                return response()->json(['success' => 'education updated!'], 201);
            } else {
                return response()->json(['errors' => 'doctor id or edu id is invalid'], 400);
            }
        }
    }

    function deleteEducation($id) {

        $edu = DB::table('doctor_education')->where('D_E_ID', $id)->first();
        if ($edu != null) {
            $edu = DB::table('doctor_education')->where('D_E_ID', $id)->delete();
            return response()->json(['success' => 'education deleted!'], 201);
        } else {
            return response()->json(['errors' => 'edu id is invalid'], 400);
        }
    }

}
