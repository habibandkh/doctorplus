<?php

namespace App\Http\Controllers\DoctorApi\doctor;

use App\DoctorServiceView;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use function emptyArray;

class DoctorApiServiceController extends Controller {

    function getAllServices($id) {

        $doctor = DB::table('doctor')->where('doctorID', $id)->first();
        if ($doctor != null) {
            $services = DB::table('speciality')
                    ->join('doctor_services', 'speciality.specialityID', '=', 'doctor_services.specialty_id')
                    ->join('doctorspeciality', 'speciality.specialityID', '=', 'doctorspeciality.F_specialityID')
                    ->where('doctorspeciality.F_doctorID', $id)
                    ->select('services_id', 'service_name')
                    ->get();
            return response()->json($services, 201);
        } else {
            return response()->json(['errors' => 'doctor id is invalid'], 400);
        }
    }

    function addService(Request $request) {

        $data = ['doctor_id' => 'required|integer'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {

            $doctor_id = $request->doctor_id;
            $add_service_id = $request->add_service_id;
            $remove_service_id = $request->remove_service_id;

           
            if ($add_service_id[0] != null) {

                $var = $this->add_services($doctor_id, $add_service_id);
            }
            if ($remove_service_id[0] != null) {

                $var1 = $this->remove_services($doctor_id, $remove_service_id);
            }

            $services = DB::table('doctor_services')
                    ->join('doctor_services_view', 'doctor_services.services_id', '=', 'doctor_services_view.services_id')
                    ->where('doctor_services_view.doctor_id', $doctor_id)
                    ->select('service_name', 'doctor_services_view.id', 'doctor_services_view.services_id')
                    ->groupBy('service_name')
                    ->get();

            return response()->json($services, 201);
        }
    }

    function add_services($doctor_id, $add_service_id) {

        foreach ($add_service_id as $ID) {

            $ser = DB::table('doctor_services_view')
                    ->where('doctor_id', $doctor_id)
                    ->where('services_id', $ID)
                    ->first();

            if ($ser == null) {

                $insert = new DoctorServiceView();
                $insert->doctor_id = $doctor_id;
                $insert->services_id = $ID;
                $insert->save();
            }
        }
        return 0;
    }

    function remove_services($doctor_id, $remove_service_id) {

        foreach ($remove_service_id as $ID) {
            
            $ser = DB::table('doctor_services_view')
                    ->where('doctor_id', $doctor_id)
                    ->where('services_id', $ID)
                    ->first();
            
            if ($ser != null) {

                $x = DB::table('doctor_services_view')
                        ->where('doctor_id', $doctor_id)
                        ->where('services_id', $ID)
                        ->delete();
            }
        }
        return 0;
    }

    function viewMyService($id) {

        $doctor = DB::table('doctor')->where('doctorID', $id)->first();
        if ($doctor != null) {
            $services = DB::table('doctor_services')
                    ->join('doctor_services_view', 'doctor_services.services_id', '=', 'doctor_services_view.services_id')
                    ->where('doctor_services_view.doctor_id', $id)
                    ->select('doctor_services.services_id', 'service_name', 'doctor_services_view.id')
                    ->groupBy('service_name')
                    ->get();
            
            return response()->json($services, 201);
        } else {
            return response()->json(['errors' => 'doctor id is invalid'], 400);
        }
    }

    function deleteService($id) {
        $ser = DB::table('doctor_services_view')->where('id', $id)->first();
        if ($ser != null) {
            $ser = DB::table('doctor_services_view')->where('id', $id)->delete();
            return response()->json(['success' => 'service deleted!'], 201);
        } else {
            return response()->json(['errors' => 'service id is invalid'], 400);
        }
    }

}
