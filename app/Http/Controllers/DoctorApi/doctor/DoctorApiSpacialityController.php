<?php

namespace App\Http\Controllers\DoctorApi\doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Doctorspeciality;
use DB;

class DoctorApiSpacialityController extends Controller {

    function get_all_p_p($doctor_id) {

        $all_speciality = DB::table('speciality')->get();
        $education = DB::table('doctor_education')->where('D_FID', $doctor_id)->get();
        $experience = DB::table('doctor_experience')->where('doctor_FID', $doctor_id)->get();
        $doctor = DB::table('doctor')->select('pmdc', 'doctor_picture', 'doctorID')->where('doctorID', $doctor_id)->get();

        $MyPrimaryS = $this->myPrimaryspaciality($doctor_id);
        $speciality = $this->get_speciality($doctor_id);
        $Condition = $this->get_condition($doctor_id);
        $services = $this->get_services($doctor_id);

        $my_Condition = $this->get_condition_link($doctor_id);
        $my_services = $this->get_services_link($doctor_id);

        return response()->json(['doctor' => $doctor, 'education' => $education, 'experience' => $experience, 'all_Speciality' => $all_speciality, 'P_Speciality' => $MyPrimaryS, 'S_Speciality' => $speciality, 'Condition' => $Condition, 'services' => $services, 'my_Condition' => $my_Condition, 'my_services' => $my_services], 201);
    }

    function myPrimaryspaciality($doctor_id) {
        $MyPrimaryS = DB::table('doctor')
                ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                ->select('id_s_d', 'specialityName', 'photo', 'specialityID')
                ->where('doctor.doctorID', $doctor_id)
                ->where('doctorspeciality.type', 'P')
                ->get();
        return $MyPrimaryS;
    }

    function getAllPrimaryAndSecondarySpecialization() {
        $speciality = DB::table('speciality')->get();
        return response()->json($speciality, 201);
    }

    function addPrimarySpeciality(Request $request) {
        $data = ['doctor_id' => 'required|integer', 'spaciality_id' => 'required|integer'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $doctor_id = $request->doctor_id;
            $spaciality_id = $request->spaciality_id;

            $doctorPrimary = DB::table('doctorspeciality')->where('F_doctorID', $doctor_id)->where('type', 'P')->first();
            if ($doctorPrimary != null) {
                $ser = DB::table('doctorspeciality')->where('id_s_d', $doctorPrimary->id_s_d)->delete();
            }
            $doctor = DB::table('doctor')->where('doctorID', $doctor_id)->first();
            $speciality = DB::table('speciality')->where('specialityID', $spaciality_id)->first();

            if ($doctor != null && $speciality != null) {
                $insert = new Doctorspeciality();
                $insert->F_doctorID = $doctor_id;
                $insert->F_specialityID = $spaciality_id;
                $insert->type = 'P';
                $insert->save();

                DB::table('doctor')->where('doctorID', $doctor_id)->update(['Expert' => $speciality->specialityName]);


                $MyPrimaryS = $this->myPrimaryspaciality($doctor_id);

                $Condition = $this->get_condition($doctor_id);
                $services = $this->get_services($doctor_id);

                $all_Condition = $this->get_condition_link($doctor_id);
                $all_services = $this->get_services_link($doctor_id);

                return response()->json(['Speciality' => $MyPrimaryS, 'Condition' => $Condition, 'services' => $services, 'my_Condition' => $all_Condition, 'my_services' => $all_services], 201);
            } else {
                return response()->json(['errors' => 'doctor id or spaciality id is invalid'], 400);
            }
        }
    }

    function viewMyPrimarySpeciality($id) {
        $doctorid = $id;
        $doctor = DB::table('doctor')->where('doctorID', $doctorid)->first();
        if ($doctor != null) {
            $services = DB::table('doctor')
                    ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                    ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                    ->select('id_s_d', 'specialityName', 'photo', 'specialityID')
                    ->where('doctor.doctorID', $id)
                    ->where('doctorspeciality.type', 'P')
                    ->get();
            return response()->json($services, 201);
        } else {
            return response()->json(['errors' => 'doctor id is invalid'], 400);
        }
    }

    function addSpeciality(Request $request) {

        $data = ['doctor_id' => 'required|integer'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $doctor_id = $request->doctor_id;

            $add_spaciality_id = $request->add_spaciality_id;
            $remove_spaciality_id = $request->remove_spaciality_id;
            if ($add_spaciality_id[0] != null) {
                $var = $this->addSpacialities($doctor_id, $add_spaciality_id);
            }
            if ($remove_spaciality_id[0] != null) {
                $var1 = $this->removeSpacialities($doctor_id, $remove_spaciality_id);
            }


            $speciality = $this->get_speciality($doctor_id);
            $Condition = $this->get_condition($doctor_id);
            $services = $this->get_services($doctor_id);

            $all_Condition = $this->get_condition_link($doctor_id);
            $all_services = $this->get_services_link($doctor_id);

            return response()->json(['Speciality' => $speciality, 'Condition' => $Condition, 'services' => $services, 'my_Condition' => $all_Condition, 'my_services' => $all_services], 201);
        }
    }

    function get_speciality($doctor_id) {
        $speciality = DB::table('doctor')
                ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                ->select('id_s_d', 'specialityName', 'photo', 'specialityID')
                ->where('doctor.doctorID', $doctor_id)
                ->where('doctorspeciality.type', 'S')
                ->get();
        return $speciality;
    }

    function get_condition($doctor_id) {
        $Condition = DB::table('speciality')
                ->join('doctor_condition', 'speciality.specialityID', '=', 'doctor_condition.specialty_id')
                ->join('doctorspeciality', 'speciality.specialityID', '=', 'doctorspeciality.F_specialityID')
                ->where('doctorspeciality.F_doctorID', $doctor_id)
                ->select('id', 'name')
                ->get();
        return $Condition;
    }

    function get_services($doctor_id) {
        $services = DB::table('speciality')
                ->join('doctor_services', 'speciality.specialityID', '=', 'doctor_services.specialty_id')
                ->join('doctorspeciality', 'speciality.specialityID', '=', 'doctorspeciality.F_specialityID')
                ->where('doctorspeciality.F_doctorID', $doctor_id)
                ->select('services_id', 'service_name')
                ->get();
        return $services;
    }

   //my get condition
    function get_condition_link($doctor_id) {
        $Condition = DB::table('doctor_condition')
                ->join('doctor_condition_view', 'doctor_condition.id', '=', 'doctor_condition_view.condition_id')
                ->where('doctor_condition_view.doctor_id', $doctor_id)
              ->select('doctor_condition.id','name', 'doctor_condition_view.id AS fkid')
                ->groupBy('name')
                ->get();
        return $Condition;
    }

    //get my services
    function get_services_link($doctor_id) {
        $services = DB::table('doctor_services')
                ->join('doctor_services_view', 'doctor_services.services_id', '=', 'doctor_services_view.services_id')
                ->where('doctor_services_view.doctor_id', $doctor_id)
                ->select('doctor_services.services_id','service_name', 'doctor_services_view.id')
                ->groupBy('service_name')
                ->get();
        return $services;
    }


    function addSpacialities($doctor_id, $add_spaciality_id) {
        foreach ($add_spaciality_id as $ID) {
            $spa = DB::table('doctorspeciality')
                    ->where('F_specialityID', $ID)
                    ->where('F_doctorID', $doctor_id)
                    ->where('type', 'S')
                    ->first();

            if ($spa == null) {

                $insert = new Doctorspeciality();
                $insert->F_doctorID = $doctor_id;
                $insert->F_specialityID = $ID;
                $insert->type = 'S';
                $insert->save();
            }
        }
        return 0;
    }

    function removeSpacialities($doctor_id, $remove_spaciality_id) {
        foreach ($remove_spaciality_id as $ID) {
            $spa = DB::table('doctorspeciality')
                    ->where('id_s_d', $ID)
                    ->where('F_doctorID', $doctor_id)
                    ->where('type', 'S')
                    ->first();

            if ($spa != null) {
                DB::table('doctorspeciality')
                        ->where('id_s_d', $ID)
                        ->where('F_doctorID', $doctor_id)
                        ->where('type', 'S')
                        ->delete();
            }
        }
        return 0;
    }

    function viewAllSpeciality($id) {
        $doctorid = $id;
        $doctor = DB::table('doctor')->where('doctorID', $doctorid)->first();
        if ($doctor != null) {
            $services = DB::table('doctor')
                    ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                    ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                    ->select('id_s_d', 'specialityName', 'photo', 'specialityID')
                    ->where('doctor.doctorID', $id)
                    ->where('doctorspeciality.type', 'S')
                    ->get();
            return response()->json($services, 201);
        } else {
            return response()->json(['errors' => 'doctor id is invalid'], 400);
        }
    }

    function deleteSpeciality(Request $request) {
         $speciality_id = $request->speciality_id;
         $doctor_id = $request->doctor_id;

        $ser = DB::table('doctorspeciality')->where('id_s_d', $speciality_id)->first();
        if ($ser != null) {
            $ser = DB::table('doctorspeciality')->where('id_s_d', $speciality_id)->delete();

            $MyPrimaryS = $this->myPrimaryspaciality($doctor_id);
            $speciality = $this->get_speciality($doctor_id);
            $Condition = $this->get_condition($doctor_id);
            $services = $this->get_services($doctor_id);

            $all_Condition = $this->get_condition_link($doctor_id);
            $all_services = $this->get_services_link($doctor_id);

            return response()->json(['P_Speciality' => $MyPrimaryS,'S_Speciality' => $speciality, 'Condition' => $Condition, 'services' => $services, 'my_Condition' => $all_Condition, 'my_services' => $all_services], 201);
        } else {
            return response()->json(['errors' => 'speciality id is invalid'], 400);
        }
    }

}
