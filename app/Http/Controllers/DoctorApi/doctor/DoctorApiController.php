<?php

namespace App\Http\Controllers\DoctorApi\doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Doctor;
use DB;
use App\Rating;
use Carbon\Carbon;
use Image;
use File;
use App\Socialmedia;
use App\Visitingtime;
use App\Appointment;
use App\Events\PatientEvent;
use App\Doctorspeciality;
use App\Speciality;
use App\DoctorService;
use App\Doctorexperiance;
use App\Doctoreduction;
use App\DoctorNotification;

class DoctorApiController extends Controller {

    public function getDoctorInfo($id) {

        $Doctors = DB::table('doctor')->select('doctorID', 'title', 'doctor_name', 'doctor_picture', 'experience', 'doctor_Short_Biography')->where('doctorID', $id)->get();
        foreach ($Doctors as $Doctor) {
            $doctor_education = DB::table('doctor_education')->where('D_FID', $id)->get();
            $Doctor->doctor_education = $doctor_education;

            $my_clinic = $this->get_doctor_my_clinic($id);
            $Doctor->my_clinic = $my_clinic;

            $my_feedback = $this->get_doctor_my_feedback($id);
            $Doctor->my_feedback = $my_feedback;

            $MyPrimary = $this->myPrimaryspaciality($id);
            $Doctor->MyPrimary = $MyPrimary;


            $speciality = $this->get_speciality($id);
            $Doctor->speciality = $speciality;

            $my_Condition = $this->get_condition_link($id);
            $Doctor->my_Condition = $my_Condition;

            $my_services = $this->get_services_link($id);
            $Doctor->my_services = $my_services;

            $patient_experience = $this->get_doctor_feedback_patient_experience($id);
            $Doctor->patient_experience = $patient_experience;
            
            $appointments_count = DB::table('appointment')->where('doctor_id', $id)->count();
            $Doctor->total_patient = (string)$appointments_count;
            
            $count_view_account = DB::table('doctor_view')->where('doctor_id', $id)->count();
            $Doctor->view_account = (string)$count_view_account;
            
            $total = DB::table('feedback')->where('feedback_doctor_id', $id)->count();
            $Doctor->total_review = (string)$total;
        }

        return response()->json($Doctors);
    }
      function get_doctor_feedback_patient_experience($id) {
        $total = DB::table('feedback')->where('feedback_doctor_id', $id)->where('comment', '!=', '')->count();
        if ($total != 0) {
            $feedbacks = DB::table('feedback')->select('date', 'comment', 'feedback_patient_id', 'feedback_user_id', 'overall_experience', 'doctor_checkup', 'staff_behavior', 'clinic_environment')->where('feedback_doctor_id', $id)->where('comment', '!=', '')->get();
            foreach ($feedbacks as $feedback) {
                $star = $feedback->overall_experience + $feedback->overall_experience + $feedback->doctor_checkup + $feedback->staff_behavior + $feedback->clinic_environment;
                $x = number_format($star);
                $feedback->star = $x;
                if ($feedback->feedback_patient_id == 0) {
                    $user = DB::table('users')->select('name', 'pic')->where('id', $feedback->feedback_user_id)->first();
                } else {
                    $user = DB::table('pateint')->select('p_name As name', 'p_photo as pic')->where('patientID', $feedback->feedback_patient_id)->first();
                }
                $feedback->user = $user;
            }
        } else {
            $feedbacks = 'no comment';
        }
        return $feedbacks;
    }


    // function get_doctor_feedback_patient_experience($id) {
    //     $total = DB::table('feedback')->where('feedback_doctor_id', $id)->where('comment', '!=', '')->count();
    //     if ($total != 0) {
    //         $feedbacks = DB::table('feedback')->select('date', 'comment', 'feedback_user_id', 'overall_experience', 'doctor_checkup', 'staff_behavior', 'clinic_environment')->where('feedback_doctor_id', $id)->where('comment', '!=', '')->get();
    //         foreach ($feedbacks as $feedback) {
    //             $star=$feedback->overall_experience + $feedback->overall_experience + $feedback->doctor_checkup + $feedback->staff_behavior + $feedback->clinic_environment;
    //             $x= number_format($star);
    //             $feedback->star = $x;
    //             $user = DB::table('users')->select('name', 'pic')->where('id', $feedback->feedback_user_id)->first();
    //             $feedback->user = $user;
    //         }
           


    //     } else {
    //         $feedbacks = 'no comment';
    //     }
    //     return $feedbacks;
    // }

    function myPrimaryspaciality($doctor_id) {
        $MyPrimaryS = DB::table('doctor')
                ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                ->select('id_s_d', 'specialityName', 'photo', 'specialityID')
                ->where('doctor.doctorID', $doctor_id)
                ->where('doctorspeciality.type', 'P')
                ->get();
        return $MyPrimaryS;
    }

    function get_speciality($doctor_id) {
        $speciality = DB::table('doctor')
                ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                ->select('id_s_d', 'specialityName', 'photo', 'specialityID')
                ->where('doctor.doctorID', $doctor_id)
                ->where('doctorspeciality.type', 'S')
                ->get();
        return $speciality;
    }

//my get condition
    function get_condition_link($doctor_id) {
        $Condition = DB::table('doctor_condition')
                ->join('doctor_condition_view', 'doctor_condition.id', '=', 'doctor_condition_view.condition_id')
                ->where('doctor_condition_view.doctor_id', $doctor_id)
                ->select('doctor_condition.id', 'name', 'doctor_condition_view.id AS fkid')
                ->groupBy('name')
                ->get();
        return $Condition;
    }

    //get my services
    function get_services_link($doctor_id) {
        $services = DB::table('doctor_services')
                ->join('doctor_services_view', 'doctor_services.services_id', '=', 'doctor_services_view.services_id')
                ->where('doctor_services_view.doctor_id', $doctor_id)
                ->select('doctor_services.services_id', 'service_name', 'doctor_services_view.id')
                ->groupBy('service_name')
                ->get();
        return $services;
    }

    function get_doctor_my_feedback($id) {
        $total = DB::table('feedback')->where('feedback_doctor_id', $id)->count();
        if ($total != 0) {
            $overall_experience_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('overall_experience', 1)->count();
            $overall_experience = ($overall_experience_1 / $total) * 100;

            $doctor_checkup_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('doctor_checkup', 1)->count();
            $doctor_checkup = ($doctor_checkup_1 / $total) * 100;

            $staff_behavior_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('staff_behavior', 1)->count();
            $staff_behavior = ($staff_behavior_1 / $total) * 100;

            $clinic_environment_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('clinic_environment', 1)->count();
            $clinic_environment = ($clinic_environment_1 / $total) * 100;

            $star = ($overall_experience_1 + $overall_experience_1 + $doctor_checkup_1 + $staff_behavior_1 + $clinic_environment_1) / $total;

            $overall_experiences = number_format($overall_experience, 1);
            $doctor_checkups = number_format($doctor_checkup, 1);
            $staff_behaviors = number_format($staff_behavior, 1);
            $clinic_environments = number_format($clinic_environment, 1);
            $stars = number_format($star, 1);

            $feedback_Collection = collect([['Stars' => $stars, 'overall_experience' => $overall_experiences, 'doctor_checkup' => $doctor_checkups, 'staff_behavior' => $staff_behaviors, 'clinic_environment' => $clinic_environments]]);
        } else {
            $feedback_Collection = collect([['Stars' => 5, 'overall_experience' => 100, 'doctor_checkup' => 100, 'staff_behavior' => 100, 'clinic_environment' => 100]]);
        }
        return $feedback_Collection;
    }

    function get_doctor_my_clinic($id) {

        $address = DB::table('address')
                ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                ->where('address_doctor_view.doctor_id', $id)
                ->select('address_id', 'name', 'address')
                ->groupby('name')
                ->get();
        return $address;
    }

    // function getNotificationList($doctor_id) {
    //     $noti = DB::table('doctor_notification')
    //             ->where('doctor_id', $doctor_id)
    //             ->select('id', 'doctor_id', 'text', 'status', 'date')
    //             ->orderBy('id', 'DESC')
    //             ->get();
    //     return response()->json($noti, 201);
    // }
  function getNotificationList($doctor_id) {
        
        $count = DB::table('doctor_notification')
                ->where('doctor_id', $doctor_id)
                ->select('id', 'doctor_id', 'text', 'status', 'date')
                ->orderBy('id', 'DESC')
                ->count();

        $notification = DB::table('doctor_notification')->where('doctor_id', $doctor_id)->select('id', 'doctor_id', 'user_id', 'patient_id', 'text', 'status', 'date', 'is_chat')->orderBy('id', 'DESC')->paginate(20);
        foreach ($notification as $noti) {
            if ($noti->patient_id == 0) {
                $user = DB::table('users')->where('id', $noti->user_id)->first();
                $noti->name = $user->name;
            } else {
                $pateint = DB::table('pateint')->where('patientID', $noti->patient_id)->first();
                $noti->name = $pateint->p_name;
            }
        }
        return response()->json($notification,201);
    }
    function getuUreadNotificationList($doctor_id) {
        $noti = DB::table('doctor_notification')
                ->where('doctor_id', $doctor_id)
                ->where('status', 0)
                ->select('id', 'doctor_id', 'text', 'status', 'date')
                ->orderBy('id', 'DESC')
                ->get();
        return response()->json($noti, 201);
    }

    function readNotification($noti_id) {
        $update = DB::table('doctor_notification')->where('id', $noti_id)->update(['status' => 1]);
        return response()->json('done!', 201);
    }

    function submitProProfile($id) {
        ///by default it 0
        ///when submit the value is 1 and is in pendding mode
        ///when the profile as been review and confirm the value is 2 and user is allow to access all pachage
        DB::table('doctor')->where('doctorID', $id)
                ->update(['under_review' => 1
        ]);
        $status_review = DB::table('doctor')->select('under_review')->where('doctorID', $id)->get();

        return response()->json($status_review, 201);
    }

    public function signup(Request $request) {

        $data = ['title' => 'required', 'name' => 'required', 'gender' => 'required', 'city' => 'required', 'email' => 'required', 'password' => 'min:6', 'confirm_pass' => 'required_with:password|same:password|min:6'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $is_doctor = DB::table('doctor')->where('doctor_email', $request->email)->first();
            $is_user = DB::table('users')->where('email', $request->email)->first();

            if ($is_doctor != null || $is_user != null) {
                $mess = 'The Email ' . $request->email . ' already registerd to system!';
                return response()->json($mess, 400);
            } else {

                $doctor = new Doctor();
                $doctor->title = $request->title;
                $doctor->doctor_name = $request->name;
                $doctor->doctor_gender = $request->gender;
                $doctor->city = $request->city;
                $doctor->doctor_email = $request->email;
                $doctor->doctor_status = 0;
                $doctor->email_verification = 0;
                $doctor->email_verification_token = Str::random(60);
                $doctor->doctor_password = $request->password;
                $doctor->save();

                $doctor_info = DB::table('doctor')->orderBy('doctorID', 'DESC')->first();
                //send Email
                $doctorID = $doctor_info->doctorID;
                $Email_Token = $doctor_info->email_verification_token;
              //  $url = url('/email-active', [$doctorID, $Email_Token]);
             //  $url = route('email.active',$doctorID, $Email_Token,\App::getLocale());
              $url = route('email.active', ['language'=>'en',$doctorID, $Email_Token]);
               //  dd($url);
                $to_name = $request->name;
                $to_email = $request->email;

                $data = array("name" => $request->name, "body" => "Please Confirm Your Acount", "active_link" => $url);
                Mail::send('public.Email.mail', $data, function($message) use ($to_name, $to_email) {
                    $message->to($to_email)
                            ->subject('DoctorPlus Email Confirmation');
                    // $message->from('habibandkh@gmail.com','Test Mail');
                });
                
          //insert a row in feedback
            $user = new Rating();
            $user->feedback_doctor_id = $doctorID;
           // $user->feedback_user_id = 0;
            $user->overall_experience = '1';
            $user->doctor_checkup = '1';
            $user->staff_behavior = '1';
            $user->clinic_environment = '1';
          //  $user->comment = '';
            $user->save();
            
                return response()->json(['email_verification' => 'we just send a confirmation message to your email address', 'doctor_info' => $doctor_info], 201);
            }
        }
    }

    public function doctorUpdatePass(Request $request) {


        $doctor_info = DB::table('doctor')->where('doctor_email', $request->email_address)->first();


        if ($doctor_info != null) {

           
            $doctorID = $doctor_info->doctorID;
            $Email_Token = $doctor_info->email_verification_token;
           
            $url = route('email.reset', ['language' => 'en', $doctorID, $Email_Token]);
            //  dd($url);
            $to_name = $doctor_info->doctor_name;
            $to_email = $request->email_address;

            $data = array("name" => $to_name, "body" => "Rest Password", "active_link" => $url);
            Mail::send('public.ResetPassword.reset', $data, function($message) use ($to_name, $to_email) {
                $message->to($to_email)
                        ->subject('DoctorPlus Reset Password');
                // $message->from('habibandkh@gmail.com','Test Mail');
            });

            return response()->json(['email_verification' => 'We Just send a link to your email address to reset your password'], 201);
        } else {
            $mess = 'The Email ' . $request->email_address . ' not registerd with system!';
            return response()->json($mess, 400);
        }
    }

    public function signin(Request $request) {
        $email = $request->email;
        $password = $request->password;
        $data = ['email' => 'required|Email', 'password' => 'required'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {


            $is_doctor = DB::table('doctor')->where('doctor_email', $email)->where('doctor_password', $password)->first();
            if ($is_doctor) {
                $email_verification = DB::table('doctor')->select('email_verification')->where('doctor_email', $email)->where('doctor_password', $password)->first();

                if ($email_verification->email_verification == 0) {
                    return response()->json(['errors' => 'please confirm your Email to active your account'], 400);
                } else {
                    $url = asset('images/doctor/');
                } return response()->json(['doctor_info' => $is_doctor, 'pic_link' => $url], 200);
            } else {
                return response()->json('Incorrect Email or Password', 400);
            }
        }
    }

    public function viewDoctorProfile($id) {

        $doctor = DB::table('doctor')->where('doctorID', $id)->select('doctorID', 'title', 'city', 'doctor_name', 'doctor_picture', 'lang', 'under_review', 'professional_profile_status', 'experience', 'Expert', 'doctor_Short_Biography', 'doctor_last_name', 'doctor_gender', 'doctor_fee', 'doctor_regDate', 'doctor_status', 'district', 'doctor_clinicAddress')->first();

        if ($doctor != null) {
            return response()->json(['doctor_info' => $doctor], 200);
        } else {
            return response()->json(['errors' => 'doctor id is invalid'], 400);
        }
    }


    public function updateDoctorProfile(Request $request) {

        $doctorid = $request->doctor_id;
        $doctor_title = $request->title;
        $doctor_name = $request->name;
        $doctor_last_name = $request->last_name;
        $doctor_father_name = $request->father_name;
       // $doctor_email = $request->email;
        $doctor_phone = $request->phone;
        $doctor_gender = $request->gender;
        $doctor_city = $request->city;
        $doctor_fee = $request->fee;
        $doctor_birthday = $request->birthday;
        $doctor_short_biography = $request->short_biography;
        $doctor_Experience = $request->Experience;
        //$doctor_Expert = $request->primary_specialty;


        $data = [
            'name' => 'required'
            // , 'last_name' => 'required'
            //  , 'father_name' => 'required'
            , 'title' => 'required'
            , 'phone' => 'required|max:10'
            , 'gender' => 'required'
            , 'city' => 'required'
            , 'fee' => 'required|integer'
            , 'birthday' => 'required'
            //  , 'short_biography' => 'max:100000'
            , 'Experience' => 'required|integer'
            , 'primary_specialty' => 'required'
        ];

        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            if ($request->hasFile('photo')) {

                $image = $request->file('photo');
                $image_name = 'doctor-' . time() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('/images/doctor/'), $image_name);
                $update = DB::table('doctor')->where('doctorID', $doctorid)->update(['doctor_picture' => $image_name]);
            }
            $update = DB::table('doctor')
                    ->where('doctorID', $doctorid)
                    ->update([
                'title' => $doctor_title,
                'doctor_name' => $doctor_name,
                'doctor_last_name' => $doctor_last_name,
                'doctor_father_name' => $doctor_father_name,
                'doctor_birthday' => $doctor_birthday,
                'doctor_gender' => $doctor_gender,
                //'doctor_email' => $doctor_email,
                'doctor_phoneNo' => $doctor_phone,
                'city' => $doctor_city,
                'doctor_fee' => $doctor_fee,
                'doctor_Short_Biography' => $doctor_short_biography,
                'experience' => $doctor_Experience,
                //'Expert' => $doctor_Expert,
            ]);
            return response()->json($update, 201);
        }
    }

    function forgetPassword(Request $req) {

        $email = $req->email_address;
        $doctor = DB::table('doctor')->where('doctor_email', $email)->first();
        if ($doctor != null) {

            $to_name = $doctor->doctor_name;
            $to_email = $doctor->doctor_email;
            $pass = $doctor->doctor_password;


            $data = array("name" => $to_name, "password" => $pass);
            Mail::send('public.Email.forgetpass', $data, function($message) use ($to_name, $to_email) {
                $message->to($to_email)
                        ->subject('DoctorPlus Email Confirmation');
                // $message->from('phd@focus-web.org', 'DoctorPlus');
            });
            return response()->json(['we send the password to your email address'], 201);
        } else {
            return response()->json(['the email not match!'], 400);
        }
    }

//    function deleteDoctor($id) {
//        DB::table('doctorspeciality')->where('F_doctorID', $id)->delete();
//        DB::table('doctor_education')->where('D_FID', $id)->delete();
//        DB::table('doctor_services')->where('doctor_s_id', $id)->delete();
//        DB::table('doctor_experience')->where('doctor_FID', $id)->delete();
//        DB::table('doctor')->where('doctorID', $id)->delete();
//
//        return redirect()->route('doctor.index')->with('message', 'Doctor account deleted successfully');
//    }

    function getDoctorPmdc($id) {
        $doctor = DB::table('doctor')->where('doctorID', $id)->first();
        if ($doctor != null) {
            $pmdc = $doctor->pmdc;
            $image = $doctor->doctor_picture;
            return response()->json(['pmdc_number' => $pmdc, 'image' => $image], 200);
        } else {
            return response()->json(['errors' => 'doctor id is invalid'], 400);
        }
    }

    function updateDoctorPmdc(Request $request) {
        $data = ['doctor_id' => 'required|integer', 'pmdc_number' => 'required'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $doctorid = $request->doctor_id;
            $pmdc = $request->pmdc_number;

            $doctor = DB::table('doctor')->where('doctorID', $doctorid)->first();

            if ($doctor != null) {
                $image = $request->file('doctor_image');
                if ($image != null) {
                    $new_name = rand() . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('/images/doctor/'), $new_name);

                    DB::table('doctor')->where('doctorID', $doctorid)
                            ->update(['pmdc' => $pmdc,
                                'doctor_picture' => $new_name,
                    ]);
                } else {
                     DB::table('doctor')->where('doctorID', $doctorid)
                            ->update(['pmdc' => $pmdc,
                               
                    ]);
                }
                return response()->json(['success' => 'pmdc updated!'], 201);
            } else {
                return response()->json(['errors' => 'doctor id is invalid'], 400);
            }
        }
    }

}
