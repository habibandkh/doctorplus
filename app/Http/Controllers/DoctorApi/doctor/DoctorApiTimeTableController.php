<?php

namespace App\Http\Controllers\DoctorApi\doctor;

use App\Http\Controllers\Controller;
use App\Visitingtime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Illuminate\Support\Carbon;
use DB;
use App\Http\Controllers\NotificationController;

class DoctorApiTimeTableController extends Controller {

    public function createVisitTime(Request $request) {

        $data = ['doctor_id' => 'required|integer'
            , 'start_time' => 'required|date_format:H:i'
            , 'end_time' => 'required|date_format:H:i|after:start_time'
            , 'Interval_time' => 'required|numeric'
            , 'clinic_address_id' => 'required|numeric'
            , 'days' => 'required'
        ];

        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $doctor_id = $request->doctor_id;
            $clinic_address_id = $request->clinic_address_id;
            $from = $request->start_time;
            $to = $request->end_time;
            $days = $request->days;
            $interval = (int) $request->Interval_time;

//check if there already created timetable !
//$is_day_avail = DB::table('visitingtime')->whereBetween('visit_time', [$from, $to])->where('doctorID', $doctor_id)->first();

            $is_day_avail = DB::table('visitingtime')
                    ->whereIn('day', $days)
                    ->where('doctorID', $doctor_id)
                    ->where('doctor_active', 1)
                   // ->where('clinic_address_id', $clinic_address_id)
                    ->whereBetween('visit_time', [$from, $to])
                    ->select('day')
                    ->groupBy('day')
                    ->get();


            if (!$is_day_avail->isEmpty()) {
                foreach ($is_day_avail as $value) {
                    $values[] = $value->day;
                    $is_day = implode(',', $values);
                }
                return response()->json('you already created a Time-Slot for This days: ' . $is_day . '', 400);
            } else {
                $res = $this->createtimetable($days, $doctor_id, $from, $to, $interval, $clinic_address_id);
                return response()->json('timetable created ', 201);
            }
        }
    }

    function createtimetable($days, $doctor_id, $from, $to, $interval, $clinic_address_id) {
        $x = $this->insertFromTime($days, $doctor_id, $from, $clinic_address_id);

        $y = $this->insertByTimeInterval($days, $doctor_id, $from, $to, $interval, $clinic_address_id);

        return 0;
    }

    function insertFromTime($days, $doctor_id, $from, $clinic_address_id) {

        foreach ($days as $day) {

            $user = new Visitingtime();
            $user->doctorID = $doctor_id;
            $user->day = $day;
            $user->visit_time = $from;
            $user->doctor_active = "1";
            $user->time_off_on = "1";
            $user->clinic_address_id = $clinic_address_id;
            $user->save();
        }
        return 0;
    }

    // function insertByTimeInterval($days, $doctor_id, $from, $to, $interval, $clinic_address_id) {

    //     $timestamp = 0;
    //     $time = "0";
    //     for ($i = 1; $i <= 2100; $i++) {
    //         if (date('H:i', strtotime($from)) < date('H:i', strtotime($to))) {
    //             $timestamp = strtotime($from) + $interval;
    //             $time = date('H:i', $timestamp);

    //             foreach ($days as $day) {

    //                 $user = new Visitingtime();
    //                 $user->doctorID = $doctor_id;
    //                 $user->day = $day;
    //                 $user->visit_time = $time;
    //                 $user->clinic_address_id = $clinic_address_id;
    //                 $user->doctor_active = "1";
    //                 $user->time_off_on = "1";
    //                 $user->save();
    //             }
    //         }
    //         $from = $time;
    //     }
    //     return 0;
    // }
    
        function insertByTimeInterval($days, $doctor_id, $from, $to, $interval, $clinic_address_id) {
        $date = now()->format('yy-m-d');
        $date_And_time = $date . ' ' . $from;
        $base = Carbon::parse($date_And_time);

        $timestamp = 0;
        $time = "0";
        $my_minute = $interval / 60;
        for ($i = 1; $i <= 2100; $i++) {
              
            $my_custom_date_plus_interval = $base->copy()->addMinutes($my_minute)->toDateTimeString();
            if (date('Y-m-d', strtotime($my_custom_date_plus_interval)) <= date('Y-m-d', strtotime($date))) {
                if (date('H:i', strtotime($from)) < date('H:i', strtotime($to))) {
                    $timestamp = strtotime($from) + $interval;
                    $time = date('H:i', $timestamp);

                    foreach ($days as $day) {

                        $user = new Visitingtime();
                        $user->doctorID = $doctor_id;
                        $user->day = $day;
                        $user->visit_time = $time;
                        $user->clinic_address_id = $clinic_address_id;
                        $user->doctor_active = "1";
                        $user->time_off_on = "1";
                        $user->save();
                    }
                }
            }
             $from = $time;
             $my_minute = $my_minute+($interval / 60);
        }
        return 0;
    }

//    public function viewTimetable($id) {
//        $address_count = DB::table('address')
//                ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
//                ->where('address_doctor_view.doctor_id', $id)
//                ->count();
//        if ($address_count != 0) {
//            $address = DB::table('address')
//                    ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
//                    ->where('address_doctor_view.doctor_id', $id)
//                    ->get();
//
//            foreach ($address as $addres) {
//
//                $address_id = $addres->address_id;
//
//                $Mon = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Monday')->orderBy('visit_time', 'asc')->get();
//                $Tue = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Tuesday')->orderBy('visit_time', 'asc')->get();
//                $Wed = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Wednesday')->orderBy('visit_time', 'asc')->get();
//                $Thu = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Thursday')->orderBy('visit_time', 'asc')->get();
//                $Fri = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Friday')->orderBy('visit_time', 'asc')->get();
//                $Sat = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Saturday')->orderBy('visit_time', 'asc')->get();
//                $Sun = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Sunday')->orderBy('visit_time', 'asc')->get();
//
//                $weak_Collection[] = collect(
//                        [[
//                        'hospital_name' => $addres->name
//                        , 'hospital_address' => $addres->address
//                        , 'Monday' => $Mon
//                        , 'Tuesday' => $Tue
//                        , 'Wednesday' => $Wed
//                        , 'Thursday' => $Thu
//                        , 'Friday' => $Fri
//                        , 'Saturday' => $Sat
//                        , 'Sunday' => $Sun
//                ]]);
//            }
//            return response()->json(['doctor_timetable' => $weak_Collection], 201);
//        } else {
//            return response()->json('no data', 201);
//        }
//    }
    function viewTimetable(Request $request) {

        $id = $request->doctor_id;
        $address_id = $request->clinic_id;
        $address = DB::table('address')
                ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                ->where('address_doctor_view.doctor_id', $id)
                ->where('address_doctor_view.address_id', $address_id)
                ->first();

        if ($address != null) {

            $Mon = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Monday')->orderBy('visit_time', 'asc')->get();
            $Tue = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Tuesday')->orderBy('visit_time', 'asc')->get();
            $Wed = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Wednesday')->orderBy('visit_time', 'asc')->get();
            $Thu = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Thursday')->orderBy('visit_time', 'asc')->get();
            $Fri = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Friday')->orderBy('visit_time', 'asc')->get();
            $Sat = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Saturday')->orderBy('visit_time', 'asc')->get();
            $Sun = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id)->where('doctorID', '=', $id)->where('day', '=', 'Sunday')->orderBy('visit_time', 'asc')->get();

            return response()->json(['hospital_name' => $address->name, 'hospital_address' => $address->address, 'Monday' => $Mon, 'Tuesday' => $Tue, 'Wednesday' => $Wed, 'Thursday' => $Thu, 'Friday' => $Fri, 'Saturday' => $Sat, 'Sunday' => $Sun], 201);
        } else {
            return response()->json('no data', 201);
        }
    }

    public function dayStatus(Request $request) {

        $data = [
            'doctor_id' => 'required|integer'
            , 'clinic_address_id' => 'required|integer'
            , 'day' => 'required'
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $doctorid = $request->doctor_id;
            $clinic_address_id = $request->clinic_address_id;
            $day = $request->day;

            $id = Visitingtime::select('doctor_active')->where('doctorID', '=', $doctorid)->where('clinic_address_id', '=', $clinic_address_id)->where('day', '=', $day)->first();

            if ($id->doctor_active == 1) {

                $time_status = Visitingtime::where('doctorID', '=', $doctorid)->where('clinic_address_id', '=', $clinic_address_id)->where('day', '=', $day)->update(array('doctor_active' => 0));
               // $date_now = now()->format('yy-m-d');
            $appointment = DB::table('appointment')
                    ->where('doctor_id', $doctorid)
                    ->where('clinic_address_id', $clinic_address_id)
                    ->where('Status', 0)
                    ->where('day', $day)
                    ->where('appointment_date', '>=', $day)
                    ->get();

            if (!$appointment->isEmpty()) {
                foreach ($appointment as $appo) {
                    $cancel = DB::table('appointment')->where('appID', $appo->appID)->update(['Status' => 1, 'cancel_by_doctor' => 1]);
                    $noti = new NotificationController();
                    $res = $noti->appCancelNoti_cancel_by_doctor($appo);
                }
            }
            } else {

                $time_status = Visitingtime::where('doctorID', '=', $doctorid)->where('day', '=', $day)->update(array('doctor_active' => 1));
            }
         
        }
        return response()->json('', 201);
    }

    public function timeStatus(Request $request) {
        $data = [
            'vtID' => 'required|integer'
            , 'status' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $vtid = $request->vtID;
            $status = $request->status;
            $check = Visitingtime::select('*')->where('vtID', $vtid)->first();

            if ($check != null) {
                $x = $this->timeStatusUpdate($status, $vtid);
                return response()->json('updated', 201);
            } else {
                return response()->json('record not match!', 400);
            }
        }
    }

    public function timeStatusUpdate($status, $vtid) {

        if ($status == 1) {
            $Dayshow = Visitingtime::where('vtID', $vtid)->update(array('time_off_on' => 0));
        } else {
          
         
            $Dayshow = Visitingtime::where('vtID', '=', $vtid)->update(array('time_off_on' => 1));
           
        }
        return 0;
    }

    public function deleteTimatable(Request $request) {
        $data = [
            'doctor_id' => 'required|integer'
            , 'clinic_address_id' => 'required|integer'
        ];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $doctorid = $request->doctor_id;
            $clinic_address_id = $request->clinic_address_id;
            $id = Visitingtime::select('*')->where('doctorID', '=', $doctorid)->where('clinic_address_id', '=', $clinic_address_id)->first();
            if ($id != null) {
                $x = $this->deleteTimatableNow($doctorid, $clinic_address_id);
                return response()->json('timetable deleted!', 200);
            } else {
                return response()->json('record not match!', 400);
            }
        }
    }

   public function deleteTimatableNow($doctorid, $clinic_address_id) {
        $date_now = now()->format('yy-m-d');
        ////delete the timetable
        $x=DB::table('visitingtime')->where('doctorID', $doctorid)->where('clinic_address_id', $clinic_address_id)->delete();
        ////send notification to the 
        $appointment = DB::table('appointment')
                ->where('doctor_id', $doctorid)
                ->where('clinic_address_id', $clinic_address_id)
                ->where('Status', 0)
                ->where('appointment_date', '>=', $date_now)
                ->get();
        
        if (!$appointment->isEmpty()) {
            foreach ($appointment as $appo) {
                $cancel = DB::table('appointment')->where('appID', $appo->appID)->update(['Status' => 1, 'cancel_by_doctor' => 1]);
                $noti = new NotificationController();
                $res = $noti->appCancelNoti_cancel_by_doctor($appo);
            }
        }
        return 0;
    }

}
