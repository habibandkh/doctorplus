<?php

namespace App\Http\Controllers\DoctorApi\doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Clinic;
use App\Clinic_address;
use Carbon\Carbon;

class DoctorApiClinicController extends Controller {

    // function addClinic(Request $request) {
    //     $data = ['doctor_id' => 'required|integer', 'name' => 'required', 'address' => 'required', 'city' => 'required'];
    //     $validator = Validator::make($request->all(), $data);
    //     if ($validator->fails()) {
    //         return response()->json(['errors' => $validator->errors()], 400);
    //     } else {
    //         $doctorid = $request->doctor_id;
    //         $doctor = DB::table('doctor')->where('doctorID', $doctorid)->first();
    //         if ($doctor != null) {
    //             $clinic = new Clinic();
    //             $clinic->name = $request->name;
    //             $clinic->address = $request->address;
    //             $clinic->city = $request->city;
    //             $clinic->doctor_id = $doctorid;
    //             $clinic->save();

    //             $address_id = DB::table('address')->where('doctor_id', $doctorid)->orderBy('id', 'DESC')->first();

    //             $add = new Clinic_address();
    //             $add->address_id = $address_id->id;
    //             $add->doctor_id = $doctorid;
    //             $add->save();

    //             return response()->json(['success' => 'clinic  added!'], 201);
    //         } else {
    //             return response()->json(['errors' => 'doctor id is invalid'], 400);
    //         }
    //     }
    // }
 function addClinic(Request $request) {
        $data = ['doctor_id' => 'required|integer', 'name' => 'required', 'address' => 'required', 'city' => 'required'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $doctorid = $request->doctor_id;
            $count_clinic = DB::table('address_doctor_view')->where('doctor_id', $doctorid)->count();

            //you can add more then for clinic

            if (!($count_clinic > 3)) {
                $clinic = new Clinic();
                $clinic->name = $request->name;
                $clinic->address = $request->address;
                $clinic->city = $request->city;
                $clinic->doctor_id = $doctorid;
                $clinic->save();

                $address_id = DB::table('address')->where('doctor_id', $doctorid)->orderBy('id', 'DESC')->first();

                $add = new Clinic_address();
                $add->address_id = $address_id->id;
                $add->doctor_id = $doctorid;
                $add->save();

                return response()->json(['success' => 'clinic  added!'], 201);
            } else {
                return response()->json(['errors' =>  'you can not add more then 4 clinic'], 400);
            }
        }
    }
    function viewAllClinic() {
        $clinic = DB::table('address')->get();
        return response()->json($clinic, 201);
    }

    function updateClinic(Request $request) {
        $data = ['clinic_id' => 'required|integer', 'name' => 'required', 'address' => 'required', 'city' => 'required'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {

            $clinic_id = $request->clinic_id;
            $edu = DB::table('address')->where('id', $clinic_id)->first();
            if ($edu != null) {
                DB::table('address')->where('id', $clinic_id)
                        ->update([
                            'name' => $request->name,
                            'address' => $request->address,
                            'city' => $request->city]);
                return response()->json(['success' => 'clinic updated!'], 201);
            } else {
                return response()->json(['errors' => 'clinic id is invalid'], 400);
            }
        }
    }

    function deleteClinic($id) {

        $edu = DB::table('address')->where('id', $id)->first();
        if ($edu != null) {
            $edu = DB::table('address')->where('id', $id)->delete();
            return response()->json(['success' => 'clinic deleted!'], 201);
        } else {
            return response()->json(['errors' => 'clinic id is invalid'], 400);
        }
    }

    function viewMyClinic($id) {
        $address = DB::table('address')
                ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                ->where('address_doctor_view.doctor_id', $id)
                ->select('address_doctor_view.id', 'name', 'address', 'address_id')
                ->get();
        return response()->json($address, 201);
    }

    // function saveClinic(Request $request) {
    //     $data = ['doctor_id' => 'required|integer', 'clinic_id' => 'required|integer'];
    //     $validator = Validator::make($request->all(), $data);
    //     if ($validator->fails()) {
    //         return response()->json(['errors' => $validator->errors()], 400);
    //     } else {
    //         $doctorid = $request->doctor_id;
    //         $clincid = $request->clinic_id;

    //         $doctor = DB::table('doctor')->where('doctorID', $doctorid)->first();
    //         $my_clinic = DB::table('address')->where('id', $clincid)->first();

    //         if ($doctor != null && $my_clinic != null) {

    //             $add = new Clinic_address();
    //             $add->address_id = $clincid;
    //             $add->doctor_id = $doctorid;
    //             $add->save();

    //             return response()->json(['success' => 'clinic  added!'], 201);
    //         } else {
    //             return response()->json(['errors' => 'doctor id or clinic id  is invalid'], 400);
    //         }
    //     }
    // }
   function saveClinic(Request $request) {
        $data = ['doctor_id' => 'required|integer', 'clinic_id' => 'required|integer'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $doctorid = $request->doctor_id;
            $clincid = $request->clinic_id;

            
            $count_clinic = DB::table('address_doctor_view')->where('doctor_id', $doctorid)->count();
        
            if (!($count_clinic > 3)) {

                $add = new Clinic_address();
                $add->address_id = $clincid;
                $add->doctor_id = $doctorid;
                $add->save();

                return response()->json(['success' => 'clinic  added!'], 201);
            } else {
                  return response()->json(['errors' =>  'you can not add more then 4 clinic'], 400);
            }
        }
    }
    function viewSaveClinic($id) {

        $address = DB::table('address')
                ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                ->join('visitingtime', 'address_doctor_view.address_id', '=', 'visitingtime.clinic_address_id')
                //  ->where('address_doctor_view.doctor_id', $id)
                ->where('visitingtime.doctorID', $id)
                ->select('address_doctor_view.id', 'name', 'address', 'address_id', 'clinic_address_id')
                ->groupby('visitingtime.clinic_address_id')
                ->get();
        foreach ($address as $addr) {
            $from = DB::table('visitingtime')
                    ->select('visit_time')
                    ->where('doctorID', $id)
                    ->where('clinic_address_id', $addr->clinic_address_id)
                    ->min('visit_time');
            $to = DB::table('visitingtime')
                    ->select('visit_time')
                    ->where('doctorID', $id)
                    ->where('clinic_address_id', $addr->clinic_address_id)
                    ->max('visit_time');


            $addr->From = $from;
            $addr->To = $to;
        }
        return response()->json($address, 201);
    }

    function doctor_only_my_created_clinic($id) {

        $address = DB::table('address')
                ->where('doctor_id', $id)
                ->select('id', 'name', 'address')
                ->get();
        return response()->json($address, 200);
    }

    function doctor_only_my_editable_clinic($id) {

        $address = DB::table('address')
                ->join('address_doctor_view', 'address.id', '=', 'address_doctor_view.address_id')
                ->where('address.doctor_id', $id)
                ->select('address.id', 'name', 'address', 'address_id', 'address.doctor_id', DB::raw('count(address_id) as total'))
                ->groupBy('address_id')
                ->having('total', '<', 2)
                ->get();
        return response()->json($address, 200);
    }

    function removeSaveClinic(Request $request) {
        $doctorid = $request->doctor_id;
        $clincid = $request->address_id;

        $edu = DB::table('address_doctor_view')->where('address_id', $clincid)->where('doctor_id', $doctorid)->first();
        if ($edu != null) {
            $res = $this->call_to_remove_or_delete_clinic($doctorid, $clincid);
            return response()->json(['success' => 'clinic Removed!'], 201);
        } else {
            return response()->json(['errors' => 'invalid data!'], 400);
        }
    }

    function call_to_remove_or_delete_clinic($doctorid, $clincid) {

        $edu = DB::table('address_doctor_view')->where('address_id', $clincid)->count();
        if ($edu == 1) {
            $res = $this->call_to_delete_clinic($doctorid, $clincid);
            return 0;
        } else {

            $res = $this->call_to_remove_clinic($doctorid, $clincid);
            return 0;
        }
    }

    function call_to_delete_clinic($doctorid, $clincid) {

        $vis_del = DB::table('visitingtime')->where('doctorID', $doctorid)->where('clinic_address_id', $clincid)->delete();
        $res = $this->callToRemoveAppAndSendNoti($doctorid, $clincid);
        $edu1 = DB::table('address_doctor_view')->where('doctor_id', $doctorid)->where('address_id', $clincid)->delete();
        $edu = DB::table('address')->where('id', $clincid)->delete();

        return 0;
    }

    function call_to_remove_clinic($doctorid, $clincid) {

        $vis_del = DB::table('visitingtime')->where('doctorID', $doctorid)->where('clinic_address_id', $clincid)->delete();
        $res = $this->callToRemoveAppAndSendNoti($doctorid, $clincid);
        $edu = DB::table('address_doctor_view')->where('doctor_id', $doctorid)->where('address_id', $clincid)->delete();
        return 0;
    }

    function callToRemoveAppAndSendNoti($doctorid, $clincid) {
        $now = Carbon::now();
        $today = $now->toDateString();
        $appointments = DB::table('appointment')
                ->where('doctor_id', $doctorid)
                ->where('clinic_address_id', $clincid)
                ->where('cancel_by_user', 0)
                ->where('cancel_by_doctor', 0)
                ->where('appointment_date', '>=', $today)
                ->get();
        foreach ($appointments as $appt) {

            $cancel = DB::table('appointment')
                    ->where('appID', $appt->appID)
                    ->update([
                'Status' => 1,
                'cancel_by_doctor' => 1,
            ]);

            //store notification
            // send notification to user
        }
        return 0;
    }

}
