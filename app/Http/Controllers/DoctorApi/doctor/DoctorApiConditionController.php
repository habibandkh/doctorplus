<?php

namespace App\Http\Controllers\DoctorApi\doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Doctor_condition;
use App\Doctor_condition_view;
use DB;

class DoctorApiConditionController extends Controller {

    function getAllCondition($id) {

        $doctor = DB::table('doctor')->where('doctorID', $id)->first();
        if ($doctor != null) {
            $services = DB::table('speciality')
                    ->join('doctor_condition', 'speciality.specialityID', '=', 'doctor_condition.specialty_id')
                    ->join('doctorspeciality', 'speciality.specialityID', '=', 'doctorspeciality.F_specialityID')
                    ->where('doctorspeciality.F_doctorID', $id)
                    ->select('id', 'name')
                    ->get();
            return response()->json($services, 201);
        } else {
            return response()->json(['errors' => 'doctor id is invalid'], 400);
        }
    }

    function addCondition(Request $request) {
        $data = ['doctor_id' => 'required|integer'];
        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $doctor_id = $request->doctor_id;
            $add_condition_id = $request->add_condition_id;
            $remove_condition_id = $request->remove_condition_id;
            
            if ($add_condition_id[0] != null) {
                $var = $this->add_condition($doctor_id,$add_condition_id);
            }
            if ($remove_condition_id[0] != null) {
                $var1 = $this->remove_condition($doctor_id,$remove_condition_id);
            }
          

            $Condition = DB::table('doctor_condition')
                    ->join('doctor_condition_view', 'doctor_condition.id', '=', 'doctor_condition_view.condition_id')
                    ->where('doctor_condition_view.doctor_id', $doctor_id)
                    ->select('name','doctor_condition_view.id','doctor_condition_view.condition_id')
                    ->groupBy('name')
                    ->get();
            return response()->json($Condition, 201);
        }
    }

    function add_condition($doctor_id, $add_condition_id) {

        foreach ($add_condition_id as $ID) {
                $con = DB::table('doctor_condition_view')
                        ->where('doctor_id', $doctor_id)
                        ->where('condition_id', $ID)
                        ->first();
                if ($con == null) {
                    $insert = new Doctor_condition_view();
                    $insert->doctor_id = $doctor_id;
                    $insert->condition_id = $ID;
                    $insert->save();
                }
            }
        return 0;
    }

    function remove_condition($doctor_id, $remove_condition_id) {
       
          foreach ($remove_condition_id as $ID) {
                $con = DB::table('doctor_condition_view')
                        ->where('doctor_id', $doctor_id)
                        ->where('condition_id', $ID)
                        ->first();
                if ($con != null) {
               DB::table('doctor_condition_view')
                        ->where('doctor_id', $doctor_id)
                        ->where('condition_id', $ID)
                        ->delete();
                }
            }
        return 0;
    }
    function viewMyCondition($id) {

        $doctor = DB::table('doctor')->where('doctorID', $id)->first();
        if ($doctor != null) {
            $services = DB::table('doctor_condition')
                    ->join('doctor_condition_view', 'doctor_condition.id', '=', 'doctor_condition_view.condition_id')
                    ->where('doctor_condition_view.doctor_id', $id)
                    ->select('name', 'doctor_condition_view.id AS fkid')
                    ->groupBy('name')
                    ->get();
            return response()->json($services, 201);
        } else {
            return response()->json(['errors' => 'doctor id is invalid'], 400);
        }
    }

    function deleteCondition($id) {

        $ser = DB::table('doctor_condition_view')->where('id', $id)->first();
        if ($ser != null) {
            $ser = DB::table('doctor_condition_view')->where('id', $id)->delete();
            return response()->json(['success' => 'Condition deleted!'], 201);
        } else {
            return response()->json(['errors' => 'Condition id is invalid'], 400);
        }
    }

}
