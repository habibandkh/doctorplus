<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Events\EndPool;
use DB;
use Illuminate\Http\Request;

class AppoinmentController extends Controller {

    public function index(Request $req) {

        $id = $req->id;
        $obg = new HomeController();
        $id_pateint = session('id_pateint');
        $pateint = DB::table('users')->join('pateint', 'pateint.user_ID', '=', 'users.id')
                        ->where('pateint.user_ID', $id_pateint)->get();

        $doctors = DB::table('doctor')->where('doctorID', $id)->get();

        foreach ($doctors as $doctor) {

            $id = $doctor->doctorID;

            $speciality = $obg->get_speciality($id, 'all');
            $doctor->speciality = $speciality;

            $available = $obg->get_availablity($id);
            $doctor->available = $available;

            $address = $obg->get_address($id);
            $doctor->Address = $address;

            $stars = $obg->get_stars($id);
            $doctor->stars = $stars;

            $total_stars = $obg->get_total_stars($id);
            $doctor->total_stars = $total_stars;

            $count_patient = $obg->get_count_patients($id);
            $doctor->count_patient = $count_patient;

            $account_view = $obg->get_count_account_view($id);
            $doctor->account_view = $account_view;

            $userid = session('id_pateint');
            $bookmarkdoctor = DB::table('favorite_doctor')->where('doctor_id', $id)->where('user_id', $userid)->first();
            $doctor->doctor_bookmark = $bookmarkdoctor;
        }
        return view('public/booking', compact("doctors", 'pateint'));
    }

//    public function storeAppointment1(Request $store) {
//
//
//
//        $appointment = new Appointment();
//        $appointment->user_id = $id_user;
//        $appointment->doctor_id = $store->getDocotrID;
//        $appointment->pateint_id = $store->getPateintID;
//        $appointment->appointment_date = $store->app_date;
//        $appointment->appointment_time = $store->app_time;
//        $appointment->Status = '0';
//        $appointment->save();
//        event(new EndPool($store->getDocotrID));
//        return redirect()->back();
//    }

    public function storeAppointment(Request $store) {
        $noti = new NotificationController();
        $id_user = session('id_pateint');


        $id_user = $id_user;
        $id_doctor = $store->getDocotrID;
        $familyMemberId = $store->getPateintID;
        $clinic_id = $store->clinic_id;
        $DateOfAppointment = $store->app_date;
        $TimeOfAppointment = $store->app_time;
        $Name = $store->name;
        $Age = $store->age;
        $Gender = $store->gender;
        $relation_ship = $store->relation_ship;
        $Token = $store->token;
        //get day from date
        $Timestamp = strtotime($DateOfAppointment);
        $The_day = date('l', $Timestamp);

        $user = DB::table('users')->select('id', 'name', 'gender')->where('id', $id_user)->first();

        $doctor = DB::table('doctor')->select('doctorID', 'title', 'doctor_name')->where('doctorID', $id_doctor)->first();


        if ($Token == 'self') {

            $appointment = new Appointment();
            $appointment->user_id = $id_user;
            $appointment->doctor_id = $id_doctor;
            $appointment->clinic_address_id = $clinic_id;
            $appointment->appointment_date = $DateOfAppointment;
            $appointment->appointment_time = $TimeOfAppointment;
            $appointment->feedback = '0';
            $appointment->Status = '0';
            $appointment->day = $The_day;
            $appointment->save();

            DB::table('users')->where('id', $id_user)->update(['name' => $Name, 'birthday' => $Age, 'gender' => $Gender]);
            // call for notification
            $doc_noti = $noti->appConfirmNotification_doctor($user, $doctor, $DateOfAppointment, $TimeOfAppointment);
            $res = $noti->appConfirmNotification_user($user, $doctor, $DateOfAppointment, $TimeOfAppointment);
            \Session::flash('Doctor_title', $doctor->title);
            \Session::flash('Doctor_name', $doctor->doctor_name);
            \Session::flash('flash_date', $DateOfAppointment);
            \Session::flash('flash_Time', $TimeOfAppointment);
            event(new EndPool($id_doctor));
            return redirect()->route('success.appointment.page');
        }
        if ($Token == 'other') {


            $appointment = new Appointment();
            $appointment->user_id = $id_user;
            $appointment->doctor_id = $id_doctor;
            $appointment->pateint_id = $familyMemberId;
            $appointment->clinic_address_id = $clinic_id;
            $appointment->appointment_date = $DateOfAppointment;
            $appointment->appointment_time = $TimeOfAppointment;
            $appointment->feedback = '0';
            $appointment->Status = '0';
            $appointment->day = $The_day;
            $appointment->save();


            $patient = DB::table('pateint')->where('patientID', $familyMemberId)->first();
            $doc_noti = $noti->appConfirmNotification_doctor($user, $doctor, $DateOfAppointment, $TimeOfAppointment);
            $res = $noti->appConfirmNotification_family($user, $patient, $doctor, $DateOfAppointment, $TimeOfAppointment);

            \Session::flash('Doctor_title', $doctor->title);
            \Session::flash('Doctor_name', $doctor->doctor_name);
            \Session::flash('flash_date', $DateOfAppointment);
            \Session::flash('flash_Time', $TimeOfAppointment);
            event(new EndPool($id_doctor));
            return redirect()->route('success.appointment.page');
        }
    }

    public function edit(Request $request) {

        if (request()->ajax()) {

            $id = request()->ID;
            $day = request()->dayselected;
            $date_selected = request()->DATE;
            $Clinic_id = request()->Clinic_id;
            $data1 = DB::table('appointment')->where('doctor_id', $id)->where('clinic_address_id', $Clinic_id)->where('appointment_date', $date_selected)->get();
            $data = DB::table('visitingtime')
                    ->where('doctorID', $id)
                    ->where('time_off_on', 1)
                    ->where('doctor_active', 1)
                    ->where('clinic_address_id', $Clinic_id)
                    ->where('day', $day)
                    ->get();
            return response()->json(['success' => $data, 'used' => $data1]);
        }
    }

    public function SuccessAppointmentView() {
        return view('public.boookingsuccess');
    }

}
