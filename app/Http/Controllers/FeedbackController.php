<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Rating;
use Illuminate\Support\Carbon;

class FeedbackController extends Controller {

    public function index() {
        $now = Carbon::now();
        $future = $now->addDays(1);
        $tomorrow = $future->toDateString();
        $pateint_id = session('id_pateint');
        $feedback = DB::table('appointment')
                ->join('doctor', 'doctor.doctorID', '=', 'appointment.doctor_id')
                ->where('user_id', $pateint_id)
                ->where('Status', 0)
                ->where('cancel_by_user', 0)
                ->where('appointment_date', '<', $tomorrow)
                ->where('feedback', 0)
                ->paginate(5);
        return view('pateint.feedback', compact('feedback'));
    }

    public function doctorFeedback() {
        $id = session()->get('id_doctor');
        $total = DB::table('feedback')->where('feedback_doctor_id', $id)->count();

        if ($total != 0) {

            $overall_experience_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('overall_experience', 1)->count();
            $overall_experience = ($overall_experience_1 / $total) * 100;

            $doctor_checkup_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('doctor_checkup', 1)->count();
            $doctor_checkup = ($doctor_checkup_1 / $total) * 100;

            $staff_behavior_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('staff_behavior', 1)->count();
            $staff_behavior = ($staff_behavior_1 / $total) * 100;

            $clinic_environment_1 = DB::table('feedback')->where('feedback_doctor_id', $id)->where('clinic_environment', 1)->count();
            $clinic_environment = ($clinic_environment_1 / $total) * 100;

            $star = ($overall_experience_1 + $overall_experience_1 + $doctor_checkup_1 + $staff_behavior_1 + $clinic_environment_1) / $total;
            $stars = number_format($star, 1);
        } else {
            $stars = number_format(5, 1);
        }

        $feedbacks = DB::table('feedback')->where('feedback_doctor_id', $id)->where('comment', '!=', null)->paginate(5);
        foreach ($feedbacks as $feedback) {
            $overall_experienceA = $feedback->overall_experience;
            $doctor_checkupA = $feedback->doctor_checkup;
            $staff_behaviorA = $feedback->staff_behavior;
            $clinic_environmentA = $feedback->clinic_environment;
            $star_review = $overall_experienceA + $overall_experienceA + $doctor_checkupA + $staff_behaviorA + $clinic_environmentA;
            $feedback->star_review = $star_review;

            if ($feedback->feedback_patient_id == 0) {
                //get user name
                $user = DB::table('users')->select('name')->where('id', $feedback->feedback_user_id)->first();

                $str = $user->name;
                $len = strlen($str);
                $str1 = '';
                for ($i = 0; $i < $len; $i++) {
                    if ($i != 0 && $i != ($len - 1)) {
                        $str1 .= '*';
                    } else {
                        $str1 .= $str[$i];
                    }
                }
                $feedback->name = $str1;
            } else {
                $user = DB::table('pateint')->select('p_name')->where('patientID', $feedback->feedback_patient_id)->first();
                $str = $user->p_name;
                $len = strlen($str);
                $str1 = '';
                for ($i = 0; $i < $len; $i++) {
                    if ($i != 0 && $i != ($len - 1)) {
                        $str1 .= '*';
                    } else {
                        $str1 .= $str[$i];
                    }
                }
                $feedback->name = $str1;
            }
        }

        //dd($feedbacks);
        return view('doctor.feedback', compact('feedbacks', 'stars', 'overall_experience', 'doctor_checkup', 'staff_behavior', 'clinic_environment', 'total'));
    }

    public function store(Request $request) {

        $ap_id = $request->appID;
        $doctorid = $request->doctorid;
        $userid = $request->userid;

        $overall_experience = $request->overall_experience;

        $doctor_checkup = $request->doctor_checkup;
        $staff_behavior = $request->staff_behavior;
        $clinic_environment = $request->clinic_environment;
        $comment = $request->comment;

        $user = new Rating();
        $user->feedback_doctor_id = $doctorid;
        $user->feedback_user_id = $userid;
        $user->overall_experience = $overall_experience;
        $user->doctor_checkup = $doctor_checkup;
        $user->staff_behavior = $staff_behavior;
        $user->clinic_environment = $clinic_environment;
        $user->comment = $comment;
        $user->save();
        DB::table('appointment')->where('appID', $ap_id)->update(['feedback' => 1]);
        return redirect()->back();
    }

}
