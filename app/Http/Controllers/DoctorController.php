<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Events\EndPool;
use DB;
use App\Doctor;
use App\Socialmedia;
use App\Visitingtime;
use App\Appointment;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Events\PatientEvent;
use App\Http\Controllers\NotificationController;
use App\Rating;

class DoctorController extends Controller {

    public function UnreadNotificationAppointment(Request $re) {
        if (request()->ajax()) {
            $data = DB::table('doctor_notification')->where('status', 0)->where('doctor_id', session('id_doctor'))->limit(3)->get();
            return response()->json(['success' => $data]);
        }
    }

    public function signupDoctor(Request $request) {

        $is_doctor = DB::table('doctor')->where('doctor_email', $request->email)->first();
        $is_user = DB::table('users')->where('email', $request->email)->first();

        if ($is_doctor != null || $is_user != null) {
            $mess = 'The Email ' . $request->email . ' already registerd to system!';
            //return response()->json($mess, 400);
            return back()->with('u-message', $mess);
        } else {

            $doctor = new Doctor();
            $doctor->title = $request->title;
            $doctor->doctor_name = $request->name;
            $doctor->doctor_gender = $request->gender;
            $doctor->city = $request->city;
            $doctor->doctor_email = $request->email;
            $doctor->doctor_phoneNo = $request->phone;
            $doctor->doctor_status = 0;
            $doctor->email_verification = 0;
            $doctor->email_verification_token = Str::random(60);
            $doctor->doctor_password = $request->password;
            $doctor->save();

            $doctor_info = DB::table('doctor')->orderBy('doctorID', 'DESC')->first();
            //send Email
            $doctorID = $doctor_info->doctorID;
            $Email_Token = $doctor_info->email_verification_token;
            $url = url('/email-active', [$doctorID, $Email_Token]);
            // dd($url);
            $to_name = $request->name;
            $to_email = $request->email;

            $data = array("name" => $request->name, "body" => "Please Confirm Your Acount", "active_link" => $url);
            Mail::send('public.Email.mail', $data, function($message) use ($to_name, $to_email) {
                $message->to($to_email)
                        ->subject('DoctorPlus Email Confirmation');
                // $message->from('habibandkh@gmail.com','Test Mail');
            });

            //insert a row in feedback
            $user = new Rating();
            $user->feedback_doctor_id = $doctorID;
            // $user->feedback_user_id = 0;
            $user->overall_experience = '1';
            $user->doctor_checkup = '1';
            $user->staff_behavior = '1';
            $user->clinic_environment = '1';
            //  $user->comment = '';
            $user->save();
            return back()->with('message', 'we just send a confirmation message to your email address');
        }
    }

    public function profile() {
        $id = session()->get('id_doctor');
        $year = now()->format('Y');
        $month = now()->format('m');

        $obj = new HomeController();
        $obj1 = new DoctorApi\doctor\DoctorApiController();

        $doctors = DB::table('doctor')->where('doctorID', $id)->get();

        foreach ($doctors as $doctor) {

            $id = $doctor->doctorID;
            $MyPrimary = $obj1->myPrimaryspaciality($id);
            $doctor->MyPrimary = $MyPrimary;
            $speciality = $obj->get_speciality($id, 'S');
            $doctor->speciality = $speciality;


            $available = $obj->get_availablity($id);
            $doctor->available = $available;

            $address = $obj->get_address($id);
            $doctor->Address = $address;

            $my_clinic = $obj->get_my_clinic($id);
            $doctor->my_clinic = $my_clinic;

            $all_clinic = $obj->get_all_clinic($id);
            $doctor->all_clinic = $all_clinic;

            $stars = $obj->get_stars($id);
            $doctor->stars = $stars;

            $total_stars = $obj->get_total_stars($id);
            $doctor->total_stars = $total_stars;

            $count_patient = $obj->get_count_patients($id);
            $doctor->count_patient = $count_patient;

            $account_view = $obj->get_count_account_view($id);
            $doctor->account_view = $account_view;

            /////second section

            $doctor_experience = DB::table('doctor_experience')->where('doctor_FID', $id)->get();
            $doctor->doctor_experience = $doctor_experience;

            $doctor_education = DB::table('doctor_education')->where('D_FID', $id)->get();
            $doctor->doctor_education = $doctor_education;

            $speciality_all = $obj->get_speciality($id, 'all');
            $doctor->speciality_all = $speciality_all;

            $my_Condition = $obj->get_condition_link($id);
            $doctor->my_Condition = $my_Condition;

            $my_services = $obj->get_services_link($id);
            $doctor->my_services = $my_services;

            $all_Condition = $obj->get_all_condition($id);
            $doctor->all_Condition = $all_Condition;

            $all_services = $obj->get_all_services($id);
            $doctor->all_services = $all_services;

            $speciality_non = DB::table('speciality')->get();
            $doctor->get_my_none_spa = $speciality_non;
        }
        return view('doctor.profile', compact("doctors"));
    }

    public function acceptAppointment($appid) {

        Appointment::where('appID', '=', $appid)->update(array('Status' => 1));
        $data = DB::table('appointment')->where('appID', $appid)->first();
        $app_id = $data->appID;
        $doctor_id = $data->doctor_id;
        $user_id = $data->user_id;
        $appointment_date = $data->appointment_date;
        $appointment_time = $data->appointment_time;
        $Status = $data->Status;

        $doctor = DB::table('doctor')->select('doctor_name', 'doctor_picture')->where('doctorID', $doctor_id)->first();

        event(new PatientEvent('app', 'doc', 'pa'));

        return back();
    }

    public function cancelAppointment(Request $request) {

        $app_id = $request->appid;

        $cancel = DB::table('appointment')->where('appID', $app_id)->update(['Status' => 1, 'cancel_by_doctor' => 1]);
        $app = DB::table('appointment')->where('appID', $app_id)->first();

        $noti = new NotificationController();
        $res = $noti->appCancelNoti_cancel_by_doctor($app);
         event(new EndPool('1'));
        return back()->with('message', 'Appointment Cancel Successfully!');
    }

    public function viewPatientProfile($pid) {
        // dd('hahha');
    }

    public function home() {

        $now = Carbon::now();
        $today = $now->toDateString();
        $noww = Carbon::now();
        $future = $noww->addDays(1);
        $tomorrow = $future->toDateString();
        $doctor_id = session()->get('id_doctor');




        $MyTodayAppointment = DB::table('appointment')
                ->join('users', 'appointment.user_id', '=', 'users.id')
                ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                ->where('doctor.doctorID', $doctor_id)
                ->where('appointment.Status', 0)
                ->where('appointment.appointment_date', '=', $today)
                ->where('appointment.pateint_id', null)
                ->select('doctorID', 'doctor_picture', 'pic', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
                ->orderByRaw("Status ASC , appointment_date ASC")
                ->paginate(5);

        $MyFamilyTodayAppointment = DB::table('appointment')
                ->join('users', 'appointment.user_id', '=', 'users.id')
                ->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')
                ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                ->where('doctor.doctorID', $doctor_id)
                ->where('appointment.Status', 0)
                ->where('appointment.appointment_date', '=', $today)
                ->select('doctorID', 'doctor_picture', 'p_photo', 'appID', 'patientID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
                ->orderByRaw("Status ASC , appointment_date ASC")
                ->paginate(5);

        // dd($MyFamilyTodayAppointment);

        $MyUpcommingAppointment = DB::table('appointment')
                ->join('users', 'appointment.user_id', '=', 'users.id')
                ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                ->where('doctor.doctorID', $doctor_id)
                ->where('appointment.Status', 0)
                ->where('appointment.appointment_date', '>', $today)
                ->where('appointment.pateint_id', null)
                ->select('doctorID', 'doctor_picture', 'pic', 'appID', 'Status', 'users.name As user_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
                ->orderByRaw("Status ASC , appointment_date ASC")
                ->paginate(5);

        $MyFamilyUpcommingAppointment = DB::table('appointment')
                ->join('users', 'appointment.user_id', '=', 'users.id')
                ->join('pateint', 'appointment.pateint_id', '=', 'pateint.patientID')
                ->join('address', 'appointment.clinic_address_id', '=', 'address.id')
                ->join('doctor', 'appointment.doctor_id', '=', 'doctor.doctorID')
                ->where('doctor.doctorID', $doctor_id)
                ->where('appointment.Status', 0)
                ->where('appointment.appointment_date', '>', $today)
                ->select('doctorID', 'doctor_picture', 'p_photo', 'appID', 'patientID', 'Status', 'p_name', 'title', 'doctor_name', 'doctor_fee', 'appointment_date', 'appointment_time', 'address.name As clinic_name', 'address')
                ->orderByRaw("Status ASC , appointment_date ASC")
                ->paginate(5);

        return view('doctor.home', compact('MyTodayAppointment', 'MyFamilyTodayAppointment', 'MyUpcommingAppointment', 'MyFamilyUpcommingAppointment'));
    }

    public function socialMedia() {

        $doctor_id = session()->get('id_doctor');

        $socials = DB::table('socialmedia')->where('doctor_id', $doctor_id)->first();

        if ($socials == null) {
            $social = new Socialmedia();
            $social->doctor_id = $doctor_id;
            $social->facebook = 'https://facebook.com/';
            $social->Twitter = 'https://twitter.com/';
            $social->Instagram = 'https://instagram.com/';
            $social->Pinterest = 'https://pinterest.com/';
            $social->Linkedin = 'https://linkedin.com/';
            $social->Youtube = 'https://youtube.com/';
            $social->save();
        }
        return view('doctor.socialmedia', compact('socials'));
    }

    public function saveSocialMedia(Request $request) {


        $doctor_id = session()->get('id_doctor');
        $query = DB::table('socialmedia')->where('doctor_id', $doctor_id)->first();

        if ($query) {
            DB::table('socialmedia')
                    ->where('doctor_id', $doctor_id)
                    ->update([
                        'facebook' => $request->facebook,
                        'Twitter' => $request->Twitter,
                        'Instagram' => $request->Instagram,
                        'Pinterest' => $request->Pinterest,
                        'Linkedin' => $request->Linkedin,
                        'Youtube' => $request->Youtube,
            ]);
            return back()->with('success', 'Data updated successfully!');
        } else {

            $social = new Socialmedia();
            $social->doctor_id = $doctor_id;
            $social->save();

            DB::table('socialmedia')
                    ->where('doctor_id', $doctor_id)
                    ->update([
                        'facebook' => $request->facebook,
                        'Twitter' => $request->Twitter,
                        'Instagram' => $request->Instagram,
                        'Pinterest' => $request->Pinterest,
                        'Linkedin' => $request->Linkedin,
                        'Youtube' => $request->Youtube,
            ]);
            return back()->with('success', 'Data seved successfully!');
        }
    }

    public function getNotification(Request $re) {
        $doctor_id = session()->get('id_doctor');
        DB::table('doctor_notification')->where('status', 0)->where('doctor_id', $doctor_id)->update(['status' => 1]);

        $notifications = DB::table('doctor_notification')->where('doctor_id', $doctor_id)->select('id', 'doctor_id', 'user_id', 'patient_id', 'text', 'status', 'date', 'is_chat')->orderBy('id', 'DESC')->paginate(5);
        $noti_count = DB::table('doctor_notification')->where('doctor_id', $doctor_id)->select('id', 'doctor_id', 'user_id', 'patient_id', 'text', 'status', 'date', 'is_chat')->orderBy('id', 'DESC')->count();
        if ($noti_count > 0) {
            foreach ($notifications as $noti) {

                if ($noti->patient_id == 0) {
                    $user = DB::table('users')->where('id', $noti->user_id)->first();
                    $noti->name = $user->name;
                } else {
                    $pateint = DB::table('pateint')->where('patientID', $noti->patient_id)->first();
                    $noti->name = $pateint->p_name;
                }
            }
        }
        return view('doctor.notification', compact('notifications'));
    }

    function deleteNotification(Request $re) {
        $noti_id = $re->id;
        DB::table('doctor_notification')->where('id', $noti_id)->delete();


        return redirect()->back();
    }

    function readNotification($noti_id) {
        $update = DB::table('doctor_notification')->where('id', $noti_id)->update(['status' => 1]);
        return back();
    }

    public function ajaxnotificationAppointment(Request $re) {
        if (request()->ajax()) {
            $data = DB::table('appointment')->where('status', 0)->where('doctor_id', session('id_doctor'))->get();
            // dd($data->user_id);
            return response()->json(['success' => $data]);
        }
    }

    public function patientsProfile() {
        $id = session()->get('id_doctor');

        $patientprofile = DB::table('appointment')
                ->join('pateint', 'pateint.patientID', '=', 'appointment.pateint_id')
                ->select('pateint.patientID', 'pateint.p_photo', 'pateint.p_name', 'pateint.gender', 'pateint.p_phoneNo', 'pateint.birthday', 'pateint.bloodgroup', DB::raw('COUNT(*) AS total'))
                ->where('doctor_id', $id)
                ->groupBy('pateint.patientID', 'pateint.p_photo', 'pateint.p_name', 'pateint.gender', 'pateint.p_phoneNo', 'pateint.birthday', 'pateint.bloodgroup')
                ->paginate(5);

        $userprofiles = DB::table('appointment')
                ->join('users', 'users.id', '=', 'appointment.user_id')
                ->select('users.id', 'users.pic', 'users.name', 'users.gender', 'users.phone', 'users.birthday', 'users.bloodgroup', DB::raw('COUNT(*) AS total'))
                ->where('doctor_id', $id)
                ->where('pateint_id', null)
                ->groupBy('users.id', 'users.pic', 'users.name', 'users.gender', 'users.phone', 'users.birthday', 'users.bloodgroup')
                ->paginate(5);

        return view('doctor.patientsProfile', compact('patientprofile', 'userprofiles'));
    }

    public function appointmentList() {

        $doctor_id = session()->get('id_doctor');

        $appointment = DB::table('appointment')
                ->join('pateint', 'pateint.patientID', '=', 'appointment.pateint_id')
                ->join('doctor', 'doctor.doctorID', '=', 'appointment.doctor_id')
                ->where('doctor.doctorID', $doctor_id)
                ->orderBy('appID', 'desc')
                ->paginate(10);

        return view('doctor.appointment', compact('appointment'));
    }

    public function timeEdit($doctorid, $day) {

        $Dayshow = Visitingtime::where('doctorID', '=', $doctorid)->where('day', '=', $day)->get();
        return view('doctor.scheduling.edit', compact('Dayshow'));
    }

    public function dayStatus(Request $request) {

        $visit_id = $request->visitid;

        $data = DB::table('visitingtime')->select('*')->where('vtID', '=', $visit_id)->first();

        $doctorid = $data->doctorID;
        $day = $data->day;
        $clinicid = $data->clinic_address_id;

        $id = Visitingtime::select('doctor_active')->where('doctorID', '=', $doctorid)->where('day', '=', $day)->where('clinic_address_id', '=', $clinicid)->first();

        if ($id->doctor_active == 1) {
            $off = Visitingtime::where('doctorID', '=', $doctorid)->where('day', '=', $day)->where('clinic_address_id', '=', $clinicid)->update(array('doctor_active' => 0));
        } else {
            $on = Visitingtime::where('doctorID', '=', $doctorid)->where('day', '=', $day)->where('clinic_address_id', '=', $clinicid)->update(array('doctor_active' => 1));
        }

        return redirect()->back();
    }

    public function timeChange(Request $request) {
        $vtid = $request->vtid;
        $id = Visitingtime::select('time_off_on')->where('vtID', '=', $vtid)->first();

        if ($id->time_off_on == 1) {
            $Dayshow = Visitingtime::where('vtID', '=', $vtid)->limit(1)
                    ->update(array('time_off_on' => 0));
        } else {
            $Dayshow = Visitingtime::where('vtID', '=', $vtid)->limit(1)
                    ->update(array('time_off_on' => 1));
        }


        return redirect()->back();
    }

    public function scheduling(Request $request) {
        $id = session()->get('id_doctor');

        $obj = new HomeController();
        $address = $obj->get_address($id);
        $my_all_clinic = $obj->get_my_clinic($id);

        if ($address != null) {

            foreach ($address as $address_id) {

                $Mon_id = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Monday')->orderBy('visit_time', 'asc')->first();
                $Tue_id = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Tuesday')->orderBy('visit_time', 'asc')->first();
                $Wed_id = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Wednesday')->orderBy('visit_time', 'asc')->first();
                $Thu_id = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Thursday')->orderBy('visit_time', 'asc')->first();
                $Fri_id = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Friday')->orderBy('visit_time', 'asc')->first();
                $Sat_id = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Saturday')->orderBy('visit_time', 'asc')->first();
                $Sun_id = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Sunday')->orderBy('visit_time', 'asc')->first();


                $Mon = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Monday')->orderBy('visit_time', 'asc')->get();
                $Tue = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Tuesday')->orderBy('visit_time', 'asc')->get();
                $Wed = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Wednesday')->orderBy('visit_time', 'asc')->get();
                $Thu = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Thursday')->orderBy('visit_time', 'asc')->get();
                $Fri = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Friday')->orderBy('visit_time', 'asc')->get();
                $Sat = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Saturday')->orderBy('visit_time', 'asc')->get();
                $Sun = DB::table('visitingtime')->select('*')->where('clinic_address_id', '=', $address_id->address_id)->where('doctorID', '=', $id)->where('day', '=', 'Sunday')->orderBy('visit_time', 'asc')->get();

                $address_id->Saturday = $Sat;
                $address_id->Sunday = $Sun;
                $address_id->Monday = $Mon;
                $address_id->Tuesday = $Tue;
                $address_id->Wednesday = $Wed;
                $address_id->Thursday = $Thu;
                $address_id->Friday = $Fri;

                $address_id->Sat = $Sat_id;
                $address_id->Sun = $Sun_id;
                $address_id->Mon = $Mon_id;
                $address_id->Tus = $Tue_id;
                $address_id->Wed = $Wed_id;
                $address_id->Thu = $Thu_id;
                $address_id->Fri = $Fri_id;
            }
        }

        return view('doctor.scheduling.index', compact('my_all_clinic', 'address'));
    }

    public function createVisitTime(Request $request) {

        $doctor_id = session()->get('id_doctor');
        $clinic_address_id = $request->clinic_address_id;
        $from = $request->start_time;
        $to = $request->end_time;
        $days = $request->days;
        $interval = (int) $request->Interval_time;

        $is_day_avail = DB::table('visitingtime')
                ->whereIn('day', $days)
                ->where('doctorID', $doctor_id)
                ->where('doctor_active', 1)
                ->whereBetween('visit_time', [$from, $to])
                ->select('day')
                ->groupBy('day')
                ->get();


        if (!$is_day_avail->isEmpty()) {
            foreach ($is_day_avail as $value) {
                $values[] = $value->day;
                $is_day = implode(',', $values);
            }
            //    dd('hi');
            return redirect()->back()->with('message', "you already created a Time-Slot for This days: " . $is_day . "");
        } else {

            $res = $this->createtimetable($days, $doctor_id, $from, $to, $interval, $clinic_address_id);
            return redirect()->back()->with('message', "timetable created!");
        }
    }

    function createtimetable($days, $doctor_id, $from, $to, $interval, $clinic_address_id) {
        $x = $this->insertFromTime($days, $doctor_id, $from, $clinic_address_id);

        $y = $this->insertByTimeInterval($days, $doctor_id, $from, $to, $interval, $clinic_address_id);

        return 0;
    }

    function insertFromTime($days, $doctor_id, $from, $clinic_address_id) {

        foreach ($days as $day) {

            $user = new Visitingtime();
            $user->doctorID = $doctor_id;
            $user->day = $day;
            $user->visit_time = $from;
            $user->doctor_active = "1";
            $user->time_off_on = "1";
            $user->clinic_address_id = $clinic_address_id;
            $user->save();
        }
        return 0;
    }

    function insertByTimeInterval($days, $doctor_id, $from, $to, $interval, $clinic_address_id) {

        $date = now()->format('yy-m-d');
        $date_And_time = $date . ' ' . $from;
        $base = Carbon::parse($date_And_time);

        $timestamp = 0;
        $time = "0";
        $my_minute = $interval / 60;
        for ($i = 1; $i <= 2100; $i++) {

            $my_custom_date_plus_interval = $base->copy()->addMinutes($my_minute)->toDateTimeString();
            if (date('Y-m-d', strtotime($my_custom_date_plus_interval)) <= date('Y-m-d', strtotime($date))) {
                if (date('H:i', strtotime($from)) < date('H:i', strtotime($to))) {
                    $timestamp = strtotime($from) + $interval;
                    $time = date('H:i', $timestamp);

                    foreach ($days as $day) {

                        $user = new Visitingtime();
                        $user->doctorID = $doctor_id;
                        $user->day = $day;
                        $user->visit_time = $time;
                        $user->clinic_address_id = $clinic_address_id;
                        $user->doctor_active = "1";
                        $user->time_off_on = "1";
                        $user->save();
                    }
                }
            }
            $from = $time;
            $my_minute = $my_minute + ($interval / 60);
        }
        return 0;
    }

    public function edit($id) {
        if (request()->ajax()) {

            DB::table('visitingtime')->where('vtID', $id)->update(array('time_off_on' => 1));
            return response()->json();
        }
    }

    public function deleteTimatable(Request $request) {

        $clinic_id = $request->id;

        $doctor_id = $request->session()->get('id_doctor');
        DB::table('visitingtime')->where('doctorID', $doctor_id)->where('clinic_address_id', $clinic_id)->delete();

        return redirect()->back();
    }

    public function savePassword(Request $request) {
        $doctor_id = session('id_doctor');
        $oldpass = $request->old_pass;
        $newpass = $request->new_pass;

        $is_pass = DB::table('doctor')->where('doctorID', '=', $doctor_id)->where('doctor_password', '=', $oldpass)->first();

        if ($is_pass) {
            DB::table('doctor')
                    ->where('doctorID', $doctor_id)
                    ->update([
                        'doctor_password' => $newpass,
            ]);
            return back()->with('success', 'password changed successfully');
        } else {
            return back()->with('danger', 'the old password is wrong');
        }
    }

    public function updateDoctorProfile(Request $request) {

        $doctorid = session('id_doctor');
        $doctor_title = $request->title;
        $doctor_name = $request->name;

        $doctor_email = $request->email;
        $doctor_phone = $request->phone;
        $doctor_gender = $request->gender;
        $doctor_city = $request->city;
        $doctor_fee = $request->fee;
        $doctor_birthday = $request->birthday;
        $doctor_short_biography = $request->short_biography;
        $doctor_Experience = $request->Experience;
        $pmdc_num = $request->pmdc_num;
        if ($request->hasFile('photo')) {

            $image = $request->file('photo');
            $image_name = 'doctor-' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/images/doctor/'), $image_name);
            $update = DB::table('doctor')->where('doctorID', $doctorid)->update(['doctor_picture' => $image_name]);
        }
        $update = DB::table('doctor')
                ->where('doctorID', $doctorid)
                ->update([
            'title' => $doctor_title,
            'doctor_name' => $doctor_name,
            'doctor_birthday' => $doctor_birthday,
            'doctor_gender' => $doctor_gender,
            'doctor_email' => $doctor_email,
            'doctor_phoneNo' => $doctor_phone,
            'city' => $doctor_city,
            'doctor_fee' => $doctor_fee,
            'doctor_Short_Biography' => $doctor_short_biography,
            'experience' => $doctor_Experience,
            'pmdc' => $pmdc_num,
        ]);
        return redirect()->back();
    }

}
