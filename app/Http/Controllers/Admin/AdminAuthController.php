<?php

namespace App\Http\Controllers\Admin;

use Image;
use App\Events\EndPool;
use App\Appointment;
use DB;
use Session;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminAuthController extends Controller {

    public function home(Request $re) {
        return view('admin.home');
    }

    public function notification(Request $re) {
        $notifications = DB::table('message')->paginate(10);
        return view('admin.notification', compact('notifications'));
    }

    public function deleteNotification(Request $re) {
        $id = $re->id;
        DB::table('message')->where('id', $id)->delete();

        return redirect()->back();
    }

    public function readNotification(Request $re) {
        $id = $re->id;
        DB::table('message')->where('id', $id)->update(['status' => 1]);

        return redirect()->back();
    }

    public function ajaxnotification() {
        if (request()->ajax()) {

            $data = DB::table('message')->where('status', 0)->get();
            return response()->json(['success' => $data]);
        }
    }

    public function adminProfile(Request $re) {
        $id = session('id_admin');
        $profiles = DB::table('admins')->where('id', $id)->first();
        return view('admin.profile', compact('profiles'));
    }

    public function adminEditProfile() {
        $id = session('id_admin');
        $adminEdit = DB::table('admins')->where('id', $id)->first();
        return view('admin.editProfile', compact('adminEdit'));
    }

    public function adminUpdateProfile(Request $request) {
        $admin_ID = $request->adminID;


        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'age' => 'required',
            'gender' => 'required'
        ]);
        if ($request->hasFile('photo')) {
            $this->validate($request, [
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            $image = $request->file('photo');
            $image_name = 'admin-' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/images/admin/'), $image_name);

            DB::table('admins')
                    ->where('id', $admin_ID)
                    ->update([
                        'pic' => $image_name,
                        'name' => $request->name,
                        'email' => $request->email,
                        'phone' => $request->phone,
                        'age' => $request->age,
                        'gender' => $request->gender,
            ]);
        } else {

            DB::table('admins')
                    ->where('id', $admin_ID)
                    ->update([
                        'name' => $request->name,
                        'email' => $request->email,
                        'phone' => $request->phone,
                        'age' => $request->age,
                        'gender' => $request->gender,
            ]);
        }
        return back()->with('success', 'admin member update successful');
    }

    public function login(Request $re) {
        $email = $re->adminemail;
        $pass = $re->adminpassword;

        $is_Admin = DB::table('admins')->where('email', '=', $email)->where('password', '=', $pass)->first();

        if ($is_Admin) {
            $id_admin = $is_Admin->id;
            $name_admin = $is_Admin->name;
            $email_admin = $is_Admin->email;
            $pic_admin = $is_Admin->pic;
            $type_admin = $is_Admin->type;

            session(['name_admin' => $name_admin]);
            session(['id_admin' => $id_admin]);
            session(['email_admin' => $email_admin]);
            session(['pic_admin' => $pic_admin]);
            session(['type_admin' => $type_admin]);
            session(['is_admin' => 'Phd']);


            return view('admin/home');
        }
    }

    public function bloglogin(Request $re) {
        $email = $re->adminemail;
        $pass = $re->adminpassword;

        $is_Admin = DB::table('admins')->where('email', '=', $email)->where('password', '=', $pass)->first();

        if ($is_Admin) {
            $id_admin = $is_Admin->id;
            $name_admin = $is_Admin->name;
            $email_admin = $is_Admin->email;
            $pic_admin = $is_Admin->pic;
            $type_admin = $is_Admin->type;

            session(['name_admin' => $name_admin]);
            session(['id_admin' => $id_admin]);
            session(['email_admin' => $email_admin]);
            session(['pic_admin' => $pic_admin]);
            session(['type_admin' => $type_admin]);
            session(['is_admin' => 'Phd']);



            $posts = DB::table('post')->get();
            return view('admin.blog.blog', compact('posts'));
        }
    }

    public function registerUser(Request $request) {

        $number = $request->mobilenumber;
        $password = $request->pass;
        $token = $request->token_appointment;

        if ($token == 'token_appointment') {
            $Doctor__ID = $request->DocotsID;
            $date = $request->date_of_appointment;
            $time = $request->time_of_appointment;

            $insert = new User();
            $insert->phone = $number;
            $insert->password = $password;
            $insert->save();
            $user_id = DB::table('users')->select('id')->where('phone', $number)->first();

            $appointment = new Appointment();
            $appointment->user_id = $user_id->id;
            $appointment->doctor_id = $Doctor__ID;
            $appointment->appointment_date = $date;
            $appointment->appointment_time = $time;
            $appointment->created_at = '2020/02/01';
            $appointment->Status = '0';
            $appointment->save();

            event(new EndPool($Doctor__ID));

            $doct = DB::table('doctor')->select('doctor_name')->where('doctorID', $Doctor__ID)->first();
            Session::flash('flash_date', $date);
            Session::flash('flash_Time', $time);
            Session::flash('Doctor_name', $doct->doctor_name);
            Session::flash('account', 'Your Acount Created Successfully!');
            return redirect()->route('success.appointment.page');
        }
        if ($token == 'token_signup') {

            $insert = new User();
            $insert->phone = $number;
            $insert->password = $password;
            $insert->save();
            return redirect()->route('login.page.user');
        }
    }

}
