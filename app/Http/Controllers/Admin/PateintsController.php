<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PateintsController extends Controller {

    public function index() {
        $allpateintlist = DB::table('pateint')->paginate(5);
        $allUserlist = DB::table('users')->paginate(5);
        
        return view('admin.familyMember', compact('allpateintlist','allUserlist'));
    }

    public function userList() {
        $user_info = DB::table('users')->paginate(8);
        return view('admin.userList', compact('user_info'));
    }

    public function checkFamilyMemberView(Request $request) {
        $pateint_id=$request->id;
        $document = DB::table('document')->where('pateint_id', $pateint_id)->get();
        $pateintview = DB::table('pateint')->where('patientID', $pateint_id)->first();
        return view('admin.viewpateint', compact('pateintview', 'document'));
    }

    public function getAllAppointmentList() {
        $appointments = DB::table('appointment')
                ->join('pateint', 'pateint.patientID', '=', 'appointment.pateint_id')
                ->join('doctor', 'doctor.doctorID', '=', 'appointment.doctor_id')
                ->orderBy('appID', 'desc')
                ->paginate(5);

        return view('admin.appointment', compact('appointments'));
    }

   
    public function update(Request $request, $id) {
        //
    }

    public function destroy($id) {


        $update = DB::table('users')->where('id', $id)->delete();

        return redirect()->back();
    }

}
