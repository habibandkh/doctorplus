<?php

namespace App\Http\Controllers\Admin;

use App\Blog;
use App\Category;
use App\Comment;
use App\Events\NewComment;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Image;
use Illuminate\Support\Facades\URL;

class BlogController extends Controller {

    public function index() {

        $posts = DB::table('post')->get();
        return view('admin.blog.blog', compact('posts'));
    }

    public function publicBlog(Request $request) {

        $value = $request->session()->get('language');

        if ($value != null) {
            $posts = DB::table('post')->where('lang', $value)->orderBy('post_id', 'desc')->paginate(6);
            $latest = DB::table('post')->where('lang', $value)->orderBy('post_id', 'desc')->limit(5)->get();
        } else {
            $posts = DB::table('post')->where('lang', 'en')->orderBy('post_id', 'desc')->paginate(6);
            $latest = DB::table('post')->where('lang', 'en')->orderBy('post_id', 'desc')->limit(5)->get();
        }
        $categories = DB::table('categories')->get();
        return view('public.blog', compact('posts', 'categories', 'latest'));
    }

    public function publicBlogFilterCategory(Request $request) {

        $c_id = $request->id;
        $value = $request->session()->get('language');

        if ($value != null) {
            $posts = DB::table('post')->where('lang', $value)->orderBy('post_id', 'desc')->where('category_id', $c_id)->paginate(6);
            $latest = DB::table('post')->where('lang', $value)->orderBy('post_id', 'desc')->limit(5)->get();
        } else {
            $posts = DB::table('post')->where('lang', 'en')->orderBy('post_id', 'desc')->where('category_id', $c_id)->paginate(6);
            $latest = DB::table('post')->where('lang', 'en')->orderBy('post_id', 'desc')->limit(5)->get();
        }

        $categories = DB::table('categories')->get();
        return view('public.blog', compact('posts', 'categories', 'latest'));
    }

    public function adminBlogFilterCategory(Request $request) {

        $c_id = $request->id;
        $value = $request->session()->get('language');


        $posts = DB::table('post')->where('category_id', $c_id)->paginate(6);
        $latest = DB::table('post')->limit(5)->get();

        $categories = DB::table('categories')->get();
        return view('admin.blog.blogview', compact('posts', 'categories', 'latest'));
    }

    public function publicblogView(Request $request) {
        $id = $request->id;

        $posts = DB::table('post')->where('post_id', $id)->get();
        $comment = DB::table('comment')->where('postid', $id)->get();
        $latest = DB::table('post')->orderBy('post_id', 'desc')->limit(5)->get();
        $categories = DB::table('categories')->get();
        // dd($post);
        return view('public.blogview', compact('posts', 'categories', 'latest', 'comment'));
    }

    public function blogView(Request $request) {
        $id = $request->id;
        $posts = DB::table('post')->where('post_id', $id)->get();
        $latest = DB::table('post')->limit(5)->get();
        $categories = DB::table('categories')->get();
        // dd($post);
        return view('admin.blog.blogview', compact('posts', 'categories', 'latest'));
    }

    public function blogViewAdd() {
        $categories = DB::table('categories')->get();
        return view('admin.blog.blogadd', compact('categories'));
    }

    public function blogViewEdit(Request $request) {
        $id = $request->id;
        $posts = DB::table('post')->where('post_id', $id)->get();
        $categories = DB::table('categories')->get();
        return view('admin.blog.blogedit', compact('categories', 'posts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        // dd($request);
        $name = $request->bname;
        $dec = $request->bdec;
        $cat = $request->bcat;
        $status = $request->status;
        $language = $request->lang;

        $this->validate($request, [
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $image = $request->file('photo');

        $image_name = 'blog-' . time() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('/images/blog/'), $image_name);
        $blog = new Blog();
        $blog->post_title = $name;
        $blog->post_body = $dec;
        $blog->post_img = $image_name;
        $blog->category_id = $cat;
        $blog->post_view = 0;
        $blog->status = $status;
        $blog->lang = $language;
        $blog->save();

        return redirect()->back()->with('message', 'Blog post created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request) {
        //   dd($id);
        $name = $request->bname;
        $dec = $request->bdec;
        $cat = $request->bcat;
        $status = $request->status;
        $id = $request->postid;
        $postview = $request->postview;

        if ($request->hasFile('photo')) {
            $this->validate($request, [
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            $image = $request->file('photo');

            $image_name = 'blog-' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/images/blog/'), $image_name);

            DB::table('post')
                    ->where('post_id', $id)
                    ->update([
                        'post_title' => $name,
                        'post_body' => $dec,
                        'post_img' => $image_name,
                        'category_id' => $cat,
                        'post_view' => $postview,
                        'status' => $status,
            ]);
        } else {

            DB::table('post')
                    ->where('post_id', $id)
                    ->update([
                        'post_title' => $name,
                        'post_body' => $dec,
                        'category_id' => $cat,
                        'post_view' => $postview,
                        'status' => $status,
            ]);
        }
        return back()->with('message', 'post edited  successful');
    }

    public function deleteBlog(Request $request) {
        $id = $request->id;
        DB::table('post')->where('post_id', $id)->delete();
        return back()->with('message', 'post deleted  successful');
    }

//////////////////////////////////////////
//////////////categories/////////////////
////////////////////////////////////////
    public function category() {
        $categories = DB::table('categories')->paginate(5);
        return view('admin.category.categories', compact('categories'));
    }

    public function addCategory(Request $request) {
        $cat = new Category();
        $cat->name = $request->cat_name;
        $cat->save();
        return redirect()->back()->with('message', 'Category created successfully');
    }

    public function editCategory(Request $request) {

        $name = $request->cat_name;
        $id = $request->cat_id;

        DB::table('categories')
                ->where('cat_id', $id)
                ->update([
                    'name' => $name,
        ]);

        return redirect()->back()->with('message', 'Category updated successfully');
    }

    public function deleteCategory(Request $request) {
        $id = $request->id;
        DB::table('categories')->where('cat_id', $id)->delete();
        return back()->with('message', 'Category deleted  successful');
    }

    public function Comments() {
        $comments = DB::table('comment')->get();
        return view('admin.comment', compact('comments'));
    }

    public function deleteComment(Request $request) {
        $id = $request->id;
        DB::table('comment')->where('com_id', $id)->delete();
        return back()->with('message', 'comment deleted  successful');
    }

    public function CommentAdd(Request $request) {
        $cat = new Comment();
        $cat->postid = $request->postid;
        $cat->com_name = $request->name;
        $cat->com_email = $request->email;
        $cat->comment = $request->comments;
        $cat->save();

        return redirect()->back()->with('message', 'Comment created successfully');
    }

}
