<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Speciality;
use DB;
use Image;

class SpecialityController extends Controller {

    public function specialities() {
        $specialities = DB::table('speciality')->paginate(5);
        return view('admin.specialities', compact('specialities'));
    }

    public function specialitiesAdd(Request $request) {
        $this->validate($request, [
            'spe_en' => 'required',
            'spe_fa' => 'required',
            'spe_pa' => 'required',
            'photo' => 'required'
        ]);
        if ($request->hasFile('photo')) {
            $this->validate($request, [
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            $image = $request->file('photo');
            $image_name = 'speciality-' . time() . '.' . $image->getClientOriginalExtension();
          
        $image->move(public_path('/images/specialities/'), $image_name);

            $spe = new Speciality();
            $spe->specialityName = $request->spe_en;
            $spe->farsi_name = $request->spe_fa;
            $spe->pashto_name = $request->spe_pa;
            $spe->photo = $image_name;
            $spe->save();
        }



        return redirect()->back()->with('message', 'speciality Added successfully');
    }

//    public function specialitiesEdit(Request $request) {
//
//        $name = $request->cat_name;
//        $id = $request->cat_id;
//        DB::table('speciality')
//                ->where('cat_id', $id)
//                ->update(['name' => $name,]);
//        return redirect()->back()->with('message', 'Category updated successfully');
//    }

    public function specialitiesDelete($id) {
        DB::table('speciality')->where('specialityID', $id)->delete();
        return back()->with('message', 'speciality deleted  successful');
    }

}
