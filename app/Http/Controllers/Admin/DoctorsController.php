<?php

namespace App\Http\Controllers\Admin;

use DB;
use Image;
use App\Doctor;
use App\Doctorspeciality;
use App\Speciality;
use App\DoctorService;
use App\Doctorexperiance;
use App\Doctoreduction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;

class DoctorsController extends Controller {
  public function adminConfirmdoctor(Request $request) {
           
           DB::table('doctor')
                    ->where('doctorID', $request->doctorid)
                    ->update([
                        'under_review' => 2
                      
            ]);
             $doctors = DB::table('doctor')->paginate(10);
        return view('admin.doctorindex', compact('doctors'));
            
    }
  public function adminCanceldoctor(Request $request) {
        DB::table('doctor')
                    ->where('doctorID', $request->doctorid)
                    ->update([
                        'under_review' => 1
                      
            ]);
             $doctors = DB::table('doctor')->paginate(10);
        return view('admin.doctorindex', compact('doctors'));
    }

   public function index(Request $request) {

        if ($request->ajax()) {
            $doctors = DB::table('doctor');
            return Datatables::of($doctors)
                            ->addIndexColumn()
                            ->addColumn('action', function($doctors) {

                                $btn = '<a href="' . route('admin.doctor.view', $doctors->doctorID) . '" class="edit btn btn-outline-info btn-sm m-2">View</a>';
                                if ($doctors->under_review == 1) {
                                    $btn = $btn . '<a href="' . route('confirm.doctor.status', $doctors->doctorID) . '" class="edit btn btn-outline-warning btn-sm m-2 "><span class="spinner-grow spinner-grow-sm text-danger"></span> pending to confirm it!</a>';
                                }
                                if ($doctors->under_review == 0) {

                                    $btn = $btn . '<a href="#" type="button" class="edit btn btn-outline-primary btn-sm m-2 disabled" disabled>no request!</a>';
                                }
                                if ($doctors->under_review == 2) {

                                    $btn = $btn . '<a href="' . route('cancel.doctor.status', $doctors->doctorID) . '" class="edit btn btn-outline-danger btn-sm m-2">cancel it!</a>';
                                }

                                return $btn;
                            })
                            ->rawColumns(['action'])
                            ->make(true);
        }
        return view('admin.doctorindex');
    }

    public function create() {
        return view('admin.doctorcreate');
    }

    public function storeDoctor(Request $request) {

        if ($request->hasFile('photo')) {
            $this->validate($request, [
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
            ]);
            $image = $request->file('photo');
            $image_name = 'doctor-' . time() . '.' . $image->getClientOriginalExtension();

            $image->move(public_path('/images/doctor/'), $image_name);

            $doctor = new Doctor();
            $doctor->doctor_name = $request->name;
            $doctor->doctor_last_name = $request->l_name;
            $doctor->doctor_father_name = $request->f_name;
            $doctor->doctor_email = $request->email;
            $doctor->doctor_password = $request->password;
            $doctor->doctor_birthday = $request->birth;
            $doctor->doctor_gender = $request->gender;
            $doctor->doctor_phoneNo = $request->phone;
            $doctor->doctor_Short_Biography = $request->biog;
            $doctor->doctor_fee = $request->doctor_fee;
            $doctor->doctor_clinicAddress = $request->address;
            $doctor->doctor_picture = $image_name;
            $doctor->doctor_status = '1';
            $doctor->doctor_regDate = '2019';
            $doctor->save();
        } else {
            $doctor = new Doctor();
            $doctor->doctor_name = $request->name;
            $doctor->doctor_last_name = $request->l_name;
            $doctor->doctor_father_name = $request->f_name;
            $doctor->doctor_email = $request->email;
            $doctor->doctor_password = $request->password;
            $doctor->doctor_birthday = $request->birth;
            $doctor->doctor_gender = $request->gender;
            $doctor->doctor_phoneNo = $request->phone;
            $doctor->doctor_Short_Biography = $request->biog;
            $doctor->doctor_fee = $request->doctor_fee;
            $doctor->doctor_status = '1';
            $doctor->doctor_regDate = '2019';
            $doctor->save();
        }

        return redirect()->back()->with('message', 'Doctor account created successfully');
    }

    public function show(Request $request) {

        $id = $request->id;
        $doctorspeciality = DB::table('doctor')
                ->join('doctorspeciality', 'doctor.doctorID', '=', 'doctorspeciality.F_doctorID')
                ->join('speciality', 'doctorspeciality.F_specialityID', '=', 'speciality.specialityID')
                ->where('doctor.doctorID', $id)
                ->get();
        
        $doctor_services = DB::table('doctor_services')
                    ->join('doctor_services_view', 'doctor_services.services_id', '=', 'doctor_services_view.services_id')
                    ->where('doctor_services_view.doctor_id', $id)
                    ->select('doctor_services.services_id', 'service_name', 'doctor_services_view.id')
                    ->groupBy('service_name')
                    ->get();

        $speciality = DB::table('speciality')->get();
        $doctor_experience = DB::table('doctor_experience')->where('doctor_FID', $id)->get();
        $doctor_education = DB::table('doctor_education')->where('D_FID', $id)->get();
        
        $doctor = DB::table('doctor')->where('doctorID', $id)->first();

        return view('admin.doctorshow', compact('doctor', 'doctor_education', 'doctor_experience', 'doctor_services', 'speciality', 'doctorspeciality'));
    }

    public function editDoctor(Request $request) {
        $id = $request->id;
        $doctor = DB::table('doctor')->where('doctorID', $id)->first();
        //dd($id);
        return View('admin.doctoredit', compact('doctor'));
    }

    public function doctorUpdate(Request $request) {
        if ($request->hasFile('photo')) {
            $this->validate($request, [
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
            ]);
            $image = $request->file('photo');

            $image_name = 'doctor-' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/images/doctor/'), $image_name);

            DB::table('doctor')
                    ->where('doctorID', $request->doctorID)
                    ->update([
                        'doctor_picture' => $image_name,
                        'doctor_name' => $request->name,
                        'doctor_last_name' => $request->l_name,
                        'doctor_father_name' => $request->f_name,
                        'doctor_email' => $request->email,
                        'doctor_password' => $request->password,
                        'doctor_birthday' => $request->birth,
                        'doctor_gender' => $request->gender,
                        'doctor_phoneNo' => $request->phone,
                        'doctor_Short_Biography' => $request->biog,
                        'doctor_fee' => $request->doctor_fee,
                        'doctor_status' => '1',
                        'doctor_regDate' => '2019',
            ]);
        } else {
            DB::table('doctor')
                    ->where('doctorID', $request->doctorID)
                    ->update([
                        'doctor_name' => $request->name,
                        'doctor_last_name' => $request->l_name,
                        'doctor_father_name' => $request->f_name,
                        'doctor_email' => $request->email,
                        'doctor_password' => $request->password,
                        'doctor_birthday' => $request->birth,
                        'doctor_gender' => $request->gender,
                        'doctor_phoneNo' => $request->phone,
                        'doctor_Short_Biography' => $request->biog,
                        'doctor_fee' => $request->doctor_fee,
                        'doctor_status' => '1',
                        'doctor_regDate' => '2019',
            ]);
        }

        return redirect()->back()->with('message', 'Doctor account updated successfully');
    }

    function addSexperiance(Request $request) {
        $edu = new Doctorexperiance();
        $edu->doctor_FID = $request->doc_experiance_id;

        $edu->experience_name = $request->experiance_name;
        $edu->start = $request->start_date_experiance;
        $edu->end = $request->end_date_experiance;

        $edu->save();
        return redirect()->back();
    }

    function addeducation(Request $request) {

        $edu = new Doctoreduction();
        $edu->D_FID = $request->doctor_id_edu;
        $edu->school_name = $request->School_name;
        $edu->title_of_study = $request->Study_title;
        $edu->start_Date = $request->start_date;
        $edu->end_Date = $request->end_date;
        $edu->save();
        return redirect()->back();
    }

    function addService(Request $request) {
        $ds = new DoctorService();
        $ds->service_name = $request->service_name;
        $ds->doctor_s_id = $request->doctor_id_service;
        $ds->save();
        return redirect()->back();
    }

    function addSpeciality(Request $request) {


        $insert = new Doctorspeciality();
        $insert->F_doctorID = $request->doct_addSpecialityid;
        $insert->F_specialityID = $request->spacia_idd;
        $insert->save();
        return redirect()->back();
    }

    function updateexperiance(Request $request) {

        DB::table('doctor_experience')->where('experience_id', $request->eduction_idxx)
                ->update(['experience_name' => $request->School_namexx,
                    'start' => $request->start_datexx,
                    'end' => $request->end_datexx]);
        return redirect()->back();
    }

    function updateeducation(Request $request) {
        DB::table('doctor_education')->where('D_E_ID', $request->eduction_id)
                ->update(
                        ['school_name' => $request->School_name,
                            'title_of_study' => $request->Study_title,
                            'start_Date' => $request->start_date,
                            'end_Date' => $request->end_date]);
        return redirect()->back();
    }

    function updateService(Request $request) {
        DB::table('doctor_services')->where('services_id', $request->service_id)
                ->update(['service_name' => $request->service_name,]);
        return redirect()->back();
    }

    function deleteDoctor($id) {
        $id = $request->id;
        DB::table('doctorspeciality')->where('F_doctorID', $id)->delete();
        DB::table('doctor_education')->where('D_FID', $id)->delete();
        DB::table('doctor_services')->where('doctor_s_id', $id)->delete();
        DB::table('doctor_experience')->where('doctor_FID', $id)->delete();
        DB::table('doctor')->where('doctorID', $id)->delete();

        return redirect()->route('doctor.index')->with('message', 'Doctor account deleted successfully');
    }

    function deleteSpeciality(Request $request) {
        $id = $request->id;
        DB::table('doctorspeciality')->where('id_s_d', $id)->delete();
        return redirect()->back();
    }

    function deleteeducation(Request $request) {
        $id = $request->id;
        DB::table('doctor_education')->where('D_E_ID', $id)->delete();
        return redirect()->back();
    }

    function deleteService(Request $request) {
        $id = $request->id;
        DB::table('doctor_services')->where('services_id', $id)->delete();
        return redirect()->back();
    }

    function deleteexperiance(Request $request) {
        $id = $request->id;
        DB::table('doctor_experience')->where('experience_id', $id)->delete();
        return redirect()->back();
    }

}
