<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdmin {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (session('is_Admin') == 'PHD_ADMIN') {
            return $next($request);
        } else {
            return redirect('home');
        }
    }

}
