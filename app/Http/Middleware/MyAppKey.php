<?php

namespace App\Http\Middleware;

use Closure;

class MyAppKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token=$request->header('DoctorPlusAPPKEY');
        if($token!='_!_@_#_$_D_o_c_t_o_r_P_l_u_s_$_#_@_!_'){
            return response()->json(['message'=>'Invalid token'],401);
        }
        return $next($request);
    }
}
