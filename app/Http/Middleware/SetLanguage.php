<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\URL;

class SetLanguage {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {


        $value = $request->session()->get('language');

        if ($value != null) {
            \App::setLocale($value);
            URL::defaults(['language' => $value]);
        } else {
            \App::setLocale($request->language);
            URL::defaults(['language' => \App::getLocale()]);
        }


        return $next($request);
    }

}
