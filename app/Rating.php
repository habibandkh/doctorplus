<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
     protected $table = "feedback";
    public $timestamps = false;
}
