<div class="chat-body">
    <div class="chat-scroll">
        <ul class="list-unstyled">
            @foreach($messages as $message)
            <li class="media  {{ ($message->from == session('id_doctor')) ? 'sent' : 'received' }}">
                <div class="media-body">
                    @if($message->type=='text')
                    <div class="msg-box">
                        <div>
                            <p>{{ $message->message }}</p>
                            <ul class="chat-msg-info">
                                <li>
                                    <div class="chat-time">
                                        <span>{{ date('d M y, h:i a', strtotime($message->date)) }}</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @endif
                    @if($message->type=='file')
                    <div class="msg-box">
                        <div>
                            <div class="chat-msg-attachments">
                                <div class="chat-attachment">
                                    <img src="{{asset('doctor/public/chat/file/'.$message->file)}}" alt="Attachment">
                                    <div class="chat-attach-caption">{{ $message->message }}</div>
                                    <a href="{{asset('doctor/public/chat/file/'.$message->file)}}" class="chat-attach-download">
                                        <i class="fas fa-download"></i>
                                    </a>
                                </div>
                            </div>
                            <ul class="chat-msg-info">
                                <li>
                                    <div class="chat-time">
                                        <span>{{ date('d M y, h:i a', strtotime($message->date)) }}</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @endif

                </div>
            </li>

            @endforeach


        </ul>
    </div>
</div>
<div class="chat-footer">
    <div class="input-group">
        <div class="input-group-prepend">
            <div class="btn-file btn "  data-toggle="modal" data-target="#send_file_to_doctor">
                <i class="fa fa-paperclip"></i>

            </div>
        </div>
        <input type="text" id="input-msg-send"class="input-msg-send form-control" placeholder="Type something">
        <div class="input-group-append">
            <button type="button" class="btn msg-send-btn"><i class="fab fa-telegram-plane"></i></button>
        </div>
    </div>
</div>


<div class="modal fade call-modal" id="send_file_to_doctor">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form>
                    <div class="call-box incoming-box">
                        <div class="input-group-prepend">
                            <input type="file" id="file" name="file" required>
                        </div>
                        <br>
                        <div class="input-group-prepend">
                            <input type="text" class="form-control"name="message" id="message" placeholder="Caption" required>
                        </div>
                        <br>
                        <div id="success" class="row">

                        </div>
                        <div class="input-group-append">
                            <button onclick="uploadFile()" type="button" class="btn btn-success text-dark"><i class="fab fa-telegram-plane"></i> Send</button>
                        </div>
                    </div>
                    <!-- Outgoing Call -->
                </form>
            </div>
        </div>
    </div>
</div>