<div  @if(Request::segment(1)=='en') class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar " @else class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar text-right" @endif style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
    <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; top: 0px; left: 30px;"><div class="profile-sidebar">
            <div class="widget-profile pro-widget-content">
                <div class="profile-info-widget">
                    <a href="#" class="booking-doc-img">
                        <img src="{{asset('images/doctor/'.session('pic_doctor'))}}" alt="User Image">
                    </a>
                    <div class="profile-det-info">
                        <h3>{{session('name_doctor')}}</h3>
                        <div class="patient-details" dir="ltr">
                            <h5>
                                <i class="fas fa-birthday-cake"></i> 
                                {{ Carbon\Carbon::parse(session('birthday'))->format('d M Y ')}},
                                {{Carbon\Carbon::parse(session('birthday'))->diff(Carbon\Carbon::now())->format('%y years')}}
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dashboard-widget">
                <nav class="dashboard-menu">
                    <ul>
                        <li @if(Request::segment(2)=='doctor-dashboard') class="active" @endif > 
                             <a href="{{route('doctor.dashboard')}}">
                                <i class="fas fa-columns"></i>
                                <span>{{__('Dashboard')}}</span>
                            </a>
                        </li>

                        
                        <li @if(Request::segment(2)=='my-patient') class="active" @endif   > 
                             <a href="{{route('doctor.view.patient')}}">
                                <i class="fas fa-user-injured"></i>
                                <span>{{__('My Patients')}}</span>
                            </a>
                        </li>

                        <li @if(Request::segment(2)=='scheduling') class="active" @endif   > 
                             <a href="{{route('doctor.scheduling')}}">
                                <i class="fas fa-hourglass-start"></i>
                                <span>{{__('schedule timetable')}}</span>
                            </a>
                        </li>
                       <li @if(Request::segment(2)=='doctor.Private.Chat') class="active" @endif > 
                            <a href="{{route('doctor.Private.Chat')}}">
                                <i class="fas fa-user-secret"></i>
                                        <span>{{__('Private Chat')}}</span>
                            </a>
                        </li>
                        <li @if(Request::segment(2)=='doctor-feedback') class="active" @endif   > 
                             <a href="{{route('doctor.feedback')}}">
                                <i class="fas fa-star"></i>
                                <span>{{__('Reviews')}}</span>
                            </a>
                        </li>
                        <li @if(Request::segment(2)=='doctor-notification') class="active" @endif   > 
                             <a href="{{route('doctor.Notification')}}">
                                <i class="fas fa-bell"></i>
                                <span>{{__('Notification')}}</span>
                               
                            </a>
                        </li>
                        <li @if(Request::segment(2)=='doctor-profile') class="active" @endif   > 
                             <a href="{{route('doctor.profile')}}">
                                <i class="fas fa-user-cog"></i>
                                <span>{{__('My Profile')}}</span>
                            </a>
                        </li>
                        <li @if(Request::segment(2)=='my-social-media') class="active" @endif   > 
                             <a href="{{route('social.media')}}">
                                <i class="fas fa-share-alt"></i>
                                <span>{{__('Social Media')}}</span>
                            </a>
                        </li>
                        <li @if(Request::segment(2)=='doctor-change-password') class="active" @endif   > 
                             <a href="{{route('doctor.change.password')}}">
                                <i class="fas fa-lock"></i>
                                <span>{{__('Change Password')}}</span>
                            </a>
                        </li>

                    </ul>
                </nav>
            </div>
        </div><div class="resize-sensor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; z-index: -1; visibility: hidden;"><div class="resize-sensor-expand" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 365px; height: 988px;"></div></div><div class="resize-sensor-shrink" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%"></div></div></div></div></div>