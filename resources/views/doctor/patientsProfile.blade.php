<html lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>DoctorPlus</title>

    <!-- Favicons -->
    <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">

    <!-- Bootstrap CSS -->

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Fontawesome CSS -->

    <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">

    <!-- Main CSS -->

    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
<![endif]-->

</head>
@include('doctor.header')
<div class="content" style="transform: none; min-height: 239.6px;" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
     <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('doctor.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">

                <div class="row row-grid">

                    @foreach($patientprofile as $patientpro)
                    <div class="col-md-6 col-lg-4 col-xl-3">
                        <div class="card widget-profile pat-widget-profile">
                            <div class="card-body">
                                <div class="pro-widget-content">
                                    <div class="profile-info-widget">
                                        <a href="javascript:void(0)" class="booking-doc-img">
                                            <img src="{{asset('images/pateint/'.$patientpro->p_photo)}}" alt="User Image">
                                        </a>
                                        <div class="profile-det-info">
                                            <h3><a href="javascript:void(0)">{{$patientpro->p_name}}</a></h3>
                                            <div class="patient-details @if(Request::segment(1)=='en') @else text-center @endif">
                                                 <h5 ><b>{{__('Total Appointment')}} : </b> {{$patientpro->total}}</h5>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="patient-info @if(Request::segment(1)=='en') @else text-right @endif"">
                                    <ul>
                                        <li>{{__('Phone')}} <span class="@if(Request::segment(1)=='en') float-right @else float-left  @endif">{{$patientpro->p_phoneNo}}</span></li>
                                        <li> {{__('age')}}<span class="@if(Request::segment(1)=='en') float-right @else float-left  @endif">{{Carbon\Carbon::parse($patientpro->birthday)->diff(Carbon\Carbon::now())->format('%y Years')}}, 
                                                @if($patientpro->gender==1) male @else female @endif

                                            </span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach()
                    @foreach($userprofiles as $userprofile)
                    <div class="col-md-6 col-lg-4 col-xl-3">
                        <div class="card widget-profile pat-widget-profile">
                            <div class="card-body">
                                <div class="pro-widget-content">
                                    <div class="profile-info-widget">
                                        <a href="javascript:void(0)" class="booking-doc-img">
                                            <img src="{{asset('images/user/'.$userprofile->pic)}}" alt="User Image">
                                        </a>
                                        <div class="profile-det-info text-center">
                                            <h3><a href="javascript:void(0)">{{$userprofile->name}}</a></h3>
                                            <div class="patient-details @if(Request::segment(1)=='en') @else text-center @endif">
                                                <h5 ><b>{{__('Total Appointment')}} : </b> {{$userprofile->total}}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="patient-info @if(Request::segment(1)=='en') @else text-right @endif" >
                                    <ul>
                                        <li>{{__('Phone')}} <span class="@if(Request::segment(1)=='en') float-right @else float-left  @endif">{{$userprofile->phone}}</span></li>
                                        <li> {{__('age')}}<span class="@if(Request::segment(1)=='en') float-right @else float-left  @endif"> {{Carbon\Carbon::parse($userprofile->birthday)->diff(Carbon\Carbon::now())->format('%y Years')}},   @if($userprofile->gender==1) male @else female @endif</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach()
                </div>
                <div class="d-flex align-items-center justify-content-center">{{$patientprofile->links()}}</div>
            </div>
        </div>
    </div>
</div>
@include('doctor.footer')

<script src="{{asset("assets/js/jquery.min.js")}}"></script>
<script src="{{asset("assets/js/popper.min.js")}}"></script>
<script src="{{asset("assets/js/bootstrap.min.js")}}"></script>

<script src="{{asset('assets/plugins/theia-sticky-sidebar/ResizeSensor.js')}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')}}"></script>

<!-- Circle Progress JS -->
<script src="{{asset('assets/js/circle-progress.min.js')}}"></script>

<script src="{{asset("assets/js/script.js")}}"></script>
<div class="sidebar-overlay"></div>
</body>
</html>