<html lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>DoctorPlus</title>

    <!-- Favicons -->
    <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">

    <!-- Bootstrap CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Fontawesome CSS -->

    <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">

    <!-- Select2 CSS -->
    <link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css')}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/dropzone/dropzone.min.css')}}">

    <!-- Main CSS -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">


    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>


</head>
@include('doctor.header')
<div class="content" style="transform: none; min-height: 239.6px;" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('doctor.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <!-- Page Content -->

                @foreach($doctors as $doctor)
                <!-- Doctor Widget -->
                <div class="card" dir="ltr">
                    <div class="card-body">
                        <div class="doctor-widget">
                            <div class="doc-info-left">
                                <div class="doctor-img">
                                    <img src="{{asset('images/doctor/'.$doctor->doctor_picture)}}" class="img-fluid" alt="User Image">
                                </div>
                                <div class="doc-info-cont">
                                    <h4 class="doc-name">{{$doctor->title}} {{$doctor->doctor_name}}</h4>
                                    @foreach($doctor->speciality as $special)
                                    <p class="doc-department"> {{$special->specialityName}}</p>

                                    @endforeach

                                    <div class="rating">
                                        <div class="d-none">  {{$rating=number_format($doctor->stars,1)}} </div>
                                        @foreach(range(1,5) as $i)
                                        @if($rating >0)
                                        @if($rating >0.5)
                                        <i class="fas fa-star  filled"></i>
                                        @else
                                        <i class="fas fa-star-half filled" ></i>
                                        @endif
                                        @else
                                        <i class="fas  fa-star"></i>
                                        @endif
                                        <?php $rating--; ?>
                                        @endforeach
                                        <span class="d-inline-block average-rating">({{$doctor->total_stars}})</span>
                                    </div>

                                    <div class="clinic-details">
                                        @foreach($doctor->Address as $add)
                                        <p class="doc-location"><i class="fas fa-map-marker-alt"></i> {{$add->name}}({{ date('H:i', strtotime($add->From))}} , {{ date('H:i', strtotime($add->To))}})
                                            <!--                                            <a href="javascript:void(0);">Get Directions</a>-->
                                        </p>
                                        @endforeach


                                    </div>

                                </div>
                            </div>
                            <div class="doc-info-right">
                                <div class="clini-infos">
                                    <ul>
                                        <li><i class="far fa-user"></i> {{$doctor->count_patient}} {{__('Patients')}}</li>
                                        <li><i class="far fa-eye"></i>  {{$doctor->account_view}} {{__('View Account')}}</li>

                                        <li><i class="far fa-money-bill-alt"></i>{{__('AFN')}} {{$doctor->doctor_fee}} </li>
                                    </ul>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Doctor Widget -->
                @if(session()->has('message'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

                <!-- Doctor Details Tab -->
                <div class="card" dir="ltr">
                    <div class="card-body pt-0">

                        <!-- Tab Menu -->
                        <nav class="user-tabs mb-4 " @if(Request::segment(1)=='en') @else dir="rtl" @endif>
                            <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#doc_overview" data-toggle="tab">{{__('Overview')}}</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="#update_Profile" data-toggle="tab">{{__('Update Profile')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#Pro_Profile" data-toggle="tab">{{__('Professional Profile')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#doc_locations" data-toggle="tab">{{__('Practice Info')}}</a>
                                </li>
                            </ul>
                        </nav>
                        <div class="tab-content pt-0">
                            <div role="tabpanel" id="doc_overview" class="tab-pane fade show active" >
                                <div class="row @if(Request::segment(1)=='en') @else text-right @endif"  @if(Request::segment(1)=='en') @else dir="rtl" @endif>
                                    <div class="col-md-12 col-lg-9" >

                                        <!-- About Details -->
                                        <div class="widget about-widget">
                                            <h4 class="widget-title">{{__('About Me')}}</h4>
                                            <p>{{$doctor->doctor_Short_Biography}}</p>
                                        </div>

                                        <div class="widget education-widge2t">
                                            <h4 class="widget-title">{{__('Education')}}</h4>
                                            <div class="experi1ence-box ">
                                                <ul class="expe1rience-list">
                                                    @foreach($doctor->doctor_education as $edu)
                                                    <li>
                                                        
                                                        <div class="experience-content">
                                                            <div class="timeline-content">
                                                                <a href="#/" class="name">{{$edu->school_name}}</a>
                                                                <div>{{$edu->title_of_study}}</div>
                                                                <span class="time">{{$edu->start_Date }} - {{$edu->end_Date}}</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /Education Details -->

                                        <!-- Experience Details -->
                                        <div class="widget experience-wid2get">
                                            <h4 class="widget-title">{{__('Work')}} &amp; {{__('Experience')}}</h4>
                                            <div class="experienc1e-box">
                                                <ul class="experi1ence-list">

                                                    @foreach($doctor->doctor_experience as $exp)
                                                    <li>
                                                        
                                                        <div class="experience-content">
                                                            <div class="timeline-content">
                                                                <a href="#/" class="name">{{$exp->experience_name}}</a>

                                                                <span class="time">{{$exp->start }} - {{$exp->end}}</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    @endforeach

                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /Experience Details -->

                                        <!-- Awards Details -->

                                        <!-- /Awards Details -->
                                        <!-- Specializations List -->
                                        <div class="service-l1ist">
                                            <h4>{{__('Specializations')}}</h4>
                                            <ul class="clearfix">
                                                @foreach($doctor->speciality_all as $spe)
                                                <li>{{$spe->specialityName}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <!-- /Specializations List -->
                                        <!-- Services List -->
                                        <div class="service-lis1t">
                                            <h4>{{__('Services')}}</h4>
                                            <ul class="clearfix">
                                                @foreach($doctor->my_services as $service)
                                                <li>{{$service->service_name}}</li>
                                                @endforeach

                                            </ul>
                                        </div>
                                        <!-- /Services List -->
                                        <div class="service-lis1t" >
                                            <h4>{{__('Conditions')}}</h4>
                                            <ul class="clearfix">
                                                @foreach($doctor->my_Condition as $Condition)
                                                <li>{{$Condition->name}}</li>
                                                @endforeach

                                            </ul>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <!-- /Overview Content -->


                            <div role="tabpanel" id="update_Profile" class="tab-pane fade @if(Request::segment(1)=='en') @else text-right @endif">
                                <div class="widget review-listing">
                                    <div class="row">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <div class="card">
                                                    <div class="card-body">
                                                        @if(session()->has('message'))
                                                        <div class="alert alert-success">
                                                            {{ session()->get('message') }}
                                                        </div>
                                                        @endif
                                                        <form action="{{route('doctor.update.profile')}}" method="post" enctype="multipart/form-data">
                                                            {{csrf_field()}}
                                                            <input class="form-control" name="doctorID" type="hidden" value="{{ $doctor->doctorID}}">

                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('Title')}} <span class="text-danger">*</span></label>
                                                                        <select required="" id="select_patient" class="form-control" name="title">

                                                                            <option value="{{$doctor->title}}" selected>{{$doctor->title}}</option>
                                                                            <!--<option value="Doctor">Doctor</option>-->
                                                                            <option value="Dr.">Dr.</option>
                                                                            <option value="Prof.">Prof.</option>
                                                                            <option value="Assist.Prof.">Assist.Prof.</option>
                                                                            <option value="Assist.Prof.Dr.">Assist.Prof.Dr.</option>
                                                                            <option value="Assoc.Prof.">Assoc.Prof.</option>
                                                                            <!--                                                                            <option value="Brig.Dr.">Brig.Dr.</option>
                                                                                                                                                        <option value="Brig.(R)Dr.">Brig.(R)Dr.</option>
                                                                                                                                                        <option value="Brig.(R)Prof.Dr.">Brig.(R)Prof.Dr.</option>
                                                                                                                                                        <option value="Col.Dr.">Col.Dr.</option>
                                                                                                                                                        <option value="Col.(R)Dr.">Col.(R)Dr.</option>
                                                                                                                                                        <option value="Lft.Col.Dr.">Lft.Col.Dr.</option>
                                                                                                                                                        <option value="Lft.(R)Col.Dr.">Lft.(R)Col.Dr.</option>
                                                                                                                                                        <option value="Cap.Dr.">Cap.Dr.</option>
                                                                                                                                                        <option value="Cap.(R)Dr.">Cap.(R)Dr.</option>-->
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('Name')}} <span class="text-danger">*</span></label>
                                                                        <input required="" class="form-control" value="{{$doctor->doctor_name}}"type="text" name="name">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('Email')}} <span class="text-danger">*</span></label>
                                                                        <input required="" class="form-control" value="{{$doctor->doctor_email}}" type="email" name="email">
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label> {{__('Phone')}}<span class="text-danger">*</span></label>
                                                                        <input required="" maxlength="10" value="{{ $doctor->doctor_phoneNo}}"minlength="10"class="form-control" type="tel" name="phone">
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group gender-select">
                                                                        <label class="gen-label"> {{__('Gender')}}<span class="text-danger">*</span></label><br>
                                                                        @if($doctor->doctor_gender=='male')
                                                                        <div class="form-check-inline">
                                                                            <label class="form-check-label">
                                                                                <input required="" type="radio" checked name="gender" value="male" class="form-check-input">{{__('Male')}}
                                                                            </label>
                                                                        </div>
                                                                        @else
                                                                        <div class="form-check-inline">
                                                                            <label class="form-check-label">
                                                                                <input required="" type="radio" name="gender" value="male" class="form-check-input">{{__('Male')}}
                                                                            </label>
                                                                        </div>
                                                                        @endif
                                                                        @if($doctor->doctor_gender=='female')
                                                                        <div class="form-check-inline">
                                                                            <label class="form-check-label">
                                                                                <input required="" type="radio" checked name="gender" value="female" class="form-check-input">{{__('Female')}}
                                                                            </label>
                                                                        </div>
                                                                        @else
                                                                        <div class="form-check-inline">
                                                                            <label class="form-check-label">
                                                                                <input required="" type="radio" name="gender" value="female" class="form-check-input">{{__('Female')}}
                                                                            </label>
                                                                        </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label> {{__('City')}}<span class="text-danger">*</span></label>
                                                                                <select required="" id="select_patient" class="form-control" name="city" value="{{$doctor->city}}">
                                                                                    <option value="{{$doctor->city}}" selected>{{$doctor->city}}</option>
                                                                                    <option value="kabul">kabul</option>
                                                                                    <option value="badakhshan">badakhshan</option>
                                                                                    <option value="badghiz">badghiz</option>
                                                                                    <option value="baghlan">baghlan</option>
                                                                                    <option value="balkh">balkh</option>
                                                                                    <option value="bamiyan">bamiyan</option>
                                                                                    <option value="daikundi">daikundi</option>
                                                                                    <option value="faryab">faryab</option>
                                                                                    <option value="ghazni">ghazni</option>
                                                                                    <option value="ghor">ghor</option>
                                                                                    <option value="helmand">helmand</option>
                                                                                    <option value="herat">herat</option>
                                                                                    <option value="jowzjan">jowzjan</option>
                                                                                    <option value="kandahar">kandahar</option>
                                                                                    <option value="kapisa">kapisa</option>
                                                                                    <option value="khost">khost</option>
                                                                                    <option value="kunar">kunar</option>
                                                                                    <option value="kunduz">kunduz</option>
                                                                                    <option value="laghman">laghman</option>
                                                                                    <option value="logar">logar</option>
                                                                                    <option value="maidan wardak">maidan wardak</option>
                                                                                    <option value="nangarhar">nangarhar</option>
                                                                                    <option value="paktia">paktia</option>
                                                                                    <option value="paktika">paktika</option>
                                                                                    <option value="panjshir">panjshir</option>
                                                                                    <option value="parwan">parwan</option>
                                                                                    <option value="samangaan">samangaan</option>
                                                                                    <option value="sar-e-pul">sar-e-pul</option>
                                                                                    <option value="takhar">takhar</option>
                                                                                    <option value="zabul">zabul</option>
                                                                                    <option value="nimroz">nimroz</option>
                                                                                    <option value="farah">farah</option>
                                                                                    <option value="nuristan">nuristan</option>
                                                                                    <option value="uruzgan">uruzgan</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label>{{__('Fee')}}<span class="text-danger">*</span></label>
                                                                                <input required="" type="text" value="{{ $doctor->doctor_fee}}"class="form-control" name="fee">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label> {{__('Date of Birth')}}<span class="text-danger">*</span></label>
                                                                        <div >
                                                                            <input required="" type="date" type="text" value="{{ $doctor->doctor_birthday}}"class="form-control datetimepicker" name="birthday">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('Avatar')}}</label>
                                                                        <div class="profile-upload">
                                                                            <div class="upload-input">
                                                                                <input type="file" value="{{ $doctor->doctor_picture}}" class="form-control" name="photo">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('Years Of Experience')}} <span class="text-danger">*</span></label>
                                                                        <input required=""  maxlength="10" pattern="\d{10}" title="Please enter exactly 10 digits" value="{{ $doctor->experience}}"minlength="10"class="form-control" type="number" name="Experience">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('MOHEF License Number')}}<span class="text-danger">*</span></label>
                                                                        <input required=""  value="{{ $doctor->pmdc}}"class="form-control" type="tel" name="pmdc_num">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>{{__('Short Biography')}} <span class="text-danger">*</span></label>
                                                                <textarea required="" class="form-control"  rows="3" cols="30" name="short_biography">{{ $doctor->doctor_Short_Biography}}</textarea>
                                                            </div>
                                                            <div class="m-t-20 text-center">
                                                                <button type="submit" class="btn btn-primary submit-btn">{{__('save')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>			
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Business Hours Content -->
                            <div role="tabpanel" id="Pro_Profile" class="tab-pane fade  @if(Request::segment(1)=='en') @else text-right @endif" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="card-box">
                                                    <h3 class="card-title"> {{__('Primary Specialty')}}<span  onclick="addSpeciality('{{$doctor->doctorID}}')"class="fa fa-plus text-success btn btn-default border border-2"></span></h3>

                                                    <div class="experience-box">
                                                        <div class="row">
                                                            @foreach($doctor->MyPrimary as $spa)
                                                            <div class="col-md-4 p-1">
                                                                <button type="button" id="serv_name" class="btn btn-default" style="padding-top: 0;padding-bottom: 0;">
                                                                    <a href="{{route('delete.Speciality',[$spa->id_s_d])}}" style=""> 
                                                                        <span  class="remove"style="position: relative;left: -12px;display: inline-block;padding: 3px 6px;border-radius: 20px 20px 20px 20px;margin: 5px 5px;">
                                                                            <i class="btn btn-sm bg-danger-light"> <b>X</b> </i> </span></a>   {{$spa->specialityName}}
                                                                </button>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="card-box">
                                                    <h3 class="card-title"> {{__('Secondary Specialty')}}<span  onclick="add_Secondry_Speciality('{{$doctor->doctorID}}')"class="fa fa-plus text-success btn btn-default border border-2"></span></h3>
                                                    <div class="experience-box">
                                                        <div class="row">
                                                            @foreach($doctor->speciality as $spa)
                                                            <div class="col-md-4 p-1">
                                                                <button type="button" id="serv_name" class="btn btn-default" style="padding-top: 0;padding-bottom: 0;">
                                                                    <a href="{{route('delete.Speciality',[$spa->id_s_d])}}" style=""> 
                                                                        <span class="remove"style="position: relative;left: -12px;display: inline-block;padding: 3px 6px;border-radius: 20px 20px 20px 20px;margin: 5px 5px;">
                                                                            <i class="btn btn-sm bg-danger-light"><b>X</b></i> </span></a>{{$spa->specialityName}}
                                                                </button>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="card-box">
                                                    <h3 class="card-title">{{__('My Services')}}<span  onclick="addservice('{{$doctor->doctorID}}')"class="fa fa-plus text-success btn btn-default border border-2"></span></h3>
                                                    <div class="experience-box">
                                                        <div class="row">
                                                            @foreach($doctor->my_services as $service)
                                                            <div class="col-md-4 p-1">
                                                                <button type="button" id="serv_name" class="btn  btn-default " style="padding-top: 0;padding-bottom: 0;">
                                                                    <a href="{{route('delete.service.doctor',[$service->id])}}" style=""> 
                                                                        <span class="remove"style="position: relative;left: -12px;display: inline-block;padding: 3px 6px;border-radius: 20px 20px 20px 20px;margin: 5px 5px;">
                                                                            <i class="btn btn-sm bg-danger-light"><b>X</b></i>
                                                                        </span>
                                                                    </a>{{$service->service_name}}
                                                                </button>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="card-box">
                                                    <h3 class="card-title">{{__('Condition Treatment')}}<span  onclick="addcondition('{{$doctor->doctorID}}')"class="fa fa-plus text-success btn btn-default border border-2"></span></h3>
                                                    <div class="experience-box">
                                                        <div class="row">
                                                            @foreach($doctor->my_Condition as $condition)
                                                            <div class="col-md-4 p-1">
                                                                <button type="button" id="serv_name" class="btn  btn-default " style="padding-top: 0;padding-bottom: 0;">
                                                                    <a href="{{route('delete.condition.doctor',[$condition->fkid])}}" style=""> 
                                                                        <span class="remove"style="position: relative;left: -12px;display: inline-block;padding: 3px 6px;border-radius: 20px 20px 20px 20px;margin: 5px 5px;">
                                                                            <i class="btn btn-sm bg-danger-light"><b>X</b></i>
                                                                        </span>
                                                                    </a>{{$condition->name}}
                                                                </button>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="card-box">
                                                    <h3 class="card-title"> {{__('Education Information')}}<span  onclick="addeductaion('{{$doctor->doctorID}}')"class="fa fa-plus text-success btn btn-default border border-2"></span></h3>
                                                    <div class="experience-box">
                                                        <ul class="experience-list">
                                                            @foreach($doctor->doctor_education as $edu)
                                                            <li>
                                                                <div class="experience-content">
                                                                    <span class="edit"  onclick="editEducation('{{$edu->D_E_ID}}','{{$edu->school_name}}','{{$edu->title_of_study}}','{{$edu->start_Date}}','{{$edu->end_Date}}')"  style="position: relative;left: -12px;display: inline-block;padding: 3px 6px;border-radius: 20px 20px 20px 20px; " >
                                                                        <i class="fa fa-edit"></i>
                                                                    </span>
                                                                    <a href="{{route('delete.education',[$edu->D_E_ID])}}" style=""> 
                                                                        <span class="remove" style="position: relative;left: -12px;display: inline-block;padding: 3px 6px;border-radius: 20px 20px 20px 20px;margin: 5px 5px;">
                                                                            <i class="fa fa-remove"></i> 
                                                                        </span>
                                                                    </a>
                                                                    <div class="timeline-content">
                                                                        <a  class="name">{{$edu->school_name}}</a>
                                                                        <div>{{$edu->title_of_study}}</div>
                                                                        <span class="time">{{$edu->start_Date }}- {{$edu->end_Date}}</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- Locations Content -->
                            <div role="tabpanel" id="doc_locations" class="tab-pane fade" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
                                <h3 class="@if(Request::segment(1)=='en') @else text-right @endif">{{__('List of My clinics and hospitals.')}}</h3>
                                <!-- Location List -->
                                @foreach($doctor->my_clinic as $clinic)
                                <div class="location-list">
                                    <div class="row">
                                        <!-- Clinic Content -->
                                        <div class="col-md-4">
                                            <div class="clinic-content">
                                                <h4 class="clinic-name"><a href="#"></a>{{$clinic->name}}</h4>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="clinic-content">
                                                <p class="doc-speciality">{{$clinic->address}}</p>
                                            </div>
                                        </div>
                                        <!-- /Clinic Timing -->
                                        <div class="col-md-4">
                                            <div class="consult-price">
                                                <a href="{{route('remove.clinic.doctor',[$clinic->address_id])}}" class="btn btn-sm bg-danger-light">
                                                    {{__('remove')}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Location List -->
                                @endforeach
                                <br>
                                <h3 class="@if(Request::segment(1)=='en') @else text-right @endif"> {{__('List of all available clinics and hospitals.')}}<span  onclick="add_clinic('{{$doctor->doctorID}}')"class="fa fa-plus text-success btn btn-default border border-2"></span></h3>
                                <div class="tab-pane show" id="upcoming-appointments">
                                    <div class="card card-table mb-0">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-center mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>{{__('Clinic Name')}}</th>
                                                            <th>{{__('Address')}}</th>
                                                            <th>{{__('Action')}}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($doctor->all_clinic as $clinic)
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <p> {{$clinic->name}} </p>
                                                                </h2>
                                                            </td>
                                                            <td>{{$clinic->address}}</td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="{{route('save.clinic.doctor',[$clinic->id])}}" class="btn btn-sm bg-success-light">
                                                                       {{__('add this')}} 
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>		
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Locations Content -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /Business Hours Content -->

        </div>
    </div>
</div>
<!-- /Doctor Details Tab -->
@endforeach
</div>


</div>
</div>
</div>
</div>
@include('doctor.footer')
<script src="{{asset("assets/js/jquery.min.js")}}"></script>
<script src="{{asset("assets/js/popper.min.js")}}"></script>
<script src="{{asset("assets/js/bootstrap.min.js")}}"></script>

<script src="{{asset('assets/plugins/theia-sticky-sidebar/ResizeSensor.js')}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')}}"></script>

<!-- Circle Progress JS -->
<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>

<!-- Dropzone JS -->
<script src="{{asset('assets/plugins/dropzone/dropzone.min.js')}}"></script>

<!-- Bootstrap Tagsinput JS -->
<script src="{{asset('assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js')}}"></script>

<!-- Profile Settings JS -->
<script src="{{asset('assets/js/profile-settings.js')}}"></script>

<script src="{{asset("assets/js/script.js")}}"></script>
<div class="sidebar-overlay"></div>
<script type="text/javascript">

                                    function addSpeciality(doctorid){

                                    $('#doctoraddSpeciality').val(doctorid);
                                    $('#model_add_Speciality').modal('show');
                                    }

                                    function add_Secondry_Speciality(doctorid){
                                    $('#doctor_experiance_id').val(doctorid);
                                    $('#model_add_expariance').modal('show');
                                    }

                                    function addservice(doctorid){
                                    $('#doc_id_s').val(doctorid);
                                    $('#model_add_services').modal('show');
                                    }
                                    function add_clinic(doctorid){

                                    $('#model_add_clinic').modal('show');
                                    }
                                    function addcondition(doctorid){

                                    $('#model_add_condition').modal('show');
                                    }

                                    function editcall(id, name){

                                    $('#servID').val(id);
                                    $('#servicename').val(name);
                                    $('#model_services').modal('show');
                                    }

                                    function addeductaion(doctorid){

                                    $('#doc_id_edu').val(doctorid);
                                    $('#model_add_edu').modal('show');
                                    }


                                    function editEducation(ed_id, school_name, study_title, start, end){
                                    $('input[name="eduction_id"]').val(ed_id);
                                    $('input[name="School_name"]').val(school_name);
                                    $('input[name="Study_title"]').val(study_title);
                                    $('input[name="start_date"]').val(start);
                                    $('input[name="end_date"]').val(end);
                                    $('#model_update_edu').modal('show');
                                    }

                                    function editexperiance(ed_id, school_name, start, end){
                                    $('input[name="eduction_idxx"]').val(ed_id);
                                    $('input[name="School_namexx"]').val(school_name);
                                    $('input[name="start_datexx"]').val(start);
                                    $('input[name="end_datexx"]').val(end);
                                    $('#model_update_experiance').modal('show');
                                    }


</script>
<div class="modal fade" id="model_add_Speciality" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('add Primary Speciality')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" action="{{route('add.primary.Speciality')}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">

                                    <select class="form-control" name="spaciality_id">
                                        @foreach($doctor->get_my_none_spa as $spa)
                                        <option value="{{$spa->specialityID}}">{{$spa->specialityName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                                <input type="submit" name="upload" value="{{__('add Primary Speciality')}}" class="btn btn-success btn-sm" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">{{__('Close')}}</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="model_add_expariance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('Add Secondry Speciality')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" action="{{route('add.secondry.Speciality')}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select class="form-control" name="add_spaciality_id">
                                        @foreach($doctor->get_my_none_spa as $spa)

                                        <option value="{{$spa->specialityID}}">{{$spa->specialityName}}</option>

                                        @endforeach
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6">
                                <input type="submit" name="upload" value="{{__('Add Secondry Speciality')}}" class="btn btn-success btn-sm" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">{{__('Close')}}</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="model_add_services" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('Add Service')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" action="{{route('add.doctor.service')}}">
                        @csrf
                        <input type="hidden" id="doctoraddSservices" class="form-control" name="doct_addSpecialityid" />
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select class="form-control" name="service_id">
                                        @foreach($doctor->all_services as $spa)
                                        <option value="{{$spa->services_id}}">{{$spa->service_name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6">
                                <input type="submit" name="upload" value="{{__('Add Service')}}" class="btn btn-success btn-sm" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">{{__('Close')}}</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="model_add_condition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('Add Condition')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" action="{{route('add.doctor.condition')}}">
                        @csrf
                        <input type="hidden" id="doctoraddSservices" class="form-control" name="doct_addSpecialityid" />
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select class="form-control" name="condition_id">
                                        @foreach($doctor->all_Condition as $spa)
                                        <option value="{{$spa->id}}">{{$spa->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6">
                                <input type="submit" name="upload" value="{{__('Add Condition')}}" class="btn btn-success btn-sm" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">{{__('Close')}}</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="model_update_edu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('update Education')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body ">
                <div class="panel-body">
                    <form method="post" action="{{route('update.education') }}">
                        @csrf
                        <div class="row form-group">
                            <input type="hidden" id="doc_id" class="form-control" name="eduction_id" />
                            <div class="col-md-4">
                                <label>{{__('School Name')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" placeholder="{{__('School Name')}}"class="form-control" name="School_name"  />
                            </div>

                        </div>

                        <div class="row form-group">
                            <div class="col-md-4">
                                <label>{{__('Study Title')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" placeholder="{{__('Study Title')}}"class="form-control" name="Study_title"  />
                            </div>

                        </div>
                        <div class="row ">
                            <div class="col-md-4">
                                <label class="label">{{__('start date')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" id="service_name" placeholder="{{__('start date')}}"class="form-control" name="start_date"  />
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-4">
                                <label>{{__('End date')}} </label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" id="service_name" placeholder="{{__('End date')}}"class="form-control" name="end_date"  />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <input type="submit" name="{{__('save')}}" value="Save" class="btn btn-success" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">{{__('Close')}}</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="model_services" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" action="{{route('update.service') }}">
                        @csrf
                        <div class="row">

                            <input type="hidden" id="servID" class="form-control" name="service_id" />
                            <div class="col-md-9">
                                <input type="text" id="servicename" class="form-control" name="service_name"  />
                            </div>
                            <div class="col-md-3">
                                <input type="submit" name="upload" value="update" class="btn btn-success" />
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">{{__('Close')}}</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="model_add_edu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('Add Education')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" action="{{route('add.education') }}">
                        @csrf
                        <div class="row form-group">
                            <input type="hidden" id="doc_id_edu" class="form-control" name="doctor_id_edu" />
                            <div class="col-md-4">
                                <label>{{__('School Name')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" placeholder="School name"class="form-control" name="School_name"  />
                            </div>

                        </div>

                        <div class="row form-group">
                            <div class="col-md-4">
                                <label>{{__('Study Title')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" placeholder="Study title" class="form-control" name="Study_title"  />
                            </div>

                        </div>
                        <div class="row ">
                            <div class="col-md-4">
                                <label class="label">{{__('start date')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" id="service_name" placeholder="start date"class="form-control" name="start_date"  />
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-4">
                                <label>{{__('End date')}} </label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" id="service_name" placeholder="End date "class="form-control" name="end_date"  />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <input type="submit" name="upload" value="{{__('save')}}" class="btn btn-success" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">{{__('Close')}}</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="model_add_clinic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('Add New Clinic Hospital')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" action="{{route('add.new.clinic') }}">
                        @csrf
                        <div class="row form-group">
                            <input type="hidden" id="doc_id_edu" class="form-control" name="doctor_id_edu" />
                            <div class="col-md-4">
                                <label>{{__('Name')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" placeholder="clinic & hospital name"class="form-control" name="name"  />
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-4">
                                <label>{{__('Address')}}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" placeholder="clinic & hospital address"class="form-control" name="address"  />
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-md-4">
                                <label>{{__('City')}} </label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select required="" id="select_patient" class="form-control" name="city" value="{{$doctor->city}}">
                                        <option value="" >{{__('Select a city')}}</option>
                                        <option value="kabul">kabul</option>
                                        <option value="badakhshan">badakhshan</option>
                                        <option value="badghiz">badghiz</option>
                                        <option value="baghlan">baghlan</option>
                                        <option value="balkh">balkh</option>
                                        <option value="bamiyan">bamiyan</option>
                                        <option value="daikundi">daikundi</option>
                                        <option value="faryab">faryab</option>
                                        <option value="ghazni">ghazni</option>
                                        <option value="ghor">ghor</option>
                                        <option value="helmand">helmand</option>
                                        <option value="herat">herat</option>
                                        <option value="jowzjan">jowzjan</option>
                                        <option value="kandahar">kandahar</option>
                                        <option value="kapisa">kapisa</option>
                                        <option value="khost">khost</option>
                                        <option value="kunar">kunar</option>
                                        <option value="kunduz">kunduz</option>
                                        <option value="laghman">laghman</option>
                                        <option value="logar">logar</option>
                                        <option value="maidan wardak">maidan wardak</option>
                                        <option value="nangarhar">nangarhar</option>
                                        <option value="paktia">paktia</option>
                                        <option value="paktika">paktika</option>
                                        <option value="panjshir">panjshir</option>
                                        <option value="parwan">parwan</option>
                                        <option value="samangaan">samangaan</option>
                                        <option value="sar-e-pul">sar-e-pul</option>
                                        <option value="takhar">takhar</option>
                                        <option value="zabul">zabul</option>
                                        <option value="nimroz">nimroz</option>
                                        <option value="farah">farah</option>
                                        <option value="nuristan">nuristan</option>
                                        <option value="uruzgan">uruzgan</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <input type="submit" name="create" value="{{__('save')}}" class="btn btn-success"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
