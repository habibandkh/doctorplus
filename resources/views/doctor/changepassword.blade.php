<html lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
   <title>DoctorPlus</title>


    <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
<![endif]-->

</head>
@include('doctor.header')
<div class="content" style="transform: none; min-height: 239.6px;" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('doctor.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="card @if(Request::segment(1)=='en') @else text-right @endif">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-6">
                                @if($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>    
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                @if($message = Session::get('danger'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>    
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                <!-- Change Password Form -->
                                <form action="{{route('doctor.password.save')}}" method="post">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label>{{__('Old Password')}}</label>
                                        <input required="" type="password" name="old_pass"class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('New Password')}}</label>
                                        <input required="" id="password"name="new_pass" type="password" class="form-control" >
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('Confirm Password')}}</label>
                                        <input required="" id="confirm_password" type="password" class="form-control">
                                    </div>
                                    <div class="submit-section">
                                        <button type="submit" class="btn btn-primary submit-btn">{{__('Save Changes')}}</button>
                                    </div>
                                </form>
                                <!-- /Change Password Form -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('doctor.footer')
<script type="text/javascript">


    var password = document.getElementById("password")
            , confirm_password = document.getElementById("confirm_password");

    function validatePassword() {
        if (password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;

    function myFunction() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
    function myFunction1() {
        alert('hi');
        var x = document.getElementById("confirm_password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>
<script src="{{asset("assets/js/jquery.min.js")}}"></script>
<script src="{{asset("assets/js/popper.min.js")}}"></script>
<script src="{{asset("assets/js/bootstrap.min.js")}}"></script>

<script src="{{asset('assets/plugins/theia-sticky-sidebar/ResizeSensor.js')}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')}}"></script>

<!-- Circle Progress JS -->
<script src="{{asset('assets/js/circle-progress.min.js')}}"></script>

<script src="{{asset("assets/js/script.js")}}"></script>
<div class="sidebar-overlay"></div>
</body>
</html>
