<html lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
   <title>DoctorPlus</title>

    <!-- Favicons -->
    <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">

    <!-- Bootstrap CSS -->

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Fontawesome CSS -->

    <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">

    <!-- Main CSS -->

    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
<![endif]-->

</head>
@include('doctor.header')
<div class="content" style="transform: none; min-height: 239.6px;" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('doctor.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="row" >
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <h2 class="card-title @if(Request::segment(1)=='en') @else text-center @endif">{{__('Schedule Timing')}} <a class="edit-link float-right" data-toggle="modal" href="#add_time_slot"><i class="fa fa-plus-circle"></i> {{__('Add Slot')}}</a></h2>
                                <hr>
                                @if(session()->has('message'))
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    {{ session()->get('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                <div class="profile-box  @if(Request::segment(1)=='en') @else text-right @endif" >
                                    <div class="d-none">  {{ $counter=0}}</div> 
                                    @foreach($address as $clinic)
                                    <h4 class="card-title">{{__('Name')}}: {{$clinic->name}}</h4>
                                    <h4 class="card-title">{{__('Address')}}: {{$clinic->address}}</h4>
                                    <h5 class="card-title">{{__('Available')}}: ({{ date('H:i A', strtotime($clinic->From))}} , {{ date('H:i A', strtotime($clinic->To))}})<a class="btn btn-sm bg-danger-light   text-center @if(Request::segment(1)=='en') float-right @else float-left @endif"  href="#" onclick="deletetimetable('{{$clinic->address_id}}')"><i class="fa fa-times"></i> {{__('Delete Clinic')}}</a></h5>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card schedule-widget mb-0">
                                                <!-- Schedule Header -->
                                                <div class="schedule-header">
                                                    <div class="d-none">{{  $counter = $counter+1}}</div> 
                                                    <!-- Schedule Nav -->
                                                    <div class="schedule-nav">
                                                        <ul class="nav nav-tabs nav-justified">

                                                            @if(!$clinic->Saturday->isEmpty())
                                                            @if($clinic->Sat->doctor_active ==1)
                                                            <li class="nav-item" ondblclick="tern_off_days('{{$clinic->Sat->vtID}}')" >
                                                                <a class="nav-link" data-toggle="tab" href="#slot_saturday{{$counter}}">{{__('Saturday')}}</a>
                                                            </li>
                                                            @else
                                                            <li class="nav-item " ondblclick="tern_on_days('{{$clinic->Sat->vtID}}')" >
                                                                <a class="nav-link bg-dark" data-toggle="tab" href="#">{{__('Saturday')}}</a>
                                                            </li>
                                                            @endif
                                                            @endif



                                                            @if(!$clinic->Sunday->isEmpty())
                                                            @if($clinic->Sun->doctor_active ==1)
                                                            <li class="nav-item" ondblclick="tern_off_days('{{$clinic->Sun->vtID}}')" >
                                                                <a class="nav-link" data-toggle="tab" href="#slot_sunday{{$counter}}">{{__('Sunday')}}</a>
                                                            </li>
                                                            @else
                                                            <li class="nav-item " ondblclick="tern_on_days('{{$clinic->Sun->vtID}}')" >
                                                                <a class="nav-link bg-dark" data-toggle="tab" href="#">{{__('Sunday')}}</a>
                                                            </li>
                                                            @endif
                                                            @endif



                                                            @if(!$clinic->Monday->isEmpty())
                                                            @if($clinic->Mon->doctor_active ==1)
                                                            <li class="nav-item" ondblclick="tern_off_days('{{$clinic->Mon->vtID}}')" >
                                                                <a class="nav-link" data-toggle="tab" href="#slot_monday{{$counter}}">{{__('Monday')}}</a>
                                                            </li>
                                                            @else
                                                            <li class="nav-item " ondblclick="tern_on_days('{{$clinic->Mon->vtID}}')" >
                                                                <a class="nav-link bg-dark" data-toggle="tab" href="#">{{__('Monday')}}</a>
                                                            </li>
                                                            @endif
                                                            @endif



                                                            @if(!$clinic->Tuesday->isEmpty())
                                                            @if($clinic->Tus->doctor_active ==1)
                                                            <li class="nav-item" ondblclick="tern_off_days('{{$clinic->Tus->vtID}}')" >
                                                                <a class="nav-link" data-toggle="tab" href="#slot_tuesday{{$counter}}">{{__('Tuesday')}}</a>
                                                            </li>
                                                            @else
                                                            <li class="nav-item " ondblclick="tern_on_days('{{$clinic->Tus->vtID}}')" >
                                                                <a class="nav-link bg-dark" data-toggle="tab" href="#">{{__('Tuesday')}}</a>
                                                            </li>
                                                            @endif
                                                            @endif




                                                            @if(!$clinic->Wednesday->isEmpty())
                                                            @if($clinic->Wed->doctor_active ==1)
                                                            <li class="nav-item" ondblclick="tern_off_days('{{$clinic->Wed->vtID}}')" >
                                                                <a class="nav-link" data-toggle="tab" href="#slot_wednesday{{$counter}}">{{__('Wednesday')}}</a>
                                                            </li>
                                                            @else
                                                            <li class="nav-item " ondblclick="tern_on_days('{{$clinic->Wed->vtID}}')" >
                                                                <a class="nav-link bg-dark" data-toggle="tab" href="#">{{__('Wednesday')}}</a>
                                                            </li>
                                                            @endif
                                                            @endif




                                                            @if(!$clinic->Thursday->isEmpty())
                                                            @if($clinic->Thu->doctor_active ==1)
                                                            <li class="nav-item" ondblclick="tern_off_days('{{$clinic->Thu->vtID}}')" >
                                                                <a class="nav-link" data-toggle="tab" href="#slot_thursday{{$counter}}">{{__('Thursday')}}</a>
                                                            </li>
                                                            @else
                                                            <li class="nav-item " ondblclick="tern_on_days('{{$clinic->Thu->vtID}}')" >
                                                                <a class="nav-link bg-dark" data-toggle="tab" href="#">{{__('Thursday')}}</a>
                                                            </li>
                                                            @endif
                                                            @endif




                                                            @if(!$clinic->Friday->isEmpty())
                                                            @if($clinic->Fri->doctor_active ==1)
                                                            <li class="nav-item" ondblclick="tern_off_days('{{$clinic->Fri->vtID}}')" >
                                                                <a class="nav-link" data-toggle="tab" href="#slot_friday{{$counter}}">{{__('Friday')}}</a>
                                                            </li>
                                                            @else
                                                            <li class="nav-item " ondblclick="tern_on_days('{{$clinic->Fri->vtID}}')" >
                                                                <a class="nav-link bg-dark" data-toggle="tab" href="#">{{__('Friday')}}</a>
                                                            </li>
                                                            @endif
                                                            @endif



                                                        </ul>
                                                    </div>
                                                    <!-- /Schedule Nav -->

                                                </div>
                                                <!-- /Schedule Header -->

                                                <!-- Schedule Content -->
                                                <div class="tab-content schedule-cont">
                                                    <!-- Saturday Slot -->
                                                    <div id="slot_saturday{{$counter}}" class="tab-pane fade">
                                                        <div class="doc-times">
                                                            @foreach($clinic->Saturday as $timeslot)
                                                            <div class="m-2 p-2">
                                                                @if($timeslot->time_off_on==1)
                                                                <a  onclick="turn_off_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-success">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button><i class="la la-lightbulb-o" aria-hidden="true"></i></a>
                                                                @else
                                                                <a onclick="turn_on_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-danger">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button></a>
                                                                @endif
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <!-- /Saturday Slot -->
                                                    <!-- Sunday Slot -->
                                                    <div id="slot_sunday{{$counter}}" class="tab-pane fade">
                                                        <div class="doc-times">
                                                            @foreach($clinic->Sunday as $timeslot)
                                                            <div class="m-2 p-2">
                                                                @if($timeslot->time_off_on==1)
                                                                <a  onclick="turn_off_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-success">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button><i class="la la-lightbulb-o" aria-hidden="true"></i></a>
                                                                @else
                                                                <a onclick="turn_on_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-danger">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button></a>
                                                                @endif
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <!-- /Sunday Slot -->

                                                    <!-- Monday Slot -->
                                                    <div id="slot_monday{{$counter}}" class="tab-pane fade ">
                                                        <div class="doc-times">
                                                            @foreach($clinic->Monday as $timeslot)
                                                            <div class="m-2 p-2">
                                                                @if($timeslot->time_off_on==1)
                                                                <a  onclick="turn_off_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-success">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button><i class="la la-lightbulb-o" aria-hidden="true"></i></a>
                                                                @else
                                                                <a onclick="turn_on_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-danger">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button></a>
                                                                @endif
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                        <!-- /Slot List -->
                                                    </div>
                                                    <!-- /Monday Slot -->

                                                    <!-- Tuesday Slot -->
                                                    <div id="slot_tuesday{{$counter}}" class="tab-pane fade">
                                                        <div class="doc-times">
                                                            @foreach($clinic->Tuesday as $timeslot)
                                                            <div class="m-2 p-2">
                                                                @if($timeslot->time_off_on==1)
                                                                <a  onclick="turn_off_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-success">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button><i class="la la-lightbulb-o" aria-hidden="true"></i></a>
                                                                @else
                                                                <a onclick="turn_on_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-danger">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button></a>
                                                                @endif
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <!-- /Tuesday Slot -->

                                                    <!-- Wednesday Slot -->
                                                    <div id="slot_wednesday{{$counter}}" class="tab-pane fade">
                                                        <div class="doc-times">
                                                            @foreach($clinic->Wednesday as $timeslot)
                                                            <div class="m-2 p-2">
                                                                @if($timeslot->time_off_on==1)
                                                                <a  onclick="turn_off_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-success">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button><i class="la la-lightbulb-o" aria-hidden="true"></i></a>
                                                                @else
                                                                <a onclick="turn_on_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-danger">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button></a>
                                                                @endif
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <!-- /Wednesday Slot -->

                                                    <!-- Thursday Slot -->
                                                    <div id="slot_thursday{{$counter}}" class="tab-pane fade">
                                                        <div class="doc-times">
                                                            @foreach($clinic->Thursday as $timeslot)
                                                            <div class="m-2 p-2">
                                                                @if($timeslot->time_off_on==1)
                                                                <a  onclick="turn_off_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-success">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button><i class="la la-lightbulb-o" aria-hidden="true"></i></a>
                                                                @else
                                                                <a onclick="turn_on_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-danger">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button></a>
                                                                @endif
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <!-- /Thursday Slot -->

                                                    <!-- Friday Slot -->
                                                    <div id="slot_friday{{$counter}}" class="tab-pane fade">
                                                        <div class="doc-times">
                                                            @foreach($clinic->Friday as $timeslot)
                                                            <div class="m-2 p-2">
                                                                @if($timeslot->time_off_on==1)
                                                                <a  onclick="turn_off_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-success">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button><i class="la la-lightbulb-o" aria-hidden="true"></i></a>
                                                                @else
                                                                <a onclick="turn_on_time_slot('{{$timeslot->vtID}}')" style="text-decoration: none;"><button class="btn btn-danger">{{ date('H:i a', strtotime($timeslot->visit_time))}}</button></a>
                                                                @endif
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <!-- /Friday Slot -->
                                                </div>
                                                <!-- /Schedule Content -->
                                            </div>
                                        </div>
                                    </div>
                                    <br> <hr>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    function tern_off_days(visit_id)
    {

    if (confirm('Are you sure you want to off this day?')) {
    $(document).ready(function ()
    {
    var id = visit_id;
    var url = '{{route("doctor.day.change", ":id")}}';
    url = url.replace(':id', id);
    window.location.href = url;
    });
    }
    }
    function tern_on_days(visit_id)
    {

    if (confirm('Are you sure you want to on this day?')) {
    $(document).ready(function ()
    {
    var id = visit_id;
    var url = '{{route("doctor.day.change", ":id")}}';
    url = url.replace(':id', id);
    window.location.href = url;
    });
    }
    }
    function deletetimetable(clinicid)
    {

    if (confirm("{{__('Are you sure you want to delete this clinic?')}}")) {
    $(document).ready(function ()
    {
    var id = clinicid;
    var url = '{{route("doctor.delete.timatabel", ":id")}}';
    url = url.replace(':id', id);
    window.location.href = url;
    });
    }
    }
    function turn_off_time_slot(visit_id)
    {

    if (confirm('Are you sure to turn off this time slot?')) {
    $(document).ready(function ()

    {
    var id = visit_id;
    var url = '{{route("doctor.time.change", ":id")}}';
    url = url.replace(':id', id);
    window.location.href = url;
    });
    }

    }

    function turn_on_time_slot(visit_id)
    {

    if (confirm('Are you sure to turn on this time slot?')) {
    $(document).ready(function ()
    {
    var id = visit_id;
    var url = '{{route("doctor.time.change", ":id")}}';
    url = url.replace(':id', id);
    window.location.href = url;
    });
    }
    }

</script>
@include('doctor.footer')
<script src="{{asset("assets/js/jquery.min.js")}}"></script>
<script src="{{asset("assets/js/popper.min.js")}}"></script>
<script src="{{asset("assets/js/bootstrap.min.js")}}"></script>

<script src="{{asset('assets/plugins/theia-sticky-sidebar/ResizeSensor.js')}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')}}"></script>

<!-- Circle Progress JS -->
<script src="{{asset('assets/js/circle-progress.min.js')}}"></script>

<script src="{{asset("assets/js/script.js")}}"></script>
<div class="sidebar-overlay"></div>
<div class="modal fade custom-modal " id="add_time_slot" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span></span> {{__('Add Time Slots')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body @if(Request::segment(1)=='en') @else text-right @endif">
                <form method="post" action="{{route('doctor.create.time')}}">
                    @csrf
                    <div class="hours-info">
                        <div class="row form-row hours-cont">
                            <div class="col-12 col-md-10">
                                <div class="row form-row">
                                    <div class="col-md-12">
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input checked type="checkbox" class="form-check-input" class="" name="days[]" value="Saturday">{{__('Saturday')}}
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input checked type="checkbox" class="form-check-input" class="" name="days[]" value="Sunday">{{__('Sunday')}}
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input checked type="checkbox" class="form-check-input" class="" name="days[]" value="Monday">{{__('Monday')}}
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input checked type="checkbox" class="form-check-input" class="" name="days[]" value="Tuesday">{{__('Tuesday')}}
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input checked type="checkbox" class="form-check-input" class="" name="days[]" value="Wednesday">{{__('Wednesday')}}
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input checked type="checkbox" class="form-check-input" class="" name="days[]" value="Thursday">{{__('Thursday')}}
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input checked type="checkbox" class="form-check-input" class="" name="days[]" value="Friday">{{__('Friday')}}
                                            </label>
                                        </div>

                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label>{{__('Start Time')}}</label>
                                            <div class="time-icon">
                                                <input type="text" name="start_time" class="form-control" id="datetimepicker3">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label>{{__('End Time')}}</label>
                                            <div class="time-icon">
                                                <input type="text" name="end_time"class="form-control" id="datetimepicker4">
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="name">{{__('Interval')}}</label>
                                        <select required name="Interval_time" class="body-font-size form-control frm-input speciail pl-1">
                                            <option value=''>{{__('Select Interval')}}</option>
                                            <option value='300'>5 minute{{__('')}}</option>
                                            <option value='600'>10 minute{{__('')}}</option>
                                            <option value='900'>15 minute{{__('')}}</option>
                                            <option value='1200'>20 minute{{__('')}}</option>
                                            <option value='1500'>25 minute{{__('')}}</option>
                                            <option value='1800'>30 minute{{__('')}}</option>
                                            <option value='2100'>35 minute{{__('')}}</option>
                                            <option value='2400'>40 minute{{__('')}}</option>
                                            <option value='2700'>45 minute{{__('')}}</option>
                                            <option value='3000'>50 minute{{__('')}}</option>
                                            <option value='3300'>55 minute{{__('')}}</option>
                                            <option value='3600'>1 hour{{__('')}}</option>
                                            <option value='5400'>1.5 hour{{__('')}}</option>
                                            <option value='7200'>2 hour{{__('')}}</option>
                                            <option value='9000'>2.5 hour{{__('')}}</option>
                                            <option value='10800'>3 hour{{__('')}}</option>
                                            <option value='12600'>3.5 hour{{__('')}}</option>
                                            <option value='14400'>4 hour{{__('')}}</option>
                                            <option value='16200'>4.5 hour{{__('')}}</option>
                                            <option value='18000'>5 hour{{__('')}}</option>
                                        </select>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">               
                                            <label>{{__('Timing Slot Duration')}}</label>
                                            <select required class="select form-control"name="clinic_address_id">
                                                <option value="">{{__('select clinic')}}</option>
                                                @foreach($my_all_clinic as $my_clinic)
                                                <option value="{{$my_clinic->address_id}}">{{$my_clinic->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>

                    <div class="submit-section text-center">
                        <button type="submit" class="btn btn-primary submit-btn">{{__('Save Changes')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>