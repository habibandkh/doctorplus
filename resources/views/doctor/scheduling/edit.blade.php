<html lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
   <title>DoctorPlus</title>

    <!-- Favicons -->
    <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">

    <!-- Bootstrap CSS -->
  
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Fontawesome CSS -->

    <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">

    <!-- Main CSS -->
    
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
<![endif]-->
</head>
@include('doctor.header')
<div class="content" style="transform: none; min-height: 239.6px;" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('doctor.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="card">
                    <div class="card-body">
                        <div class="page-wrapper">
                            <div class="content">
                                <div class="card text-center" >
                                    DAY:
                                    <div class="card-body">
                                        <h3 class="card-title"> Click on a specific time slot to disable and enable it.</h3>
                                        <div class="row">
                                            @foreach ($Dayshow as $time)
                                            <div class="col-md-3" style="margin: 10px 0px;">
                                                @if($time->time_off_on==1)
                                                <a href="{{route('doctor.time.change', [$time->vtID]) }}" style="text-decoration: none;"><button class="btn btn-success">{{$time->visit_time}}</button><i class="la la-lightbulb-o" aria-hidden="true"></i></a>
                                                @else
                                                <a href="{{route('doctor.time.change', [$time->vtID]) }}" style="text-decoration: none;"><button class="btn btn-danger">{{$time->visit_time}}</button></a>
                                                @endif
                                            </div>
                                            @endforeach  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                            $(document).on('click', '.doctorOn', function () {
                                //alert('success');
                                var id = $(this).attr('id');

                                //$('#form_result').html('');
                                $.ajax({
                                    url: "/ajax-crud/" + id + "/edit",
                                    dataType: "json",
                                    success: function (html) {

                                    }
                                })
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('doctor.footer')
<script src="{{asset("assets/js/jquery.min.js")}}"></script>
<script src="{{asset("assets/js/popper.min.js")}}"></script>
<script src="{{asset("assets/js/bootstrap.min.js")}}"></script>

<script src="{{asset('assets/plugins/theia-sticky-sidebar/ResizeSensor.js')}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')}}"></script>

<!-- Circle Progress JS -->
<script src="{{asset('assets/js/circle-progress.min.js')}}"></script>

<script src="{{asset("assets/js/script.js")}}"></script>
<div class="sidebar-overlay"></div>
</body>
</html>