<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>DoctorPlus</title>
    <!-- Favicons -->
    <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">

    <!-- Bootstrap CSS -->

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Fontawesome CSS -->

    <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
    <!-- Main CSS -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
<![endif]-->

</head>
@include('doctor.header')
<div class="content" style="transform: none; min-height: 239.6px;" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('doctor.sidebar')
            <!-- / Profile Sidebar -->

            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="content">
                    <div class="row @if(Request::segment(1)=='en') @else text-right @endif">
                        <div class="col-12" >
                            <h4  class="page-title">{{__('List of all Notifications of the Doctor')}}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-block">
                                    <div class="table-responsive">
                                        <table class="table mb-0 new-patient-table">
                                            <thead>
                                            <th>{{__('Name')}}</th>
                                            <th>{{__('Message')}}</th>
                                            <th>{{__('Date')}}</th>
                                            <th>{{__('Action')}}</th>
                                            </thead>
                                            <tbody>
                                                @foreach($notifications as $notify)
                                                @if($notify->status==0)
                                                <tr>
                                                    <td>{{$notify->name}}</td>
                                                    <td> 
                                                        <p>{{$notify->text}}</p>
                                                    </td>
                                                    <td>{{$notify->date}}</td>
                                                    <td>
                                                        <a onclick="deleteNoti('{{$notify->id}}')" href="#" style="text-decoration: none;"><button class="btn btn-danger btn-primary-one "><i class="fa fa-remove"></i>{{__('Delete')}} </button></a>
                                                        <a href="{{route('doctor.read.notification', [$notify->id]) }}" style="text-decoration: none;"><button class="btn btn-primary btn-primary-two"><i class="fa fa-eye"></i> {{__('read')}}</button></a>
                                                    </td>
                                                </tr>
                                                @else
                                                <tr>
                                                    <td>{{$notify->name}}</td>
                                                    <td> 
                                                        <p>{{$notify->text}}</p>
                                                    </td>
                                                    <td>{{$notify->date}}</td>
                                                    <td>
                                                        <a onclick="deleteNoti('{{$notify->id}}')" href="#" style="text-decoration: none;"><button class="btn btn-danger btn-primary-one "><i class="fa fa-remove"></i> {{__('Delete')}}</button></a>
                                                    </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                        <br>
                                        <div class="d-flex align-items-center justify-content-center h-100">{{$notifications->links()}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
@include('doctor.footer')
<script>
    function deleteNoti(noti_id)
    {

    if (confirm('Are you sure you want to delete it?')) {
    $(document).ready(function ()
    {
    var id = noti_id;
    var url = '{{route("doctor.delete.noti", ":id")}}';
    url = url.replace(':id', id);
    window.location.href = url;
    });
    }
    }
</script>

<script src="{{asset("assets/js/jquery.min.js")}}"></script>
<script src="{{asset("assets/js/popper.min.js")}}"></script>
<script src="{{asset("assets/js/bootstrap.min.js")}}"></script>

<script src="{{asset('assets/plugins/theia-sticky-sidebar/ResizeSensor.js')}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')}}"></script>

<!-- Circle Progress JS -->
<script src="{{asset('assets/js/circle-progress.min.js')}}"></script>

<script src="{{asset("assets/js/script.js")}}"></script>
<div class="sidebar-overlay"></div>
</body>
</html>