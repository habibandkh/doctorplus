<html lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
   <title>DoctorPlus</title>

    <!-- Favicons -->
    <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">

    <!-- Bootstrap CSS -->

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Fontawesome CSS -->

    <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">

    <!-- Main CSS -->

    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
<![endif]-->

</head>
@include('doctor.header')
<div class="content" style="transform: none; min-height: 239.6px;" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('doctor.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="row" dir="ltr">
                    <div class="col-md-12">
                        <div class="card dash-card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 col-lg-3">
                                        <div class="dash-widget dct-border-rht">
                                            <div class="circle-bar circle-bar1">
                                                <div class="circle-graph1" data-percent="100"><canvas width="500" height="500" style="height: 400px; width: 400px;"></canvas>
                                                    <img src="{{asset('assets/img/icon-01.png')}}" class="img-fluid" alt="patient" style="padding-bottom: 90px;">
                                                </div>
                                            </div>
                                            <div class="dash-widget-info" style="padding-right:90px;padding-bottom: 90px;">
                                                <h6>{{__('Total Patient')}}</h6>
                                                <h3>{{DB::table('appointment')->where('doctor_id',session('id_doctor'))->count()}}</h3>
                                               
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-lg-3">
                                        <div class="dash-widget dct-border-rht" >
                                            <div class="d-none">
                                               
                                            </div>
                                            <div class="circle-bar circle-bar2">
                                                <div class="circle-graph2" data-percent="{{DB::table('appointment')->where('doctor_id',session('id_doctor'))->where('appointment_date',date("Y-m-d"))->count()}}"><canvas width="500" height="500" style="height: 400px; width: 400px;"></canvas>
                                                    <img src="{{asset('assets/img/icon-02.png')}}" class="img-fluid"  style="padding-bottom: 90px;">
                                                </div>
                                            </div>
                                            
                                            <div class="dash-widget-info" style="padding-right:90px;padding-bottom: 90px;">
                                                <h6>{{__('Today Patient')}}</h6>
                                                <h3>{{DB::table('appointment')->where('doctor_id',session('id_doctor'))->where('appointment_date',date("Y-m-d"))->count()}}</h3>
                                               
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-lg-3">
                                        <div class="dash-widget dct-border-rht">
                                            <div class="circle-bar circle-bar3">
                                                <div class="circle-graph3" data-percent="{{DB::table('appointment')->where('doctor_id',session('id_doctor'))->where('appointment_date',date("Y-m-d"))->count()}}"><canvas width="500" height="500" style="height: 400px; width: 400px;"></canvas>
                                                    <img src="{{asset('assets/img/icon-03.png')}}" class="img-fluid" alt="Patient" style="padding-bottom: 90px;">
                                                </div>
                                            </div>
                                            <div class="dash-widget-info" style="padding-right:90px;padding-bottom: 90px;">
                                                <h6>{{__('Today Appointments')}}</h6>
                                                <h3>{{DB::table('appointment')->where('doctor_id',session('id_doctor'))->where('appointment_date',date("Y-m-d"))->count()}}</h3>
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-3">
                                        <div class="dash-widget">
                                            <div class="circle-bar circle-bar3">
                                                <div class="circle-graph3" data-percent="100"><canvas width="500" height="500" style="height: 400px; width: 400px;"></canvas>
                                                    <img src="{{asset('assets/img/icon-03.png')}}" class="img-fluid" alt="Patient" style="padding-bottom: 90px;">
                                                </div>
                                            </div>
                                            <div class="dash-widget-info" style="padding-right:90px;padding-bottom: 90px;">
                                                <h6>{{__('All Appointments')}}</h6>
                                                <h3>{{DB::table('appointment')->where('doctor_id',session('id_doctor'))->count()}}</h3>
                                              
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="mb-4 @if(Request::segment(1)=='en') @else text-right @endif">{{__('Patient Appointments')}}</h4>
                        <!-- /Doctor Widget -->
                        @if(session()->has('message'))
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            {{ session()->get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="appointment-tab">
                            <!-- Appointment Tab -->
                            <ul class="nav nav-tabs nav-tabs-solid nav-tabs-rounded">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#today-appointments" data-toggle="tab">{{__('Today')}}</a>
                                </li> 
                                <li class="nav-item">
                                    <a class="nav-link" href="#upcoming-appointments" data-toggle="tab">{{__('Upcoming')}}</a>
                                </li>

                            </ul>
                            <!-- /Appointment Tab -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="today-appointments">
                                    <div class="card card-table mb-0">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-center mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>{{__('Patient Name')}}</th>
                                                            <th>{{__('Appointment Date')}}</th>

                                                            <th>{{__('clinic name')}}</th>

                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($MyTodayAppointment as  $Today_appo)
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{asset('images/user/'.$Today_appo->pic)}}" alt="User Image"></a>
                                                                    <a href="#">{{$Today_appo->user_name}} </a>
                                                                </h2>
                                                            </td>
                                                            <td>{{$Today_appo->appointment_date}} 
                                                                <span class="d-block text-info">{{$regdate = \Carbon\Carbon::parse($Today_appo->appointment_time)->format('h:i A')}} </span>
                                                            </td>
                                                            <td>
                                                                {{$Today_appo->clinic_name}}
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">

                                                                    <a href="#" onclick="cancelAppointment('{{$Today_appo->appID}}')" class="btn btn-sm bg-danger-light">
                                                                        <i class="fas fa-remove"></i> {{__('Cancel it')}}
                                                                    </a>

                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                        @foreach($MyFamilyTodayAppointment as  $Today_appo)
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{asset('images/pateint/'.$Today_appo->p_photo)}}" alt="User Image"></a>
                                                                    <a href="#">{{$Today_appo->p_name}} </a>
                                                                </h2>
                                                            </td>
                                                            <td>{{$Today_appo->appointment_date}} 
                                                                <span class="d-block text-info">{{$regdate = \Carbon\Carbon::parse($Today_appo->appointment_time)->format('h:i A')}} </span>
                                                            </td>
                                                            <td>
                                                                {{$Today_appo->clinic_name}}
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="#" onclick="cancelAppointment('{{$Today_appo->appID}}')" class="btn btn-sm bg-danger-light">
                                                                        <i class="fas fa-remove"></i> {{__('Cancel it')}}
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>		
                                            </div>
                                        </div>	
                                        <div class="d-flex align-items-center justify-content-center h-100">{{$MyTodayAppointment->links()}}</div>
                                    </div>	
                                </div>
                                <!-- Upcoming Appointment Tab -->
                                <div class="tab-pane show" id="upcoming-appointments">
                                    <div class="card card-table mb-0">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-center mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>{{__('Patient Name')}}</th>
                                                            <th>{{__('Appointment Date')}}</th>
                                                            <th>{{__('clinic name')}}</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($MyUpcommingAppointment as  $Today_appo)
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{asset('images/user/'.$Today_appo->pic)}}" alt="User Image"></a>
                                                                    <a href="#">{{$Today_appo->user_name}} </a>
                                                                </h2>
                                                            </td>
                                                            <td>{{$Today_appo->appointment_date}} 
                                                                <span class="d-block text-info">{{$regdate = \Carbon\Carbon::parse($Today_appo->appointment_time)->format('h:i A')}} </span>
                                                            </td>
                                                            <td>
                                                                {{$Today_appo->clinic_name}}
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">

                                                                    <a href="#" onclick="cancelAppointment('{{$Today_appo->appID}}')" class="btn btn-sm bg-danger-light">
                                                                        <i class="fas fa-remove"></i> {{__('Cancel it')}}
                                                                    </a>

                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                        @foreach($MyFamilyUpcommingAppointment as  $Today_appo)
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{asset('images/pateint/'.$Today_appo->p_photo)}}" alt="User Image"></a>
                                                                    <a href="#">{{$Today_appo->p_name}} </a>
                                                                </h2>
                                                            </td>
                                                            <td>{{$Today_appo->appointment_date}} 
                                                                <span class="d-block text-info">{{$regdate = \Carbon\Carbon::parse($Today_appo->appointment_time)->format('h:i A')}} </span>
                                                            </td>
                                                            <td>
                                                                {{$Today_appo->clinic_name}}
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="#" onclick="cancelAppointment('{{$Today_appo->appID}}')" class="btn btn-sm bg-danger-light">
                                                                        <i class="fas fa-remove"></i> {{__('Cancel it')}}
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>		
                                            </div>
                                        </div>	
                                        <div class="d-flex align-items-center justify-content-center h-100">{{$MyUpcommingAppointment->links()}}</div>
                                    </div>	
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('doctor.footer')
<script>
    function cancelAppointment(app_id)
    {

    if (confirm("{{__('Are you sure you want to cancel this appointment?')}}")) {
    $(document).ready(function ()
    {
    var id = app_id;
    var url = '{{route("cancel.app.by.doctor", ":id")}}';
    url = url.replace(':id', id);
    window.location.href = url;
    });
    }
    }
</script>
<script src="{{asset("assets/js/jquery.min.js")}}"></script>
<script src="{{asset("assets/js/popper.min.js")}}"></script>
<script src="{{asset("assets/js/bootstrap.min.js")}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/ResizeSensor.js')}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')}}"></script>
<script src="{{asset('assets/js/circle-progress.min.js')}}"></script>
<script src="{{asset("assets/js/script.js")}}"></script>
<div class="sidebar-overlay"></div>
</body>
</html>