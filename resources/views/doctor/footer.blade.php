<footer class="footer">
    <!-- Footer Top -->
    <div class="footer-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget footer-about">
                        <div class="footer-logo">
                            <img src="{{asset('assets/img/logofooter.png')}}" alt="logo">
                        </div>
                        <div class="footer-about-content">
                            <p>  {{__('DoctorPlus is an online platform that connects patients and doctors.')}}</p>
                            <div class="social-icon">
                                <ul>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-instagram"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-dribbble"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Footer Widget -->
                </div>
                <div class="col-lg-3 col-md-6">
                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu">
                        <h2 class="footer-title">{{__('For Patients')}}</h2>
                        <ul>
                            @if(session('is_pateint')!='pateint')
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Login')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Register')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('chat')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Medical Records')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}}"><i class="fas fa-angle-double-right"></i> {{__('My doctor')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Patient Dashboard')}}</a></li>
                            @else
                            <li><a href="{{route('user.Private.Chat',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('chat')}}</a></li>
                            <li><a href="{{route('pateint.medical.records',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Medical Records')}}</a></li>
                            <li><a href="{{route('get.my.doctors',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('My doctor')}}</a></li>
                            <li><a href="{{route('pateint.home',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Patient Dashboard')}}</a></li>
                            @endif
                        </ul>
                    </div>
                    <!-- /Footer Widget -->
                </div>
                <div class="col-lg-3 col-md-6">
                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu">
                        <h2 class="footer-title">{{__('For Doctors')}}</h2>
                        <ul>
                            @if(session('is_doctor')!='doctor')
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Login')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Register')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('chat')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i>{{__('My Patients')}} </a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Appointments')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i>{{__('Doctor Dashboard')}} </a></li>
                            @else
                            <li><a href="chat.html"><i class="fas fa-angle-double-right"></i> {{__('Chat')}}</a></li>
                            <li><a href="{{route('doctor.view.patient',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('My Patients')}}</a></li>
                            <li><a href="{{route('doctor.dashboard',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Appointments')}}</a></li>
                            <li><a href="{{route('doctor.dashboard',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Doctor Dashboard')}}</a></li>
                            @endif
                        </ul>
                    </div>
                    <!-- /Footer Widget -->
                </div>
                <div class="col-lg-3 col-md-6">
                    <!-- Footer Widget -->
                    <div class="footer-widget footer-contact">
                        <h2 class="footer-title">{{__('Contact Us')}}</h2>
                        <div class="footer-contact-info">
                            <div class="footer-address">
                                <span><i class="fas fa-map-marker-alt"></i></span>
                                <p> {{__('Sherpur kabul')}}<br> {{__('Afghanistan')}}</p>
                            </div>
                            <p>
                                <i class="fas fa-phone-alt"></i>
                                +93 799858388
                            </p>
                            <p class="mb-0">
                                <i class="fas fa-envelope"></i>
                                admin@doctorplus.af
                            </p>
                        </div>
                    </div>
                    <!-- /Footer Widget -->

                </div>

            </div>
        </div>
    </div>
    <!-- /Footer Top -->
    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="container-fluid">
            <!-- Copyright -->
            <div class="copyright">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="copyright-text">
                            <p class="mb-0">© {{__('© 2020 DoctorPlus. All rights reserved to Focus.')}}</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <!-- Copyright Menu -->
                        <div class="copyright-menu">
                            <ul class="policy-menu">
                                <li><a href="term-condition.html">{{__('Terms and Conditions')}}</a></li>
                                <li><a href="privacy-policy.html">{{__('Policy')}}</a></li>
                            </ul>
                        </div>
                        <!-- /Copyright Menu -->
                    </div>
                </div>
            </div>
            <!-- /Copyright -->
        </div>
    </div>
    <!-- /Footer Bottom -->
</footer>
<!-- /Footer -->
<script>

    function getnotif() {
//alert('hi');

    $.ajax({

    url: "{{route('doctor.unread.appointment.notification')}}",
            dataType: "json",
            success: function (data) {
            var notif = '';
            if (data.success) {
            var response = data.success;
            for (var i in response) {
            notif = notif + '<li>' + '<div >' + '' + '<p class="noti-title">' + response[i].text + '' + '</p>' + '' + '</div>' + '</li>';
            }
            $('.notification-box').text('');
            $('.notification-box').append(notif);
            }
            },
            });
    }

</script>
<script>
    var receiver_id = '';
    var my_id = {{session('id_doctor')}};
    $(document).ready(function () {

    // ajax setup form csrf token
    $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;
    var pusher = new Pusher('4cb4028f2ae5f863e78e', {
    cluster: 'ap2',
            forceTLS: true
    });
    var channel = pusher.subscribe('my-channel');
    channel.bind('my-event', function (data) {

    // alert(JSON.stringify(data));
    if (my_id == data.from) {

    $('#' + data.to).click();
    }
    else if (my_id == data.to) {
    if (receiver_id == data.from) {

    // if receiver is selected, reload the selected user ...
    $('#' + data.from).click();
    } else {
    // if receiver is not seleted, add notification for that user
    var pending = parseInt($('#' + data.from).find('.pending').html());
    if (pending) {
    $('#' + data.from).find('.pending').html(pending + 1);
    } else {
    $('#' + data.from).append('<span class="pending">1</span>');
    }
    }
    }
    });
    ////////event-click-to-get-chat-data
    $('.users').click(function () {
    //alert('hi');
    $('.users').removeClass('active');
    $(this).addClass('active');
    $(this).find('.pending').remove();
    receiver_id = $(this).attr('id');
    var url = "{{ route('doctor.Get.Chat', ":receiver_id") }}";
    url = url.replace(':receiver_id', receiver_id);
    $.ajax({
    type: "get",
            url: url,
            data: "",
            cache: false,
            success: function (data) {
            $('#messages_contect_1').html(data);
            }
    });
    });
    //////////////////send message
    $(document).on('click', '.input-group-append .msg-send-btn', function (e) {
    var message = $('#input-msg-send').val();
    // check if enter key is pressed and message is not null also receiver is selected
    if (message != '' && receiver_id != '') {
    //alert(receiver_id);
    $('#input-msg-send').val(''); // while pressed enter text box will be empty

    $.ajax({
    type: "post",
            url: "{{route('send.message.u')}}", // need to create this post route
            enctype: 'multipart/form-data',
            data: {receiver_id:receiver_id, message:message},
            cache: false,
            success: function (data) {


            },
            error: function (jqXHR, status, err) {
            },
            complete: function () {
            scrollToBottomFunc();
            }
    })
    }
    });
    });
    function uploadFile() {
    var mess = $('#message').val();
    var receiverid = $('#receiver_id').val();
    var fd = new FormData(),
            myFile = document.getElementById("file").files[0];
    fd.append('file', myFile);
    fd.append('message', mess);
    fd.append('receiver_id', receiverid);
    $.ajax({
    url: "{{route('upload.File.To.User')}}",
            enctype: 'multipart/form-data',
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(data){
            if (data.success){

            $('#success').html('<div class="text-success text-center"><b>' + data.success + '</b></div><br /><br />');
            $('#send_file_to_doctor').modal('hide');
            } else{
            $('#success').html('<div class="text-danger text-center"><b>' + data.error + '</b></div><br /><br />');
            }

            }
    });
    }

// make a function to scroll down auto
    function scrollToBottomFunc() {
//alert('scroll');
    $('.chat-body .chat-scroll').animate({
    scrollTop: $('.chat-scroll').get(0).scrollHeight
    }, 200);
    $(".chat-scroll").animate({ scrollTop: 20000000 }, "slow");
    }
</script>

