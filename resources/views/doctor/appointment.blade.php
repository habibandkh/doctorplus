<html lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>DoctorPlus</title>

    <!-- Favicons -->
    <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">

    <!-- Bootstrap CSS -->

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Fontawesome CSS -->

    <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">

    <!-- Main CSS -->

    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    
<![endif]-->

</head>

@include('doctor.header')
<div class="content" style="transform: none; min-height: 239.6px;" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('doctor.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="appointments">
                    <!-- Appointment List -->
<!--                    <div class="appointment-list">
                        <div class="profile-info-widget">
                            <a href="patient-profile.html" class="booking-doc-img">
                                <img src="assets/img/patients/patient.jpg" alt="User Image">
                            </a>
                            <div class="profile-det-info">
                                <h3><a href="patient-profile.html">Ali khan</a></h3>
                                <div class="patient-details">
                                    <h5><i class="far fa-clock"></i> 14 Nov 2019, 10.00 AM</h5>
                                    <h5><i class="fas fa-map-marker-alt"></i> kabul, Ansari</h5>
                                    <h5><i class="fas fa-envelope"></i> alikhan786@gmail.com</h5>
                                    <h5 class="mb-0"><i class="fas fa-phone"></i> +93 7XXXXXXXX</h5>
                                </div>
                            </div>
                        </div>
                        <div class="appointment-action">
                            <a href="#" class="btn btn-sm bg-info-light" data-toggle="modal" data-target="#appt_details">
                                <i class="far fa-eye"></i> View
                            </a>
                            <a href="javascript:void(0);" class="btn btn-sm bg-success-light">
                                <i class="fas fa-check"></i> Accept
                            </a>
                            <a href="javascript:void(0);" class="btn btn-sm bg-danger-light">
                                <i class="fas fa-times"></i> Cancel
                            </a>
                        </div>
                    </div>-->
                    <!-- /Appointment List -->

             

                    @foreach ($appointment as $app)
                    <!-- Appointment List -->
                    <div class="appointment-list">
                        <div class="profile-info-widget">
                            <a href="patient-profile.html" class="booking-doc-img">
                                <img src="assets/img/patients/patient.jpg" alt="User Image">
                            </a>
                            <div class="profile-det-info">
                                <h3><a href="patient-profile.html">{{$app->p_name}}</a></h3>
                                <div class="patient-details">
                                    <h5><i class="far fa-clock"></i> {{$app->appointment_date}}, {{$app->appointment_time}} AM</h5>
                                    <h5><i class="fas fa-map-marker-alt"></i> Newyork, United States</h5>

                                    <h5 class="mb-0"><i class="fas fa-phone"></i> {{$app->p_phoneNo}}</h5>
                                </div>
                            </div>
                        </div>
                        <div class="appointment-action">
                            <a href="#" class="btn btn-sm bg-info-light" data-toggle="modal" data-target="#appt_details">
                                <i class="far fa-eye"></i> {{__('View')}}
                            </a>
                            <a href="javascript:void(0);" class="btn btn-sm bg-success-light">
                                <i class="fas fa-check"></i> {{__('Accept')}}
                            </a>
                            <a href="javascript:void(0);" class="btn btn-sm bg-danger-light">
                                <i class="fas fa-times"></i> {{__('Cancel')}}
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="d-flex align-items-center justify-content-center h-100">{{$appointment->links()}}</div>
            </div>
        </div>

    </div>

</div>

@include('doctor.footer')

<script src="{{asset("assets/js/jquery.min.js")}}"></script>
<script src="{{asset("assets/js/popper.min.js")}}"></script>
<script src="{{asset("assets/js/bootstrap.min.js")}}"></script>

<script src="{{asset('assets/plugins/theia-sticky-sidebar/ResizeSensor.js')}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')}}"></script>

<!-- Circle Progress JS -->
<script src="{{asset('assets/js/circle-progress.min.js')}}"></script>

<script src="{{asset("assets/js/script.js")}}"></script>
<div class="sidebar-overlay"></div>
</body>
</html>