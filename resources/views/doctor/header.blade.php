<head>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
        <script>

// Enable pusher logging - don't include this in production
Pusher.logToConsole = true;
var pusher = new Pusher('4cb4028f2ae5f863e78e', {
    cluster: 'ap2',
    forceTLS: true
});

var channel = pusher.subscribe('isAppointment-channel');
channel.bind('isAppointment-event', function (data) {
    //  alert(data.doctorid);
    var x = $('#messages').text();
    var y = 1;
    var xx = parseInt(x);
    var z = xx + y;
    $('#messages').text('');
    $('#messages').append(z);
//    var x = document.getElementById("myAudio");
//    x.play();
});
        </script>
      <style>


            .nav-pills .nav-link.active, .nav-pills .show > .nav-link{
                background-color: #17A2B8;
            }
            .dropdown-menu{
                top: 60px;
                right: 0px;
                left: unset;
                width: 460px;
                box-shadow: 0px 5px 7px -1px #c1c1c1;
                padding-bottom: 0px;
                padding: 0px;
            }
            .dropdown-menu:before{
                content: "";
                position: absolute;
                top: -20px;
                right: 12px;
                border:10px solid #343A40;
                border-color: transparent transparent #343A40 transparent;
            }
            .MY-head{
                padding:10px 10px;
                border-radius: 3px 3px 0px 0px;
                width: 100%;
            }

            .notification-box{
                padding: 10px 0px; 
            }
            .bg-gray{
                background-color: #eee;
            }
            @media (max-width: 640px) {
                .dropdown-menu{
                    top: 50px;
                    left: -16px;  
                    width: 290px;
                } 
                .nav{
                    display: block;
                }
                .nav .nav-item,.nav .nav-item a{
                    padding-left: 0px;
                }
                .message{
                    font-size: 13px;
                }
            }
        </style>
</head>
<body>
    <!-- Main Wrapper -->
    <div class="main-wrapper">

        <!-- Header -->
        <header class="header">
            <nav class="navbar navbar-expand-lg header-nav">
                <div class="navbar-header">
                    <a id="mobile_btn" href="javascript:void(0);">
                        <span class="bar-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </a>
                    <a href="{{route('home')}}" class="navbar-brand logo">
                        <img src="{{asset('assets/img/logo.png')}}" class="img-fluid" alt="Logo">
                    </a>
                </div>
                <div class="main-menu-wrapper">
                    <div class="menu-header">
                        <a href="{{route('home')}}" class="menu-logo">
                            <img src="{{asset('assets/img/logo.png')}}" class="img-fluid" alt="Logo">
                        </a>
                        <a id="menu_close" class="menu-close" href="javascript:void(0);">
                            <i class="fas fa-times"></i>
                        </a>
                    </div>
                    <ul class="main-nav">
                        
                        <li><a href="{{route('home')}}" >{{__('Home')}}</a></li>
                        <li><a href="{{route('public.blog.list')}}" >{{__('Blog')}}</a></li>
                        <li><a href=" "></a></li>
                        
               <li class="has-submenu">
                    <a href="#">{{__('Language')}} <i class="fas fa-chevron-down"></i></a>
                    <ul class="submenu" >
                        <li class="text-center"><a href="{{route('locale','en')}}">{{__('English')}}</a></li>
                        <li class="text-center"><a href="{{route('locale','fa')}}">{{__('Dari')}}</a></li>
                        <li class="text-center"><a href="{{route('locale','pa')}}">{{__('Pashto')}}</a></li>
                    </ul>
                </li>
   </ul>
                </div>		 

                <ul class="nav header-navbar-rht">

                    @if(session('is_pateint')!='pateint' && session('is_doctor') !='doctor')
                    <li class="nav-item">
                        <a class="nav-link header-login" href="{{route('login.page.user')}}">{{__('login')}}</a>
                        <a class="nav-link header-login" href="{{url('register-page')}}">{{__('Sign up')}} </a>
                    </li>
                    @endif
                    @if(session('is_doctor')=='doctor')
                    <li class="nav-item dropdown" onclick="getnotif()">
                        <a class="nav-link text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell text-dark"style=""></i> <span class="badge badge-pill text-danger" id="messages"> {{DB::table('doctor_notification')->where('status',0)->where('doctor_id',session('id_doctor'))->count()}}</span>
                        </a>

                        <ul class="dropdown-menu">
                            <li class="MY-head">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span>{{__('Notifications')}}</span>
                                    </div>
                                </div>
                            </li>
                            <hr>
                            <div class="notification-box">

                            </div>

                            <hr>
                            <li class="MY-head">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-12">
                                        <a href="{{route('doctor.Notification')}}" >{{__('View All')}}</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown has-arrow logged-item ">
                        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="true">
                            <span class="user-img">
                                <img class="rounded-circle" src="{{asset('images/doctor/'.session('pic_doctor'))}}" width="31" alt="Ryan Taylor">
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right ">
                            <div class="user-header">
                                <div class="avatar avatar-sm">
                                    <img src="{{asset('images/doctor/'.session('pic_doctor'))}}" alt="User Image" class="avatar-img rounded-circle">
                                </div>
                                <div class="user-text">
                                    <h6>{{session('name_doctor')}}</h6>
                                </div>
                            </div>
                            <a class="dropdown-item" href="{{route('doctor.dashboard')}}">{{__('Dashboard')}}</a>
                            <a class="dropdown-item" href="{{route('doctor.profile')}}">{{__('My Profile')}}</a>
                            <a class="dropdown-item" href="{{route('logout.user')}}">{{__('Logout')}}</a>
                        </div>
                    </li>
                    @endif

                    <!-- User Menu -->
                    @if(session('is_pateint')=='pateint')
                    <li class="nav-item dropdown has-arrow logged-item ">
                        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="true">
                            <span class="user-img">
                                <img class="rounded-circle" src="{{asset('images/user/'.session('pic_pateint'))}}" width="31" alt="Ryan Taylor">
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right ">
                            <div class="user-header">
                                <div class="avatar avatar-sm">
                                    <img src="{{asset('images/user/'.session('pic_pateint'))}}" alt="User Image" class="avatar-img rounded-circle">
                                </div>
                                <div class="user-text">
                                    <h6>{{session('name_pateint')}}</h6>

                                </div>
                            </div>
                            <a class="dropdown-item" href="{{route('pateint.home')}}">{{__('Dashboard')}}</a>
                            <a class="dropdown-item" href="{{route('my.profile')}}">{{__('Profile Settings')}}</a>
                            <a class="dropdown-item" href="{{route('logout.user')}}">{{__('Logout')}}</a>
                        </div>
                    </li>
                    @endif
                    <!-- /User Menu -->

                </ul>
            </nav>
        </header>


