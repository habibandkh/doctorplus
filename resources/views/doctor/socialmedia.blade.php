<html lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>DoctorPlus</title>
    <!-- Favicons -->
    <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
    <!-- Bootstrap CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Fontawesome CSS -->
    <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
    <!-- Main CSS -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
</head>

@include('doctor.header')
<div class="content" style="transform: none; min-height: 239.6px;" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('doctor.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="card " dir="ltr">
                    <div class="card-body">
                        <!-- Social Form -->
                        @if($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>    
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        <!-- Profile Settings Form -->
                        <form method="post" action="{{route('save.social.media')}}" enctype="multipart/form-data">
                            {{csrf_field()}}                                                                                          
                            
                            <div class="row">
                                <div class="col-md-12 col-lg-8">
                                    <div class="form-group">
                                        <label>{{__('Facebook URL')}}</label>
                                        <input type="text" value="{{$socials->facebook}}"name="facebook"class="form-control" placeholder="https://facebook.com/...">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12 col-lg-8">
                                    <div class="form-group">
                                        <label>{{__('Twitter URL')}}</label>
                                        <input type="text" value="{{$socials->Twitter}}" name="Twitter"class="form-control" placeholder="https://twitter.com/...">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12 col-lg-8">
                                    <div class="form-group">
                                        <label>{{__('Instagram URL')}}</label>
                                        <input type="text" value="{{$socials->Instagram}}" name="Instagram"class="form-control" placeholder="https://instagram.com/...">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12 col-lg-8">
                                    <div class="form-group">
                                        <label>{{__('Pinterest URL')}}</label>
                                        <input type="text" value="{{$socials->Pinterest}}" name="Pinterest"class="form-control" placeholder="https://pinterest.com/...">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12 col-lg-8">
                                    <div class="form-group">
                                        <label>{{__('Linkedin URL')}}</label>
                                        <input type="text" value="{{$socials->Linkedin}}" name="Linkedin"class="form-control" placeholder="https://linkedin.com/...">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12 col-lg-8">
                                    <div class="form-group">
                                        <label>{{__('Youtube URL')}}</label>
                                        <input type="text" value="{{$socials->Youtube}}" name="Youtube"class="form-control" placeholder="https://youtube.com/...">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="submit-section">
                                <button type="submit" class="btn btn-primary submit-btn">{{__('Save Changes')}}</button>
                            </div>
                            
                        </form>
                        <!-- /Social Form -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('doctor.footer')
<script src="{{asset("assets/js/jquery.min.js")}}"></script>
<script src="{{asset("assets/js/popper.min.js")}}"></script>
<script src="{{asset("assets/js/bootstrap.min.js")}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/ResizeSensor.js')}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')}}"></script>
<!-- Circle Progress JS -->
<script src="{{asset('assets/js/circle-progress.min.js')}}"></script>
<script src="{{asset("assets/js/script.js")}}"></script>
<div class="sidebar-overlay"></div>
</body>
</html>