<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Doccure</title>

        <!-- Favicons -->
        <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">


        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- Fontawesome CSS -->
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">

        <link rel="stylesheet" href="{{asset('assets/css/bootstrap-datetimepicker.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/plugins/fancybox/jquery.fancybox.min.css')}}">
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://js.pusher.com/7.0/pusher.min.js"></script>


    </head>
    @include('doctor.header')

    <body class="chat-page" data-gr-c-s-loaded="true">
        <div class="main-wrapper">
            <div class="content" style="min-height: 239.6px;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="chat-window">
                                <!-- Chat Left -->
                                <div class="chat-cont-left">
                                    <div class="chat-header">
                                        <span>Chats</span>
                                        <a href="javascript:void(0)" class="chat-compose">
                                            <i class="material-icons">{{__('control point')}}</i>
                                        </a>
                                    </div>
                                    <form class="chat-search">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <i class="fas fa-search"></i>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Search">
                                        </div>
                                    </form>
                                    <div class="chat-users-list">
                                        <div class="chat-scroll ">
                                            @foreach($patients as $user)
                                            <a href="javascript:void(0);" class="media users" id="{{$user->id}}">
                                                <input type="hidden" id="receiver_id" value="{{ $user->id }}">     
                                                <span class="pending"></span>
                                                <div class="media-img-wrap">
                                                    <div class="avatar avatar-online">
                                                        <img src="{{asset('images/user/'.$user->pic)}}" alt="User Image" class="avatar-img rounded-circle">
                                                    </div>
                                                </div>
                                                <div class="media-body">
                                                    <div>
                                                        <div class="user-name">{{$user->name}}</div>
                                                        <!--<div class="user-last-chat">I'll call you later </div>-->
                                                    </div>
                                                    <!--<div>-->
                                                    <!--    <div class="last-chat-time block">8:01 PM</div>-->
                                                    <!--</div>-->
                                                </div>
                                            </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <!-- /Chat Left -->
                                <!-- Chat Right -->
                                <div class="chat-cont-right">
                                    <div class="chat-header">
                                        <a id="back_user_list" href="javascript:void(0)" class="back-user-list">
                                            <i class="material-icons">chevron left</i>
                                        </a>
                                        <div class="media">
                                            <div class="media-img-wrap">
                                                <div class="avatar avatar-online">
                                                    <img src="{{asset('images/doctor/'.$doctorInfos->doctor_picture)}}" alt="User Image" class="avatar-img rounded-circle">
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <div class="user-name">{{$doctorInfos->doctor_name}}</div>
                                                <div class="user-status">{{__('online')}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="messages_contect_1" >

                                    </div>
                                </div>
                                <!-- /Chat Right -->
                            </div>
                        </div>
                    </div>
                    <!-- /Row -->
                </div>
            </div>		
            <!-- /Page Content -->
        </div>
        <!-- /Main Wrapper -->
        <!-- Voice Call Modal -->
        <!-- /Voice Call Modal -->
        <!-- Video Call Modal -->
        <div class="modal fade call-modal" id="video_call">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">

                        <!-- Incoming Call -->
                        <div class="call-box incoming-box">
                            <div class="call-wrapper">
                                <div class="call-inner">
                                    <div class="call-user">
                                        <img class="call-avatar" src="assets/img/patients/patient.jpg" alt="User Image">
                                        <h4>Richard Wilson</h4>
                                        <span>Calling ...</span>
                                    </div>							
                                    <div class="call-items">
                                        <a href="javascript:void(0);" class="btn call-item call-end" data-dismiss="modal" aria-label="Close"><i class="material-icons">call_end</i></a>
                                        <a href="video-call.html" class="btn call-item call-start"><i class="material-icons">videocam</i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Incoming Call -->

                    </div>
                </div>
            </div>
        </div>


        @include('doctor.footer')
       
<script src="{{asset("assets/js/popper.min.js")}}"></script>
<script src="{{asset("assets/js/bootstrap.min.js")}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/ResizeSensor.js')}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')}}"></script>
<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('assets/plugins/fancybox/jquery.fancybox.min.js')}}"></script>
<script src="{{asset('assets/js/circle-progress.min.js')}}"></script>
<script src="{{asset("assets/js/slick.js")}}"></script>
<script src="{{asset("assets/js/script.js")}}"></script>

<div class="sidebar-overlay"></div>
    </body>
</html>