<html lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>DoctorPlus</title>

    <!-- Favicons -->
    <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">

    <!-- Bootstrap CSS -->

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Fontawesome CSS -->

    <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">

    <!-- Main CSS -->

    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
<![endif]-->

</head>
@include('doctor.header')

<div class="content" style="transform: none; min-height: 239.6px;" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
     <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('doctor.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" >
                            <div class="card-header @if(Request::segment(1)=='en') @else text-right @endif">
                                <h4 class="card-title d-inline-block">{{__('Review')}}</h4> 
                            </div>
                            <div class="card-block" style="padding: 10px 30px;">
                                <div class="row">
                                    <div class="col-md-2 text-center "style="padding:20px 0px; ">
                                        <h1 class="rating-num">
                                            {{number_format($stars,1)}}</h1>
                                        <div class="rating">
                                            <div class="d-none">{{$rating=number_format($stars,1)}}</div>
                                            @foreach(range(1,5) as $i)
                                            @if($rating >0)
                                            @if($rating >0.5)
                                            <i class="fas fa-star  filled"></i>
                                            @else
                                            <i class="fas fa-star-half filled" ></i>
                                            @endif
                                            @else
                                            <i class="fas  fa-star"></i>
                                            @endif
                                            <?php $rating--; ?>
                                            @endforeach
                                        </div>
                                        {{ $total}} {{__('Total Review')}}
                                    </div>
                                    <div class="col-md-10 "style="padding:20px 0px; ">
                                        <div class="container"> 
                                            <div class="row ">
                                                <div class="col-md-4" > {{__('Overall Experience')}}</div> 
                                                <div class=" col-md-8">
                                                    <div class="progress"> 
                                                        <div class="progress-bar progress-bar-striped bar-success
                                                             progress-bar-animated" style="width:{{$overall_experience}}%;">{{number_format($overall_experience)}}%</div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <br> 
                                            <div class="row">
                                                <div class=" col-md-4" >{{__('Doctor Checkup')}} </div> 
                                                <div class=" col-md-8">
                                                    <div class="progress"> 
                                                        <div class="progress-bar bg-info progress-bar-striped bar-success
                                                             progress-bar-animated" style="width:{{$doctor_checkup}}%;">{{number_format($doctor_checkup)}}%</div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <br> 
                                            <div class="row ">
                                                <div class=" col-md-4" >{{__('Staff Behavior')}} </div> 
                                                <div class=" col-md-8">
                                                    <div class="progress"> 
                                                        <div class="progress-bar bg-warning progress-bar-striped bar-success
                                                             progress-bar-animated" style="width:{{$staff_behavior}}%;">{{number_format($staff_behavior)}}%</div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <br> 
                                            <div class="row ">
                                                <div class=" col-md-4" >{{__('Clinic Environment')}}</div> 
                                                <div class=" col-md-8">
                                                    <div class="progress"> 
                                                        <div class="progress-bar bg-danger progress-bar-striped bar-success
                                                             progress-bar-animated" style="width:{{$clinic_environment}}%;">{{number_format($clinic_environment)}}%</div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <br> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header @if(Request::segment(1)=='en') @else text-right @endif">
                                <h4  class="page-title">{{__('Patients Comments')}}</h4>
                            </div>
                            <div class="card-block">
                                <div class="table-responsive">
                                    <table class="table mb-0 new-patient-table">
                                        <thead>
                                        <th>{{__('Name')}}</th>
                                        <th>{{__('Comments')}}</th>
                                        <th>{{__('Review')}}</th>
                                        <th>{{__('Date')}}</th>

                                        </thead>
                                        <tbody>
                                            @foreach($feedbacks as $notify)
                                            <tr>
                                                <td>{{$notify->name}}</td>
                                                <td> 
                                                    <p>{{$notify->comment}}</p>
                                                </td>
                                                <td>
                                                    <div class="rating">
                                                        <div class="d-none">  {{$rating=$notify->star_review}} </div>
                                                        @foreach(range(1,5) as $i)
                                                        @if($rating >0)
                                                        @if($rating >0.5)
                                                        <i class="fas fa-star  filled"></i>
                                                        @else
                                                        <i class="fas fa-star-half filled" ></i>
                                                        @endif
                                                        @else
                                                        <i class="fas  fa-star"></i>
                                                        @endif
                                                        <?php $rating--; ?>
                                                        @endforeach
                                                    </div>
                                                </td>
                                                <td>{{$notify->date}}</td>

                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                    <div class="d-flex align-items-center justify-content-center h-100">{{$feedbacks->links()}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
@include('doctor.footer')
<script src="{{asset("assets/js/jquery.min.js")}}"></script>
<script src="{{asset("assets/js/popper.min.js")}}"></script>
<script src="{{asset("assets/js/bootstrap.min.js")}}"></script>

<script src="{{asset('assets/plugins/theia-sticky-sidebar/ResizeSensor.js')}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')}}"></script>

<!-- Circle Progress JS -->
<script src="{{asset('assets/js/circle-progress.min.js')}}"></script>

<script src="{{asset("assets/js/script.js")}}"></script>
<div class="sidebar-overlay"></div>
</body>
</html>