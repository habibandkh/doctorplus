@include('doctor.header')
@include('doctor.sidebar')

@yield('contentmenue')
</div>
<div class="sidebar-overlay" data-reff=""></div>
<script src="{{asset('assets/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/Chart.bundle.js')}}"></script>
<script src="{{asset('assets/js/chart.js')}}"></script>
<script src="{{asset('assets/js/select2.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('assets/js/app.js')}}"></script>
<script>
$(function () {
    $('#datetimepicker3').datetimepicker({
        format: 'HH:mm'
    });
    $('#datetimepicker4').datetimepicker({
        format: 'HH:mm'
    });

});
</script>
<script>
    function getnotif() {
        $.ajax({
            url: "/ajax-unread-appoinment",
            dataType: "json",
            success: function (data) {

                var time = '';
                if (data.success) {

                    var response = data.success;
                    for (var i in response) {
                        time = time + '<li class="notification-message">' + '<a href="{{url("admin/list/notification", [' + response[i].id + ']) }}">' + '<p class="noti-title">' + response[i].name + ' created an appointment  ' + '</p>' + '</a>' + '</li>';
                    }


                    $('.drop-scroll .notification-list').text('');
                    $('.drop-scroll .notification-list').append(time);

                }

            },
        });
    }
</script>

<!--<script>
    $(function () {
        $('#toggle-event').change(function () {
            $('#console-event').html('Toggle: ' + $(this).prop('checked'))
        })
    })
</script>-->

</body>



</html>