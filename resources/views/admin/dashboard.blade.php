@include('admin.header')
@include('admin.sidebar')

@yield('contentmenue')
</div>
<div class="sidebar-overlay" data-reff=""></div>

<script src="{{asset('assetsAdmin/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('assetsAdmin/js/popper.min.js')}}"></script>
<script src="{{asset('assetsAdmin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assetsAdmin/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assetsAdmin/js/Chart.bundle.js')}}"></script>
<script src="{{asset('assetsAdmin/js/chart.js')}}"></script>
<script src="{{asset('assetsAdmin/js/select2.min.js')}}"></script>
<script src="{{asset('assetsAdmin/js/moment.min.js')}}"></script>
<script src="{{asset('assetsAdmin/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('assetsAdmin/js/app.js')}}"></script>
<script>
$(document).ready(function () {

    $('form').ajaxForm({
        beforeSend: function () {
            $('#success').empty();
        },
        uploadProgress: function (event, position, total, percentComplete)
        {
            $('.progress-bar').text(percentComplete + '%');
            $('.progress-bar').css('width', percentComplete + '%');
        },
        success: function (data)
        {
            if (data.errors)
            {
                $('.progress-bar').text('0%');
                $('.progress-bar').css('width', '0%');
                $('#success').html('<span class="text-danger"><b>' + data.errors + '</b></span>');
            }
            if (data.success)
            {
                $('.progress-bar').text('Uploaded');
                $('.progress-bar').css('width', '100%');
                $('#success').html('<span class="text-success"><b>' + data.success + '</b></span><br /><br />');

//                        $('#success').append(data.image);

                $('#success').append(data.pdf);
//                        $('#success').append(data.ext);
            }
        }
    });

});
</script>
<script>
    function getnotif() {
        $.ajax({
            url: "/ajax-unread-notif",
            dataType: "json",
            success: function (data) {

                var time = '';
                if (data.success) {

                    var response = data.success;
                    for (var i in response) {
                        time = time + '<li class="notification-message">' + '<a href="{{url("admin/list/notification", [' + response[i].id + ']) }}">' + '<p class="noti-title">Dr.' + response[i].name + ' requests  to create a Doctor account ' + '</p>' + '</a>' + '</li>';
                    }


                    $('.drop-scroll .notification-list').text('');
                    $('.drop-scroll .notification-list').append(time);

                }

            },
        });
    }
</script>
</body>
</html>