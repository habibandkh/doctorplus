@if(session('type_admin')=='admin')
<div class="sidebar" id="sidebar">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 100%; height: 614px;"><div class="sidebar-inner slimscroll" style="overflow: hidden; width: 100%; height: 614px;">
            <div id="sidebar-menu" class="sidebar-menu">
                <ul>

                    <li class="menu-title"> 
                        <span>Main</span>
                    </li>
                    <li @if(Request::segment(2)=='dashboard') class="active" @endif   > 
                         <a href="{{route('admin.dashboard')}}"><i class="fe fe-home"></i> <span>Dashboard</span></a>
                    </li>
                    <li @if(Request::segment(1)=='all-appointment-list') class="active" @endif   > 
                         <a href="{{route('admin.appointment.list')}}"><i class="fe fe-layout"></i> <span>Appointments</span></a>
                    </li>
<!--                    <li @if(Request::segment(1)=='specialities-list') class="active" @endif   > 
                         <a href="{{route('admin.specialities.list')}}"><i class="fe fe-users"></i> <span>Specialities</span></a>
                    </li>-->
                    <li @if(Request::segment(1)=='phd-admin-doctors') class="active" @endif   > 
                         <a href="{{route('doctor.index')}}"><i class="fe fe-user-plus"></i> <span>Doctors</span></a>
                    </li>
                    <li @if(Request::segment(1)=='pateints-list') class="active" @endif   > 
                         <a href="{{route('admin.pateints.index')}}"><i class="fe fe-user"></i> <span>Patients</span></a>
                    </li>
                    <li @if(Request::segment(3)=='notification') class="active" @endif   > 
                         <a href="{{route('admin.notification')}}"><i class="fe fe-bell"></i> <span>Notification</span></a>
                    </li>


                    <li class="submenu ">
                        <a href="#"><i class="fe fe-document"></i> <span> Blog</span> <span class="menu-arrow"></span></a>
                        <ul style="display: none;">
                            <li @if(Request::segment(1)=='blog-view-add') class="active" @endif   > 
                                 <a href="{{route('blog.list')}}">blogs</a></li>
                            <li @if(Request::segment(1)=='Categories') class="active" @endif   > 
                                 <a href="{{route('Categories')}}">Categories</a></li>
                            <li @if(Request::segment(1)=='Comments-list') class="active" @endif   > 
                                 <a href="{{route('Comments')}}">Comments</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div><div class="slimScrollBar" style="background: rgb(204, 204, 204); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 438.878px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
</div>
@endif

@if(session('type_admin')=='blogger')
<div class="sidebar" id="sidebar">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 100%; height: 614px;"><div class="sidebar-inner slimscroll" style="overflow: hidden; width: 100%; height: 614px;">
            <div id="sidebar-menu" class="sidebar-menu">
                <ul>

                    <li class="menu-title"> 
                        <span>Blogger</span>
                    </li>

                    <li class="submenu ">
                        <a href="#"><i class="fe fe-document"></i> <span> Blog</span> <span class="menu-arrow"></span></a>
                        <ul style="display: none;">
                            <li @if(Request::segment(1)=='blog-view-add') class="active" @endif   > 
                                 <a href="{{route('blog.list')}}">blogs</a></li>
                            <li @if(Request::segment(1)=='Categories') class="active" @endif   > 
                                 <a href="{{route('Categories')}}">Categories</a></li>
                            <li @if(Request::segment(1)=='Comments-list') class="active" @endif   > 
                                 <a href="{{route('Comments')}}">Comments</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div><div class="slimScrollBar" style="background: rgb(204, 204, 204); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 438.878px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
</div>
@endif