<html lang="en"><!-- Mirrored from dreamguys.co.in/demo/doccure/admin/doctor-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Dec 2019 21:07:47 GMT --><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>DoctorPlus</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/feathericon.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/plugins/datatables/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/style.css')}}">

        <!--[if lt IE 9]>
                <script src="assets/js/html5shiv.min.js"></script>
                <script src="assets/js/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            @include('admin.header')

            <!-- /Header -->

            <!-- Sidebar -->
            @include('admin.sidebar')

            <!-- /Sidebar -->

            <!-- Page Wrapper -->
            <div class="page-wrapper" style="min-height: 722px;">
                <div class="content container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="page-title">List of Patient</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:(0);">Users</a></li>
                                    <li class="breadcrumb-item active">Patient</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">

                                        <table class=" table table-hover">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Patient ID: activate to sort column descending" style="width: 79.6px;">Patient ID</th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Patient Name: activate to sort column ascending" style="width: 180px;">Patient Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 32.4px;">Age</th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 60.4px;">Birthday</th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Address: activate to sort column ascending" style="width: 40.8px;">Gender</th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Phone: activate to sort column ascending" style="width: 73.2px;">Phone</th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last Visit: activate to sort column ascending" style="width: 100px;">Blood Group</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($allpateintlist as $p)
                                                <tr role="row" class="odd">
                                                    <td >{{$p->patientID }}</td>
                                                    <td>
                                                        <h2 >
                                                            <a href="{{route('admin.view.pateint',$p->patientID)}}" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{asset('images/pateint/'.$p->p_photo)}}" alt="User Image"></a>
                                                            <a href="profile.html">{{$p->p_name }} </a>
                                                        </h2>
                                                    </td>
                                                    <td> {{Carbon\Carbon::parse($p->birthday)->diff(Carbon\Carbon::now())->format('%y')}}</td>
                                                    <td> {{ Carbon\Carbon::parse($p->birthday)->format('d M Y ')}}</td>
                                                    <td>@if($p->gender==1) male @else female @endif</td>
                                                    <td>{{$p->p_phoneNo}}</td>
                                                    <td>{{$p->bloodgroup}}</td>
                                                </tr>
                                                @endforeach
                                                @foreach($allUserlist as $user)
                                                <tr>
                                                    <td>{{$user->id}}</td>
                                                    <td>
                                                        <h2>
                                                            <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{asset('images/user/'.$user->pic)}}" alt="User Image"></a>
                                                            <a href="profile.html">{{$user->name }} </a>
                                                        </h2>
                                                    </td>
                                                    <td> {{Carbon\Carbon::parse($user->birthday)->diff(Carbon\Carbon::now())->format('%y')}}</td>
                                                    <td> {{ Carbon\Carbon::parse($user->birthday)->format('d M Y ')}}</td>
                                                    <td>@if($user->gender==1) male @else female @endif</td>
                                                    <td>{{$user->phone}}</td>
                                                    <td>{{$user->bloodgroup}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <div class="d-flex align-items-center justify-content-center h-100">{{$allpateintlist->links()}}</div>

                                    </div>
                                </div>
                            </div>
                        </div>			
                    </div>

                </div>	
            </div>
        </div>
        <script src="{{asset('assetsAdmin/js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/popper.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
        <script src="{{asset('assetsAdmin/plugins/datatables/datatables.min.js')}}"></script>  
        <script src="{{asset('assetsAdmin/js/script.js')}}"></script>
        <div class="sidebar-overlay"></div>
    </body>
</html>