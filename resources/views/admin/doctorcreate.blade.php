<html lang="en"><!-- Mirrored from dreamguys.co.in/demo/doccure/admin/doctor-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Dec 2019 21:07:47 GMT --><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
          <title>DoctorPlus</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/feathericon.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/plugins/datatables/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/style.css')}}">
        <!--[if lt IE 9]>
                <script src="assets/js/html5shiv.min.js"></script>
                <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            @include('admin.header')

            <!-- /Header -->

            <!-- Sidebar -->
            @include('admin.sidebar')

            <!-- /Sidebar -->

            <!-- Page Wrapper -->
            <div class="page-wrapper" style="min-height: 722px;">
                <div class="content container-fluid">

                    <!-- Page Header -->
                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="page-title">Create Doctors Account</h3>
                               
                            </div>
                        </div>
                    </div>
                    <!-- /Page Header -->

                    <div class="row">
                        <div class="col-sm-12">

                            <div class="card">
                                <div class="card-body">
                                    @if(session()->has('message'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                    </div>
                                    @endif
                                    <form action="{{route('store.doctors')}}" method="post" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>First Name <span class="text-danger">*</span></label>
                                                    <input required=""class="form-control" type="text" name="name">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Last Name <span class="text-danger">*</span></label>
                                                    <input required=""class="form-control" type="text" name="l_name">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Father Name <span class="text-danger">*</span></label>
                                                    <input required=""class="form-control" type="text" name="f_name">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Email <span class="text-danger">*</span></label>
                                                    <input required="" class="form-control" type="email" name="email">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Password <span class="text-danger">*</span></label>
                                                    <input required="" class="form-control" id="password" type="password" name="password">
                                                    <input type="checkbox" onclick="myFunction()">Show Password
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Confirm Password <span class="text-danger">*</span></label>
                                                    <input required="" class="form-control" id="confirm_password" type="password">
                                                    <input type="checkbox" onclick="myFunction1()">Show Password
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Date of Birth <span class="text-danger">*</span></label>
                                                    <div class="cal-icon">
                                                        <input required="" type="text" class="form-control datetimepicker" name="birth">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group gender-select">
                                                    <label class="gen-label">Gender: <span class="text-danger">*</span></label>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input required="" type="radio" name="gender" value="male" class="form-check-input">Male
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input required=""type="radio" name="gender" value="female" class="form-check-input">Female
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Address Clinic <span class="text-danger">*</span></label>
                                                            <input required=""type="text" placeholder="clinic address"class="form-control" name="address">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Fee (afghani) <span class="text-danger">*</span></label>
                                                            <input maxlength="5" placeholder="fee"required=""type="number" class="form-control" name="doctor_fee">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Phone <span class="text-danger">*</span></label>
                                                    <input required=""maxlength="10" minlength="10"class="form-control"   pattern="[0-9]{10}" type="tel" name="phone">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Avatar</label>
                                                    <div class="profile-upload">
                                                        <div class="upload-input">
                                                            <input type="file" class="form-control" name="photo">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Short Biography <span class="text-danger">*</span></label>
                                            <textarea class="form-control" rows="3" cols="30" name="biog"></textarea>
                                        </div>
                                        <div class="m-t-20 text-center">
                                            <button type="submit" class="btn btn-primary submit-btn">Create Doctor</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>			
                    </div>

                </div>			
            </div>
            <!-- /Page Wrapper -->

        </div>
        <!-- /Main Wrapper -->
        <script src="{{asset('assetsAdmin/js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/popper.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
        <script src="{{asset('assetsAdmin/plugins/datatables/datatables.min.js')}}"></script>  
        <script src="{{asset('assetsAdmin/js/script.js')}}"></script>
        <div class="sidebar-overlay"></div>

        <script type="text/javascript">
                                                        var password = document.getElementById("password")
                                                                , confirm_password = document.getElementById("confirm_password");

                                                        function validatePassword() {
                                                            if (password.value != confirm_password.value) {
                                                                confirm_password.setCustomValidity("Passwords Don't Match");
                                                            } else {
                                                                confirm_password.setCustomValidity('');
                                                            }
                                                        }

                                                        password.onchange = validatePassword;
                                                        confirm_password.onkeyup = validatePassword;

                                                        function myFunction() {
                                                            var x = document.getElementById("password");
                                                            if (x.type === "password") {
                                                                x.type = "text";
                                                            } else {
                                                                x.type = "password";
                                                            }
                                                        }
                                                        function myFunction1() {
                                                            var x = document.getElementById("confirm_password");
                                                            if (x.type === "password") {
                                                                x.type = "text";
                                                            } else {
                                                                x.type = "password";
                                                            }
                                                        }
        </script>

    </body>
</html>