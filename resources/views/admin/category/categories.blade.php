<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>DoctorPlus</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assetsAdmin/img/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/feathericon.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/plugins/datatables/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/style.css')}}">
       
        <!--[if lt IE 9]>
                <script src="assets/js/html5shiv.min.js"></script>
                <script src="assets/js/respond.min.js"></script>
        <![endif]-->
   
    </head>
    <body>
        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            @include('admin.header')

            <!-- /Header -->

            <!-- Sidebar -->
            @include('admin.sidebar')

            <!-- /Sidebar -->

            <!-- Page Wrapper -->
            <div class="page-wrapper" style="min-height: 722px;">
                <div class="content container-fluid">
                    <!-- Page Header -->
                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="page-title">Appointments</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Appointments</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Page Header -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Recent Orders -->
                            <div class="card">
                                <div class="card-body">
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-sm-5 col-5">
                                                <h4 class="page-title text-capitalize">Categories</h4>
                                            </div>
                                            <div class="col-sm-7 col-7 text-right m-b-30">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#model_category">
                                                    <i class="fa fa-plus"></i> Add Category
                                                </button>
                                            </div>
                                            @if(session()->has('message'))
                                            <div class="alert alert-success  alert-block">
                                                <button type="button" class="close" data-dismiss="alert">×</button>    
                                                {{ session()->get('message') }}
                                            </div>
                                            @endif

                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped custom-table">
                                                        <thead>
                                                            <tr>
                                                                <th>Category ID</th>
                                                                <th>Category Name</th>

                                                                <th class="text-right">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($categories as $category)
                                                            <tr>
                                                                <td>{{$category->cat_id}}</td>

                                                                <td>{{$category->name}}</td>
                                                                <td class="text-right">
                                                                    <div class="dropdown dropdown-action">
                                                                        <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                            <a class="dropdown-item" onclick="editCat('{{$category->cat_id}}','{{$category->name}}')"><i class="fa fa-pencil m-r-5"></i> Edit</a>
                                                                            <a class="dropdown-item" href="{{route('admin.delete.category',$category->cat_id)}}" ><i class="fa fa-trash-o m-r-5"></i> Delete</a>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            @endforeach

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Recent Orders -->
                        </div>
                    </div>
                </div>			
            </div>
            <!-- /Page Wrapper -->

        </div>
        <script src="{{asset('assetsAdmin/js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/popper.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
        <script src="{{asset('assetsAdmin/plugins/datatables/datatables.min.js')}}"></script>  
        <script src="{{asset('assetsAdmin/js/script.js')}}"></script>

        <div class="sidebar-overlay"></div>

        <div class="modal fade" id="model_category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <form method="post" action="{{route('add.category') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-md-9">
                                        <input type="text" id="servicename" class="form-control" name="cat_name"  />
                                    </div>
                                    <div class="col-md-3">
                                        <input type="submit" name="upload" value="Add" class="btn btn-success" />
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="edit_category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">edit Category</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <form method="post" action="{{route('edit.category') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-md-9">
                                        <input type="hidden" id="cat_id" class="form-control" name="cat_id"  />
                                        <input type="text" id="cat_name" class="form-control" name="cat_name"  />
                                    </div>
                                    <div class="col-md-3">
                                        <input type="submit" name="upload" value="Update" class="btn btn-success" />
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <script>
                                                                                function editCat(id, name){
                                                                                $('#cat_id').val(id);
                                                                                $('#cat_name').val(name);
                                                                                $('#edit_category').modal('show');
                                                                                }
        </script>
    </body>
</html>