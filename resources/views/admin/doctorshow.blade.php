<html lang="en"><!-- Mirrored from dreamguys.co.in/demo/doccure/admin/doctor-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Dec 2019 21:07:47 GMT --><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>DoctorPlus</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/feathericon.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/plugins/datatables/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/style.css')}}">
        <!--[if lt IE 9]>
                <script src="assets/js/html5shiv.min.js"></script>
                <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            @include('admin.header')

            <!-- /Header -->

            <!-- Sidebar -->
            @include('admin.sidebar')

            <!-- /Sidebar -->

            <!-- Page Wrapper -->
            <div class="page-wrapper" style="min-height: 722px;">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-header">
                                <div class="row align-items-center">
                                    <div class="col-auto profile-image">
                                        <a href="#">
                                            <img class="rounded-circle"  src="{{asset('images/doctor/'.$doctor->doctor_picture)}}">
                                        </a>
                                    </div>
                                    <div class="col-auto profile-user-info">
                                        <h4 class="user-name mb-0">Dr. {{$doctor->doctor_name}}</h4>
                                        <h6 class="text-muted">{{$doctor->Expert}}</h6>
                                        <h6 class="text-muted">{{$doctor->doctor_email}}</h6>
                                        <div class="user-Location"><i class="fa fa-map-marker"></i> {{$doctor->doctor_clinicAddress}}</div>
                                    </div>
                                </div>
                                <br>
                                <div class="">{{$doctor->doctor_Short_Biography}}</div>
                            </div>
                            <div class="tab-content profile-tab-cont">
                                <div class="tab-pane fade active show" id="per_details_tab">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h5 class="card-title d-flex justify-content-between">
                                                        <span>Personal Details</span> 
                                                        <a class="edit-link"  href="{{route('admin.doctor.edit',$doctor->doctorID)}}"><i class="fa fa-edit mr-1"></i>Edit</a>
                                                    </h5>
                                                    <div class="row">
                                                        <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">F Name</p>
                                                        <p class="col-sm-10">{{$doctor->doctor_father_name}}</p>
                                                    </div>
                                                    <div class="row">
                                                        <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">L Name</p>
                                                        <p class="col-sm-10">{{$doctor->doctor_last_name}}</p>
                                                    </div>
                                                    <div class="row">
                                                        <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">Fee</p>
                                                        <p class="col-sm-10">{{$doctor->doctor_fee}} AFN</p>
                                                    </div>
                                                    <div class="row">
                                                        <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">Gender</p>
                                                        <p class="col-sm-10">{{$doctor->doctor_gender}}</p>
                                                    </div>
                                                    <div class="row">
                                                        <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">Date of Birth</p>
                                                        <p class="col-sm-10">{{$doctor->doctor_birthday}}</p>
                                                    </div>
                                                    <div class="row">
                                                        <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">Email ID</p>
                                                        <p class="col-sm-10">{{$doctor->doctor_email}}</p>
                                                    </div>
                                                    <div class="row">
                                                        <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">Mobile</p>
                                                        <p class="col-sm-10">{{$doctor->doctor_phoneNo}}</p>
                                                    </div>
                                                    <div class="row">
                                                        <p class="col-sm-2 text-muted text-sm-right mb-0">Address</p>
                                                        <p class="col-sm-10 mb-0">{{$doctor->doctor_clinicAddress}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
<!--                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="card-box">
                                                        <h3 class="card-title">Speciality Informations <span  onclick="addSpeciality('{{$doctor->doctorID}}')"class="fa fa-plus text-success btn btn-default border border-2"></span></h3>
                                                        <div class="experience-box">
                                                            <div class="row">
                                                                @foreach($doctorspeciality as $spa)
                                                                <div class="col-md-4 p-1">
                                                                    <button type="button" id="serv_name" class="btn btn-default" style="padding-top: 0;padding-bottom: 0;">

                                                                        <a href="{{url('delete/Speciality',[$spa->id_s_d])}}" style=""> 
                                                                            <span class="remove"style="position: relative;left: -12px;display: inline-block;padding: 3px 6px;border-radius: 20px 20px 20px 20px;">
                                                                                <i class="fa fa-remove"></i> </span></a>{{$spa->specialityName}}
                                                                    </button>

                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="card-box">
                                                        <h3 class="card-title">Services Informations <span  onclick="addservice('{{$doctor->doctorID}}')"class="fa fa-plus text-success btn btn-default border border-2"></span></h3>
                                                        <div class="experience-box">
                                                            <div class="row">
                                                                @foreach($doctor_services as $service)
                                                                <div class="col-md-4 p-1">
                                                                    <button type="button" id="serv_name" class="btn  btn-default " style="padding-top: 0;padding-bottom: 0;">
                                                                        <span class="edit" id="service_id" onclick="editcall('{{$service->services_id}}','{{$service->service_name}}')"  style="position: relative;left: -12px;display: inline-block;padding: 3px 6px;border-radius: 20px 20px 20px 20px; " >
                                                                            <i class="fa fa-edit"></i>
                                                                        </span>
                                                                        <a href="{{route('admin.delete.service',$service->services_id)}}" style=""> 
                                                                            <span class="remove"style="position: relative;left: -12px;display: inline-block;padding: 3px 6px;border-radius: 20px 20px 20px 20px;">
                                                                                <i class="fa fa-remove"></i> </span></a>{{$service->service_name}}
                                                                    </button>

                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="card-box">
                                                        <h3 class="card-title">Education Informations <span  onclick="addeductaion('{{$doctor->doctorID}}')"class="fa fa-plus text-success btn btn-default border border-2"></span></h3>
                                                        <div class="experience-box">
                                                            <ul class="experience-list">
                                                                @foreach($doctor_education as $edu)
                                                                <li>
                                                                    <div class="experience-user">
                                                                        <div class="before-circle"></div>
                                                                    </div>
                                                                    <div class="experience-content">
                                                                        <span class="edit"  onclick="editEducation('{{$edu->D_E_ID}}','{{$edu->school_name}}','{{$edu->title_of_study}}','{{$edu->start_Date}}','{{$edu->end_Date}}')"  style="position: relative;left: -12px;display: inline-block;padding: 3px 6px;border-radius: 20px 20px 20px 20px; " >
                                                                            <i class="fa fa-edit"></i>
                                                                        </span>
                                                                        <a href="{{route('admin.delete.education',$edu->D_E_ID)}}" style=""> 
                                                                            <span class="remove" style="position: relative;left: -12px;display: inline-block;padding: 3px 6px;border-radius: 20px 20px 20px 20px;">
                                                                                <i class="fa fa-remove"></i> 
                                                                            </span>
                                                                        </a>
                                                                        <div class="timeline-content">
                                                                            <a  class="name">{{$edu->school_name}}</a>
                                                                            <div>{{$edu->title_of_study}}</div>
                                                                            <span class="time">{{$edu->start_Date }}- {{$edu->end_Date}}</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="card-box mb-0">
                                                        <h3 class="card-title">Experience <span  onclick="addexperiance('{{$doctor->doctorID}}')"class="fa fa-plus text-success btn btn-default border border-2"></span></h3>
                                                        <div class="experience-box">
                                                            <ul class="experience-list">
                                                                @foreach($doctor_experience as $exp)
                                                                <li>
                                                                    <div class="experience-user">
                                                                        <div class="before-circle"></div>
                                                                    </div>
                                                                    <div class="experience-content">
                                                                        <span class="edit"  onclick="editexperiance('{{$exp->experience_id}}','{{$exp->experience_name}}','{{$exp->start}}','{{$exp->end}}')"  style="position: relative;left: -12px;display: inline-block;padding: 3px 6px;border-radius: 20px 20px 20px 20px; " >
                                                                            <i class="fa fa-edit"></i>
                                                                        </span>
                                                                        <a href="{{route('admin.delete.experiance',$exp->experience_id)}}" style=""> 
                                                                            <span class="remove" style="position: relative;left: -12px;display: inline-block;padding: 3px 6px;border-radius: 20px 20px 20px 20px;">
                                                                                <i class="fa fa-remove"></i> 
                                                                            </span>
                                                                        </a>
                                                                        <div class="timeline-content">
                                                                            <a href="#/" class="name">{{$exp->experience_name}}</a>
                                                                            <span class="time">{{$exp->start}} , {{$exp->end}}</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            <a class="btn btn-sm btn-danger"style="float: right; margin:30px;" href="{{url('admin/doctor/delete',[$doctor->doctorID])}}" >Delete Doctor Account</a>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>			
            </div>
            <!-- /Page Wrapper -->

        </div>
        <!-- /Main Wrapper -->
        <script src="{{asset('assetsAdmin/js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/popper.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
        <script src="{{asset('assetsAdmin/plugins/datatables/datatables.min.js')}}"></script>  
        <script src="{{asset('assetsAdmin/js/script.js')}}"></script>
        <div class="sidebar-overlay"></div>


        <script type="text/javascript">

                                                                            function addSpeciality(doctorid){
                                                                            $('#doctoraddSpeciality').val(doctorid);
                                                                            $('#model_add_Speciality').modal('show');
                                                                            }
                                                                            function addexperiance(doctorid){
                                                                            $('#doctor_experiance_id').val(doctorid);
                                                                            $('#model_add_expariance').modal('show');
                                                                            }
                                                                            function addservice(doctorid){
                                                                            $('#doc_id_s').val(doctorid);
                                                                            $('#model_add_services').modal('show');
                                                                            }
                                                                            function editcall(id, name){
                                                                            $('#servID').val(id);
                                                                            $('#servicename').val(name);
                                                                            $('#model_services').modal('show');
                                                                            }
                                                                            function addeductaion(doctorid){
                                                                            $('#doc_id_edu').val(doctorid);
                                                                            $('#model_add_edu').modal('show');
                                                                            }

                                                                            function editEducation(ed_id, school_name, study_title, start, end){
                                                                            $('input[name="eduction_id"]').val(ed_id);
                                                                            $('input[name="School_name"]').val(school_name);
                                                                            $('input[name="Study_title"]').val(study_title);
                                                                            $('input[name="start_date"]').val(start);
                                                                            $('input[name="end_date"]').val(end);
                                                                            $('#model_update_edu').modal('show');
                                                                            }
                                                                            function editexperiance(ed_id, school_name, start, end){
                                                                            $('input[name="eduction_idxx"]').val(ed_id);
                                                                            $('input[name="School_namexx"]').val(school_name);
                                                                            $('input[name="start_datexx"]').val(start);
                                                                            $('input[name="end_datexx"]').val(end);
                                                                            $('#model_update_experiance').modal('show');
                                                                            }


        </script>
        <div class="modal fade" id="model_add_Speciality" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">add Speciality</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <form method="post" action="{{route('add.Speciality')}}">
                                @csrf
                                <input type="hidden" id="doctoraddSpeciality" class="form-control" name="doct_addSpecialityid" />
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Country</label>

                                            <select class="form-control" name="spacia_idd">
                                                @foreach($speciality as $spa)
                                                <option value="{{$spa->specialityID}}">{{$spa->specialityName}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">

                                    </div>
                                    <div class="col-md-6">
                                        <input type="submit" name="upload" value="Add Speciality" class="btn btn-success btn-sm" />
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="model_update_experiance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Update Experiance</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <form method="post" action="{{route('update.experiance') }}">
                                @csrf
                                <div class="row form-group">
                                    <input type="hidden"  class="form-control" name="eduction_idxx" />
                                    <div class="col-md-4">
                                        <label> Name</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" placeholder="School name"class="form-control" name="School_namexx"  />
                                    </div>

                                </div>

                                <div class="row ">
                                    <div class="col-md-4">
                                        <label class="label">start date</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="service_name" placeholder="start date"class="form-control" name="start_datexx"  />
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-md-4">
                                        <label>End date </label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="service_name" placeholder="End date "class="form-control" name="end_datexx"  />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="submit" name="upload" value="Save" class="btn btn-success" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="model_add_expariance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Experience</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <form method="post" action="{{route('add.experiance') }}">
                                @csrf
                                <div class="row form-group">
                                    <input type="hidden" id="doctor_experiance_id" class="form-control" name="doc_experiance_id" />
                                    <div class="col-md-4">
                                        <label> Name</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" placeholder="School name"class="form-control" name="experiance_name"/>
                                    </div>

                                </div>


                                <div class="row ">
                                    <div class="col-md-4">
                                        <label class="label">start date</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="service_name" placeholder="start date"class="form-control" name="start_date_experiance"  />
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-md-4">
                                        <label>End date </label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="service_name" placeholder="End date "class="form-control" name="end_date_experiance"  />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="submit" name="upload" value="Save" class="btn btn-success" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="model_update_edu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">update Education</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <form method="post" action="{{route('admin.update.education') }}">
                                @csrf
                                <div class="row form-group">
                                    <input type="hidden" id="doc_id" class="form-control" name="eduction_id" />
                                    <div class="col-md-4">
                                        <label>School Name</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" placeholder="School name"class="form-control" name="School_name"  />
                                    </div>

                                </div>

                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <label>Study Title</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" placeholder="Study title"class="form-control" name="Study_title"  />
                                    </div>

                                </div>
                                <div class="row ">
                                    <div class="col-md-4">
                                        <label class="label">start date</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="service_name" placeholder="start date"class="form-control" name="start_date"  />
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-md-4">
                                        <label>End date </label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="service_name" placeholder="End date "class="form-control" name="end_date"  />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="submit" name="upload" value="Save" class="btn btn-success" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="model_services" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <form method="post" action="{{route('update.service') }}">
                                @csrf
                                <div class="row">

                                    <input type="hidden" id="servID" class="form-control" name="service_id" />
                                    <div class="col-md-9">
                                        <input type="text" id="servicename" class="form-control" name="service_name"  />
                                    </div>
                                    <div class="col-md-3">
                                        <input type="submit" name="upload" value="update" class="btn btn-success" />
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="model_add_edu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Education</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <form method="post" action="{{route('admin.add.education') }}">
                                @csrf
                                <div class="row form-group">
                                    <input type="hidden" id="doc_id_edu" class="form-control" name="doctor_id_edu" />
                                    <div class="col-md-4">
                                        <label>School Name</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" placeholder="School name"class="form-control" name="School_name"  />
                                    </div>

                                </div>

                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <label>Study Title</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" placeholder="Study title"class="form-control" name="Study_title"  />
                                    </div>

                                </div>
                                <div class="row ">
                                    <div class="col-md-4">
                                        <label class="label">start date</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="service_name" placeholder="start date"class="form-control" name="start_date"  />
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-md-4">
                                        <label>End date </label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="service_name" placeholder="End date "class="form-control" name="end_date"  />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="submit" name="upload" value="Save" class="btn btn-success" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="model_add_services" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" >
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Save Service</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <form method="post" action="{{route('add.service') }}">
                                @csrf
                                <div class="row">

                                    <input type="hidden" id="doc_id_s" class="form-control" name="doctor_id_service" />
                                    <div class="col-md-9">
                                        <input type="text" id="service_name" class="form-control" name="service_name"  />
                                    </div>
                                    <div class="col-md-3">
                                        <input type="submit" name="upload" value="Save" class="btn btn-success" />
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" value="Reset"data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>