<html lang="en">
    <!-- Mirrored from dreamguys.co.in/demo/doccure/admin/doctor-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Dec 2019 21:07:47 GMT --><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>DoctorPlus</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/feathericon.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/plugins/datatables/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/style.css')}}">
        <!--[if lt IE 9]>
                <script src="assets/js/html5shiv.min.js"></script>
                <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            @include('admin.header')

            <!-- /Header -->
            <!-- Sidebar -->
            @include('admin.sidebar')

            <!-- /Sidebar -->

            <!-- Page Wrapper -->
            <div class="page-wrapper" style="min-height: 722px;">
                <div class="content container-fluid">
                    <!-- Page Header -->
                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="page-title">List of Notification</h3>

                            </div>
                        </div>
                    </div>
                    <!-- /Page Header -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-block">
                                        <div class="table-responsive">
                                            <table class="table mb-0 new-patient-table">
                                                <thead>
                                                <th>Name</th>
                                                <th>phone</th>
                                                <th>message</th>
                                                <th>action</th>
                                                </thead>
                                                <tbody>
                                                    @foreach($notifications as $notify)
                                                    @if($notify->status==0)
                                                    <tr style="background-color: #9dc2d0;">

                                                        <td>{{$notify->name}}</td>
                                                        <td>{{$notify->phone}}</td>
                                                        <td> 
                                                            <p>{{$notify->message}}</p>

                                                        </td>
                                                        <td>
                                                            <a href="{{route('admin.delete.notification', $notify->id) }}" style="text-decoration: none;"><button class="btn btn-danger btn-primary-one "><i class="fa fa-remove"></i> Delete</button></a>
                                                            <a href="{{route('admin.read.notification', $notify->id) }}" style="text-decoration: none;"><button class="btn btn-primary btn-primary-two"><i class="fa fa-eye"></i> read</button></a>
                                                        </td>
                                                    </tr>
                                                    @else
                                                    <tr>
                                                        <td>{{$notify->name}}</td>
                                                        <td>{{$notify->phone}}</td>
                                                        <td> 
                                                            <p>{{$notify->message}}</p>
                                                        </td>
                                                        <td>
                                                            <a href="{{route('admin.delete.notification', $notify->id) }}" style="text-decoration: none;"><button class="btn btn-danger btn-primary-one "><i class="fa fa-remove"></i> Delete</button></a>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <meta name="csrf-token" content="{{csrf_token()}}">
                                            <br>
                                            <div class="d-flex align-items-center justify-content-center h-100">{{$notifications->links()}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>			
                    </div>
                </div>	
            </div>
            <!-- /Page Wrapper -->
        </div>
        <!-- /Main Wrapper -->
        <script src="{{asset('assetsAdmin/js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/popper.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
        <script src="{{asset('assetsAdmin/plugins/datatables/datatables.min.js')}}"></script>  
        <script src="{{asset('assetsAdmin/js/script.js')}}"></script>
        <div class="sidebar-overlay"></div>
        <script type="text/javascript">
function showmessage() {
    var m = $('#mess').data('message');
    alert(m);
    var m = '';
}
        </script>
    </body>
</html>