<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
       <title>DoctorPlus</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/feathericon.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/plugins/datatables/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/style.css')}}">
        <!--[if lt IE 9]>
                <script src="assets/js/html5shiv.min.js"></script>
                <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            @include('admin.header')

            <!-- /Header -->

            <!-- Sidebar -->
            @include('admin.sidebar')

            <!-- /Sidebar -->

            <!-- Page Wrapper -->
            <div class="page-wrapper" style="min-height: 722px;">
                <div class="content container-fluid">

                    <!-- Page Header -->
                    <div class="page-header">
                        <div class="row">
                            <div class="col">
                                <h3 class="page-title">Edit Profile</h3>

                            </div>
                        </div>
                    </div>
                    <!-- /Page Header -->

                    <div class="row">
                        <div class="col-md-12">
                            <br />
                            @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            @if($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>    
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            <form method="post" action="{{route('admin.updateprofile')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input class="form-control" name="adminID" type="hidden" value="{{ $adminEdit->id}}">

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>First Name <span class="text-danger">*</span></label>
                                            <input class="form-control" name="name" type="text" value="{{ $adminEdit->name}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Last Name <span class="text-danger">*</span></label>
                                            <input class="form-control" name="email" type="email" value="{{ $adminEdit->email}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Age <span class="text-danger">*</span></label>

                                            <input class="form-control" name="age"type="text" value="{{ $adminEdit->age}}">

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group gender-select">
                                            <label class="gen-label">Gender: <span class="text-danger">*</span></label>
                                            @if($adminEdit->gender==1)
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input  type="radio" id="male" name="gender" checked value="1" class="form-check-input">Male
                                                </label>
                                            </div>
                                            @else
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input  type="radio" id="male" name="gender"  value="1" class="form-check-input">Male
                                                </label>
                                            </div>

                                            @endif
                                            @if($adminEdit->gender==0)
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input  type="radio" id="female" name="gender" checked value="0"class="form-check-input">Female
                                                </label>
                                            </div>
                                            @else
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input  type="radio" id="female" name="gender"  value="0"class="form-check-input">Female
                                                </label>
                                            </div>
                                            @endif

                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Phone </label>
                                            <input class="form-control" name="phone"type="text" value="{{ $adminEdit->phone}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Avatar</label>
                                            <div class="profile-upload">
                                                <div class="upload-img">
                                                    <img alt="" src="{{asset('images/admin/'.$adminEdit->pic)}}" width="50" height="40">
                                                </div>
                                                <div class="upload-input">
                                                    <input type="file" name="photo"class="form-control" value="{{ old('photo') }}" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="m-t-20 text-center">
                                    <button type="submit"class="btn btn-primary submit-btn">update Data</button>
                                </div>
                            </form>


                        </div>
                    </div>

                </div>	
            </div>
            <!-- /Page Wrapper -->

        </div>
        <script src="{{asset('assetsAdmin/js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/popper.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
        <script src="{{asset('assetsAdmin/plugins/datatables/datatables.min.js')}}"></script>  
        <script src="{{asset('assetsAdmin/js/script.js')}}"></script>

        <div class="sidebar-overlay"></div>
    </body>
</html>