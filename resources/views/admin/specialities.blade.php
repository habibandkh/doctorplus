<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
           <title>DoctorPlus</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/feathericon.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/plugins/datatables/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/style.css')}}">
        <!--[if lt IE 9]>
                <script src="assets/js/html5shiv.min.js"></script>
                <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">


            @include('admin.header')

            @include('admin.sidebar')

            <div class="page-wrapper" style="min-height: 722px;">
                <div class="content container-fluid">

                    <!-- Page Header -->
                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-7 col-auto">
                                <h3 class="page-title">Specialities</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Specialities</li>
                                </ul>
                            </div>
                            <div class="col-sm-5 col">
                                <a href="#Add_Specialities_details" data-toggle="modal" class="btn btn-primary float-right mt-2">Add</a>
                            </div>
                        </div>
                    </div>
                    <!-- /Page Header -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    @if(count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif

                                    @if($message = Session::get('message'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>    
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @endif
                                    <div class="table-responsive">
                                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class=" table table-hover table-center mb-0  no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 189.2px;">#</th>
                                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Specialities: activate to sort column ascending" style="width: 138.8px;">Specialities</th>
                                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Specialities: activate to sort column ascending" style="width: 138.8px;">in Farsi</th>
                                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Specialities: activate to sort column ascending" style="width: 138.8px;">in Pashto</th>
                                                                <th class="text-right sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 100.6px;">Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($specialities as $specialitie)
                                                            <tr role="row" class="odd">
                                                                <td class="sorting_1">{{$specialitie->specialityID}}</td>

                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a  class="avatar avatar-sm mr-2">
                                                                            <img class="avatar-img" src="{{asset('images/specialities/'.$specialitie->photo)}}" >
                                                                        </a>
                                                                        <p>{{$specialitie->specialityName}}</p>

                                                                    </h2>
                                                                </td>
                                                                <td>
                                                                    <h2 class="table-avatar">

                                                                        <p>{{$specialitie->farsi_name}}</p>
                                                                    </h2>
                                                                </td>
                                                                <td>
                                                                    <h2 class="table-avatar">

                                                                        <p >{{$specialitie->pashto_name}}</p>
                                                                    </h2>
                                                                </td>
                                                                <td class="text-right">
                                                                    <div class="actions">
<!--                                                                        <a class="btn btn-sm bg-success-light" data-toggle="modal" href="#edit_specialities_details">
                                                                            <i class="fe fe-pencil"></i> Edit
                                                                        </a>-->
<!--                                                                        <a href="{{url('admin/delete/specialities/'.$specialitie->specialityID)}}" class="btn btn-sm bg-danger-light">
                                                                            <i class="fe fe-trash"></i> Delete
                                                                        </a>-->
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            @endforeach()
                                                        </tbody>
                                                    </table>
                                                       <div class="d-flex align-items-center justify-content-center ">{{$specialities->links()}}</div>
                                           
                                                </div>
                                                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>			
                    </div>
                </div>			
            </div>
            <!-- /Page Wrapper -->


            <!-- Add Modal -->
            <div class="modal fade" id="Add_Specialities_details" aria-hidden="true" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Add Specialities</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            
                            <form method="post" action="{{route('admin.specialities.add')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row form-row">
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label>Specialities in English</label>
                                            <input required="" type="text" name="spe_en"class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Specialities in Persian</label>
                                            <input required="" type="text" name="spe_fa"class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Specialities in Pashto</label>
                                            <input required="" type="text" name="spe_pa"class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label>Image</label>
                                            <input required="" type="file" class="form-control" name="photo">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- /ADD Modal -->

            <!-- Edit Details Modal -->
            <div class="modal fade" id="edit_specialities_details" aria-hidden="true" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Edit Specialities</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="row form-row">
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label>Specialities</label>
                                            <input type="text" class="form-control" value="Cardiology">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label>Image</label>
                                            <input type="file" class="form-control">
                                        </div>
                                    </div>

                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Edit Details Modal -->

            <!-- Delete Modal -->
            <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <!--	<div class="modal-header">
                                        <h5 class="modal-title">Delete</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                        </button>
                                </div>-->
                        <div class="modal-body">
                            <div class="form-content p-2">
                                <h4 class="modal-title">Delete</h4>
                                <p class="mb-4">Are you sure want to delete?</p>
                                <button type="button" class="btn btn-primary">Save </button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Delete Modal -->
        </div>
        <!-- /Main Wrapper -->

        <!-- jQuery -->
        <script src="{{asset('assetsAdmin/js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/popper.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
        <script src="{{asset('assetsAdmin/plugins/datatables/datatables.min.js')}}"></script>  
        <script src="{{asset('assetsAdmin/js/script.js')}}"></script>

        <div class="sidebar-overlay"></div>



        <!-- Mirrored from dreamguys.co.in/demo/doccure/admin/specialities.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Dec 2019 21:07:46 GMT -->
    </body></html>
