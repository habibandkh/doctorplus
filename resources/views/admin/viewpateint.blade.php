<html lang="en"><!-- Mirrored from dreamguys.co.in/demo/doccure/admin/doctor-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Dec 2019 21:07:47 GMT --><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>DoctorPlus</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/feathericon.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/plugins/datatables/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/style.css')}}">

        <!--[if lt IE 9]>
                <script src="assets/js/html5shiv.min.js"></script>
                <script src="assets/js/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            @include('admin.header')

            <!-- /Header -->

            <!-- Sidebar -->
            @include('admin.sidebar')

            <!-- /Sidebar -->

            <!-- Page Wrapper -->
            <div class="page-wrapper" style="min-height: 722px;">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-7 col-6">
                            <h4 class="page-title">My Profile</h4>
                        </div>

                    </div>
                    <div class="card-box profile-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-view">
                                    <div class="profile-img-wrap">
                                        <div class="profile-img">
                                            <a href="#"><img class="avatar" src="{{asset('images/pateint/'.$pateintview->p_photo)}}" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="profile-basic">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="profile-info-left">
                                                    <h3 class="user-name m-t-0 mb-0">{{$pateintview->p_name}}</h3>
                                                    <div class="staff-id">Pateint ID : {{$pateintview->patientID}} </div>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <ul class="personal-info">
                                                    <li>
                                                        <span class="title">Phone:</span>
                                                        <span class="text">{{$pateintview->p_phoneNo}}</span>
                                                    </li>
                                                    <li>
                                                        <span class="title">Age:</span>
                                                        <span class="text">{{$pateintview->birthday}}</span>
                                                    </li>
                                                    <li>
                                                        <span class="title">Gender:</span>
                                                        @if($pateintview->gender==1)
                                                        <span class="text">male</span>
                                                        @endif
                                                        @if($pateintview->gender==0)
                                                        <span class="text">female</span>
                                                        @endif
                                                        @if($pateintview->gender==2)
                                                        <span class="text">other</span>
                                                        @endif
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>                        
                            </div>
                        </div>
                    </div>
                    <div class="container">    
                        <br>
                        <br><br>
                        <br>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">list of all documents related to the pateint. <span class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i> add document</span></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    @foreach($document as $doc)
                                    <div class="col-md-3">
                                        <div class="card" style="width: 18rem;">
                                            <iframe src="{{asset('document/pateint/'.$doc->document)}}" type="application"  frameborder="2"></iframe>
                                            <div class="card-body">
                                                <h5 class="card-title">{{$doc->details}}</h5>

                                                <a href="{{asset('document/pateint/'.$doc->document)}}" target="_blank"class="btn btn-primary btn-sm">View</a>
                                                <a href="#" class="btn btn-danger btn-sm float-right"><span class="fa fa-remove"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  
    <script src="{{asset('assetsAdmin/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('assetsAdmin/js/popper.min.js')}}"></script>
    <script src="{{asset('assetsAdmin/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assetsAdmin/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assetsAdmin/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
    <script src="{{asset('assetsAdmin/plugins/datatables/datatables.min.js')}}"></script>  
    <script src="{{asset('assetsAdmin/js/script.js')}}"></script>
    <div class="sidebar-overlay"></div>
</body>
</html>



