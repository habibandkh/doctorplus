<html lang="en"><!-- Mirrored from dreamguys.co.in/demo/doccure/admin/doctor-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Dec 2019 21:07:47 GMT --><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>DoctorPlus</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/feathericon.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/plugins/datatables/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/style.css')}}">

        <!--[if lt IE 9]>
                <script src="assets/js/html5shiv.min.js"></script>
                <script src="assets/js/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            @include('admin.header')

            <!-- /Header -->

            <!-- Sidebar -->
            @include('admin.sidebar')

            <!-- /Sidebar -->

            <!-- Page Wrapper -->
            <div class="page-wrapper" style="min-height: 722px;">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-3">
                            <h4 class="page-title">List of all Users of the system</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-block">
                                    <div class="table-responsive">
                                        <table class="table mb-0 new-patient-table">
                                            <thead>
                                            <th>Name</th>
                                            <th>Father Name</th>
                                            <th>Phone</th>
                                            <th>Age</th>
                                            <th>Gender</th>
                                            <th>Email</th>
                                            <th >Action</th>
                                            </thead>
                                            <tbody>
                                                @foreach($user_info as $p)
                                                <tr>
                                                    <td>
                                                        <img width="28" height="28" class="rounded-circle" src="{{asset('images/user/'.$p->pic)}}" alt=""> 
                                                        <h2>{{$p->name }}</h2>
                                                    </td>
                                                    <td>{{$p->f_name}}</td>
                                                    <td>{{$p->phone}}</td>
                                                    <td>{{$p->age}}</td>
                                                    <td >{{$p->gender}}</td>
                                                    <td >{{$p->email}}</td>

                                                    <td><a href="{{url('admin/delete/user', [$p->id]) }}" style="text-decoration: none;"><button class="btn btn-danger btn-primary-one "><i class="fa fa-remove"></i> Delete</button></a></tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <meta name="csrf-token" content="{{ csrf_token() }}">

                                        <br>
                                        <div class="d-flex align-items-center justify-content-center h-100">{{$user_info->links()}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="{{asset('assetsAdmin/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('assetsAdmin/js/popper.min.js')}}"></script>
<script src="{{asset('assetsAdmin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assetsAdmin/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('assetsAdmin/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
<script src="{{asset('assetsAdmin/plugins/datatables/datatables.min.js')}}"></script>  
<script src="{{asset('assetsAdmin/js/script.js')}}"></script>
<div class="sidebar-overlay"></div>
</body>
</html>

