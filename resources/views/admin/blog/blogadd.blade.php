<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>DoctorPlus</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/feathericon.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/plugins/datatables/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/style.css')}}">
        <!--[if lt IE 9]>
                <script src="assets/js/html5shiv.min.js"></script>
                <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Main Wrapper -->
        <div class="main-wrapper">
            <!-- Header -->
            @include('admin.header')
            <!-- /Header -->
            <!-- Sidebar -->
            @include('admin.sidebar')
            <!-- /Sidebar -->
            <!-- Page Wrapper -->
            <div class="page-wrapper" style="min-height: 722px;">
                <div class="content container-fluid">
                    <!-- Page Header -->
<!--                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="page-title">Appointments</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Appointments</li>
                                </ul>
                            </div>
                        </div>
                    </div>-->
                    <!-- /Page Header -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Recent Orders -->
                            <div class="card">
                                <div class="card-body">
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-lg-8 offset-lg-2">
                                                <h4 class="page-title">Add Blog</h4>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-lg-8 offset-lg-2">
                                                @if(count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                @endif
                                                @if(session()->has('message'))
                                                <div class="alert alert-success  alert-block">
                                                    <button type="button" class="close" data-dismiss="alert">×</button>    
                                                    {{ session()->get('message') }}
                                                </div>
                                                @endif

                                                <form action="{{route('store.blog')}}" method="post" enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    <div class="form-group">
                                                        <label>Blog Name</label>
                                                        <input required="" class="form-control" type="text" name="bname">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Blog Images</label>
                                                        <div>
                                                            <input required="" class="form-control" type="file" name="photo">
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
<!--                                                                <label>Blog Category</label>-->
                                                                <select name="bcat" class="select select2-hidden-accessible" required="" tabindex="-1" aria-hidden="true">
                                                                    <option value="">Select a Category</option>
                                                                    @foreach($categories as $category)
                                                                    <option value="{{$category->cat_id}}">{{$category->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
<!--                                                                <label>Post Language</label>-->
                                                                <select name="lang" class="select select2-hidden-accessible" required="" tabindex="-1" aria-hidden="true">
                                                                    <option value="">Select a Language</option>
                                                                    <option value="en">English</option>
                                                                    <option value="fa">Persian</option>
                                                                    <option value="pa">Pashto</option>
                                                                 
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Blog Description</label>
                                                        <textarea cols="30" rows="6" required="" name="bdec"class="form-control"></textarea>
                                                    </div>

                                                    <div class="form-group" >
                                                        <label class="display-block">Blog Status</label>
                                                        <div class="form-check form-check-inline">
                                                            <input required=""class="form-check-input" type="radio" name="status" id="blog_active" value="1" checked="">
                                                            <label class="form-check-label" for="blog_active">
                                                                Active
                                                            </label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="status" id="blog_inactive" value="0">
                                                            <label class="form-check-label" for="blog_inactive">
                                                                Inactive
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="m-t-20 text-center">
                                                        <button type="submit" class="btn btn-primary submit-btn">Publish Blog</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Recent Orders -->
                        </div>
                    </div>
                </div>			
            </div>
            <!-- /Page Wrapper -->

        </div>
        <script src="{{asset('assetsAdmin/js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/popper.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
        <script src="{{asset('assetsAdmin/plugins/datatables/datatables.min.js')}}"></script>  
        <script src="{{asset('assetsAdmin/js/script.js')}}"></script>

        <div class="sidebar-overlay"></div>
    </body>
</html>