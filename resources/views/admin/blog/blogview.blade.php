<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>DoctorPlus</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/feathericon.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/plugins/datatables/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/style.css')}}">
        <!--[if lt IE 9]>
                <script src="assets/js/html5shiv.min.js"></script>
                <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            @include('admin.header')

            <!-- /Header -->

            <!-- Sidebar -->
            @include('admin.sidebar')

            <!-- /Sidebar -->

            <!-- Page Wrapper -->
            <div class="page-wrapper" style="min-height: 722px;">
                <div class="content container-fluid">
                    <!-- Page Header -->
                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="page-title">Appointments</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Appointments</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Page Header -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Recent Orders -->
                            <div class="card">
                                <div class="card-body">
                                    <div class="content">
                                        @foreach($posts as $post)
                                        <div class="row">
                                            <div class="col-sm-8 col-4">
                                                <h4 class="page-title">post details</h4>
                                            </div>
                                            <div class="col-sm-4 col-8 text-right m-b-30">
                                                <a class="btn btn-primary btn-rounded float-right" href="{{route('admin.edit.blog',$post->post_id)}}"><i class="fa fa-plus"></i> Edit Blog</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="blog-view">
                                                    <article class="blog blog-single-post">
                                                        <h3 class="blog-title">{{$post->post_title}}</h3>
                                                        <div class="blog-info clearfix">
                                                            <div class="post-left">
                                                                <ul>
                                                                    <li><a href="#."><i class="fa fa-calendar"></i> <span>{{$post->date}}</span></a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="post-right"><a href="#."><i class="fa fa-eye-slash"></i>{{$post->post_view}} Comment</a></div>
                                                        </div>
                                                        <div class="blog-image">
                                                            <img alt="" src="{{asset('images/blog/'.$post->post_img)}}" class="img-fluid">
                                                        </div>
                                                        <div class="blog-content">
                                                            <p>{{$post->post_body}}</p>
                                                        </div>
                                                    </article>
                                                    <div class="widget blog-share clearfix">
                                                        <h3>Share the post</h3>
                                                        <ul class="social-share">
                                                            <li><a href="#." title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                                            <li><a href="#." title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                                            <li><a href="#." title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                                            <li><a href="#." title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                                                            <li><a href="#." title="Youtube"><i class="fa fa-youtube"></i></a></li>
                                                        </ul>
                                                    </div>

                                                    <div class="widget blog-comments clearfix">
                                                        <h3>Comments (3)</h3>
                                                        <ul class="comments-list">
                                                            <li>
                                                                <div class="comment">
                                                                    <div class="comment-author">
                                                                        <img class="avatar" alt="" src="assets/img/user.jpg">
                                                                    </div>
                                                                    <div class="comment-block">
                                                                        <span class="comment-by">
                                                                            <span class="blog-author-name">Pamela Curtis</span>
                                                                            <span class="float-right">
                                                                                <span class="blog-reply"><a href="#."><i class="fa fa-reply"></i> Reply</a></span>
                                                                            </span>
                                                                        </span>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                                                        <span class="blog-date">December 13, 2017</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                </div>

                                            </div>
                                            <aside class="col-md-4">
                                                <div class="widget search-widget">
                                                    <h5>Blog Search</h5>
                                                    <form class="search-form">
                                                        <div class="input-group">
                                                            <input type="text" placeholder="Search..." class="form-control">
                                                            <div class="input-group-append">
                                                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="widget post-widget">
                                                    <h5>Latest Posts</h5>
                                                    <ul class="latest-posts">
                                                        @foreach($latest as $lat)
                                                        <li>
                                                            <div class="post-thumb">
                                                                <a href="blog-details.html">
                                                                    <img class="img-fluid" src="{{asset('images/blog/'.$lat->post_img)}}" alt="">
                                                                </a>
                                                            </div>
                                                            <div class="post-info">
                                                                <h4>
                                                                    <a href="{{route('admin.view.blog',$lat->post_id)}}">{{$lat->post_title}}</a>
                                                                </h4>
                                                                <p><i aria-hidden="true" class="fa fa-calendar"></i> {{$lat->date}}</p>
                                                            </div>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>


                                            </aside>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <!-- /Recent Orders -->
                        </div>
                    </div>
                </div>			
            </div>
            <!-- /Page Wrapper -->

        </div>
        <script src="{{asset('assetsAdmin/js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/popper.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
        <script src="{{asset('assetsAdmin/plugins/datatables/datatables.min.js')}}"></script>  
        <script src="{{asset('assetsAdmin/js/script.js')}}"></script>

        <div class="sidebar-overlay"></div>
    </body>
</html>