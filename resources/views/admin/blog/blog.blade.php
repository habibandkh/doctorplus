<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>DoctorPlus</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/feathericon.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/plugins/datatables/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assetsAdmin/css/style.css')}}">
        <!--[if lt IE 9]>
                <script src="assets/js/html5shiv.min.js"></script>
                <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            @include('admin.header')

            <!-- /Header -->

            <!-- Sidebar -->
            @include('admin.sidebar')

            <!-- /Sidebar -->

            <!-- Page Wrapper -->
            <div class="page-wrapper" style="min-height: 722px;">
                <div class="content container-fluid">
                    <!-- Page Header -->
                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="page-title">Appointments</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Appointments</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Page Header -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Recent Orders -->
                            <div class="card">
                                <div class="card-body">
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-sm-8 col-4">
                                                <h4 class="page-title">Blog</h4>
                                            </div>

                                            <div class="col-sm-4 col-8 text-right m-b-30">
                                                <a class="btn btn-primary btn-rounded float-right" href="{{route('blog.view.add')}}"><i class="fa fa-plus"></i> Add Blog</a>
                                            </div>
                                        </div>
                                        @if(session()->has('message'))
                                        <div class="alert alert-success  alert-block">
                                            <button type="button" class="close" data-dismiss="alert">×</button>    
                                            {{ session()->get('message') }}
                                        </div>
                                        @endif
                                        <div class="row">
                                            @foreach($posts as $post)
                                            <div class="col-sm-6 col-md-6 col-lg-4">

                                                <div class="blog grid-blog">
                                                    <div class="dropdown profile-action">
                                                        <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="{{route('admin.delete.post',$post->post_id)}}"><i class="fa fa-remove m-r-3"></i> Delete</a>
                                                        </div>
                                                    </div>
                                                    <div class="blog-image">
                                                        <a href="{{route('admin.view.blog',$post->post_id)}}"><img class="img-fluid" src="{{asset('images/blog/'.$post->post_img)}}" alt=""></a>
                                                    </div>
                                                    <div class="blog-content">
                                                        <h3 class="blog-title"><a href="{{route('admin.view.blog',$post->post_id)}}">{{$post->post_title}}</a></h3>
                                                        <p>{{ \Illuminate\Support\Str::words($post->post_body, 30, "...")}}</p>
                                                        <a href="{{route('admin.view.blog',$post->post_id)}}" class="read-more"><i class="fa fa-long-arrow-right"></i> Read More</a>
                                                        <div class="blog-info clearfix">
                                                            <div class="post-left">
                                                                <ul>
                                                                    <li><a href="#."><i class="fa fa-calendar"></i> <span>{{$post->date}}</span></a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="post-right"><i class="fa fa-eye">{{$post->post_view}}</i> <i class="fa fa-comment-o"></i>17</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Recent Orders -->
                        </div>
                    </div>
                </div>			
            </div>
            <!-- /Page Wrapper -->

        </div>
        <script src="{{asset('assetsAdmin/js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/popper.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assetsAdmin/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
        <script src="{{asset('assetsAdmin/plugins/datatables/datatables.min.js')}}"></script>  
        <script src="{{asset('assetsAdmin/js/script.js')}}"></script>

        <div class="sidebar-overlay"></div>
    </body>
</html>