<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>DoctorPlus</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fancybox/jquery.fancybox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
        <style type="text/css">

            #scrollable-dropdown-menu .tt-menu {
                max-height: 400px;
                max-width:500px;
                overflow-x: hidden;
                overflow-y: auto;
                position: fixed;

            }
            .twitter-typeahead { width: 100%; } 
            #scrollable-dropdown-menu input[type=text]{
                width:500px !important;
            }

            #scrollable-dropdown-menu_1 .tt-menu {
                max-height: 400px;
                max-width:240px;
                overflow-x: hidden;
                overflow-y: auto;
                position: fixed;

            }

            #scrollable-dropdown-menu_1 input[type=text]{
                width:240px !important;
            }

            .fa { margin-right:5px;}
            .rating .fa {font-size: 22px;}
            .rating-num { margin-top:0px;font-size: 54px; }
            .progress { margin-bottom: 5px;}
            .progress-bar { text-align: left; }
            .rating-desc .col-md-3 {padding-right: 0px;}
            .sr-only { margin-left: 5px;overflow: visible;clip: auto; }

        </style>
    </head>
    <body>
        <!-- Main Wrapper -->
        <div class="main-wrapper">
            <!-- Header -->
            @include('public.header')
            <div class="content" style="transform: none; min-height: 191.6px;">
                <div class="container" style="transform: none;">
                    <div class="row @if(Request::segment(1)=='en')  @else  text-right @endif" @if(Request::segment(1)=='en')  @else  dir="rtl" @endif style="transform: none;">
                         @foreach($posts as $post)
                         <div class="col-lg-8 col-md-12">
                            <div class="blog-view">
                                <div class="blog blog-single-post">
                                    <div class="blog-image">
                                        <a href="javascript:void(0);"><img alt="" src="{{asset('images/blog/'.$post->post_img)}}" class="img-fluid"></a>
                                    </div>
                                    <h3 class="blog-title">{{$post->post_title}}</h3>
                                    <div class="blog-info clearfix">
                                        <div class="post-left">
                                            <ul>
                                                <li><i class="far fa-calendar"></i> {{$post->date}}</li>
                                                <li><i class="far fa-comments"></i> ( {{ $total = DB::table('comment')->where('postid',$post->post_id)->count() }}) Comments</li>
<!--                                                <li><i class="fa fa-tags"></i>Health Tips</li>-->
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="blog-content">
                                        {{$post->post_body}}
                                    </div>
                                </div>
                                <!--                                <div class="card blog-share clearfix">
                                                                    <div class="card-header">
                                                                        <h4 class="card-title">Share the post</h4>
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <ul class="social-share">
                                                                            <li><a href="#" title="Facebook"><i class="fab fa-facebook"></i></a></li>
                                                                            <li><a href="#" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                                                                            <li><a href="#" title="Linkedin"><i class="fab fa-linkedin"></i></a></li>
                                                                            <li><a href="#" title="Google Plus"><i class="fab fa-google-plus"></i></a></li>
                                                                            <li><a href="#" title="Youtube"><i class="fab fa-youtube"></i></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>-->
                                <!--                                <div class="card author-widget clearfix">
                                                                    <div class="card-header">
                                                                        <h4 class="card-title">About Author</h4>
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <div class="about-author">
                                                                            <div class="about-author-img">
                                                                                <div class="author-img-wrap">
                                                                                    <a href="doctor-profile.html"><img class="img-fluid rounded-circle" alt="" src="assets/img/doctors/doctor-thumb-02.jpg"></a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="author-details">
                                                                                <a href="doctor-profile.html" class="blog-author-name">Dr. Darren Elder</a>
                                                                                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                <div class="card blog-comments clearfix">
                                    <div class="card-header">
                                        <h4 class="card-title"> {{__('Comments')}}(12)</h4>
                                    </div>
                                    <div class="card-body pb-0">
                                        <ul class="comments-list">
                                            @foreach($comment as $com)
                                            <li>
                                                <div class="comment">
                                                    <div class="comment-author">
                                                        <img class="avatar" alt="" src="assets/img/patients/patient6.jpg">
                                                    </div>
                                                    <div class="comment-block">
                                                        <span class="comment-by">
                                                            <span class="blog-author-name">{{$com->com_name}}</span>
                                                            <p class="blog-date">{{$com->com_email}}</p>
                                                        </span>
                                                        <p>  {{$com->comment}}</p>
                                                        <p class="blog-date">{{$com->date}}</p>
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                                <div class="card new-comment clearfix">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('Leave Comment')}}</h4>
                                    </div>
                                    <div class="card-body">
                                        <form action="{{route('comment.add')}}" method="post">
                                            <input type="hidden" name="postid" value="{{$post->post_id}}" class="form-control" placeholder="Name">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <label>{{__('Name')}} <span class="text-danger">*</span></label>
                                                <input type="text" required="" name="name" id="name2" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('Your Email Address')}} <span class="text-danger">*</span></label>
                                                <input type="email" required="" name="email" id="email2" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>{{__('Comments')}}</label>
                                                <textarea  required=""name="comments" id="comments2" rows="6" class="form-control"></textarea>
                                            </div>
                                            <div class="submit-section">
                                                <button class="btn btn-primary submit-btn" type="submit">{{__('Submit')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endforeach
                        <div class="col-lg-4 col-md-12 sidebar-right theiaStickySidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                            <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; top: 0px; left: 964.6px;">
                                <!--                                <div class="card search-widget">
                                                                    <div class="card-body">
                                                                        <form class="search-form">
                                                                            <div class="input-group">
                                                                                <input type="text" placeholder="Search..." class="form-control">
                                                                                <div class="input-group-append">
                                                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>-->
                                <div class="card post-widget">
                                    <div class="card-header"dir="auto" >
                                        <h4 class="card-title">{{__('Latest Posts')}}</h4>
                                    </div>
                                    <div class="card-body">
                                        <ul class="latest-posts">
                                            @foreach($latest as $lat)
                                            <li>
                                                <div class="post-thumb">
                                                    <a href="{{route('public.view.blog',['id'=>$lat->post_id,'language'=>app()->getLocale()])}}">
                                                        <img class="img-fluid" src="{{asset('images/blog/'.$lat->post_img)}}" alt="">
                                                    </a>
                                                </div>
                                                <div class="post-info">
                                                    <h4>
                                                        <a href="{{route('public.view.blog',['id'=>$lat->post_id,'language'=>app()->getLocale()])}}">{{$lat->post_title}}</a>
                                                    </h4>
                                                    <p>{{$lat->date}}</p>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="card category-widget">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('Blog Categories')}}</h4>
                                    </div>
                                    <div class="card-body">
                                        <ul class="categories">
                                            @foreach($categories as $category)
                                            <li><a href="{{route('public.post.view',['id'=>$category->cat_id,'language'=>app()->getLocale()])}}">{{$category->name}} <span>({{ $total = DB::table('post')->where('lang',session()->get('language'))->where('category_id',$category->cat_id)->groupBy('category_id')->count() }})</span></a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="resize-sensor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; z-index: -1; visibility: hidden;"><div class="resize-sensor-expand" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 330px; height: 2268px;"></div></div><div class="resize-sensor-shrink" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%"></div></div></div></div></div>
                        <!-- /Blog Sidebar -->

                    </div>
                </div>
            </div>
            @include('public.footer')