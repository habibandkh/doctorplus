<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>DoctorPlus</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fancybox/jquery.fancybox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            @include('public.header')
            <!-- /Header -->
            <div class="content" style="min-height: 239.6px;background-color: #fff;margin-bottom: 50px" >
                <div class="container-fluid" style="margin-bottom: 50px">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <h2> TERMS AND CONDITIONS</h2>
                            <p>
                                PLEASE READ THESE USER TERMS AND CONDITIONS OF USE (“TERMS AND CONDITIONS”) CAREFULLY BEFORE USING THE SERVICES OFFERED BY DOCTORPLUS.AF (“DOCTOR PLUS” OR “US” OR “WE”). THESE TERMS AND CONDITIONS SET FORTH THE LEGALLY BINDING TERMS AND CONDITIONS FOR YOUR (“YOU” OR “YOUR” OR “USER”) USE OF THE WWW.DOCTORPLUS.AF WEBSITE AND DOMAIN NAME (“SITE”), AND ANY SUBDOMAINS, FEATURES, CONTENT, OR APPLICATIONS OFFERED BY US, YOU AND THIRD PARTIES(COLLECTIVELY, THE “SERVICE”). THE SERVICE IS INTENDED FOR USE ONLY BY LICENSED PHYSICIANS. BY USING THE SERVICE IN ANY MANNER, INCLUDING BUT NOT LIMITED TO VISITING OR BROWSING THE SITE, YOU AGREE TO BE BOUND BY THESE TERMS AND CONDITIONS. FOR CLARITY, THESE TERMS and CONDITIONS APPLY TO ALL USERS OF THE SERVICE, INCLUDING USERS WHO ARE ALSO CONTRIBUTORS OF CONTENT, INFORMATION, AND OTHER MATERIALS OR SERVICES ON THE SITE
                            </p>
                            <p>DOCTOR PLUS hopes that you will find the Service useful but reminds you that the Service is not meant to serve as a substitute for your own professional medical judgment. You should always exercise your professional judgment in evaluating your patients, and should carefully consider any treatment plan based upon any Content (as defined below in clause 4). DOCTOR PLUS encourages you to confirm the information made available or otherwise obtained through the Service with other reliable sources before undertaking any treatment based on such Content.</p>
                            <h3>Use of this Service</h3>
                            <p>1.1 Use of the Service includes accessing, browsing, posting to, or registering to use the Service.</p>
                            <p>1.2. By using our Service, you confirm that you accept all of these Terms and Conditions and that you agree to comply with them and all other operating rules, policies and procedures that may be published from time to time on the Site by DOCTOR PLUS.</p>
                            <p>1.3. If you do not agree to these Terms and Conditions, you must not use the Service.</p>
                            <p>1.4. You represent and warrant that (a) you are a Doctor, (b) all registration information you submit is accurate and truthful, and (c) your use of the Service does not violate any applicable law or regulation.</p>
                            <p>1.5. We reserve the right to refuse access to, or registration for, the Service to anyone without providing a reason.</p>
                            <p>1.6. We reserve the right at any time to add to, change or delete from the Service any service or functionality as may exist from time to time.</p>
                            <h3>Eligibility</h3>
                            <p>2.1. Subject to these Terms and Conditions, the Service is open to all Doctors from anywhere in the Afghanistan.</p>
                            <p>2.2. Access to, and use of, any sections of the Service is conditional upon registration as a member of the DOCTOR PLUS community and on you meeting certain criteria (see clause 5 below). DOCTOR PLUS reserves the right, at its discretion, to change any criteria from time to time.</p>
                            <p>2.3. Medical Practitioner who are not fully registered with a recognized medical regulatory body (MOPH), will be denied access to the Service.</p>
                            <h3>Registration</h3>
                            <p>3.1. Registration applications can be either online signup or through an invitation by an existing registered user. All applications are considered, though the ones originated by invitations have priority.</p>
                            <p>3.2. As a condition to using the Service, you will be required to register with DOCTOR PLUS using your email address and password chosen by you (collectively, your “DOCTOR PLUS User ID”). You shall provide DOCTOR PLUS with accurate, complete, and updated registration information. DOCTOR PLUS reserves the right to refuse registration of, or cancel a DOCTOR PLUS User ID in its sole discretion. You are solely responsible for all activity that occurs under your DOCTOR PLUS User ID and shall be responsible for maintaining the confidentiality of your password. You shall never use another User’s account or allow any third party to use yours. You will immediately notify DOCTOR PLUS in writing of any unauthorized use of your account, or other account related security breach of which you are aware. You grant us the right to transmit, monitor, retrieve, store, and use your DOCTOR PLUS User ID and related registration information in connection with the operation of the Service for the effective & efficient operation of the Site.</p>
                            <p>3.3. Registration to the Site is free of charge. We try to ensure that the Service availability is uninterrupted and that transmissions will be error-free. However, we do not guarantee that our site, or any content on it, will always be available or be uninterrupted. We may suspend, withdraw, discontinue or change all or any part of our Service without notice, including to allow for repairs, maintenance, or the introduction of new facilities or services. We will not be liable to you if for any reason the Service is unavailable at any time or for any period. We, of course, try to limit the frequency and duration of any suspension or restriction. We make no assurance that the Service will continue to be available in the future.</p>
                            <h3>DOCTOR PLUS Content</h3>
                            <p>4.1. The term “Content” includes, without limitation, any tools, services, advertisements, advice, suggestions, videos, audio clips, written discussion comments, information, data, text, photographs, software, scripts, graphics, and interactive features generated, provided, or otherwise made accessible by DOCTOR PLUS, its content providers or other partners, or other Users on or through the Service including all User Submissions (as defined below in clause 6).</p>
                            <p>4.2. You agree that the Service contains Content specifically provided by DOCTOR PLUS or its partners, and that as between the parties such Content (except for User Submissions provided by you) is the property of DOCTOR PLUS and is protected under international intellectual property laws, including copyrights, trademarks, service marks, patents, trade secrets or other proprietary rights and laws. You shall abide by all copyright notices, information, and restrictions contained in any such Content. You shall not sell, license, rent, modify, distribute, copy, reproduce, transmit, publicly display, publicly perform, publish, adapt, edit, create derivative works from, display in frames, link to, or otherwise commercially exploit any Content or third party submissions or other proprietary rights not owned by you, (i) without the express written consent of the respective owners or other valid right, and (ii) in any way that violates any third party right. You may print or download a single copy of the Content from the Site solely for personal, non-commercial use, provided you keep intact all copyright and other proprietary notices.</p>
                            <p>4.3. DOCTOR PLUS reserves the right to remove any Content from the Service at any time, for any reason (including, but not limited to, upon receipt of claims or allegations from third parties or authorities relating to such Content or if DOCTOR PLUS is concerned that you may have violated the Terms and Conditions).</p>
                            <h3>Types of Users</h3>
                            <p>5.1. All functionalities/sections of the Service are restricted and are only accessible to “Registered Users”. For instance, to post any content to the Service or post a comment, you have to be a “Registered User” of the DOCTOR PLUS community – see 5.2.1 below.</p>
                            <p>5.2. There are two types of Service users: (i) (i) Registered Users and (ii) Unregistered Users (“Visitors”):</p>
                            <h3>5.2.3. Unregistered Users</h3>
                            <p>Unregistered Users are users of the Service who are not registered. Unregistered Users have no access to the site.<br><br>
                                5.3. If you are Unregistered user and apply to become a Registered User, we reserve the right to ask for proof of your qualification as a Medical Practitioner. Additionally, if our verification process reveals that you are not fully registered with a recognized medical regulatory body, the request to become a Registered User will be denied. In the event that your registration with a relevant medical regulatory body for doctors is subsequently revoked, your rights to continue to access and use the Service as a Registered User will cease.<br><br>
                                5.4. While we take steps to authenticate Medical practitioner qualifications, we cannot guarantee that every Registered User on the Service is indeed qualified or a registered practicing doctor.
                            </p>
                            <h3>User Submissions</h3>
                            <p>The Site provides you with the ability to upload, submit, disclose, distribute or otherwise post (hereafter, “posting”) content, videos, audio clips, written discussion comments, data, text, photographs, software, scripts, graphics, works of authorship or other information to the Site (the “User Submissions”). You own all rights, title and interests in and to all User Submissions provided by you (including, without limitation, any submissions as part of any “Questions”, “Share” features). Please note, however, this excludes any responses by other users or by us to the content you have posted.<br><br>
                                Such User Submissions provided by you, are non-confidential and are the property of the originating User. By posting User Submissions on the Site or otherwise through the Service: <br><br>
                                6.2. You represent and warrant: that you own or otherwise control all rights to post such User Submissions and that disclosure and Use of such User Submissions by DOCTOR PLUS (including without limitation, publishing content on or at the Site) will not infringe or violate the rights of any third party, including without limitation any privacy, publicity, contract or other rights of any person or entity; <br><br>
                                6.3. You understand that DOCTOR PLUS shall have the right to reformat, excerpt, or translate any materials, content or information submitted by you; and that all information publicly posted or privately transmitted through the Site is the sole responsibility of the person from which such content originated; and that DOCTOR PLUS cannot guarantee the identity of or the authenticity of any data posted by any other Users with whom you may interact in the course of using the Service. <br><br>
                                6.4. Your User Submissions will not contain any personally identifiable information with respect to any patient or other third party. In particular, you must remove any information that would associate your submission with a specific patient or individual. In countries where health and patient privacy laws apply, the use of de-identified health information is allowed, so long as identifiers have been removed and a key is not disclosed that would allow the information to be re-identified. You will not disclose to DOCTOR PLUS a key to re-identify any patient data. <br><br>
                                6.5. You shall not post any User Submissions that contain misleading, false, or inappropriate language or statements. <br><br>
                                6.6. DOCTOR PLUS does not endorse any User Submissions. DOCTOR PLUS has the right, but not the obligation, to monitor the Site, Service, Content, or User Submissions. DOCTOR PLUS may remove any User Submission at any time for any reason (including, but not limited to, upon receipt of claims or allegations from third parties or authorities relating to such User Submission), or for no reason at all. DOCTOR PLUS will consider requests to remove User Submissions on a case-by-case basis once we are contacted by the User. <br><br>
                                6.7. Under no circumstances will DOCTOR PLUS be responsible or liable in any way for any User Submissions, including, but not limited to, any errors or omissions in any User Submissions, or any loss or damage of any kind incurred in connection with use of or exposure to any User Submissions posted, emailed, accessed, transmitted or otherwise made available via the Service. <br><br>
                                6.8. You warrant that any contribution you make to the Service (including comment, discussion or article post) complies with these User Terms and Conditions, and you will be liable to us and indemnify us for any breach of this warranty. This means you will be responsible for any loss or damage DOCTOR PLUS suffers as a result of your breach of warranty (see also clause 15.3). <br><br>6.9. We have the right to disclose your identity to any third party who claims that any content posted or uploaded by you to our site constitutes a violation of their intellectual property rights, or of their right to privacy. <br><br>
                                6.10. We have the right to remove any posting you make on our site if, in our opinion, your post does not comply with the User Conduct section of these terms and conditions (clause 8) or we otherwise deem to be inappropriate for the Service. <br><br>
                                6.11. The views expressed by other users on the Service do not represent DOCTOR PLUS views or values. <br><br>
                                6.12. We reserve the right to remove material you post without notice from the Service and from any discussion, and DOCTOR PLUS does not warrant that it will keep or continue to make available any material that you have posted to this site or to any discussion for any length of time, and you are advised to make a copy of any message or material you wish to retain.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            @include('public.footer')
        </div>
        <script src="{{asset("assets/js/jquery.min.js")}}"></script>
        <script src="{{asset("assets/js/popper.min.js")}}"></script>
        <script src="{{asset("assets/js/bootstrap.min.js")}}"></script>
        <script src="{{asset("assets/plugins/fancybox/jquery.fancybox.min.js")}}"></script>
        <script src="{{asset("assets/js/slick.js")}}"></script>
        <script src="{{asset("assets/js/script.js")}}"></script>
    </body>
</html>
