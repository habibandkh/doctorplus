@include('public.header')
<div class="content" style="min-height: 239.6px;background-color: #fff;" >
    <div class="container-fluid" style="padding-bottom: 50px">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <!-- Register Content -->
                <div class="account-content">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 col-lg-6 login-left">
                            <img src="assets/img/login-banner.png" class="img-fluid" alt="Doccure Register">	
                        </div>
                        <div class="col-md-12 col-lg-6 login-right">
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif
                            <div class="login-header">
                                <h3> {{__('Patient Register')}}<a href="{{route('Join.As.Doctor')}}">{{__('Are you a Doctor?')}}</a></h3>
                            </div>
                            <!-- Register Form -->
                            <form method="post" action="{{route('user.register.them')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group form-focus">
                                    <input  required="" name="name" type="text" class="form-control floating">
                                    <label class="focus-label">{{__('Name')}}</label>
                                </div>
                                <div class="form-group form-focus">
                                    <input type="text" required=""  name="f_name"  class="form-control floating">
                                    <label class="focus-label">{{__('Father Name')}}</label>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">

                                            <input type="tel" id="phone" name="phone" required="">
                                            <span id="valid-msg" class="hide"></span>
                                            <span id="error-msg" class="hide"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="number"  name="age" required="" placeholder="{{__('age')}}" class="form-control floating">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input  required=""type="radio" name="gender" value="1" class="form-check-input">{{__('Male')}}
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input   required=""type="radio" name="gender" value="0"class="form-check-input">{{__('Female')}}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row ">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input required="" minlength="6" type="password" id="password"class="form-control" name="pass"placeholder="password">
                                            <input type="checkbox" onclick="myFunction()">{{__('Show Password')}}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input required="" minlength="6" type="password" id="confirm_password"class="form-control" placeholder="confirm password">
                                            <input type="checkbox" onclick="myFunction1()">{{__('Show Password')}}
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="label">{{__('upload photo:')}}</label>
                                        <div class="form-group">
                                            <input type="file" name="photo"class="form-control" placeholder="">
                                        </div>
                                    </div>
                                </div>


                                <div class="text-right">
                                    <a class="forgot-link" href="{{route('login.page.user')}}">{{__('Already have an account?')}}</a>
                                </div>
                                <button class="btn btn-primary btn-block btn-lg login-btn" type="submit">{{__('Sign up')}}</button>
<!--                                <div class="login-or">
                                    <span class="or-line"></span>
                                    <span class="span-or">or</span>
                                </div>
                                <div class="row form-row social-login">
                                    <div class="col-6">
                                        <a href="#" class="btn btn-facebook btn-block"><i class="fab fa-facebook-f mr-1"></i> Login</a>
                                    </div>
                                    <div class="col-6">
                                        <a href="#" class="btn btn-google btn-block"><i class="fab fa-google mr-1"></i> Login</a>
                                    </div>
                                </div>-->
                            </form>
                            <!-- /Register Form -->
                        </div>
                    </div>
                </div>
                <!-- /Register Content -->
            </div>
        </div>
    </div>
</div>
@include('public.footer')