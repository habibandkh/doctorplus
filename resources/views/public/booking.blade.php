<!DOCTYPE html>
<html lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>DoctorPlus</title>
    <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
    <link rel="stylesheet" href="">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/fancybox/jquery.fancybox.min.css')}}" rel="stylesheet">
    <link href="{{asset('booking/date_picker.css')}}" rel="stylesheet">
    <link href="{{asset('booking/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    <style>
        #loading {
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            position: fixed;
            display: block;
            opacity: 1;
            background-color: #fff;
            z-index: 99;
            text-align: center;
        }

        #loading-image {
            position: absolute;

        }
    </style>

</head>
<body>

    <!-- Main Wrapper -->
    <div class="main-wrapper">

        <!-- Header -->
        @include('public.header')

        <div class="content" style="min-height: 262px;">
            <div class="container">
                @foreach($doctors as $doctor)
                <div class="row">

                    <div class="col-12">

                        <!-- Doctor Widget -->
                        <div class="card">
                            <div class="card-body">
                                <div class="doctor-widget">
                                    <div class="doc-info-left">
                                        <div class="doctor-img">
                                            <img src="{{asset('images/doctor/'.$doctor->doctor_picture)}}" class="img-fluid" alt="User Image">
                                        </div>
                                        <div class="doc-info-cont">
                                            <h4 class="doc-name">{{$doctor->title}} {{$doctor->doctor_name}}</h4>
                                            @foreach($doctor->speciality as $special)
                                            <p class="doc-department"> {{$special->specialityName}}</p>

                                            @endforeach

                                            <div class="rating">
                                                <div class="d-none">  {{$rating=number_format($doctor->stars,1)}} </div>
                                                @foreach(range(1,5) as $i)
                                                @if($rating >0)
                                                @if($rating >0.5)
                                                <i class="fas fa-star  filled"></i>
                                                @else
                                                <i class="fas fa-star-half filled" ></i>
                                                @endif
                                                @else
                                                <i class="fas  fa-star"></i>
                                                @endif
                                                <?php $rating--; ?>
                                                @endforeach
                                                <span class="d-inline-block average-rating">({{$doctor->total_stars}})</span>
                                            </div>



                                        </div>
                                    </div>
                                    <div class="doc-info-right">
                                        <div class="clini-infos">

                                            <div class="d-none"> 
                                                {{ $n=$doctor->account_view}}
                                                {{ $precision = 1}}
                                                {{$n_format=0}}
                                                @if ($n < 900) 
                                                {{ $n_format = number_format($n)}}

                                                @elseif ($n < 900000) 
                                                {{  $n_format = number_format($n / 1000, $precision). ' K'}}


                                                @elseif  ($n < 900000000) 
                                                {{   $n_format = number_format($n / 1000000, $precision). ' M'}}


                                                @elseif  ($n < 900000000000) 
                                                {{  $n_format = number_format($n / 1000000000, $precision). ' B' }}


                                                @elseif ($n < 900000000000000) 
                                                {{   $n_format = number_format($n / 1000000000000, $precision). ' T'}}
                                                @endif
                                            </div>
                                            <ul>
                                                <li><i class="far fa-user"></i> {{$doctor->count_patient}} {{__('Patients')}}</li>
                                                <li><i class="far fa-eye"></i>  {{$n_format}}  {{__('View Account')}}</li>

                                                <li><i class="far fa-money-bill-alt"></i>{{__('AFN')}} {{$doctor->doctor_fee}} </li>
                                            </ul>
                                        </div>
                                        <div class="doctor-action">

                                            @if(session('is_pateint')=='pateint')
                                            @if($doctor->doctor_bookmark == null)
                                            <a href="{{route('bookmark.doctor',[$doctor->doctorID])}}" class="btn btn-white fav-btn">
                                                <i class="far fa-bookmark"></i>
                                            </a>
                                            @else

                                            <a href="{{route('cancel.bookmark.doctor',[$doctor->doctorID])}}" class="btn btn-white fav-btn bg-danger">
                                                <i class="far fa-bookmark"></i>
                                            </a>

                                            @endif
                                            @endif
                                            <!--                                            <a href="chat.html" class="btn btn-white msg-btn">
                                                                                            <i class="far fa-comment-alt"></i>
                                                                                        </a>
                                                                                        <a href="javascript:void(0)" class="btn btn-white call-btn" data-toggle="modal" data-target="#voice_call">
                                                                                            <i class="fas fa-phone"></i>
                                                                                        </a>
                                                                                        <a href="javascript:void(0)" class="btn btn-white call-btn" data-toggle="modal" data-target="#video_call">
                                                                                            <i class="fas fa-video"></i>
                                                                                        </a>-->
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Schedule Widget -->
                        <div class="card booking-schedule schedule-widget">

                            <!-- Schedule Header -->
                            <div class="schedule-header">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="tab-pane fade show active" id="book" role="tabpanel" aria-labelledby="book-tab">
                                            <div class="main_title_3">
                                                <h3><strong>1</strong>{{__('Select Clinic')}}</h3>
                                            </div>
                                            <div class="row text-center">
                                                @foreach($doctor->Address as $add)
                                                <div class="col-6 myclinic">
                                                    <a id="{{"address_boxs".$add->clinic_address_id}}" href="javascript:void(0);" onclick="getclinicid('{{$add->clinic_address_id}}','{{"#address_boxs".$add->clinic_address_id}}')" class="btn btn-default  bg-light border border-2">
                                                        <span class="fas fa-map-marker-alt"></span>{{$add->name}}({{ date('H:i', strtotime($add->From))}} , {{ date('H:i', strtotime($add->To))}})</a>
                                                </div>
                                                @endforeach
                                            </div>
                                            <input type="hidden" id="my_hidden_clinic_id">
                                            <input type="hidden" id="my_hidden_clinic_id_plus">
                                            <div class="form-group add_bottom_45">
                                            </div>
                                            <div class="main_title_3">
                                                <h3><strong>2</strong>{{__('Select your date')}}</h3>
                                            </div>
                                            <div class="form-group add_bottom_45">
                                                <div id="calendar"></div>
                                                <input type="hidden" id="my_hidden_input">
                                                <ul class="legend">
                                                    <li><strong></strong>{{__('Available')}}</li>
                                                    <li><strong></strong>{{__('Not available')}}</li>
                                                </ul>
                                            </div>
                                            <div class="main_title_3">
                                                <h3><strong>3</strong>{{__('Select your time')}}</h3>
                                            </div>
                                            <div class="row show_all_time_slute">
                                            </div>
                                            <div class="row show_all_time_slute_1">
                                            </div>
                                            <div class="main_title_3">
                                                <h3><strong>4</strong>{{__('Who is Patient!')}}</h3>
                                            </div>
                                            <!-- /row -->
                                            <div class="card">
                                                <div class="card-body pt-0">

                                                    <!-- Tab Menu -->
                                                    <nav class="user-tabs mb-4">
                                                        <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                                                            <li class="nav-item">
                                                                <a class="nav-link active" href="#doc_overview" data-toggle="tab">{{__('My Self')}}</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" href="#doc_locations" data-toggle="tab">{{__('Other')}}</a>
                                                            </li>

                                                        </ul>
                                                    </nav>
                                                    <!-- /Tab Menu -->

                                                    <!-- Tab Content -->
                                                    <div class="tab-content pt-0">
                                                        <!-- Overview Content -->
                                                        <div role="tabpanel" id="doc_overview" class="tab-pane fade show active">
                                                            <div class="row">
                                                                <div class="col-md-6 offset-md-3">
                                                                    @if(session('is_pateint')=='pateint')
                                                                    <form action="{{route('store.appointment')}}" method="post" id="appointment_form">
                                                                        @else
                                                                        <form action="{{route('store.appointment')}}" method="post" id="appointment_form_signup">
                                                                            @endif
                                                                            {{csrf_field()}}
                                                                            <div class="summary @if(Request::segment(1)=='en') @else text-right @endif"  @if(Request::segment(1)=='en') @else dir="rtl" @endif>
                                                                                <ul>
                                                                                    <input  type="hidden"  name="token" value="self">
                                                                                    <input  type="hidden" id="hidden_clinic_id" name="clinic_id">
                                                                                    <input  type="hidden" id="get_Docotr_ID" name="getDocotrID" value="{{$doctor->doctorID}}" >
                                                                                    <li >{{__('Date')}}: <strong  ><input name="app_date"  style="border-style:none;background: transparent;" type="text" class="myappdate" id="my_hidden_input_date"></strong></li>
                                                                                    <li >{{__('Time')}}: <strong  ><input name="app_time" style="border-style:none;background: transparent;"type="text"  class="myapptime"  id="visit_time"></strong></li>
                                                                                    <li >{{__('Dr.Name')}}: <strong ><input disabled="" value="{{$doctor->doctor_name}}" style="border-style:none;background: transparent;" type="text"></strong></li>
                                                                                    <li >{{__('Fee')}}: <strong ><input disabled="" value="{{$doctor->doctor_fee }} AFN " style="border-style:none;background: transparent;" type="text"></strong></li>
                                                                                    <li >{{__('Patient')}}: <strong ><input disabled="" value="{{session('name_pateint')}}" style="border-style:none;background: transparent;" type="text"></strong></li>
                                                                                </ul>
                                                                            </div>
                                                                            <br>
                                                                            <br>
                                                                            <hr>
                                                                            @if(session('is_pateint')=='pateint')
                                                                            <div class="submit-section proceed-btn text-center">
                                                                                <button  type="submit"  class="btn btn-primary submit-btn" >{{__('confirm and Book Now')}}</button>
                                                                            </div>
                                                                            @else
                                                                            <div class="submit-section proceed-btn text-center">
                                                                                <button  type="submit"  class="btn btn-primary submit-btn" >{{__('confirm and Book Now')}}</button>
                                                                            </div>
                                                                            @endif
                                                                        </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /Overview Content -->
                                                        <!-- Locations Content -->
                                                        <div role="tabpanel" id="doc_locations" class="tab-pane fade">
                                                            <!-- Review Listing -->
                                                            <div class="row">
                                                                <div class="col-md-6 offset-md-3">
                                                                    @if(session('is_pateint')=='pateint')
                                                                    <form action="{{route('store.appointment')}}" method="post" id="appointment_form_other" dir="auto">
                                                                        @else
                                                                        <form action="{{route('store.appointment')}}" method="post" id="appointment_form_signup_other">
                                                                            @endif
                                                                            {{csrf_field()}}
                                                                            <div class="summary @if(Request::segment(1)=='en') @else text-right @endif" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
                                                                                <ul >
                                                                                    <input  type="hidden"  name="token" value="other">
                                                                                    <input  type="hidden" id="hidden_clinic_id_other" name="clinic_id">
                                                                                    <input  type="hidden" id="get_Docotr_ID" name="getDocotrID" value="{{$doctor->doctorID}}" >
                                                                                    <li>{{__('Date')}}: <strong  ><input name="app_date"  style="border-style:none;background: transparent;" type="text" class="myappdate" id="my_hidden_input_date"></strong></li>
                                                                                    <li>{{__('Time')}}: <strong  ><input name="app_time" style="border-style:none;background: transparent;"type="text"  class="myapptime"  id="visit_time"></strong></li>
                                                                                    <li>{{__('Dr.Name')}}: <strong ><input disabled="" value="{{$doctor->doctor_name}}" style="border-style:none;background: transparent;" type="text"></strong></li>
                                                                                    <li>{{__('Fee')}}: <strong ><input disabled="" value="{{$doctor->doctor_fee }} AFN " style="border-style:none;background: transparent;" type="text"></strong></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="row col-md-12">
                                                                                @if(session('is_pateint')=='pateint')
                                                                                <select  id="select_patient" class="browser-default custom-select" name="getPateintID">
                                                                                    <option>{{__('patient')}}</option>
                                                                                    @foreach($pateint as $pa)
                                                                                    <option value="{{$pa->patientID}}">{{$pa->p_name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                @endif
                                                                            </div>
                                                                            <br>
                                                                            <br>
                                                                            <hr>
                                                                            @if(session('is_pateint')=='pateint')
                                                                            <div class="submit-section proceed-btn text-center">
                                                                                <button  type="submit"  class="btn btn-primary submit-btn" >{{__('confirm and Book Now')}}</button>
                                                                            </div>
                                                                            @else
                                                                            <div class="submit-section proceed-btn text-center">
                                                                                <button  type="submit"  class="btn btn-primary submit-btn" >{{__('confirm and Book Now')}}</button>
                                                                            </div>
                                                                            @endif
                                                                        </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /Locations Content -->
                                                    </div>
                                                </div>
                                                <!-- /Doctor Details Tab -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <script src="{{asset("booking/jquery-2.2.4.min.js")}}"></script>
        <script src="{{asset("booking/common_scripts.min.js")}}"></script>
        <script src="{{asset("booking/functions.js")}}"></script>
        <script src="{{asset("booking/bootstrap-datepicker.js")}}"></script>
        <script src="{{asset('assets/plugins/fancybox/jquery.fancybox.min.js')}}"></script>
        <script src="{{asset('assets/js/moment.min.js')}}"></script>
        <script src="{{asset('assets/js/popper.min.js')}}"></script>
        <script src="{{asset('assets/js/script.js')}}"></script>
        <script>
                                                        jQuery("#appointment_form").submit(function (evt) {

                                                        if (jQuery("input[Name='app_date']").val().length < 1) {
                                                        alert("please select a Date for Appointment");
                                                        evt.preventDefault();
                                                        return;
                                                        }
                                                        if (jQuery("input[Name='app_time']").val().length < 1) {
                                                        alert("please select a Time-slot for Appointment");
                                                        evt.preventDefault();
                                                        return;
                                                        }

                                                        });
                                                        /////////////
                                                        jQuery("#appointment_form_other").submit(function (evt) {

                                                        if (jQuery("input[Name='app_date']").val().length < 1) {
                                                        alert("please select a Date for Appointment");
                                                        evt.preventDefault();
                                                        return;
                                                        }
                                                        if (jQuery("input[Name='app_time']").val().length < 1) {
                                                        alert("please select a Time-slot for Appointment");
                                                        evt.preventDefault();
                                                        return;
                                                        }
                                                        });
        </script>
        <script>

            jQuery("#appointment_form_signup").submit(function (evt) {
            if (jQuery("input[Name='app_date']").val().length < 1) {
            alert("please select a Date for Appointment");
            evt.preventDefault();
            return;
            }
            if (jQuery("input[Name='app_time']").val().length < 1) {
            alert("please select a Time-slot for Appointment");
            evt.preventDefault();
            return;
            }

            $('#myModal').show();
            return  false;
            });
            ////////////
            jQuery("#appointment_form_signup_other").submit(function (evt) {


            if (jQuery("input[Name='app_date']").val().length < 1) {
            alert("please select a Date for Appointment");
            evt.preventDefault();
            return;
            }
            if (jQuery("input[Name='app_time']").val().length < 1) {
            alert("please select a Time-slot for Appointment");
            evt.preventDefault();
            return;
            }


            $('#myModal').show();
            return  false;
            });
        </script>
        <script>
            function getTimeVisit(selected_time) {
            // $('#visit_time').val(selected_time);
            $('.myapptime').val(selected_time);
            }
            function getclinicid(clinicid, clinicidplus){

            $('.myapptime').val('');
            $('.myappdate').val('');
            $(".show_all_time_slute").text('');
            $("#my_hidden_clinic_id").val(clinicid);
            $("#hidden_clinic_id").val(clinicid);
            $("#hidden_clinic_id_other").val(clinicid);
            $("#my_hidden_clinic_id_plus").val(clinicidplus);
            $(clinicidplus).css("background", "rgba(175, 201, 99, 0.2)");
            }

            $('#calendar').datepicker({
            startDate: '-0d',
                    todayHighlight: true,
                    //daysOfWeekDisabled: [0],
                    weekStart: 1,
                    format: "yyyy-mm-dd"
            }).on('changeDate', function (e) {

            var d = $(this).datepicker('getDate');
            var date_now = e.format();
            // $('#my_hidden_input').val(date_now);
            $('.myappdate').val(date_now);
            var weekday = new Array();
            weekday[0] = "Monday";
            weekday[1] = "Tuesday";
            weekday[2] = "Wednesday";
            weekday[3] = "Thursday";
            weekday[4] = "Friday";
            weekday[5] = "Saturday";
            weekday[6] = "Sunday";
            var DAY = weekday[d.getUTCDay()];
            var url = window.location.pathname;
            var id = url.substring(url.lastIndexOf('/') + 1);
            var clinic_id = $('#my_hidden_clinic_id').val();
            var url_id = "{{ route('ajaxs.get.time')}}";
            $.ajax({
            url: url_id,
                    dataType: "json",
                    data: {dayselected: DAY, DATE: date_now, Clinic_id: clinic_id, ID: id},
                    success: function (data) {

                    var time = '';
                    var timeee = '';
                    if (data.used) {

                    var res = data.used;
                    var response = data.success;
                    for (var i in response) {


                    time = time +
                            '<div class="col-md-3  time-slot">'
                            + '<a  id="my_slot_color' + i + '" onclick="getTimeVisit(\'' + response[i].visit_time + '\')"   class="btn-time-selected p-1 btn btn-block  mb-2 white-space-normal text-dark py-2 btn btn-default  bg-light border border-2">'
                            + response[i].visit_time + '</a>' + '</div>';
                    }
                    for (var x in res) {

                    timeee = timeee +
                            '<div class="col-md-3  time-slot">'
                            + '<a onclick="getTimeVisit(\'' + res[x].appointment_time + '\')"   class=" click_time_slute  btn-time-selected p-1 btn btn-block btn-outline-warning mb-2 white-space-normal text-dark py-2">'
                            + res[x].appointment_time + '</a>' + '</div>';
                    }
                    // alert(type(timeee));
                    $(".show_all_time_slute").text('');
                    $('.show_all_time_slute').append(time);
                    $(".show_all_time_slute_1 ").text('');
                    $('.show_all_time_slute_1').append(timeee);
                    var map = {};
                    $(".time-slot").each(function () {

                    var value = $(this).text();
                    if (map[value] == null) {
                    map[value] = true;
                    } else {
                    var x = '';
                    x = $(this).text();
                    $("a:contains(" + x + ")").closest('div').hide();
                    }
                    });
                    }

                    },
            });
            });
        </script>

        <script type="text/javascript">
            function numbercheck() {
            var myLength = $("#mynumber").val().length;
            if (myLength == 10)
            {
            $('#isvalaidnumber').prop('disabled', false);
            }
            }

            function sendToServe() {

            var number = $("#mynumber").val();
            var codeplusnumber = '93' + number;
            $('#is_number').val('');
            $('.phoneverification').val('');
            $('.the_phone_number').val('');
            // alert(DAY);
            $.ajax({
            url: "/send-number-to-server/",
                    dataType: "json",
                    data: {Phone_number: codeplusnumber},
                    success: function (data) {
                    if (data.is_phone == 'yesitis') {
                    $('#is_number').append('the phone number is already registerd to system!');
                    } else {
                    $('#loading').hide();
                    $('.phoneverification').val(data.code);
                    $('.the_phone_number').val(data.phonenumber);
                    var appointmentdate = $('.myappdate').val();
                    var appointmenttime = $('.myapptime').val();
                    var Docotr_ID = $('#get_Docotr_ID').val();
                    $('#app_date').val(appointmentdate);
                    $('#app_time').val(appointmenttime);
                    $('#Docotrid').val(Docotr_ID);
                    $('#model_phone_verification_b').show();
                    $('#myModal_b').hide();
                    }
                    },
            });
            }
            function confirmCode() {
            var thecode = $('.phoneverification').val();
            var entercode = $('.six_digit_code').val();
            if (thecode == entercode) {

            $('#model_signup_b').show();
            $('#model_phone_verification_b').hide();
            } else {
            $('#wrong_number').append('the code is invalaid');
            }
//                $('.the_phone_number').val();
            }
        </script>

        <!-- /Footer -->
        @include('public.footer')

        <!-- The Modal -->

        <div class="modal fadeIn" id="myModal_b">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Sign up </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div id="phoneverification"></div>
                        <p>To book an appointment please sign up first!</p>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <input disabled="" value="93" class="form-control">
                            </div>     
                            <div class="col-md-10">
                                <input placeholder="mobile number" id="mynumber" onkeyup="numbercheck()" type="text" class="form-control " maxlength="10"  name="account_number" pattern="[0-9]" title="please enter number only" required="required"> 
                                <span id="is_number" class="text-danger"></span>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button disabled="" type="button" onclick="sendToServe()" id="isvalaidnumber" class="proceed-btn text-center btn btn-primary " >Next</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fadeIn" id="model_phone_verification_b">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Phone Number Verification </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div id="phoneverification"></div>

                        <input type="text" class="phoneverification">
                        <input type="text" class="the_phone_number">

                        <p>please check!We just send a six digit verification code number to your number.</p>
                        <br>
                        <div class="row">

                            <div class="col-md-12">
                                <input placeholder="verification code" class="six_digit_code form-control" onkeyup="numbercheck()" type="text" class="form-control " maxlength="6"  name="account_number" pattern="[0-9]" title="please enter number only" required="required"> 
                                <span class="text-danger" id="wrong_number"></span>
                            </div>

                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" onclick="confirmCode()" class="proceed-btn text-center btn btn-primary isvalaidnumber" >confirm</button>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fadeIn" id="model_signup_b">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Sign up </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        Please Insert a Password!
                        <form method="post" action="{{route('user.register.them')}}" >
                            {{csrf_field()}}
                            <input  type="text" class="phoneverification">
                            <input  type="text" class="the_phone_number" name="mobilenumber">
                            <input  type="text" id="Docotrid"  name="DocotsID">
                            <input  type="text" id="app_date"  name="date_of_appointment">
                            <input  type="text" id="app_time"  name="time_of_appointment">

                            <input  type="text"   value="token_appointment" name="token_appointment">

                            <div class="row ">
                                <div class="col-md-12">
                                    <input required="" minlength="6" type="password" id="password"class="form-control" name="pass"placeholder="password">
                                </div>

                            </div>

                            <div class="modal-footer">
                                <button class="btn btn-primary btn-block btn-lg login-btn" type="submit">Signup</button>
                            </div>
                        </form>

                    </div>
                    <!-- Modal footer -->


                </div>
            </div>
        </div>
</body>
</html>
