<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>DoctorPlus</title>
        <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">
            @include('public.header')
            <br><br>
            <div class="content success-page-cont" style="min-height: 262px;" dir="auto">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="card success-card">
                                <div class="card-body">
                                    <div class="success-cont">
                                        <i class="fas fa-check"></i>
                                        <h3>{{ Session::get('account') }}</h3>
                                        <h3>{{__('Appointment booked Successfully!')}}</h3>
                                        <p> {{__('Appointment booked with')}} <strong>{{ Session::get('Doctor_title') }} {{ Session::get('Doctor_name') }} </strong><br> {{__('on')}} <strong> {{ Session::get('flash_date') }} , {{ Session::get('flash_Time') }}</strong></p>
                                        <!--                            <a href="invoice-view.html" class="btn btn-primary view-inv-btn">View Invoice</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('public.footer')
            <script src="{{asset("assets/js/jquery.min.js")}}"></script>
            <script src="{{asset("assets/js/popper.min.js")}}"></script>
            <script src="{{asset("assets/js/bootstrap.min.js")}}"></script>
            <script src="{{asset("assets/plugins/fancybox/jquery.fancybox.min.js")}}"></script>
            <script src="{{asset("assets/js/slick.js")}}"></script>
            <script src="{{asset("assets/js/script.js")}}"></script>
    </body>

</html>