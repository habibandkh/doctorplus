<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>DoctorPlus</title>
        <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">
            @include('public.header')

            <div class="content" style="min-height: 239.6px;background-color: #fff;" >
                <div class="container-fluid" style="padding-bottom: 150px">

                    <div class="row">
                        <div class="col-md-8 offset-md-2">

                            <!-- Login Tab Content -->
                            <div class="account-content" >
                                <div class="row align-items-center justify-content-center">
                                    <div class="col-md-7 col-lg-6 login-left">
                                    
                                        <img src="{{asset('images/logo/login-banner.png')}}" class="img-fluid" >	
                                    </div>
                                    <div class="col-md-12 col-lg-6 login-right">
                                        <div class="login-header text-center" >
                                            <h3 > {{__('Login')}}</h3>
                                        </div>
                                         @if(Session::has('message'))
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                          {{ Session::get('message') }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                                        <form method="post" action="{{route('login.user')}}">
                                            {{csrf_field()}}
                                            <div class="form-group ">
                                                <input  class="form-control floating" name="user_phone" type="tel"   id="Phone"  required="required">

                                            </div>
                                            <div class="form-group ">
                                                <input name="user_pass" type="password" class="form-control floating" id="login_password" required="required">

                                            </div>
                                            <div class="text-right">
                                                <a class="forgot-link" href="{{route('forget.password')}}">{{__('Forgot Password ?')}}</a>
                                            </div>
                                            <button class="btn btn-primary btn-block btn-lg login-btn" type="submit" >{{__('Login')}}</button>
                                            <div class="login-or">
                                                <span class="or-line"></span>
                                                <span class="span-or">{{__('or')}}</span>
                                            </div>

                                            <div class="text-center dont-have">{{__('Don’t have an account?')}} <a  data-toggle="modal" data-target="#myModal" >{{__('Register')}}</a></div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /Login Tab Content -->

                        </div>
                    </div>

                </div>

            </div>
            @include('public.footer')
            <script src="{{asset("assets/js/jquery.min.js")}}"></script>
            <script src="{{asset("assets/js/popper.min.js")}}"></script>
            <script src="{{asset("assets/js/bootstrap.min.js")}}"></script>
            <script src="{{asset("assets/plugins/fancybox/jquery.fancybox.min.js")}}"></script>
            <script src="{{asset("assets/js/slick.js")}}"></script>
            <script src="{{asset("assets/js/script.js")}}"></script>
    </body>

</html>