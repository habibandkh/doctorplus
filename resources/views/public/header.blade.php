
<header class="header" >
    <nav class="navbar navbar-expand-lg header-nav">
        <div class="navbar-header" dir="auto">
            <a id="mobile_btn" href="javascript:void(0);">
                <span class="bar-icon">
                    <span style="margin: 1px;padding: 2px;"></span>
                    <span></span>
                    <span></span>
                </span>
            </a>
            <a href="{{route('home')}}" class="navbar-brand logo">
                <img src="{{asset('assets/img/logo.png')}}" class="img-fluid" alt="Logo">
            </a>
        </div>
        <div class="main-menu-wrapper" dir="auto">
            <div class="menu-header">
                <a href="{{route('home')}}" class="menu-logo">
                    <img src="{{asset('assets/img/logo.png')}}" class="img-fluid" alt="Logo">
                </a>
                <a id="menu_close" class="menu-close" href="javascript:void(0);">
                    <i class="fas fa-times"></i>
                </a>
            </div>
            <ul class="main-nav">
                
                <li><a href="{{route('home')}}" >{{__('Home')}}</a></li>
                <li><a href="{{route('public.blog.list')}}" >{{__('Blog')}}</a></li>
                <li><a href=" "></a></li>
               
               <li class="has-submenu">
                    <a href="#">{{__('Language')}} <i class="fas fa-chevron-down"></i></a>
                    <ul class="submenu" >
                        <li class="text-center"><a href="{{route('locale','en')}}">{{__('English')}}</a></li>
                        <li class="text-center"><a href="{{route('locale','fa')}}">{{__('Dari')}}</a></li>
                        <li class="text-center"><a href="{{route('locale','pa')}}">{{__('Pashto')}}</a></li>
                    </ul>
                </li>
            </ul> 
        </div>		 


        <ul class="nav header-navbar-rht">
            @if(session('is_pateint')!='pateint' && session('is_doctor') !='doctor')
            <li class="nav-item">
                <a class="nav-link header-login " href="{{route('login.page.user')}}">{{__('Login / Sign up')}}</a>
            </li>

            <li class="nav-item">
                <a class="nav-link header-login " href="{{route('join.as.doctor')}}">{{__('Join As Doctor')}}</a>
            </li>
            @endif

            @if(session('is_doctor')=='doctor')
            <li class="nav-item dropdown has-arrow logged-item ">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="true">
                    <span class="user-img">
                        <img class="rounded-circle" src="{{asset('images/doctor/'.session('pic_doctor'))}}" width="31" alt="">
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right ">
                    <div class="user-header">
                        <div class="avatar avatar-sm">
                            <img src="{{asset('images/doctor/'.session('pic_doctor'))}}" alt="User Image" class="avatar-img rounded-circle">
                        </div>
                        <div class="user-text">
                            <h6>{{session('name_doctor')}}</h6>

                        </div>
                    </div>
                    <a class="dropdown-item" href="{{route('doctor.dashboard')}}">{{__('Dashboard')}}</a>
                    <a class="dropdown-item" href="{{route('doctor.profile')}}">{{__('Profile Settings')}}</a>
                    <a class="dropdown-item" href="{{route('logout.user')}}">{{__('Logout')}}</a>
                </div>
            </li>
            @endif
            <!-- User Menu -->
            @if(session('is_pateint')=='pateint')
            <li class="nav-item dropdown has-arrow logged-item ">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="true">
                    <span class="user-img">
                        <img class="rounded-circle" src="{{asset('images/user/'.session('pic_pateint'))}}" width="31" >
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right ">
                    <div class="user-header">
                        <div class="avatar avatar-sm">
                            <img src="{{asset('images/user/'.session('pic_pateint'))}}" alt="User Image" class="avatar-img rounded-circle">
                        </div>
                        <div class="user-text">
                            <h6>{{session('name_pateint')}}</h6>

                        </div>
                    </div>
                    <a class="dropdown-item" href="{{route('pateint.home')}}">{{__('Dashboard')}}</a>
                    <a class="dropdown-item" href="{{route('my.profile')}}">{{__('Profile Settings')}}</a>
                    <a class="dropdown-item" href="{{route('logout.user')}}">{{__('Logout')}}</a>
                </div>
            </li>
            @endif
            <!-- /User Menu -->

        </ul>
    </nav>
</header>


