<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
      <title>DoctorPlus</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fancybox/jquery.fancybox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    </head>
    <body>
        <!-- Main Wrapper -->
        <div class="main-wrapper">
            @include('public.header')
            <br>
            <div class="content" style="min-height: 239.6px;background-color: #fff;margin-bottom: 50px" >
                <div class="container-fluid" style="margin-bottom: 50px">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <!-- Account Content -->
                            <div class="account-content">
                                <div class="row align-items-center justify-content-center">
                                    <div class="col-md-7 col-lg-6 login-left">
                                         <img src="{{asset('images/logo/login-banner.png')}}" class="img-fluid" >		
                                    </div>
                                    <div class="col-md-12 col-lg-6 login-right">
                                        <div class="login-header">
                                            <h3>{{__('Forgot Password?')}}</h3>
                                            <p class="small text-muted">{{__('Enter your email to get a password reset link')}}</p>
                                        </div>
                                        <!-- Forgot Password Form -->
                                        <form action="">
                                            <div class="form-group form-focus">
                                                <input type="email" class="form-control floating">
                                                <label class="focus-label">{{__('Email')}}</label>
                                            </div>
                                            <div class="text-right">
<!--                                                <a class="forgot-link" href="login.html">{{__('Remember your password?')}}</a>-->
                                            </div>
                                            <button class="btn btn-primary btn-block btn-lg login-btn" type="submit">{{__('Reset Password')}}</button>
                                        </form>
                                        <!-- /Forgot Password Form -->
                                    </div>
                                </div>
                            </div>
                            <!-- /Account Content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('public.footer')
        <script src="{{asset("assets/js/jquery.min.js")}}"></script>
        <script src="{{asset("assets/js/popper.min.js")}}"></script>
        <script src="{{asset("assets/js/bootstrap.min.js")}}"></script>
        <script src="{{asset("assets/plugins/fancybox/jquery.fancybox.min.js")}}"></script>
        <script src="{{asset("assets/js/script.js")}}"></script>
        <div class="sidebar-overlay"></div>
</body>
</html>