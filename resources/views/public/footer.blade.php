@if(Request::segment(1)=='en')
<footer class="footer">
    <!-- Footer Top -->
    <div class="footer-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-about">
                        <div class="footer-logo">
                            <img src="{{asset('assets/img/logofooter.png')}}" alt="logo">
                        </div>
                        <div class="footer-about-content">
                            <p>  {{__('DoctorPlus is an online platform that connects patients and doctors.')}}</p>
                            <div class="social-icon">
                                <ul>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-instagram"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-dribbble"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Footer Widget -->
                </div>
                <div class="col-lg-3 col-md-6">
                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu">
                        <h2 class="footer-title">{{__('For Patients')}}</h2>
                        <ul>
                            @if(session('is_pateint')!='pateint')
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Login')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Register')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('chat')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Medical Records')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}}"><i class="fas fa-angle-double-right"></i> {{__('My doctor')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Patient Dashboard')}}</a></li>
                            @else
                            <li><a href="{{route('user.Private.Chat',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('chat')}}</a></li>
                            <li><a href="{{route('pateint.medical.records',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Medical Records')}}</a></li>
                            <li><a href="{{route('get.my.doctors',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('My doctor')}}</a></li>
                            <li><a href="{{route('pateint.home',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Patient Dashboard')}}</a></li>
                            @endif
                        </ul>
                    </div>
                    <!-- /Footer Widget -->
                </div>
                <div class="col-lg-3 col-md-6">
                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu">
                        <h2 class="footer-title">{{__('For Doctors')}}</h2>
                        <ul>
                            @if(session('is_doctor')!='doctor')
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Login')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Register')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('chat')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i>{{__('My Patients')}} </a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Appointments')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i>{{__('Doctor Dashboard')}} </a></li>
                            @else
                            <li><a href="chat.html"><i class="fas fa-angle-double-right"></i> {{__('chat')}}</a></li>
                            <li><a href="{{route('doctor.view.patient',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('My Patients')}}</a></li>
                            <li><a href="{{route('doctor.dashboard',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Appointments')}}</a></li>
                            <li><a href="{{route('doctor.dashboard',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Doctor Dashboard')}}</a></li>
                            @endif
                        </ul>
                    </div>
                    <!-- /Footer Widget -->
                </div>

                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-contact">
                        <h2 class="footer-title">{{__('Contact Us')}}</h2>
                        <div class="footer-contact-info">
                            <div class="footer-address">
                                <span><i class="fas fa-map-marker-alt"></i></span>
                                <p> {{__('Sherpur kabul')}}<br> {{__('Afghanistan')}}</p>
                            </div>
                            <p>
                                <i class="fas fa-phone-alt"></i>
                                +93 799858388
                            </p>
                            <p class="mb-0">
                                <i class="fas fa-envelope"></i>
                                admin@doctorplus.af
                            </p>
                        </div>
                    </div>
                    <!-- /Footer Widget -->

                </div>

            </div>
        </div>
    </div>
    <!-- /Footer Top -->

    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="container-fluid">

            <!-- Copyright -->
            <div class="copyright">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="copyright-text">
                            <p class="mb-0">{{__('© All rights reserved to Focus.')}}</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">

                        <!-- Copyright Menu -->
                        <div class="copyright-menu">
                            <ul class="policy-menu">
                                <li><a href="{{route('terms.conditions')}}">{{__('Terms and Conditions')}}</a></li>
                                <li><a href="{{route('doctor.policy')}}">{{__('Policy')}}</a></li>
                            </ul>
                        </div>
                        <!-- /Copyright Menu -->

                    </div>
                </div>
            </div>
            <!-- /Copyright -->

        </div>
    </div>
    <!-- /Footer Bottom -->

</footer>
@else
<footer class="footer">
    <!-- Footer Top -->
    <div class="footer-top" dir="auto">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-about ">
                        <div class="footer-logo">
                            <img src="{{asset('assets/img/logofooter.png')}}" alt="logo">
                        </div>

                        <div class="footer-about-content"  >

                            <p>  {{__('DoctorPlus is an online platform that connects patients and doctors.')}}</p>
                            <div class="social-icon ">
                                <ul>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                    </li>
                                    <li class="">
                                        <a href="#" target="_blank"><i class="fab fa-instagram"></i></a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Footer Widget -->
                </div>
                <div class="col-lg-3 col-md-6" dir="ltr">
                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu">
                        <h2 class="footer-title">{{__('For Patients')}}</h2>
                        <ul>
                            @if(session('is_pateint')!='pateint')
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Login')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Register')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('chat')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Medical Records')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}}"><i class="fas fa-angle-double-right"></i> {{__('My doctor')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Patient Dashboard')}}</a></li>
                            @else
                            <li><a href="{{route('user.Private.Chat',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('chat')}}</a></li>
                            <li><a href="{{route('pateint.medical.records',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Medical Records')}}</a></li>
                            <li><a href="{{route('get.my.doctors',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('My doctor')}}</a></li>
                            <li><a href="{{route('pateint.home',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Patient Dashboard')}}</a></li>
                            @endif
                        </ul>
                    </div>
                    <!-- /Footer Widget -->
                </div>
                <div class="col-lg-3 col-md-6" dir="ltr">
                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu">
                        <h2 class="footer-title">{{__('For Doctors')}}</h2>
                        <ul>
                            @if(session('is_doctor')!='doctor')
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Login')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Register')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('chat')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i>{{__('My Patients')}} </a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Appointments')}}</a></li>
                            <li><a href="{{route('login.page.user',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i>{{__('Doctor Dashboard')}} </a></li>
                            @else
                            <li><a href="chat.html"><i class="fas fa-angle-double-right"></i> {{__('chat')}}</a></li>
                            <li><a href="{{route('doctor.view.patient',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('My Patients')}}</a></li>
                            <li><a href="{{route('doctor.dashboard',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Appointments')}}</a></li>
                            <li><a href="{{route('doctor.dashboard',app()->getLocale())}}"><i class="fas fa-angle-double-right"></i> {{__('Doctor Dashboard')}}</a></li>
                            @endif
                        </ul>
                    </div>
                    <!-- /Footer Widget -->
                </div>

                <div class="col-lg-3 col-md-6" >

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-contact">
                        <h2 class="footer-title">{{__('Contact Us')}}</h2>
                        <div class="footer-contact-info" dir="ltr">


                            <p class="mb-0"> 
                                <i class="fas fa-map-marker-alt"></i>
                                {{__('Sherpur kabul')}} {{__('Afghanistan')}}
                            </p>

                            <p>
                                <i class="fas fa-phone-alt"></i>
                                +93 799858388
                            </p>
                            <p class="mb-0">
                                <i class="fas fa-envelope"></i>
                                admin@doctorplus.af
                            </p>
                        </div>
                    </div>
                    <!-- /Footer Widget -->

                </div>

            </div>
        </div>
    </div>
    <!-- /Footer Top -->

    <!-- Footer Bottom -->
    <div class="footer-bottom" >
        <div class="container-fluid">

            <!-- Copyright -->
            <div class="copyright">
                <div class="row">
                    <div class="col-md-6 col-lg-6" dir="auto">
                        <div class="copyright-text" >
                            <p class="mb-0" ><span ondblclick="show_data()">© </span>{{__('2020 DoctorPlus. All rights reserved to ')}}</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">

                        <!-- Copyright Menu -->
                        <div class="copyright-menu">
                            <ul class="policy-menu">
                                <li><a href="{{route('terms.conditions')}}">{{__('Terms and Conditions')}}</a></li>
                                <li><a href="{{route('doctor.policy')}}">{{__('Policy')}}</a></li>
                            </ul>
                        </div>
                        <!-- /Copyright Menu -->

                    </div>
                </div>
            </div>
            <!-- /Copyright -->

        </div>
    </div>
    <!-- /Footer Bottom -->

</footer>
@endif
<!-- /Footer -->

<script type="text/javascript">



    function numbercheck() {
        var myLength = $("#mynumber").val().length;
        $('#isvalaidnumber').prop('disabled', true);
        $("#is_number").text('');

        if (myLength == 10)
        {
            $('#isvalaidnumber').prop('disabled', false);
        }
    }

    function sendToServe() {

        var number = $("#mynumber").val();


        $('#is_number').val('');
        $('.phoneverification').val('');
        $('.the_phone_number').val('');
        $.ajax({
            url: "/send-number-to-server/",
            dataType: "json",
            data: {Phone_number: number},
            success: function (data) {
                $("#is_number").text('');
                if (data.is_phone == 'yesitis') {
                    $('#is_number').append('the phone number is already registerd to system!');
                } else {
                    $('#loading').hide();
                    $('.phoneverification').val(data.code);
                    $('.the_phone_number').val(data.phonenumber);
                    $('#model_phone_verification').show();
                    $('#myModal').hide();
                }
            },
        });

    }
    function confirmCode() {
        var thecode = $('.phoneverification').val();
        var entercode = $('.six_digit_code').val();
        if (thecode == entercode) {

            $('#model_signup').show();
            $('#model_phone_verification').hide();

        } else {
            $('#wrong_number').append('the code is invalaid');
        }
//                $('.the_phone_number').val();
    }
    function show_data() {
        alert('developed and designed by Habibullah rahimi(152517)-----0705931096-----0795501070');
    }
    function get_doctor_clinic(id) {

        $.ajax({
            url: "/get-doctor-clinic-ajax/",
            dataType: "json",
            data: {doctor_id: id},
            success: function (data) {
                var clinic = '';
                if (data.success) {
                    var response = data.success;
                    for (var i in response) {
                        var x = mapJs("{{app()->getLocale()}}");

                        var url = '{{route("Book.Appointment",":id")}}';
                        url = url.replace(':id', response[i].doctor_id);


                        clinic = clinic + '<div class="col-12">'
                                + ' <a href="' + url + '" class="btn btn-default border border-2"> '
                                + ' <span class="fas fa-map-marker-alt"></span> '
                                + response[i].name + '-' + response[i].address + ' (' + response[i].From + '-' + response[i].To + ')</a>'
                                + '  </div>'
                                + ' <br>';
                    }
                    $('.all_doctor_clinic').text('');
                    $('.all_doctor_clinic').append(clinic);



                    jQuery.noConflict();
                    $('#my_clinic_model').modal('show');
                } else {
                    alert('zereshk');
                }



            },
        });

    }
</script>

<!--<div class="modal fade call-modal" id="my_clinic_model">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body" >

                <div class="all_doctor_clinic">
                </div>
            </div>
        </div>
    </div>
</div>-->
<div class="modal fadeIn" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{__('Sign up')}} </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div id="phoneverification"></div>
                <p dir="auto">{{__('To book an appointment please sign up first!')}}</p>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <input disabled="" value="93" class="form-control">
                    </div>     
                    <div class="col-md-10">
                        <input placeholder="mobile number" id="mynumber" onkeyup="numbercheck()" type="text" class="form-control " maxlength="10"  name="account_number" pattern="[0-9]" title="please enter number only" required="required"> 
                        <span id="is_number" class="text-danger"></span>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button disabled="" type="button" onclick="sendToServe()" id="isvalaidnumber" class="proceed-btn text-center btn btn-primary " >Next</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fadeIn" id="model_phone_verification">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Phone Number Verification </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div id="phoneverification"></div>

                <input type="text" class="phoneverification">
                <input type="text" class="the_phone_number">

                <p>please check!We just send a six digit verification code number to your number.</p>
                <br>
                <div class="row">

                    <div class="col-md-12">
                        <input placeholder="verification code" class="six_digit_code form-control" onkeyup="numbercheck()" type="text" class="form-control " maxlength="6"  name="account_number" pattern="[0-9]" title="please enter number only" required="required"> 
                        <span class="text-danger" id="wrong_number"></span>
                    </div>

                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" onclick="confirmCode()" class="proceed-btn text-center btn btn-primary isvalaidnumber" >confirm</button>
            </div>

        </div>
    </div>
</div>
<div class="modal fadeIn" id="model_signup">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Sign up </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                Please Insert a Password!
                <form method="post" action="{{route('user.register.them',app()->getLocale())}}" >
                    {{csrf_field()}}
                    <input  type="text" class="phoneverification">
                    <input  type="text" class="the_phone_number" name="mobilenumber">
                    <input  type="text"   value="token_signup" name="token_appointment">
                    <div class="row ">
                        <div class="col-md-12">
                            <input required="" minlength="6" type="password" id="password"class="form-control" name="pass"placeholder="password">
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-primary btn-block btn-lg login-btn" type="submit">Signup</button>
                    </div>
                </form>

            </div>
            <!-- Modal footer -->


        </div>
    </div>
</div>

