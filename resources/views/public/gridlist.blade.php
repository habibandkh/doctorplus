<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>DoctorPlus</title>
        <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">

            @include('public.header')
            <body style="transform: none;">

                <!-- Main Wrapper -->
                <div class="main-wrapper" style="transform: none;">
                    <!-- Breadcrumb -->
                    <div class="breadcrumb-bar">
                        <div class="container-fluid">
                            <div class="row align-items-center">
                                <div class="col-md-8 col-12">
                                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Search</li>
                                        </ol>
                                    </nav>
                                    <h2 class="breadcrumb-title">Showing {{ $doctors->count() }}</strong> of {{ $doctors->total() }} results</h2>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- /Breadcrumb -->

                    <!-- Page Content -->
                    <div class="content" style="transform: none; min-height: 262px;">
                        <div class="container-fluid" style="transform: none;">

                            <div class="row" style="transform: none;">
                                <div class="col-md-12 col-lg-4 col-xl-3 theiaStickySidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                                    <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; top: 0px; left: 30px;"><div class="card search-filter">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Search Filter</h4>
                                            </div>
                                            <form method="post" action="{{route('use.filter.doctor')}}">
                                                @csrf


                                                <input type="hidden" value="{{ Session::get('spaciality_class_id') }}" name="special_id" checked="">
                                                <input type="hidden" value="{{ Session::get('District_name') }}" name="District_name" checked="">
                                                <div class="card-body">
                                                    <div class="filter-widget">
                                                        <h4>Gender {{ Cookie::get('name')}}</h4>
                                                        <div>
                                                            <label class="custom_check">
                                                                <input type="radio" value="both" name="gender" checked="">
                                                                <span class="checkmark"></span> Both
                                                            </label>
                                                        </div>
                                                        <div>
                                                            <label class="custom_check">
                                                                <input type="radio"value="female" name="gender">
                                                                <span class="checkmark"></span> Female Doctor
                                                            </label>
                                                        </div>
                                                        <div>
                                                            <label class="custom_check">
                                                                <input type="radio" value="male" name="gender" >
                                                                <span class="checkmark"></span> Male Doctor
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="filter-widget"                                                                                                     >
                                                        <h4>Select Availablity</h4>
                                                        <div>
                                                            <label class="custom_check">
                                                                <input type="radio" value="all"name="available" checked="">
                                                                <span class="checkmark"></span>All
                                                            </label>
                                                        </div>
                                                        <div>
                                                            <label class="custom_check">
                                                                <input type="radio" value="today"name="available">
                                                                <span class="checkmark"></span>Available Today
                                                            </label>
                                                        </div>
                                                        <div>
                                                            <label class="custom_check">
                                                                <input type="radio" value="next_days"name="available">
                                                                <span class="checkmark"></span>Available next 3 days
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="btn-search">
                                                        <button type="submit" class="btn btn-block">Search</button>
                                                    </div>	
                                                </div>
                                            </form>
                                        </div>
                                        <div class="resize-sensor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; z-index: -1; visibility: hidden;"><div class="resize-sensor-expand" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 378px; height: 1788px;"></div></div><div class="resize-sensor-shrink" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%"></div></div></div></div></div>

                                <div class="col-md-12 col-lg-8 col-xl-9">
                                    @if(!$doctors->isEmpty())
                                    @foreach ($doctors as $doctor)
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="doctor-widget">
                                                <div class="doc-info-left">
                                                    <div class="doctor-img">
                                                        <a href="{{url('/view-doctor-profile',[$doctor->doctorID])}}">
                                                            <img src="{{asset('images/doctor/'.$doctor->doctor_picture)}}" class="img-fluid" alt="User Image">
                                                        </a>
                                                    </div>
                                                    <div class="doc-info-cont">
                                                        <h4 class="doc-name"><a href="{{url('/view-doctor-profile',[$doctor->doctorID])}}">Dr.{{$doctor->doctor_name}}</a></h4>


                                                        @foreach($doctor->speciality as $special)
                                                        <h5 class="doc-department">{{$special->specialityName}}</h5>

                                                        @endforeach

                                                        <div class="rating">
                                                            <div class="d-none">  {{$rating=number_format($doctor->stars,1)}} </div>
                                                            @foreach(range(1,5) as $i)
                                                            @if($rating >0)
                                                            @if($rating >0.5)
                                                            <i class="fas fa-star  filled"></i>
                                                            @else
                                                            <i class="fas fa-star-half filled" ></i>
                                                            @endif
                                                            @else
                                                            <i class="fas  fa-star"></i>
                                                            @endif
                                                            <?php $rating--; ?>
                                                            @endforeach
                                                            <span class="d-inline-block average-rating">({{$doctor->total_stars}})</span>
                                                        </div>
                                                        <ul class="available-info">
                                                            @foreach($doctor->Address as $add)
                                                            <li>
                                                                <i class="fas fa-map-marker-alt"></i> {{$add->name}}({{ date('H:i', strtotime($add->From))}} , {{ date('H:i', strtotime($add->To))}})
                                                            </li>
                                                            @endforeach
                                                            <li>
                                                                <i class="far fa-clock"></i> {{$doctor->available}}
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="doc-info-right">
                                                    <div class="clini-infos">
                                                        <ul>
                                                            <li><i class="far fa-user"></i> {{$doctor->count_patient}} Patients</li>
                                                            <li><i class="far fa-eye"></i>  {{$doctor->account_view}} View Account</li>

                                                            <li><i class="far fa-money-bill-alt"></i>AFN {{$doctor->doctor_fee}} </li>
                                                        </ul>
                                                    </div>
                                                    <div class="clinic-booking">
                                                        <a class="view-pro-btn" href="{{route('view.doctor.profile',['id'=>$doctor->doctorID,'language'=>app()->getLocale()])}}">View Profile</a>
                                                        @if(session('is_pateint')!='pateint')
                                                        <a class="apt-btn"  href="{{route('login.page.user')}}" >Book Appointment</a>
                                                        @else    
                                                        <a class="apt-btn"  href="{{route('Book.Appointment',[$doctor->doctorID])}}">Book Appointment</a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <p> No Doctor Found!</p>
                                    @endif
                                    <!-- Doctor Widget -->

                                    <div class="load-more text-center">
                                        <div class=" justify-content-center  pagination pagination-sm">{{$doctors->links()}}</div>
                                    </div>	
                                </div>
                            </div>

                        </div>

                    </div>		
                    <!-- /Page Content -->
                </div>
                <!-- /Main Wrapper -->
                <!-- Footer -->
                @include('public.footer')
                <!-- /Footer -->

        </div>
        <!-- /Main Wrapper -->

        <!-- jQuery -->

        <script src="{{asset("assets/js/jquery.min.js")}}"></script>
        <script src="{{asset("assets/js/popper.min.js")}}"></script>
        <script src="{{asset("assets/js/bootstrap.min.js")}}"></script>
        <script src="{{asset("assets/js/slick.js")}}"></script>
        <script src="{{asset("assets/js/script.js")}}"></script>
