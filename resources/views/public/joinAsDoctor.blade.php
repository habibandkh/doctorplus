<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>DoctorPlus</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fancybox/jquery.fancybox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
        <style type="text/css">    
             #scrollable-dropdown-menu .tt-menu {
                 max-height: 400px;
                max-width:500px;
                overflow-x: hidden;
                overflow-y: auto;
                position: fixed;
  }
            .twitter-typeahead { width: 100%; } 
            #scrollable-dropdown-menu input[type=text]{
                width:500px !important;
            }
             #scrollable-dropdown-menu_1 .tt-menu {
                max-height: 400px;
                max-width:240px;
                overflow-x: hidden;
                overflow-y: auto;
                position: fixed;
}
             #scrollable-dropdown-menu_1 input[type=text]{
                width:240px !important;
            }
             .fa { margin-right:5px;}
            .rating .fa {font-size: 22px;}
            .rating-num { margin-top:0px;font-size: 54px; }
            .progress { margin-bottom: 5px;}
            .progress-bar { text-align: left; }
            .rating-desc .col-md-3 {padding-right: 0px;}
            .sr-only { margin-left: 5px;overflow: visible;clip: auto; }
        </style>
    </head>
    <body>
        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            @include('public.header')
            <div class="content" style="min-height: 239.6px;background-color: #fff;" >
                <div class="container-fluid" style="padding-bottom: 50px">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <!-- Account Content -->
                            <div class="account-content">
                                <div class="row align-items-center justify-content-center">
                                    <div class="col-md-5 col-lg-5 login-left">
                                        <img src="assets/img/login-banner.png" class="img-fluid" alt="Login Banner">	
                                    </div>
                                    <div class="col-md-7  login-right" dir="auto">
                                        @if(session()->has('message'))
                                        <div class="alert alert-success  alert-block">
                                            <button type="button" class="close" data-dismiss="alert">×</button>    
                                            {{ session()->get('message') }}
                                        </div>
                                        @endif
                                        @if(session()->has('u-message'))
                                        <div class="alert alert-danger  alert-block">
                                            <button type="button" class="close" data-dismiss="alert">×</button>    
                                            {{ session()->get('u-message') }}
                                        </div>
                                        @endif
                                        <div class="login-header" dir="auto">
                                            <h3> {{__('Doctor Register')}}<a href="{{url('login-page')}}">{{__('Not a Doctor?')}}</a></h3>
                                        </div>

                                        <!-- Register Form -->
                                        <form method="post" action="{{route('doctor.join.as')}}" dir="auto" @if(Request::segment(1)=='en') @else class="text-right" @endif >
                                            {{csrf_field()}}
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label> {{__('Title')}}<span class="text-danger">*</span></label>
                                                        <select required=""  class="form-control" name="title">
                                                            <option value="">{{__('Select Title')}}</option>
                                                            <!--<option value="Doctor">Doctor</option>-->
                                                            <option value="Dr.">Dr.</option>
                                                            <option value="Prof.">Prof.</option>
                                                            <option value="Assist.Prof.">Assist.Prof.</option>
                                                            <option value="Assist.Prof.Dr.">Assist.Prof.Dr.</option>
                                                            <option value="Assoc.Prof.">Assoc.Prof.</option>
                                                            <!--<option value="Brig.Dr.">Brig.Dr.</option>-->
                                                            <!--<option value="Brig.(R)Dr.">Brig.(R)Dr.</option>-->
                                                            <!--<option value="Brig.(R)Prof.Dr.">Brig.(R)Prof.Dr.</option>-->
                                                            <!--<option value="Col.Dr.">Col.Dr.</option>-->
                                                            <!--<option value="Col.(R)Dr.">Col.(R)Dr.</option>-->
                                                            <!--<option value="Lft.Col.Dr.">Lft.Col.Dr.</option>-->
                                                            <!--<option value="Lft.(R)Col.Dr.">Lft.(R)Col.Dr.</option>-->
                                                            <!--<option value="Cap.Dr.">Cap.Dr.</option>-->
                                                            <!--<option value="Cap.(R)Dr.">Cap.(R)Dr.</option>-->
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6" >
                                                    <div class="form-group ">{{__('Name')}} <span class="text-danger">*</span></label>
                                                        <input  required="" name="name"type="text" class="form-control floating" placeholder="{{__('Name')}}">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label> {{__('City')}}<span class="text-danger">*</span></label>
                                                        <select required=""  class="form-control" name="city" >
                                                            <option value="" >{{__('Select City')}}</option>
                                                            <option value="kabul">kabul</option>
                                                            <option value="badakhshan">badakhshan</option>
                                                            <option value="badghiz">badghiz</option>
                                                            <option value="baghlan">baghlan</option>
                                                            <option value="balkh">balkh</option>
                                                            <option value="bamiyan">bamiyan</option>
                                                            <option value="daikundi">daikundi</option>
                                                            <option value="faryab">faryab</option>
                                                            <option value="ghazni">ghazni</option>
                                                            <option value="ghor">ghor</option>
                                                            <option value="helmand">helmand</option>
                                                            <option value="herat">herat</option>
                                                            <option value="jowzjan">jowzjan</option>
                                                            <option value="kandahar">kandahar</option>
                                                            <option value="kapisa">kapisa</option>
                                                            <option value="khost">khost</option>
                                                            <option value="kunar">kunar</option>
                                                            <option value="kunduz">kunduz</option>
                                                            <option value="laghman">laghman</option>
                                                            <option value="logar">logar</option>
                                                            <option value="maidan wardak">maidan wardak</option>
                                                            <option value="nangarhar">nangarhar</option>
                                                            <option value="paktia">paktia</option>
                                                            <option value="paktika">paktika</option>
                                                            <option value="panjshir">panjshir</option>
                                                            <option value="parwan">parwan</option>
                                                            <option value="samangaan">samangaan</option>
                                                            <option value="sar-e-pul">sar-e-pul</option>
                                                            <option value="takhar">takhar</option>
                                                            <option value="zabul">zabul</option>
                                                            <option value="nimroz">nimroz</option>
                                                            <option value="farah">farah</option>
                                                            <option value="nuristan">nuristan</option>
                                                            <option value="uruzgan">uruzgan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="gen-label">{{__('Gender')}} <span class="text-danger"> *</span></label><br>

                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input required="" type="radio" checked name="gender" value="male" class="form-check-input">{{__('Male')}} 
                                                            </label>
                                                        </div>

                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input required="" type="radio" name="gender" value="female" class="form-check-input">{{__('Female')}} 
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="focus-label">{{__('Email')}} <span class="text-danger">*</span></label>
                                                        <input  required="" name="email"type="email" class="form-control floating" placeholder="{{__('Email')}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="focus-label">{{__('Mobile')}} <span class="text-danger">*</span></label>
                                                        <input  required="" name="phone" maxlength="10" type="tel" class="form-control floating" placeholder="{{__('Mobile')}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="focus-label">{{__('Password')}} <span class="text-danger">*</span></label>
                                                        <input  required="" name="password"type="text" class="form-control floating" placeholder="{{__('Password')}}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="text-right">
                                                <a class="forgot-link" href="{{route('login.page.user',app()->getLocale())}}">{{__('Already have an account?')}}</a>
                                            </div>
                                            <button class="btn btn-primary btn-block btn-lg login-btn" type="submit">{{__('Sign up')}}</button>
                                            <!--                                            <div class="login-or">
                                                                                            <span class="or-line"></span>
                                                                                            <span class="span-or">or</span>
                                                                                        </div>-->
                                            <!--                                            <div class="row form-row social-login">
                                                                                            <div class="col-6">
                                                                                                <a href="#" class="btn btn-facebook btn-block"><i class="fab fa-facebook-f mr-1"></i> Login</a>
                                                                                            </div>
                                                                                            <div class="col-6">
                                                                                                <a href="#" class="btn btn-google btn-block"><i class="fab fa-google mr-1"></i> Login</a>
                                                                                            </div>
                                                                                        </div>-->
                                        </form>
                                        <!-- /Register Form -->

                                    </div>
                                </div>
                            </div>
                            <!-- /Account Content -->

                        </div>
                    </div>

                </div>

            </div>

            @include('public.footer')
            <script src="{{asset("assets/js/jquery.min.js")}}"></script>
            <script src="{{asset("assets/js/popper.min.js")}}"></script>
            <script src="{{asset("assets/js/bootstrap.min.js")}}"></script>
            <script src="{{asset("assets/plugins/fancybox/jquery.fancybox.min.js")}}"></script>
            <script src="{{asset("assets/js/slick.js")}}"></script>
            <script src="{{asset("assets/js/script.js")}}"></script>
    </body>
</html>

