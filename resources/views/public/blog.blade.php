<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
       <title>DoctorPlus</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fancybox/jquery.fancybox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <a href="blog.blade.php"></a>
        <style type="text/css">
            #scrollable-dropdown-menu .tt-menu {
                max-height: 400px;
                max-width:500px;
                overflow-x: hidden;
                overflow-y: auto;
                position: fixed;

            }
            .twitter-typeahead { width: 100%; } 
            #scrollable-dropdown-menu input[type=text]{
                width:500px !important;
            }


            #scrollable-dropdown-menu_1 .tt-menu {
                max-height: 400px;
                max-width:240px;
                overflow-x: hidden;
                overflow-y: auto;
                position: fixed;

            }
            #scrollable-dropdown-menu_1 input[type=text]{
                width:240px !important;
            }
            .fa { margin-right:5px;}
            .rating .fa {font-size: 22px;}
            .rating-num { margin-top:0px;font-size: 54px; }
            .progress { margin-bottom: 5px;}
            .progress-bar { text-align: left; }
            .rating-desc .col-md-3 {padding-right: 0px;}
            .sr-only { margin-left: 5px;overflow: visible;clip: auto; }
        </style>
    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
@include('public.header')

<div class="content" style="transform: none; min-height: 191.6px;">
    <div class="container" style="transform: none;">
        <div class="row @if(Request::segment(1)=='en')  @else  text-right @endif" @if(Request::segment(1)=='en')  @else  dir="rtl" @endif style="transform: none;">
            <div class="col-lg-8 col-md-12">
                <div class="row blog-grid-row">
                    @foreach($posts as $post)
                    <div class="col-md-6 col-sm-12">
                        <div class="blog grid-blog">
                            <div class="blog-image">
                                <a href="{{route('public.view.blog',[$post->post_id])}}"><img class="img-fluid" src="{{asset('images/blog/'.$post->post_img)}}" alt="Post Image"></a>
                            </div>
                            <div class="blog-content">
                                <ul class="entry-meta meta-item">
                                    <li class="far fa-clock">
                                        {{ Carbon\Carbon::parse($post->date)->format('d-M-Y ') }}
                                    </li>
                                </ul>
                                <h3 class="blog-title"><a href="{{route('public.view.blog',[$post->post_id])}}">{{$post->post_title}}</a></h3>
                                <p>{{ \Illuminate\Support\Str::words($post->post_body, 30, "...")}}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <nav>
                            <ul class="pagination justify-content-center">
                                {{$posts->links()}}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-12 sidebar-right theiaStickySidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; top: 0px; left: 964.6px;">
<!--                    <div class="card search-widget">
                        <div class="card-body">
                            <form class="search-form">
                                <div class="input-group">
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>-->
                    <div class="card post-widget">
                        <div class="card-header">
                            <h4 class="card-title">{{__('Latest Posts')}}</h4>
                        </div>
                        <div class="card-body">
                            <ul class="latest-posts">
                                @foreach($latest as $lat)
                                <li>
                                    <div class="post-thumb">
                                        <a href="{{route('public.view.blog',[$post->post_id])}}">
                                            <img class="img-fluid" src="{{asset('images/blog/'.$lat->post_img)}}" alt="">
                                        </a>
                                    </div>
                                    <div class="post-info">
                                        <h4>
                                            <a href="{{route('public.view.blog',[$post->post_id])}}">{{$lat->post_title}}</a>
                                        </h4>
                                        <p>{{$lat->date}}</p>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="card category-widget">
                        <div class="card-header">
                            <h4 class="card-title">{{__('Blog Categories')}}</h4>
                        </div>
                        <div class="card-body">
                            <ul class="categories">
                                @foreach($categories as $category)
                                <li><a href="{{route('public.post.view',['id'=>$category->cat_id])}}">{{$category->name}} <span>({{ $total = DB::table('post')->where('lang',session()->get('language'))->where('category_id',$category->cat_id)->groupBy('category_id')->count() }})</span></a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="resize-sensor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; z-index: -1; visibility: hidden;"><div class="resize-sensor-expand" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 330px; height: 2268px;"></div></div><div class="resize-sensor-shrink" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%"></div></div></div></div></div>
            <!-- /Blog Sidebar -->

        </div>
    </div>
</div>
@include('public.footer')