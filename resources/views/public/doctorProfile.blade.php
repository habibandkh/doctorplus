<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>DoctorPlus</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fancybox/jquery.fancybox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">

            @include('public.header')

            <!-- Breadcrumb -->
            <div class="breadcrumb-bar" dir="auto">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-md-12 col-12">
                            <nav aria-label="breadcrumb" class="page-breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">{{__('Home')}}</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">{{__('Doctor Profile')}}</li>
                                </ol>
                            </nav>
                            <h2 class="breadcrumb-title">{{__('Doctor Profile')}}</h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Breadcrumb -->
            <!-- Page Content -->
            <div class="content" style="min-height: 152.6px;">
                <!-- /Doctor Widget -->

                <div class="container">
                    @if(session()->has('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session()->get('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if(session()->has('u-message'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        {{ session()->get('u-message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @foreach($doctors as $doctor)
                    <!-- Doctor Widget -->
                    <div class="card">

                        <div class="card-body">
                            <div class="doctor-widget">
                                <div class="doc-info-left">
                                    <div class="doctor-img">
                                        <img src="{{asset('images/doctor/'.$doctor->doctor_picture)}}" class="img-fluid" alt="User Image">
                                    </div>
                                    <div class="doc-info-cont">
                                        <h4 class="doc-name">{{$doctor->title}} {{$doctor->doctor_name}}</h4>
                                        @foreach($doctor->speciality as $special)
                                        <p class="doc-department"> {{$special->specialityName}}</p>

                                        @endforeach

                                        <div class="rating">
                                            <div class="d-none">  {{$rating=number_format($doctor->stars,1)}} </div>
                                            @foreach(range(1,5) as $i)
                                            @if($rating >0)
                                            @if($rating >0.5)
                                            <i class="fas fa-star  filled"></i>
                                            @else
                                            <i class="fas fa-star-half filled" ></i>
                                            @endif
                                            @else
                                            <i class="fas  fa-star"></i>
                                            @endif
                                            <?php $rating--; ?>
                                            @endforeach
                                            <span class="d-inline-block average-rating">({{$doctor->total_stars}})</span>
                                        </div>

                                        <div class="clinic-details">
                                            @foreach($doctor->Address as $add)
                                            <p class="doc-location"><i class="fas fa-map-marker-alt"></i> {{$add->name}}({{ date('H:i', strtotime($add->From))}} , {{ date('H:i', strtotime($add->To))}})
                                                <!--                                            <a href="javascript:void(0);">Get Directions</a>-->
                                            </p>
                                            @endforeach


                                        </div>

                                    </div>
                                </div>

                                <div class="doc-info-right">
                                    <div class="clini-infos">
                                        <ul>
                                            <li><i class="far fa-user"></i> {{$doctor->count_patient}} {{__('Patients')}}</li>
                                            <li><i class="far fa-eye"></i>  {{$doctor->account_view}} {{__('View Account')}}</li>

                                            <li><i class="far fa-money-bill-alt"></i>{{__('AFN')}} {{$doctor->doctor_fee}} </li>
                                        </ul>
                                    </div>
                                    <div class="doctor-action">
                                        @if(session('is_pateint')=='pateint')
                                        @if($doctor->doctor_bookmark == null)
                                        <a href="{{route('bookmark.doctor',[$doctor->doctorID])}}" class="btn btn-white fav-btn">
                                            <i class="far fa-bookmark"></i>
                                        </a>
                                        @else

                                        <a href="{{route('cancel.bookmark.doctor',[$doctor->doctorID])}}" class="btn btn-white fav-btn bg-danger">
                                            <i class="far fa-bookmark"></i>
                                        </a>

                                        @endif
                                        @endif
                                        <!--                                        <a href="chat.html" class="btn btn-white msg-btn">
                                                                                    <i class="far fa-comment-alt"></i>
                                                                                </a>
                                                                                <a href="javascript:void(0)" class="btn btn-white call-btn" data-toggle="modal" data-target="#voice_call">
                                                                                    <i class="fas fa-phone"></i>
                                                                                </a>
                                                                                <a href="javascript:void(0)" class="btn btn-white call-btn" data-toggle="modal" data-target="#video_call">
                                                                                    <i class="fas fa-video"></i>
                                                                                </a>-->
                                    </div>
                                    <div class="clinic-booking">
                                        @if(session('is_pateint')!='pateint')
                                        <a class="apt-btn"  href="{{route('login.page.user')}}" >{{__('Book Appointment')}}</a>
                                        @else    
                                        <a class="apt-btn" href="{{route('Book.Appointment',[$doctor->doctorID])}}" >{{__('Book Appointment')}}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Doctor Widget -->

                    <!-- Doctor Details Tab -->
                    <div class="card">
                        <div class="card-body pt-0">

                            <!-- Tab Menu -->
                            <nav class="user-tabs mb-4">
                                <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#doc_overview" data-toggle="tab">{{__('Overview')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#doc_locations" data-toggle="tab">{{__('Locations')}}</a>
                                    </li>
                                    <!--                                <li class="nav-item">
                                     <a class="nav-link" href="#doc_reviews" data-toggle="tab">Reviews</a>
                                     </li>
                                     <li class="nav-item">
                   <a class="nav-link" href="#doc_business_hours" data-toggle="tab">Business Hours</a>
               </li>-->
                                </ul>
                            </nav>
                            <!-- /Tab Menu -->

                            <!-- Tab Content -->
                            <div class="tab-content pt-0">

                                <!-- Overview Content -->
                                <div role="tabpanel" id="doc_overview" class="tab-pane fade show active">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-9">

                                            <!-- About Details -->
                                            <div class="widget about-widget">
                                                <h4 class="widget-title">{{__('About Me')}}</h4>
                                                <p>{{$doctor->doctor_Short_Biography}}</p>
                                            </div>
                                            <!-- /About Details -->

                                            <!-- Education Details -->
                                            <div class="widget education-widget">
                                                <h4 class="widget-title">{{__('Education')}}</h4>
                                                <div class="experience-box">
                                                    <ul class="experience-list">
                                                        @foreach($doctor->doctor_education as $edu)
                                                        <li>
                                                            <div class="experience-user">
                                                                <div class="before-circle"></div>
                                                            </div>
                                                            <div class="experience-content">
                                                                <div class="timeline-content">
                                                                    <a href="#/" class="name">{{$edu->school_name}}</a>
                                                                    <div>{{$edu->title_of_study}}</div>
                                                                    <span class="time">{{$edu->start_Date }} - {{$edu->end_Date}}</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- /Education Details -->

                                            <!-- Experience Details -->
                                            <div class="widget experience-widget">
                                                <h4 class="widget-title">{{__('Work')}} &amp; {{__('Experience')}}</h4>
                                                <div class="experience-box">
                                                    <ul class="experience-list">

                                                        @foreach($doctor->doctor_experience as $exp)
                                                        <li>
                                                            <div class="experience-user">
                                                                <div class="before-circle"></div>
                                                            </div>
                                                            <div class="experience-content">
                                                                <div class="timeline-content">
                                                                    <a href="#/" class="name">{{$exp->experience_name}}</a>

                                                                    <span class="time">{{$exp->start }} - {{$exp->end}}</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        @endforeach

                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- /Experience Details -->

                                            <!-- Awards Details -->

                                            <!-- /Awards Details -->
                                            <!-- Specializations List -->
                                            <div class="service-list">
                                                <h4>{{__('Specializations')}}</h4>
                                                <ul class="clearfix">
                                                    @foreach($doctor->speciality_all as $spe)
                                                    <li>{{$spe->specialityName}}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <!-- /Specializations List                                                                                                                                                                                                         -->
                                            <!-- Services List -->
                                            <div class="ser                                                                                                                                                                                                        vice-list">
                                                <h4>{{__('Services')}}</h4>
                                                <ul class="clearfix">                                                                                                                                                                                                        
                                                    @foreach($doctor->my_services as $service)
                                                    <li>{{$service->service_name}}</li>
                                                    @endforeach

                                                </ul>
                                            </div>
                                            <!-- /Services List -->
                                            <div class="service-list">
                                                <h4>{{__('Conditions')}}</h4>
                                                <ul class="clearfix">
                                                    @foreach($doctor->my_Condition as $Condition)
                                                    <li>{{$Condition->name}}</li>
                                                    @endforeach

                                                </ul>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <!-- /Overview Content -->

                                <!-- Locations Content -->
                                <div role="tabpanel" id="doc_locations" class="tab-pane fade">

                                    <!-- Location List -->
                                    @foreach($doctor->Address as $clinic)
                                    <div class="location-list">
                                        <div class="row">
                                            <!-- Clinic Content -->
                                            <div class="col-md-6">
                                                <div class="clinic-content">
                                                    <h4 class="clinic-name"><a href="#"></a>{{$clinic->name}}</h4>
                                                    <p class="doc-speciality">{{$clinic->address}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="clinic-timing">
                                                    <div>
                                                        <p class="timings-times">
                                                            <span>{{ date('h:i A', strtotime($add->From))}} , {{ date('h:i A', strtotime($add->To))}}</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Clinic Timing -->
                                            <div class="col-md-2">
                                                <div cl                                                                                                                                                                                                        ass="consult-price">
                                                    AFN {{$doctor->doctor_fee}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Location List -->
                                    @endforeach
                                </div>
                                <!-- /Locations Content                                                                                                                                                                                                        -->

                                <!-- Reviews Content -->
                                <div role="tabpanel" id="doc_reviews" class="tab-pane fade">

                                    <!-- Review Listing -->
                                    <div class="widget review-listing">
                                        <div class="row">
                                            <div class="col-md-4 card">
                                                <div class="rating text-center " style="padding-top: 50px;">
                                                    <div class="d-none">  {{$rating=number_format($doctor->stars,1)}} </div>
                                                    @foreach(range(1,5) as $i)
                                                    @if($rating >0)
                                                    @if($rating >0.5)
                                                    <i class="fas fa-star  filled"></i>
                                                    @else
                                                    <i class="fas fa-star-half filled" ></i>
                                                    @endif
                                                    @else
                                                    <i class="fas  fa-star"></i>
                                                    @endif
                                                    <?php $rating--; ?>
                                                    @endforeach
                                                    <span class="d-inline-block average-rating"></span>
                                                </div>
                                                <p class="text-center"><b>{{number_format($doctor->stars,1)}}</b> average based on <b>{{$doctor->total_stars}}</b> reviews.</p>
                                            </div> 
                                            <div class="col-md-8">
                                                <!-- five star  -->
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <span class="fas fa-star  filled"></span>
                                                        <span class="fas fa-star  filled"></span>
                                                        <span class="fas fa-star  filled"></span>
                                                        <span class="fas fa-star  filled"></span>
                                                        <span class="fas fa-star  filled"></span>
                                                    </div>                   
                                                    <div class="col-md-9">
                                                        <div class="progress" style="height: 20px;">
                                                            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                                                        </div>
                                                    </div> 
                                                </div> 
                                                <br>
                                                <!-- four star -->
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <span class="fas fa-star  filled"></span>
                                                        <span class="fas fa-star  filled"></span>
                                                        <span class="fas fa-star  filled"></span>
                                                        <span class="fas fa-star  filled"></span>

                                                    </div>                   
                                                    <div class="col-md-9">
                                                        <div class="progress" style="height: 20px;">
                                                            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                                                        </div>
                                                    </div> 
                                                </div> 
                                                <br>
                                                <!-- three star  -->
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <span class="fas fa-star  filled"></span>
                                                        <span class="fas fa-star  filled"></span>
                                                        <span class="fas fa-star  filled"></span>

                                                    </div>                   
                                                    <div class="col-md-9">
                                                        <div class="progress" style="height: 20px;">
                                                            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valu                                                                                                                                                                                                        emax="100">25%</div>
                                                        </div>
                                                    </div> 
                                                </div> 
                                                <br>
                                                <!-- two star -->
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <span class="fas fa-star  filled"></span>
                                                        <span class="fas fa-star  filled"></span>

                                                    </div>                   
                                                    <div class="col-md-9">
                                                        <div class="progress" style="height: 20px;">
                                                            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valu                                                                                                                                                                                                        emax="100">25%</div>
                                                        </div>
                                                    </div> 
                                                </div> 
                                                <br>
                                                <!-- one star -->
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <span class="fas fa-star  filled"></span>

                                                    </div>                   
                                                    <div class="col-md-9">
                                                        <div class="progress" style                                                                                                                                                                                                        ="height: 20px;">
                                                            <div class="progress-bar" role="progressbar"                                                                                                                                                                                                        style="width: 25%;" aria-valuenow="25" aria-valuemin=                                                                                                                                                                                                        "0" aria-valuemax="100">25%</div>
                                                        </div>
                                                    </div> 
                                                </div> 
                                                <br>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /Reviews Content -->

                                <!-- Business Hours Content -->
                                <div role="tabpanel" id="doc_business_hours" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-md-6 offset-md-3">

                                            <!-- Business Hours Widget -->
                                            <div class="widget business-widget">
                                                <div class="widget-content">
                                                    <div class="listing-hours">
                                                        <div class="listing-day current">
                                                            <div class="day">Today <span>5 Nov 2019</span></div>
                                                            <div class="time-items">
                                                                <span class="open-status"><span class="badge bg-success-light">Open Now</span></span>
                                                                <span class="time">07:00 AM - 09:00 PM</span>
                                                            </div>
                                                        </div>
                                                        <div class="listing-day">
                                                            <div class="day">Monday</div>
                                                            <div class="time-items">
                                                                <span class="time">07:00 AM - 09:00 PM</span>
                                                            </div>
                                                        </div>
                                                        <div class="listing-day">
                                                            <div class="day">Tuesday</div>
                                                            <div class="time-items">
                                                                <span class="time">07:00 AM - 09:00 PM</span>
                                                            </div>
                                                        </div>
                                                        <div class="listing-day">
                                                            <div class="day">Wednesday</div>
                                                            <div class="time-items">
                                                                <span class="time">07:00 AM - 09:00 PM</span>
                                                            </div>
                                                        </div>
                                                        <div class="listing-day">
                                                            <div class="day">Thursday</div>
                                                            <div class="time-items">
                                                                <span class="time">07:00 AM - 09:00 PM</span>
                                                            </div>
                                                        </div>
                                                        <div class="listing-day">
                                                            <div class="day">Friday</div>
                                                            <div class="time-items">
                                                                <span class="time">07:00 AM - 09:00 PM</span>
                                                            </div>
                                                        </div>
                                                        <div class="listing-day">
                                                            <div class="day">Saturday</div>
                                                            <div class="time-items">
                                                                <span class="time">07:00 AM - 09:00 PM</span>
                                                            </div>
                                                        </div>
                                                        <div class="listing-day closed">
                                                            <div class="day">Sunday</div>
                                                            <div class="time-items">
                                                                <span class="time"><span class="badge bg-danger-light">Closed</span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </d                                                                                                                                                                                                        iv>
                                                <!-- /Business Hours Widget -->

                                            </div>
                                        </div>
                                        </                                                                                                                                                                                                        div>
                                        <!-- /Business Hours Cont                                                                                                                                                                                                        ent -->

                                    </div>
                                </div>
                            </div>
                            <!-- /Doctor Details Tab -->
                            @endforeach
                        </div>
                    </div>		
                    <!-- /Page Content -->


                  


                </div>
                 @include('public.footer')
         
            <!-- Video Call Modal -->
            <script src="{{asset("assets/js/jquery.min.js")}}"></script>
            <script src="{{asset("assets/js/popper.min.js")}}"></script>
            <script src="{{asset("assets/js/bootstrap.min.js")}}"></script>
            <script src="{{asset("assets/plugins/fancybox/jquery.fancybox.min.js")}}"></script>
            <script src="{{asset("assets/js/script.js")}}"></script>
            <div class="sidebar-overlay"></div>


    </body>
</html>



