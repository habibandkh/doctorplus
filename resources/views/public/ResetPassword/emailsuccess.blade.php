<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>DoctorPlus</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fancybox/jquery.fancybox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

        <style type="text/css">
            #scrollable-dropdown-menu .tt-menu {
                max-height: 400px;
                max-width:500px;
                overflow-x: hidden;
                overflow-y: auto;
                position: fixed;
            }
            .twitter-typeahead { width: 100%; } 
            #scrollable-dropdown-menu input[type=text]{
                width:500px !important;
            }
            #scrollable-dropdown-menu_1 .tt-menu {
                max-height: 400px;
                max-width:240px;
                overflow-x: hidden;
                overflow-y: auto;
                position: fixed;
            }
            #scrollable-dropdown-menu_1 input[type=text]{
                width:240px !important;
            }
            .fa { margin-right:5px;}
            .rating .fa {font-size: 22px;}
            .rating-num { margin-top:0px;font-size: 54px; }
            .progress { margin-bottom: 5px;}
            .progress-bar { text-align: left; }
            .rating-desc .col-md-3 {padding-right: 0px;}
            .sr-only { margin-left: 5px;overflow: visible;clip: auto; }


            .elementor-77 .elementor-element.elementor-element-bd1b47c:not(.elementor-motion-effects-element-type-background), .elementor-77 .elementor-element.elementor-element-bd1b47c > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-image:url("http://amentotech.com/projects/doctreat/wp-content/uploads/2019/08/banner-img.png");}.elementor-77 .elementor-element.elementor-element-bd1b47c{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:-100px;margin-bottom:0px;padding:0px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-bd1b47c > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-77 .elementor-element.elementor-element-4d916c7{margin-top:-95px;margin-bottom:0px;padding:0px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-5b69dca{margin-top:-94px;margin-bottom:20px;padding:0px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-2236ed5:not(.elementor-motion-effects-element-type-background), .elementor-77 .elementor-element.elementor-element-2236ed5 > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-color:#e8f6ff;}.elementor-77 .elementor-element.elementor-element-2236ed5{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-77 .elementor-element.elementor-element-2236ed5 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-77 .elementor-element.elementor-element-aaffff7{padding:80px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-04d6e97{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-77 .elementor-element.elementor-element-04d6e97 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-77 .elementor-element.elementor-element-59d3be7{padding:0px 0px 100px 0px;}.elementor-77 .elementor-element.elementor-element-c695ad2:not(.elementor-motion-effects-element-type-background), .elementor-77 .elementor-element.elementor-element-c695ad2 > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-color:#e8f6ff;}.elementor-77 .elementor-element.elementor-element-c695ad2{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:0px;margin-bottom:-100px;padding:60px 0px 100px 0px;}.elementor-77 .elementor-element.elementor-element-c695ad2 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}
            .elementor-77 .elementor-element.elementor-element-bd1b47c:not(.elementor-motion-effects-element-type-background), .elementor-77 .elementor-element.elementor-element-bd1b47c > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-image:url("http://amentotech.com/projects/doctreat/wp-content/uploads/2019/08/banner-img.png");}.elementor-77 .elementor-element.elementor-element-bd1b47c{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:-100px;margin-bottom:0px;padding:0px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-bd1b47c > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-77 .elementor-element.elementor-element-4d916c7{margin-top:-95px;margin-bottom:0px;padding:0px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-5b69dca{margin-top:-94px;margin-bottom:20px;padding:0px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-2236ed5:not(.elementor-motion-effects-element-type-background), .elementor-77 .elementor-element.elementor-element-2236ed5 > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-color:#e8f6ff;}.elementor-77 .elementor-element.elementor-element-2236ed5{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-77 .elementor-element.elementor-element-2236ed5 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-77 .elementor-element.elementor-element-aaffff7{padding:80px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-04d6e97{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-77 .elementor-element.elementor-element-04d6e97 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-77 .elementor-element.elementor-element-59d3be7{padding:0px 0px 100px 0px;}.elementor-77 .elementor-element.elementor-element-c695ad2:not(.elementor-motion-effects-element-type-background), .elementor-77 .elementor-element.elementor-element-c695ad2 > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-color:#e8f6ff;}.elementor-77 .elementor-element.elementor-element-c695ad2{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:0px;margin-bottom:-100px;padding:60px 0px 100px 0px;}.elementor-77 .elementor-element.elementor-element-c695ad2 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}
        </style>
    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">

            @include('public.header')
            <div class="content" style="min-height: 239.6px;background-color: #fff;" >
                <div class="container-fluid" style="padding-bottom: 50px">
                    <br>
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <!-- Success Card -->
                            <div class="card success-card">
                                <div class="card-body">
                                    <div class="card-title">now you can change your password</div>
                                      @if(Session::has('message'))
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                          {{ Session::get('message') }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                                        @if(Session::has('success'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                          {{ Session::get('success') }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                                    <form method="post" action="{{route('doctor.password.updatereset')}}">
                                        {{csrf_field()}}
                                        
                                       <div class="form-group ">
                                           <input  value="{{$segment = Request::segment(3)}}" class="form-control floating" name="doctorid" type="hidden"    required="required">
                                        </div>
                                        <div class="form-group ">
                                            <input  placeholder="new password" min="6"class="form-control floating" name="pass" type="password"    required="required">
                                        </div>
                                        <div class="form-group ">
                                            <input placeholder="confirm new passwordi" min="6" name="conf_pass" type="password" class="form-control floating"  required="required">
                                        </div>
                                       
                                        <button class="btn btn-primary btn-block btn-lg login-btn" type="submit" >{{__('change password')}}</button>
                                       
                                    </form>
                                </div>
                            </div>
                            <!-- /Success Card -->
                        </div>
                    </div>


                </div>

            </div>

            @include('public.footer')

        </div>
        <!-- /Main Wrapper -->

        <!-- jQuery -->

        <script src="{{asset("assets/js/jquery.min.js")}}"></script>
        <script src="{{asset("assets/js/popper.min.js")}}"></script>
        <script src="{{asset("assets/js/bootstrap.min.js")}}"></script>
        <script src="{{asset("assets/plugins/fancybox/jquery.fancybox.min.js")}}"></script>
        <script src="{{asset("assets/js/slick.js")}}"></script>
        <script src="{{asset("assets/js/script.js")}}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Bootstrap JS -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <!-- Typeahead.js Bundle -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
        <!-- Typeahead Initialization -->
    </body></html>

