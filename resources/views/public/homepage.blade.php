<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>DoctorPlus</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <link type="image/x-icon" href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/fancybox/jquery.fancybox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

        <style type="text/css">

            #scrollable-dropdown-menu .tt-menu {
                max-height: 400px;
                max-width:500px;
                overflow-x: hidden;
                overflow-y: auto;
                position: fixed;
            }
            .twitter-typeahead { width: 100%; } 
            #scrollable-dropdown-menu input[type=text]{
                width:500px !important;
            }
            #scrollable-dropdown-menu_1 .tt-menu {
                max-height: 400px;
                max-width:240px;
                overflow-x: hidden;
                overflow-y: auto;
                position: fixed;
            }
            #scrollable-dropdown-menu_1 input[type=text]{
                width:240px !important;
            }
            .fa { margin-right:5px;}
            .rating .fa {font-size: 22px;}
            .rating-num { margin-top:0px;font-size: 54px; }
            .progress { margin-bottom: 5px;}
            .progress-bar { text-align: left; }
            .rating-desc .col-md-3 {padding-right: 0px;}
            .sr-only { margin-left: 5px;overflow: visible;clip: auto; }


            .elementor-77 .elementor-element.elementor-element-bd1b47c:not(.elementor-motion-effects-element-type-background), .elementor-77 .elementor-element.elementor-element-bd1b47c > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-image:url("http://amentotech.com/projects/doctreat/wp-content/uploads/2019/08/banner-img.png");}.elementor-77 .elementor-element.elementor-element-bd1b47c{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:-100px;margin-bottom:0px;padding:0px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-bd1b47c > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-77 .elementor-element.elementor-element-4d916c7{margin-top:-95px;margin-bottom:0px;padding:0px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-5b69dca{margin-top:-94px;margin-bottom:20px;padding:0px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-2236ed5:not(.elementor-motion-effects-element-type-background), .elementor-77 .elementor-element.elementor-element-2236ed5 > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-color:#e8f6ff;}.elementor-77 .elementor-element.elementor-element-2236ed5{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-77 .elementor-element.elementor-element-2236ed5 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-77 .elementor-element.elementor-element-aaffff7{padding:80px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-04d6e97{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-77 .elementor-element.elementor-element-04d6e97 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-77 .elementor-element.elementor-element-59d3be7{padding:0px 0px 100px 0px;}.elementor-77 .elementor-element.elementor-element-c695ad2:not(.elementor-motion-effects-element-type-background), .elementor-77 .elementor-element.elementor-element-c695ad2 > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-color:#e8f6ff;}.elementor-77 .elementor-element.elementor-element-c695ad2{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:0px;margin-bottom:-100px;padding:60px 0px 100px 0px;}.elementor-77 .elementor-element.elementor-element-c695ad2 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}
            .elementor-77 .elementor-element.elementor-element-bd1b47c:not(.elementor-motion-effects-element-type-background), .elementor-77 .elementor-element.elementor-element-bd1b47c > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-image:url("http://amentotech.com/projects/doctreat/wp-content/uploads/2019/08/banner-img.png");}.elementor-77 .elementor-element.elementor-element-bd1b47c{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:-100px;margin-bottom:0px;padding:0px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-bd1b47c > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-77 .elementor-element.elementor-element-4d916c7{margin-top:-95px;margin-bottom:0px;padding:0px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-5b69dca{margin-top:-94px;margin-bottom:20px;padding:0px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-2236ed5:not(.elementor-motion-effects-element-type-background), .elementor-77 .elementor-element.elementor-element-2236ed5 > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-color:#e8f6ff;}.elementor-77 .elementor-element.elementor-element-2236ed5{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-77 .elementor-element.elementor-element-2236ed5 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-77 .elementor-element.elementor-element-aaffff7{padding:80px 0px 0px 0px;}.elementor-77 .elementor-element.elementor-element-04d6e97{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-77 .elementor-element.elementor-element-04d6e97 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-77 .elementor-element.elementor-element-59d3be7{padding:0px 0px 100px 0px;}.elementor-77 .elementor-element.elementor-element-c695ad2:not(.elementor-motion-effects-element-type-background), .elementor-77 .elementor-element.elementor-element-c695ad2 > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-color:#e8f6ff;}.elementor-77 .elementor-element.elementor-element-c695ad2{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:0px;margin-bottom:-100px;padding:60px 0px 100px 0px;}.elementor-77 .elementor-element.elementor-element-c695ad2 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}
        </style>
    </head>
    <body>

        <!-- Main Wrapper -->
        <div class="main-wrapper">

            <!-- Header -->
            @include('public.header')
            <!-- /Header -->

            <!-- Home Banner -->
            <section class="section section-search">
                <div class="container-fluid">
                    <div class="banner-wrapper">
                        <div class="banner-header text-center" >
                            <h1>{{__('Search Doctor, Make an Appointment')}}</h1>
                            <p>{{__('Discover the best doctors, clinic & hospital in Afghanistan.')}}</p>
                        </div>
                        <!-- Search -->
                        <div class="search-box" dir="auto">
                            <form action="{{route('search.district.Spaciality')}}" method="post">
                                @CSRF
                                <div class="form-group search-location" id="scrollable-dropdown-menu_1">
                                    <input type="text"  class="form-control district-query" name="district" placeholder="{{__('Search City in Afg ...')}}">
                                    <span class="form-text" dir="auto">{{__('Based on your Location')}}</span>
                                </div>

                                <div class="form-group search-info" id="scrollable-dropdown-menu" dir="auto">
                                    <input type="text"  required=""class="form-control search-query" name="q" placeholder="{{__('Search Doctors,Spacialities...')}}">
                                    <span class="form-text"dir="auto">{{__('Ex : Dental or Sugar Check up etc')}}</span>
                                </div>
                                <button type="submit" class="btn btn-primary search-btn"><i class="fas fa-search"></i> <span>{{__('Search')}}</span></button>
                            </form>
                        </div>
                        <!-- /Search -->
                    </div>
                </div>
            </section>
            <!-- /Home Banner -->
            <!-- Clinic and Specialities -->
            <!--<section class="section section-specialities">-->
            <!--    <div class="container-fluid">-->
            <!--        <div class="section-header text-center">-->
            <!--            <h2>{{__('Find Doctors By Specialities')}}</h2>-->
            <!--        </div>-->
            <!--        <div class="row justify-content-center">-->
            <!--            <div class="col-md-9">-->
                            <!-- Slider -->
            <!--                <div class="specialities-slider slider">-->
            <!--                    @foreach($specialitis as $speciality)-->
            <!--                    <div class="speicality-item text-center">-->
            <!--                        <a href="{{route('get.doctor.by.class.spaciality',$speciality->id)}}">-->
            <!--                            <div class="speicality-img">-->
            <!--                                <img width="80px" src="{{asset('images/specialities/'.$speciality->photo)}}" class="img-fluid" alt="Speciality">-->
            <!--                                <span><i class="fa fa-circle" aria-hidden="true"></i></span>-->
            <!--                            </div>-->
            <!--                            <p>{{$speciality->Name}}</p>-->
            <!--                        </a>-->
            <!--                    </div>-->
            <!--                    @endforeach-->
            <!--                </div>-->
                            <!-- /Slider -->
            <!--            </div>-->
            <!--        </div>-->
            <!--    </div>-->
            <!--</section>-->
  <section class="section section-specialities @if(Request::segment(1)=='en')  @else  text-right @endif" @if(Request::segment(1)=='en')  @else  dir="rtl" @endif >
                <div class="container">
                    <div class="section-header text-center">
                     <h2>{{__('Find Doctors By Specialities')}}</h2>
                    </div>
                    <div class="row">
                        @foreach($specialitis as $speciality)
                        <div class="col-md-4">
                            <ul class="list-group">
                                <a href="{{route('get.doctor.by.class.spaciality',$speciality->id)}}">
                                    <li class="list-group-item">
                                        <i>  
                                            <img width="20px"  src="{{asset('images/specialities/'.$speciality->photo)}}" class="img-fluid" alt="Speciality">
                                        </i>
                                        {{$speciality->Name}}
                                    </li>
                                </a>
                            </ul>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
            <!-- Clinic and Specialities -->
            <!-- Popular Section -->

            <section class="section section-doctor" >
                <div class="container-fluid" >
                    <div class="row " >
                        <div class="col-lg-4 @if(Request::segment(1)=='en')  @else  text-right @endif" dir="auto" >

                            <div class="section-header ">
                                <h2>{{__('Book Our Doctor')}}</h2>
                                <p>{{__('Best Doctor Best Treatment')}}</p>
                            </div>
                            <div class="about-content">
                                <p>{{__('The aims of this service are to create a platform where patients and doctors can efficiently access /interact with each other and provide ease and comfort to both sides. It also aims to resolve the problems that patients are facing while making appointments and keeping medical files.')}} </p>
                                <p>{{__('Patients can choose doctors based on their professional profile, experience, fee, location, and other needed requirements. While doctors can access and update patients’ medical records after every checkup.')}} </p>
                                <ul>
                                    <li>{{__('Search for doctors by specialty, hospital, service, or disease.')}}</li>
                                    <li>{{__('Select based on experience, fee, rating, or location.')}}</li>
                                    <li>{{__('Book a confirmed appointment within seconds')}}</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="doctor-slider slider">
                                @foreach($doctors as $doctor)
                                <!-- Doctor Widget -->
                                <div class="profile-widget">
                                    <div class="doc-img">
                                        <a href="{{route('view.doctor.profile',['id'=>$doctor->doctorID,'language'=>app()->getLocale()])}}">
                                            <img height="200"  alt="User Image" src="{{asset('images/doctor/'.$doctor->doctor_picture)}}">
                                        </a>
                                        <a href="javascript:void(0)" class="fav-btn">
                                            <i class="far fa-bookmark"></i>
                                        </a>
                                    </div>

                                    <div class="pro-content">

                                        <h3 class="title">
                                            <a href="{{route('view.doctor.profile',['id'=>$doctor->doctorID,'language'=>app()->getLocale()])}}">{{$doctor->title}}{{$doctor->doctor_name}}</a>
                                            <i class="fas fa-check-circle verified"></i>
                                        </h3>

                                        @foreach($doctor->speciality as $special)
                                        {{$special->specialityName}}
                                        @endforeach
                                        <div class="rating">
                                            <div class="d-none">  {{$rating=number_format($doctor->stars,1)}} </div>
                                            @foreach(range(1,5) as $i)
                                            @if($rating >0)
                                            @if($rating >0.5)
                                            <i class="fas fa-star  filled"></i>
                                            @else
                                            <i class="fas fa-star-half filled" ></i>
                                            @endif
                                            @else
                                            <i class="fas  fa-star"></i>
                                            @endif
                                            <?php $rating--; ?>
                                            @endforeach
                                            <span class="d-inline-block average-rating">({{$doctor->total_stars}})</span>
                                        </div>
                                        <ul class="available-info">
                                            @foreach($doctor->Address as $add)
                                            <li>
                                                <i class="fas fa-map-marker-alt"></i> {{$add->name}}({{ date('H:i', strtotime($add->From))}} , {{ date('H:i', strtotime($add->To))}})
                                            </li>
                                            @endforeach
                                            <li>
                                                <i class="far fa-clock"></i> {{$doctor->available}}
                                            </li>
                                            <li>
                                                <i class="far fa-money-bill-alt"></i>AFN {{$doctor->doctor_fee}}
                                                <i class="fas fa-info-circle" data-toggle="tooltip" title="Lorem Ipsum"></i>
                                            </li>
                                        </ul>
                                        <div class="row row-sm">
                                            <div class="col-6">
                                                <a href="{{route('view.doctor.profile',['id'=>$doctor->doctorID,'language'=>app()->getLocale()])}}" class="btn view-btn">{{__('View Profile')}}</a>
                                            </div>
                                            <div class="col-6">
                                                @if(session('is_pateint')!='pateint')
                                                <a href="{{route('login.page.user')}}" class="btn view-btn">{{__('Book Now')}}</a>
                                                @else    
                                                <a href="{{route('Book.Appointment',[$doctor->doctorID])}}" class="btn view-btn">{{__('Book Now')}}</a>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- /Doctor Widget -->
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>
       
            <!-- /Popular Section -->
            <!-- Availabe Features -->
            @if(session()->get('language')=='en')
              <section class="section ">
                <img width="100%" src="{{asset('assets/img/HowEN.jpg')}}">
            </section>
            @endif
              @if(session()->get('language')=='fa')
              <section class="section ">
                <img width="100%" src="{{asset('assets/img/HowFa.png')}}">
            </section>
            @endif
              @if(session()->get('language')=='pa')
              <section class="section ">
                <img width="100%" src="{{asset('assets/img/HowPa.png')}}">
            </section>
            @endif
            
            @if(session()->get('language')==null)
             @if(Request::segment(1)=='en')
              <section class="section ">
                <img width="100%" src="{{asset('assets/img/HowEN.jpg')}}">
            </section>
            @endif
              @if(Request::segment(1)=='fa' )
              <section class="section ">
                <img width="100%" src="{{asset('assets/img/HowFa.png')}}">
            </section>
            @endif
              @if(Request::segment(1)=='pa' )
              <section class="section ">
                <img width="100%" src="{{asset('assets/img/HowPa.png')}}">
            </section>
            @endif
            @endif
         
            
            @include('public.footer')
        </div>
        <script src="{{asset("assets/js/jquery.min.js")}}"></script>
        <script src="{{asset("assets/js/popper.min.js")}}"></script>
        <script src="{{asset("assets/js/bootstrap.min.js")}}"></script>
        <script src="{{asset("assets/plugins/fancybox/jquery.fancybox.min.js")}}"></script>
        <script src="{{asset("assets/js/slick.js")}}"></script>
        <script src="{{asset("assets/js/script.js")}}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
        <script>
jQuery(document).ready(function ($) {
    // Set the Options for "Bloodhound" suggestion engine
    var url_d = "{{ route('get.all.district',['district' => '%QUERY%'])}}";

    var District = new Bloodhound({
        prefetch: url_d,
        remote: {
            url: url_d,
            wildcard: '%QUERY%'
        },
        datumTokenizer: Bloodhound.tokenizers.whitespace('district'),
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });

    $(".district-query").typeahead(
            {
                hint: true,
                scroll: true,
                limit: 50,
                display: 'district',
                highlight: true,
                items: 'all',
                showDefault: true,
                minLength: 0
            },
            {
                source: District.ttAdapter(),
                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'Districts',

                displayKey: function (data) {
                    return data.e_name;
                },

                // the key from the array we want to display (name,id,email,etc...)
                templates: {

                    suggestion: function (data) {
                        return '<a style="width: 240px;background-color:#f3f2f7;color:black;" class="list-group-item">' + data.e_name + '</a>'
                    }


                }
            },
            );
});
        </script>
        <script>

            jQuery(document).ready(function ($) {
                // Set the Options for "Bloodhound" suggestion engine
                var url_q = "{{ route('spa.find',['q' => '%QUERY%'])}}";
                var url_doctor = "{{ route('doctor.find',['q' => '%QUERY%'])}}";
                var speciality = new Bloodhound({
                    prefetch: url_q,
                    remote: {
                        url: url_q,
                        wildcard: '%QUERY%'
                    },
                    datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                var doctor = new Bloodhound({
                    remote: {
                        url: url_doctor,
                        wildcard: '%QUERY%'
                    },
                    datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                $(".search-query").typeahead(
                        {
                            hint: true,
                            scroll: true,
                            limit: 1,
                            display: 'q',
                            highlight: true,
                            minLength: 1
                        },
                        {
                            source: speciality.ttAdapter(),
                            // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                            name: 'specialities',
                            displayKey: function (data) {
                                return data.specialityName;
                            },

                            // the key from the array we want to display (name,id,email,etc...)
                            templates: {

                                header: [
                                    '<div style="width: 580px;background-color:#f3f2f7;color:black;" class="list-group"> <span class="title p-3 pr-2 d-block  text-left">speciality</span></div>'
                                ],
//                    empty: [
//                        '<div class="list-group-item">',
//                        'unable to find any Best data that match the current query',
//                        '</div>'
//                    ].join('\n'),
                                suggestion: function (data) {
                                    return '<a class="list-group-item ">' + data.specialityName + '</a>'
                                }


                            }
                        }, {
                    source: doctor.ttAdapter(),
                    // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                    name: 'doctors',
                    displayKey: function (data) {
                        return data.doctor_name;
                    },
                    // the key from the array we want to display (name,id,email,etc...)
                    templates: {

                        header: [
                            '<div style="width: 580px;background-color:#f3f2f7;color:black;" class="list-group"> <span class="title p-3 pr-2  text-left">Doctors</span></div>'
                        ],
                        suggestion: function (data) {
                            var url = '{{route("Book.Appointment", ":id")}}';
                            url = url.replace(':id', data.doctorID);


                            var src = '{{ URL::asset("images/doctor/") }}';

                            var img = src + '/' + data.doctor_picture;

                            return '<div class="list-group-item" style="border-radius=0px;"><span ><img class="img-circle rounded float-left" style="height: 40px;width: 40px;margin:5px;" src="' + img + '" alt=""/></span><div class="text-dark text-left " style="height: 40px;margin-top:8px;"><a  href=' + url + '" >' + '<b>' + data.title + '</b> ' + data.doctor_name + '</a></div> </div>'

                        }
                    }
                }
                );
            });
        </script>
    </body>
</html>
