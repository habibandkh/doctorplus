@include('pateint.header')
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('pateint.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('View Notification')}}</li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title">{{__('Notification')}}</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<!-- Page Content -->
<div class="content" style="transform: none; min-height: 239.6px;" @if(Request::segment(1)=='en') @else dir="rtl" @endif>
     <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            @include('pateint.sidebar')
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="content">
                    <div class="row">
                        <div class="col-12  @if(Request::segment(1)=='en') @else text-right @endif" >
                            <h4 class="page-title"> {{__('List Of  Notification')}}</h4></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-block">
                                    <!-- Invoice Table -->
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th  >{{__('Record No')}}</th>
                                                    <th  >{{__('Notification')}}</th>
                                                    <th  >{{__('Date')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($notification as $doc)
                                                <tr  scope="row">
                                                    <td>
                                                        {{$doc->id}}
                                                    </td>

                                                    <td>
                                                        <h2 >
                                                            {{$doc->text}} 
                                                        </h2>
                                                    </td>
                                                    <td>
                                                        <h2 >
                                                            {{$doc->date}} 
                                                        </h2>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="d-flex align-items-center justify-content-center h-100">{{$notification->links()}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>		
<!-- /Page Content -->
@include('pateint.footer')
