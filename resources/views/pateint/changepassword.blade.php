@include('pateint.header')
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="{{route('pateint.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Change Password')}}</li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title">{{__('Change Password')}}</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->
<div class="content" style="transform: none; min-height: 239.6px;"  @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('pateint.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="card @if(Request::segment(1)=='en') @else text-right @endif">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-6">
                                @if($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>    
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                @if($message = Session::get('danger'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>    
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                <!-- Change Password Form -->
                                <form action="{{route('password.save')}}" method="post">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label>{{__('Old Password')}}</label>
                                        <input required="" type="password" name="old_pass"class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('New Password')}}</label>
                                        <input required="" id="password"name="new_pass" type="password" class="form-control" >
                                    </div>
                                    <div class="form-group">
                                        <label>{{__('Confirm Password')}}</label>
                                        <input required="" id="confirm_password" type="password" class="form-control">
                                    </div>
                                    <div class="submit-section">
                                        <button type="submit" class="btn btn-primary submit-btn">{{__('Save Changes')}}</button>
                                    </div>
                                </form>
                                <!-- /Change Password Form -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


    var password = document.getElementById("password")
            , confirm_password = document.getElementById("confirm_password");

    function validatePassword() {
        if (password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;

    function myFunction() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
    
    function myFunction1() {
        alert('hi');
        var x = document.getElementById("confirm_password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }

</script>
@include('pateint.footer')