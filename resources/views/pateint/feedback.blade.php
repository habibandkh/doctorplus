@include('pateint.header')
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('pateint.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Feedback')}}</li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title">{{__('Feedback')}}</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->
<head>
    <style type="text/css">
        .rating {
            display: flex;
            width: 100%;
            justify-content: center;
            overflow: hidden;
            flex-direction: row-reverse;
            height: 150px;
            position: relative;
        }

        .rating-0 {
            filter: grayscale(100%);
        }

        .rating > input {
            display: none;
        }

        .rating > label {
            cursor: pointer;
            width: 40px;
            height: 40px;
            margin-top: auto;
            background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg xmlns='http://www.w3.org/2000/svg' width='126.729' height='126.73'%3e%3cpath fill='%23e3e3e3' d='M121.215 44.212l-34.899-3.3c-2.2-.2-4.101-1.6-5-3.7l-12.5-30.3c-2-5-9.101-5-11.101 0l-12.4 30.3c-.8 2.1-2.8 3.5-5 3.7l-34.9 3.3c-5.2.5-7.3 7-3.4 10.5l26.3 23.1c1.7 1.5 2.4 3.7 1.9 5.9l-7.9 32.399c-1.2 5.101 4.3 9.3 8.9 6.601l29.1-17.101c1.9-1.1 4.2-1.1 6.1 0l29.101 17.101c4.6 2.699 10.1-1.4 8.899-6.601l-7.8-32.399c-.5-2.2.2-4.4 1.9-5.9l26.3-23.1c3.8-3.5 1.6-10-3.6-10.5z'/%3e%3c/svg%3e");
            background-repeat: no-repeat;
            background-position: center;
            background-size: 76%;
            transition: .3s;
        }

        .rating > input:checked ~ label,
        .rating > input:checked ~ label ~ label {
            background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg xmlns='http://www.w3.org/2000/svg' width='126.729' height='126.73'%3e%3cpath fill='%23fcd93a' d='M121.215 44.212l-34.899-3.3c-2.2-.2-4.101-1.6-5-3.7l-12.5-30.3c-2-5-9.101-5-11.101 0l-12.4 30.3c-.8 2.1-2.8 3.5-5 3.7l-34.9 3.3c-5.2.5-7.3 7-3.4 10.5l26.3 23.1c1.7 1.5 2.4 3.7 1.9 5.9l-7.9 32.399c-1.2 5.101 4.3 9.3 8.9 6.601l29.1-17.101c1.9-1.1 4.2-1.1 6.1 0l29.101 17.101c4.6 2.699 10.1-1.4 8.899-6.601l-7.8-32.399c-.5-2.2.2-4.4 1.9-5.9l26.3-23.1c3.8-3.5 1.6-10-3.6-10.5z'/%3e%3c/svg%3e");
        }


        .rating > input:not(:checked) ~ label:hover,
        .rating > input:not(:checked) ~ label:hover ~ label {
            background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg xmlns='http://www.w3.org/2000/svg' width='126.729' height='126.73'%3e%3cpath fill='%23d8b11e' d='M121.215 44.212l-34.899-3.3c-2.2-.2-4.101-1.6-5-3.7l-12.5-30.3c-2-5-9.101-5-11.101 0l-12.4 30.3c-.8 2.1-2.8 3.5-5 3.7l-34.9 3.3c-5.2.5-7.3 7-3.4 10.5l26.3 23.1c1.7 1.5 2.4 3.7 1.9 5.9l-7.9 32.399c-1.2 5.101 4.3 9.3 8.9 6.601l29.1-17.101c1.9-1.1 4.2-1.1 6.1 0l29.101 17.101c4.6 2.699 10.1-1.4 8.899-6.601l-7.8-32.399c-.5-2.2.2-4.4 1.9-5.9l26.3-23.1c3.8-3.5 1.6-10-3.6-10.5z'/%3e%3c/svg%3e");
        }

        .emoji-wrapper {
            width: 100%;
            text-align: center;
            height: 100px;
            overflow: hidden;
            position: absolute;
            top: 0;
            left: 0;
        }

        .emoji-wrapper:before,
        .emoji-wrapper:after{
            content: "";
            height: 15px;
            width: 100%;
            position: absolute;
            left: 0;
            z-index: 1;
        }

        .emoji-wrapper:before {
            top: 0;
            background: linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 35%,rgba(255,255,255,0) 100%);
        }

        .emoji-wrapper:after{
            bottom: 0;
            background: linear-gradient(to top, rgba(255,255,255,1) 0%,rgba(255,255,255,1) 35%,rgba(255,255,255,0) 100%);
        }

        .emoji {
            display: flex;
            flex-direction: column;
            align-items: center;
            transition: .3s;
        }

        .emoji > svg {
            margin: 15px 0; 
            width: 70px;
            height: 70px;
            flex-shrink: 0;
        }

        #rating-1:checked ~ .emoji-wrapper > .emoji { transform: translateY(-100px); }
        #rating-2:checked ~ .emoji-wrapper > .emoji { transform: translateY(-200px); }
        #rating-3:checked ~ .emoji-wrapper > .emoji { transform: translateY(-300px); }
        #rating-4:checked ~ .emoji-wrapper > .emoji { transform: translateY(-400px); }
        #rating-5:checked ~ .emoji-wrapper > .emoji { transform: translateY(-500px); }

        .feedback {
            max-width: 360px;
            background-color: #fff;
            width: 100%;
            padding: 30px;
            border-radius: 8px;
            display: flex;
            flex-direction: column;
            flex-wrap: wrap;
            align-items: center;
            box-shadow: 0 4px 30px rgba(0,0,0,.05);
        }
    </style>
</head>
<div class="content" style="transform: none; min-height: 239.6px;"  @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('pateint.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header @if(Request::segment(1)=='en') @else text-right @endif">
                                <h4 class="card-title d-inline-block ">{{__('Feedback')}}</h4> 
                            </div>
                            <div class="card-block" >
                                <div class="activity">
                                    <div class="activity-box">
                                        <ul class="activity-list">
                                            @foreach($feedback as $feed)
                                            <br>
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <img width="40" src="{{asset('images/doctor/'.$feed->doctor_picture)}}" class="img-fluid rounded-circle img-thumbnail">
                                                </div>
                                                <div class="col-md-10">

                                                    <div class="activity-content">
                                                        <div class="timeline-content">
                                                            <p><strong>{{__('Dr.')}} {{$feed->doctor_name}}</strong> {{__('is waiting for your feedback for the appointment that you had at in:')}} <strong>{{$feed->appointment_date}}</strong> {{__('and :')}} <strong>{{$feed->appointment_time}}</strong>.<a href="#" onclick="myfunc('{{$feed->doctor_id}}','{{$feed->user_id}}','{{$feed->appID}}')"> {{__('click...')}}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            @endforeach

                                        </ul>
                                    </div>
                                </div>
                            </div>
                                 <div class="d-flex align-items-center justify-content-center h-100">{{$feedback->links()}}</div>
                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function validation(){
    var overall_experience = $('.overall_experience').val();
    if (overall_experience == null){
    alert('please select overall experience to submit your feedback!');
    return false;
    }
    else{
    return true;
    }
    }


    function myfunc(doctorid, userid, appID)
    {
    $('#doctorid').val(doctorid);
    $('#userid').val(userid);
    $('#appID').val(appID);
    $('#exampleModal').modal('show');
    }
</script>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Give Feedback</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="feedbackform"action="{{route('feedback.store')}}" method="post">
                    <p>give your feedback.doctors can't see who give them feedback.</p>

                    {{csrf_field()}}

                    <input type="hidden" name="doctorid" id="doctorid" >
                    <input type="hidden" name="userid" id="userid"> 
                    <input type="hidden" name="appID" id="appID"> 
                
                    <div class="row col-md-12 m-2">
                        <div class="col-md-6">
                            Overall Experience
                        </div>
                        <div class="col-md-6">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" class="overall_experience" name="overall_experience" value="1">Good
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" class="overall_experience" name="overall_experience" value="0">Bad
                                </label>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row col-md-12 m-2">
                        <div class="col-md-6">
                            Doctor Checkup
                        </div>
                        <div class="col-md-6">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input"  name="doctor_checkup" value="1">Good
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input"  name="doctor_checkup" value="0">Bad
                                </label>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row col-md-12 m-2">
                        <div class="col-md-6">
                            Staff Behavior
                        </div>
                        <div class="col-md-6">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input"  name="staff_behavior" value="1">Good
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input"  name="staff_behavior" value="0">Bad
                                </label>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row col-md-12 m-2">
                        <div class="col-md-6">
                            Clinic Environment
                        </div>
                        <div class="col-md-6">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input"  name="clinic_environment" value="1">Good
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input"  name="clinic_environment" value="0">Bad
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12 m-2">
                        <textarea name="comment" class="form-control" placeholder="your comment..." maxlength="200"></textarea>
                    </div>
                   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <input type="submit"    id="savebtn" class="btn btn-primary" value="save feedback">
            </div>
            </form>
        </div>
    </div>
</div>
</div>
@include('pateint.footer')