@include('pateint.header')
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('pateint.home')}}">{{__('Home')}}</a></li>

                    </ol>
                </nav>
                <h2 class="breadcrumb-title">{{__('Home')}}</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->
<div class="content" style="transform: none; min-height: 239.6px;"   @if(Request::segment(1)=='en') @else dir="rtl" @endif >
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;" >
            <!-- Profile Sidebar -->
            @include('pateint.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="card">
                    <div class="card-body pt-0">
                        <!-- Tab Menu -->
                        <nav class="user-tabs mb-4">
                            <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#pat_appointments" data-toggle="tab">{{__('Appointments')}}</a>
                                </li>
<!--                                <li class="nav-item">
                                    <a class="nav-link" href="#pat_medical_records" data-toggle="tab"><span class="med-records">Medical Records</span></a>
                                </li>-->
                            </ul>
                        </nav>
                        <!-- /Tab Menu -->
                        <!-- Tab Content -->
                        <div class="tab-content pt-0">
                            <!-- Appointment Tab -->
                            <div id="pat_appointments" class="tab-pane fade active show">
                                <form action="{{route('search.appointment.list')}}" method="post">
                                    @csrf
                                    <div class="row justify-content-center">
                                        <div class="row col-md-3 m-2">

                                            <select  id="select_patient" class="form-control " name="get_app_type">
                                                <option value="not">{{__('Appointment Type')}}</option>

                                                <option value="current">{{__('current')}}</option>
                                                <option value="upcoming">{{__('upcoming')}}</option>
                                                <option value="cancel">{{__('cancel')}}</option>
                                                <option value="expired">{{__('expired')}}</option>

                                            </select>

                                        </div> 
                                        <div class="row col-md-3 m-2">
                                            @if(session('is_pateint')=='pateint')
                                            <select  id="select_patient" class="form-control" name="get_user_type">
                                                <option value="-1">{{__('who is patient?')}}</option>
                                                <option value="0">{{__('Myself')}}</option>
                                                @foreach($pateint as $pa)
                                                <option value="{{$pa->patientID}}">{{$pa->p_name}}</option>
                                                @endforeach
                                            </select>
                                            @endif
                                        </div>   
                                    </div>
                                    <div class="row justify-content-center col-md-12 m-2">
                                        <input type="submit" value="{{__('search')}}"class="btn btn-success">
                                    </div>
                                </form>
                                <div class="card card-table mb-0">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-center mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>{{__('Doctor')}}</th>
                                                        <th>{{__('Patient')}}</th>
                                                        <th>{{__('Appointment Date')}}</th>
                                                        <th>{{__('Fee')}}</th>
                                                        <!--<th>{{__('Appointment Reminder')}}</th>-->
                                                        <th>{{__('Status')}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <div class="d-none">{{$mydate = Carbon\Carbon::now()->format('Y-m-d')}}</div>
                                                <div class="d-none">{{$mytime = Carbon\Carbon::now()->format('h:i:s')}}</div>
                                                @foreach ($myappointmentlist as $app)
                                                <tr>
                                                    <td>
                                                        <h2 class="table-avatar">
                                                            <a href="doctor-profile.html" class="avatar avatar-sm mr-2">
                                                                <img class="avatar-img rounded-circle" src="{{asset('images/doctor/'.$app->doctor_picture)}}" alt="User Image">
                                                            </a>
                                                            <a href="doctor-profile.html">{{__('Dr.')}} {{$app->doctor_name}}</a>
                                                        </h2>
                                                    </td>
                                                    <td>
                                                        <h2 class="table-avatar">
                                                            <a href="doctor-profile.html" class="avatar avatar-sm mr-2">
                                                                <img class="avatar-img rounded-circle" src="{{asset('images/user/'.$app->pic)}}" alt="User Image">
                                                            </a>
                                                            <a href="doctor-profile.html">{{$app->user_name}}</a>
                                                        </h2>
                                                    </td>
                                                    <td>{{$app->appointment_date}}<span class="d-block text-info">{{$app->appointment_time}}</span></td>

                                                    <td>{{$app->doctor_fee}} AFN</td>

                                                    <!--<td>{{(new Carbon\Carbon($app->appointment_date))->diffForHumans()}}</td>-->

                                                    @if($app->Status == 0  && $app->appointment_date == $mydate)
                                                    <td><span class="badge badge-pill bg-primary-light">current</span></td>
                                                    @endif
                                                    @if($app->Status == 0  && $app->appointment_date > $mydate)
                                                    <td><span class="badge badge-pill bg-success-light">upcoming</span></td>
                                                    @endif

                                                    @if($app->Status == 1 && $app->appointment_date > $mydate)
                                                    <td><span class="badge badge-pill bg-danger-light">cancel</span></td>
                                                    @endif

                                                    @if($app->appointment_date < $mydate )
                                                    <td><span class="badge badge-pill bg-danger-light">{{__('expired')}}</span></td>
                                                    @endif
                                                    <td class="text-right">
                                                        <div class="table-action">
                                                            <a href="{{route('Book.Appointment',[$app->doctorID])}}" class="btn btn-sm bg-primary-light">
                                                                <i class="fas fa-refresh"></i> {{__('Reschedule')}}
                                                            </a>
                                                            @if($app->Status == 0  && $app->appointment_date > $mydate || $app->Status == 0  && $app->appointment_date == $mydate)
                                                            <a href="{{route('pateint.cancel.appointment',[$app->appID])}}" class="btn btn-sm bg-danger-light">
                                                                <i class="far fa-remove"></i> cancel
                                                            </a>
                                                            @else
                                                           
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach

                                                @foreach ($myfamilyappointmentlist as $app)
                                                <tr>
                                                    <td>
                                                        <h2 class="table-avatar">
                                                            <a href="doctor-profile.html" class="avatar avatar-sm mr-2">
                                                                <img class="avatar-img rounded-circle" src="{{asset('images/doctor/'.$app->doctor_picture)}}" alt="User Image">
                                                            </a>
                                                            <a href="doctor-profile.html">{{__('Dr.')}} {{$app->doctor_name}} </a>
                                                        </h2>
                                                    </td>
                                                    <td>
                                                        <h2 class="table-avatar">
                                                            <a href="doctor-profile.html" class="avatar avatar-sm mr-2">
                                                                <img class="avatar-img rounded-circle" src="{{asset('images/pateint/'.$app->p_photo)}}" alt="User Image">
                                                            </a>
                                                            <a href="doctor-profile.html">{{$app->p_name}}</a>
                                                        </h2>
                                                    </td>
                                                    <td>{{$app->appointment_date}}<span class="d-block text-info">{{$app->appointment_time}}</span></td>

                                                    <td>{{$app->doctor_fee}} AFN</td>
                                                    <!--<td>{{(new Carbon\Carbon($app->appointment_date))->diffForHumans()}}</td>-->

                                                    @if($app->Status == 0  && $app->appointment_date == $mydate)
                                                    <td><span class="badge badge-pill bg-primary-light">current</span></td>
                                                    @endif
                                                    @if($app->Status == 0  && $app->appointment_date > $mydate)
                                                    <td><span class="badge badge-pill bg-success-light">upcoming</span></td>
                                                    @endif

                                                    @if($app->Status == 1 && $app->appointment_date > $mydate)
                                                    <td><span class="badge badge-pill bg-danger-light">cancel</span></td>
                                                    @endif

                                                    @if($app->appointment_date < $mydate )
                                                    <td><span class="badge badge-pill bg-danger-light">{{__('expired')}}</span></td>
                                                    @endif
                                                    <td class="text-right">
                                                        <div class="table-action">
                                                            <a href="{{route('Book.Appointment',[$app->doctorID])}}" class="btn btn-sm bg-primary-light">
                                                                <i class="fas fa-refresh"></i> {{__('Reschedule')}}
                                                            </a>
                                                               @if($app->Status == 0  && $app->appointment_date > $mydate || $app->Status == 0  && $app->appointment_date == $mydate)
                                                            <a href="{{route('pateint.cancel.appointment',[$app->appID])}}" class="btn btn-sm bg-danger-light">
                                                                <i class="far fa-remove"></i> {{__('cancel')}}
                                                            </a>
                                                            @else
                                                          
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-center h-100">{{$myappointmentlist->links()}}</div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Appointment Tab -->


                            <!-- Medical Records Tab -->
                            <div id="pat_medical_records" class="tab-pane fade">
                                <div class="card card-table mb-0">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-center mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Date </th>
                                                        <th>Description</th>
                                                        <th>Attachment</th>
                                                        <th>Created</th>
                                                        <th></th>
                                                    </tr>     
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><a href="javascript:void(0);">#MR-0010</a></td>
                                                        <td>14 Nov 2019</td>
                                                        <td>Dental Filling</td>
                                                        <td><a href="#">dental-test.pdf</a></td>
                                                        <td>
                                                            <h2 class="table-avatar">
                                                                <a href="doctor-profile.html" class="avatar avatar-sm mr-2">
                                                                    <img class="avatar-img rounded-circle" src="assets/img/doctors/doctor-thumb-01.jpg" alt="User Image">
                                                                </a>
                                                                <a href="doctor-profile.html">Dr. Ruby Perrin <span>Dental</span></a>
                                                            </h2>
                                                        </td>
                                                        <td class="text-right">
                                                            <div class="table-action">
                                                                <a href="javascript:void(0);" class="btn btn-sm bg-info-light">
                                                                    <i class="far fa-eye"></i> View
                                                                </a>
                                                                <a href="javascript:void(0);" class="btn btn-sm bg-primary-light">
                                                                    <i class="fas fa-print"></i> Print
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                </tbody>  	
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Medical Records Tab -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@include('pateint.footer')