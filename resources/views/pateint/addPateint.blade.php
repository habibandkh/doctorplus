@include('pateint.header')
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('pateint.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Add Family Member')}}</li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title">{{__('Add Family Member')}}</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->
<div class="content" style="transform: none; min-height: 239.6px;"  @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('pateint.sidebar')
            <!-- / Profile Sidebar -->

            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" >
                            <div class="card-header">
                                <h4 class="card-title d-inline-block">{{__('Family Member')}}</h4>
                            </div>
                            <div class="card-block" style="padding: 10px 30px;">
                                @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                @if($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                <form method="post" action="{{route('pateint.addfamilymember')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> {{__('First Name')}}<span class="text-danger">*</span></label>
                                                <input class="form-control" name="name" type="text" value="{{ old('name') }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('Relation Ship')}}<span class="text-danger">*</span></label>
                                                <select name="Relation" class="form-control select select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                      <option value=""></option>
                                                    <option value="Mother">{{__('Mother')}}</option>
                                                    <option value="Father">{{__('Father')}}</option>
                                                    <option value="Brother">{{__('Brother')}}</option>
                                                    <option value="Sister">{{__('Sister')}}</option>
                                                    <option value="Friends">{{__('Friends')}}</option>
                                                    <option value="Other">{{__('Other')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> {{__('Birthday')}}<span class="text-danger">*</span></label>
                                                <input class="form-control" type="date" name="Birthday"type="text" value="{{ old('Birthday') }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group gender-select">
                                                <label class="gen-label"> {{__('Gender:')}}<span class="text-danger">*</span></label><br><br>
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        <input  type="radio" name="gender" value="1" class="form-check-input">{{__('Male')}}
                                                    </label>
                                                </div>
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        <input  type="radio" name="gender" value="0"class="form-check-input">{{__('Female')}}
                                                    </label>
                                                </div>
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        <input  type="radio" name="gender" value="2"class="form-check-input">{{__('other')}}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('Phone')}} </label>
                                                <input class="form-control" maxlength="10" pattern="\d{10}" title="Please enter exactly 10 digits" name="phone"type="text" value="{{ old('phone') }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('Avatar')}}</label>
                                                <div class="profile-upload">
                                                    <div class="upload-img">
                                                        <img alt="" src="assets/img/user.jpg">
                                                    </div>
                                                    <div class="upload-input">
                                                        <input type="file" name="photo"class="form-control" value="{{ old('photo') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
<!--                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>{{__('Blood Group')}}</label>
                                                <select name="blood" class="form-control select select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">

                                                    <option value="0">{{__('unknown')}}</option>
                                                    <option value="A-">A-</option>
                                                    <option value="A+">A+</option>
                                                    <option value="B-">B-</option>
                                                    <option value="B+">B+</option>
                                                    <option value="AB-">AB-</option>
                                                    <option value="AB+">AB+</option>
                                                    <option value="O-">O-</option>
                                                    <option value="O+">O+</option>

                                                </select>
                                            </div>
                                        </div>-->
                                    </div>
                                    <div class="m-t-20 text-center">
                                        <button type="submit"class="btn btn-primary submit-btn">{{__('Create Patient')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
@include('pateint.footer')
