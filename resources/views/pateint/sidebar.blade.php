<div  @if(Request::segment(1)=='en') class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar " @else class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar text-right" @endif style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">

    <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; top: 0px; left: 20px;"><div class="profile-sidebar">
            <div class="widget-profile pro-widget-content">
                <div class="profile-info-widget">
                    <a href="#" class="booking-doc-img">
                        <img src="{{asset('images/user/'.session('pic_pateint'))}}" alt="User Image">
                    </a>
                    <div class="profile-det-info">
                        <h3>{{session('name_pateint')}}</h3>
                        <div class="patient-details" dir="ltr">
                            <h5><i class="fas fa-birthday-cake"></i> 
                                {{ Carbon\Carbon::parse(session('birthday'))->format('d M Y ')}},
                                {{Carbon\Carbon::parse(session('birthday'))->diff(Carbon\Carbon::now())->format('%Y years')}}
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dashboard-widget">
                <nav class="dashboard-menu">
                    <ul>
                        <li @if(Request::segment(2)=='pateint-home') class="active" @endif   >
                             <a href="{{route('pateint.home')}}">
                                <i class="fas fa-columns"></i>
                                <span>{{__('Dashboard')}}</span>
                            </a>
                        </li>
                        <li @if(Request::segment(2)=='pateint-family-member') class="active" @endif   >
                             <a  href="{{route('pateint.familymamber')}}">
                                <i class="fas fa-user"></i>
                                <span>{{__('Family members')}}</span>
                            </a>
                        </li>
                        <li @if(Request::segment(2)=='my-doctors') class="active" @endif   >
                             <a href="{{route('get.my.doctors')}}">
                                <i class="fas fa-heartbeat"></i>
                                <span>{{__('My Doctors')}}</span>

                            </a>
                        </li>

                        <li @if(Request::segment(2)=='pateints-medical-records') class="active" @endif   >
                             <a href="{{route('pateint.medical.records')}}">
                                <i class="fas fa-medkit"></i>
                                <span>{{__('Medical Records')}}</span>
                            </a>
                        </li>

                        <li @if(Request::segment(2)=='user-noti-list') class="active" @endif   >
                             <a href="{{route('user.noti.list',[session('id_pateint')])}}">
                                <i class="fas fa-bell"></i>
                                <span>{{__('Notification')}}</span>
                                
                            </a>
                        </li>

                        <li @if(Request::segment(2)=='user-feedback') class="active" @endif   >
                             <a href="{{route('pateint.feedback')}}">
                                <i class="fas fa-star"></i>
                                <span>{{__('feedback')}}</span>
                            </a>
                        </li>
                        <li @if(Request::segment(2)=='My-profile') class="active" @endif   >
                             <a href="{{route('my.profile')}}">
                                <i class="fas fa-user-cog"></i>
                                <span>{{__('Profile Settings')}}</span>
                            </a>
                        </li>
                        <li @if(Request::segment(2)=='private-chat-with-doctor') class="active" @endif   >
                             <a href="{{route('user.Private.Chat')}}">
                                <i class="fas fa-user-cog"></i>
                                <span>{{__('Private Chat')}}</span>
                            </a>
                        </li>
                        <li @if(Request::segment(2)=='change-password') class="active" @endif   >
                             <a href="{{route('user.change.pass')}}">
                                <i class="fas fa-lock"></i>
                                <span>{{__('Change Password')}}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('logout.user')}}">
                                <i class="fas fa-sign-out-alt"></i>
                                <span>{{__('Logout')}}</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="resize-sensor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; z-index: -1; visibility: hidden;">
            <div class="resize-sensor-expand" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
                <div style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 340px; height: 1005px;">
                </div>
            </div>
            <div class="resize-sensor-shrink" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
                <div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%">
                </div>
            </div>
        </div>
    </div>
</div>