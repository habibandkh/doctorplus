@include('pateint.header')
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('pateint.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('My Doctors')}}</li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title">{{__('My Doctors')}}</h2>
            </div>
        </div>
    </div>
</div>
<div class="content" style="transform: none; min-height: 239.6px;"  @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('pateint.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="row row-grid">
                    @foreach($mydoctors as $my_doctor)
                    <div class="col-md-6 col-lg-4 col-xl-3">
                        <div class="card widget-profile pat-widget-profile">
                            <div class="card-body">
                                <div class="pro-widget-content">
                                    <div class="profile-info-widget">
                                        <a href="{{url('view-doctor-profile/'.$my_doctor->doctorID)}}" class="booking-doc-img">
                                            <img src="{{asset('images/doctor/'.$my_doctor->doctor_picture)}}" alt="User Image">
                                        </a>
                                        <div class="profile-det-info">
                                            <h3><a href={{route('view.doctor.profile',['id'=>$my_doctor->doctorID,'language'=>app()->getLocale()])}}#">{{$my_doctor->title}}{{$my_doctor->doctor_name}}</a></h3>

                                            <div class="patient-details">
                                                <h5>{{$my_doctor->Expert}}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="patient-info" class="">
                                    <ul>
                                        <li><a href="{{route('view.doctor.profile',['id'=>$my_doctor->doctorID,'language'=>app()->getLocale()])}}"class="btn btn-primary btn-sm  btn-block">{{__('View Profile')}}</a></li>
                                        <li><a href="{{route('Book.Appointment',[$my_doctor->doctorID])}}"class="btn btn-warning btn-sm btn-block">{{__('Book Appointment')}}</a></li>


                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach()
                </div>
                <div class="d-flex align-items-center justify-content-center ">{{$mydoctors->links()}}</div>
            </div>
        </div>
    </div>
</div>
@include('pateint.footer')
