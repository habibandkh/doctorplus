@include('pateint.header')
<head>
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input { 
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
</head>
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('pateint.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Medical Records')}}</li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title">{{__('Medical Records')}}</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<!-- Page Content -->
<div class="content" style="transform: none; min-height: 239.6px;"  @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            @include('pateint.sidebar')

            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="card card-table">
                    <div class="card-header ">
                        <h4 class="card-title d-inline-block">{{__('All List Of Medical Records')}}</h4> <a href="{{route('multiple.file.upload')}}" class="btn btn-primary float-right"><i class="fa fa-plus"></i> {{__('Add Medical Records')}}</a>
                    </div>
                    <div class="card-block">
                        <div class="card-body">
                            <!-- Invoice Table -->
                            <div class="table-responsive">
                                <table class="table table-hover table-center mb-0">
                                    <thead>
                                        <tr>
                                            <th>{{__('Record No')}}</th>
                                            <th>{{__('Doctor Name')}}</th>
                                            <th>{{__('Patient Name')}}</th>
                                            <th>{{__('Record type')}}</th>
                                            <th>{{__('Date')}}</th>
                                            <th>{{__('Allow Share')}}</th>
                                            <th>{{__('Action')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($document as $doc)
                                        <tr >
                                            <td>
                                                <a href="invoice-view.html">#{{$doc->docID }}</a>
                                            </td>
                                            <td>
                                                <h2 class="table-avatar">

                                                    {{$doc->doctor_name }} 
                                                </h2>
                                            </td>
                                            <td>
                                                <h2 class="table-avatar">
                                                    {{$doc->pateint_name }} 
                                                </h2>
                                            </td>
                                            <td>{{$doc->record_type }}</td>
                                            <td class="w-30">{{$doc->date }}</td>
                                            <td>
                                                
                                                @if($doc->allow_share_records ==1)
                                                <a href="{{route('allow.share.records',[$doc->docID])}}">
                                                    <div class="btn btn-success">on
                                                    </div>
                                                </a>
                                                @endif
                                                
                                                @if($doc->allow_share_records ==0)
                                                <a href="{{route('allow.share.records',[$doc->docID])}}">
                                                    <div class="btn btn-danger">off
                                                    </div>
                                                </a>
                                                @endif
                                                
                                            </td>
                                           <td class="text-right">
                                                <div class="table-action">
                                                    <a href="{{route('view.my.records',[$doc->docID])}}" class="btn btn-sm bg-info-light">
                                                        <i class="far fa-eye"></i> {{__('View')}}
                                                    </a>
                                                    <a href="javascript:void(0)" onclick="deletetmedicalrecords('{{$doc->docID}}')" class="btn btn-sm bg-danger-light">
                                                        <i class="fas fa-"></i> {{__('Delete')}}
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="d-flex align-items-center justify-content-center h-100">{{$document->links()}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>		
<!-- /Page Content -->



@include('pateint.footer')