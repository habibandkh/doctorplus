<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
        <!-- HTML meta refresh URL redirection -->
       

    <title>Preclinic - Medical & Hospital - Bootstrap 4 Admin Template</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet"type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://malsup.github.com/jquery.form.js"></script>

</head>
<body>
    <div class="row"style="padding-left:400px;padding-top: 20px;">


        <div class="col-md-8" >
            <div class="card bg-md-white">
                <div class="card-header">
                    <h4 class="card-title d-inline-block">{{__('Upload File And Document related to patients')}} </h4> 
                </div>
                <div class="card-block" style="margin: 10px 10px;">

                    <form method="post" action="{{ route('upload') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                                 
                            <input type="hidden" name="p_id" value="{{ Request::segment(3) }}" />
                            <div class="col-md-3"><h4>{{__('documents details')}}</h4></div>
                            <div class="col-md-9">
                                <input type="text" placeholder="documents details"class="form-control" name="details"  />
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3" ><h4>{{__('Select Image')}}</h4></div>
                            <div class="col-md-6">
                                <input type="file" name="file" id="file" />
                            </div>
                            <br>
                            <br>
                            <div class="col-md-3">
                                <input type="submit" name="upload" value="{{__('Upload')}}" class="btn btn-success" />
                            </div>
                        </div>
                    </form>

                    <br />

                    <br />
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow=""
                             aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                            0%
                        </div>
                    </div>
                    <br />
                    <div id="success">

                    </div>
                    <br />
                </div>
                <div class="modal-footer">
                    <a type="button" class="btn btn-secondary text-white" data-dismiss="modal">Back</a>

                </div>
            </div>
        </div>

    </div>


<!--<script src="{{asset('assets/js/jquery-3.2.1.min.js')}}"></script>-->
    <script src="{{asset('assets/js/popper.min.js')}}"></script>
    <!--<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>-->
    <script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
    <!--<script src="{{asset('assets/js/Chart.bundle.js')}}"></script>
    <script src="{{asset('assets/js/chart.js')}}"></script>-->
    <script src="{{asset('assets/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/app.js')}}"></script>
    <script>
$(document).ready(function () {

$('form').ajaxForm({
    beforeSend: function () {
        $('#success').empty();
    },
    uploadProgress: function (event, position, total, percentComplete)
    {
        $('.progress-bar').text(percentComplete + '%');
        $('.progress-bar').css('width', percentComplete + '%');
    },
    success: function (data)
    {
        if (data.errors)
        {
            $('.progress-bar').text('0%');
            $('.progress-bar').css('width', '0%');
            $('#success').html('<span class="text-danger"><b>' + data.errors + '</b></span>');
        }
        if (data.success)
        {
            $('.progress-bar').text('Uploaded');
            $('.progress-bar').css('width', '100%');
            $('#success').html('<span class="text-success"><b>' + data.success + '</b></span><br /><br />');

            //                        $('#success').append(data.image);

            $('#success').append(data.pdf);
            //                        $('#success').append(data.ext);
        }
    }
});

});
    </script>

</body>



</html>



