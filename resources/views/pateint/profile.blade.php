@include('pateint.header')
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('pateint.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('My Profile')}}</li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title">{{__('My Profile')}}</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->
<div class="content" style="transform: none; min-height: 239.6px;"  @if(Request::segment(1)=='en') @else dir="rtl" @endif>
     <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('pateint.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="card">
                    <div class="card-body">
                        <br />
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                <button type="button" class="close" data-dismiss="alert">×</button>   
                                @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        @if($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>    
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        <!-- Profile Settings Form -->
                        <form @if(Request::segment(1)=='en') @else class="text-right" @endif method="post" action="{{route('pateint.updateprofile')}}" enctype="multipart/form-data">
                               {{csrf_field()}}
                               <div class="row form-row">
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <div class="change-avatar">
                                            <div class="profile-img">
                                                <img src="{{asset('images/user/'.session('pic_pateint'))}}" alt="User Image">
                                            </div>
                                            <div class="upload-img">
                                                <div class="change-photo-btn">
                                                    <span><i class="fa fa-upload"></i> {{__('Upload Photo')}}</span>
                                                    <input type="file" name="photo" class="upload" value="{{ old('photo') }}">
                                                </div>
                                                <small class="form-text text-muted">{{__('Allowed')}} JPG, GIF or PNG. Max size of 2MB</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>{{__('Name')}}<span class="text-danger">*</span></label>
                                        <input  name="name" type="text" value="{{$pateintEdit->name}}" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>{{__('Father Name')}}<span class="text-danger">*</span></label>
                                        <input class="form-control" name="father_name"type="text"value="{{ $pateintEdit->f_name}}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>{{__('Date of Birth')}}<span class="text-danger">*</span></label>
                                       
                                            <input class="form-control" name="birthday" type="date" value="{{ $pateintEdit->birthday}}">
                                      
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>{{__('Email')}}</label>
                                        <input type="email" name="email"class="form-control" value="{{ $pateintEdit->email}}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>{{__('Mobile')}}<span class="text-danger">*</span></label>
                                        <input type="text"  maxlength="10" pattern="\d{10}" title="Please enter exactly 10 digits" name="phone"value="{{ $pateintEdit->phone}}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group gender-select">
                                        <label class="gen-label">Gender<span class="text-danger">*</span></label><br>
                                        @if($pateintEdit->gender==1)
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input  type="radio" id="male" name="gender" checked value="1" class="form-check-input">{{__('Male')}}
                                            </label>
                                        </div>
                                        @else
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input  type="radio" id="male" name="gender"  value="1" class="form-check-input">{{__('Male')}}
                                            </label>
                                        </div>

                                        @endif
                                        @if($pateintEdit->gender==0)
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input  type="radio" id="female" name="gender" checked value="0"class="form-check-input">{{__('Female')}}
                                            </label>
                                        </div>
                                        @else
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input  type="radio" id="female" name="gender"  value="0"class="form-check-input">{{__('Female')}}
                                            </label>
                                        </div>
                                        @endif
                                        @if($pateintEdit->gender==2)
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input  type="radio" id="other" name="gender" checked value="2"class="form-check-input">{{__('other')}}
                                            </label>
                                        </div>
                                        @else
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input  type="radio" id="other" name="gender"  value="2"class="form-check-input">{{__('other')}}
                                            </label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="submit-section">
                                <button type="submit" class="btn btn-primary submit-btn">{{__('Save Changes')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('pateint.footer')