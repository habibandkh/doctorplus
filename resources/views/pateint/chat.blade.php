@include('pateint.header')

<body class="chat-page" data-gr-c-s-loaded="true">
    <div class="main-wrapper">
        <div class="content" style="min-height: 239.6px;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="chat-window">

                            <!-- Chat Left -->
                            <div class="chat-cont-left">
                                <div class="chat-header">
                                    <span>Chats</span>
                                    <a href="javascript:void(0)" class="chat-compose">
                                        <i class="material-icons">{{__('control point')}}</i>
                                    </a>
                                </div>
                                <form class="chat-search">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <i class="fas fa-search"></i>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Search">
                                    </div>
                                </form>
                                <div class="chat-users-list">
                                    <div class="chat-scroll ">
                                        @foreach($doctors as $doctor)
                                        <a href="javascript:void(0);" class="media users" id="{{$doctor->doctorID}}">
                                            <input type="hidden" id="receiver_id" value="{{ $doctor->doctorID }}">     
                                            <span class="pending"></span>
                                            <div class="media-img-wrap">
                                                <div class="avatar avatar-online">
                                                    <img src="{{asset('images/doctor/'.$doctor->doctor_picture)}}" alt="User Image" class="avatar-img rounded-circle">
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <div>
                                                    <div class="user-name">{{__('Dr.')}} {{$doctor->doctor_name}}</div>
<!--                                                    <div class="user-last-chat">I'll call you later </div>-->
                                                </div>
<!--                                                <div>
                                                    <div class="last-chat-time block">8:01 PM</div>
                                                </div>-->
                                            </div>
                                        </a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <!-- /Chat Left -->
                            <!-- Chat Right -->
                            <div class="chat-cont-right">
                                <div class="chat-header">
                                    <a id="back_user_list" href="javascript:void(0)" class="back-user-list">
                                        <i class="material-icons">chevron left</i>
                                    </a>
                                    <div class="media">
                                        <div class="media-img-wrap">
                                            <div class="avatar avatar-online">
                                                <img src="{{asset('images/user/'.$userInfos->pic)}}" alt="User Image" class="avatar-img rounded-circle">
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <div class="user-name">{{$userInfos->name}}</div>
                                            <div class="user-status">{{__('online')}}</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="messages_contect" >

                                </div>
                                
                            </div>
                            <!-- /Chat Right -->
                        </div>
                    </div>
                </div>
                <!-- /Row -->
            </div>
        </div>		
        <!-- /Page Content -->
    </div>
    <!-- /Main Wrapper -->
    <!-- Voice Call Modal -->

    <!-- /Voice Call Modal -->

    <!-- Video Call Modal -->
    <div class="modal fade call-modal" id="video_call">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">

                    <!-- Incoming Call -->
                    <div class="call-box incoming-box">
                        <div class="call-wrapper">
                            <div class="call-inner">
                                <div class="call-user">
                                    <img class="call-avatar" src="assets/img/patients/patient.jpg" alt="User Image">
                                    <h4>Richard Wilson</h4>
                                    <span>Calling ...</span>
                                </div>							
                                <div class="call-items">
                                    <a href="javascript:void(0);" class="btn call-item call-end" data-dismiss="modal" aria-label="Close"><i class="material-icons">call_end</i></a>
                                    <a href="video-call.html" class="btn call-item call-start"><i class="material-icons">videocam</i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Incoming Call -->

                </div>
            </div>
        </div>
    </div>


    @include('pateint.footer')