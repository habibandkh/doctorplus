@include('pateint.header')
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('pateint.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('List Of Family Member')}}</li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title">{{__('List Of Family Member')}}</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->
<div class="content" style="transform: none; min-height: 239.6px;"  @if(Request::segment(1)=='en') @else dir="rtl" @endif>
     <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('pateint.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" >
                            <div class="card-header">
                                <h4 class="card-title d-inline-block">{{__('Family Member Profile')}}</h4> <a href="{{route('pateint.familymamber.Edit',[$pateintview->patientID]) }}" class="btn btn-primary float-right"><i class="fa fa-plus"></i> {{__('Edit Profile')}}</a>
                            </div>
                            <div class="card-block" style="padding: 10px 30px;">
                                <div class="content">
                                    <div class="card-box profile-header">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="widget-profile pro-widget-content">
                                                    <div class="profile-info-widget">
                                                        <a href="#" class="booking-doc-img">
                                                            <img src="{{asset('images/pateint/'.$pateintview->p_photo)}}" alt="User Image">
                                                        </a>
                                                        <div class="profile-det-info">
                                                            <h3>{{$pateintview->p_name}}</h3>
                                                            <div class="patient-details">

                                                                <h5 class="mb-0"><i class="fas fa-phone"></i> {{$pateintview->p_phoneNo}}</h5>
                                                                <h5 class="mb-0"><i class="fas fa-calendar"></i> {{$pateintview->birthday}}</h5>
                                                                <h5 class="mb-0"><i class="fas fa-user-md"></i>  <span class="title">{{__('Gender:')}}</span>
                                                                    @if($pateintview->gender==1)
                                                                    <span class="text">{{__('male')}}</span>
                                                                    @endif
                                                                    @if($pateintview->gender==0)
                                                                    <span class="text">{{__('female')}}</span>
                                                                    @endif
                                                                    @if($pateintview->gender==2)
                                                                    <span class="text">{{__('other')}}</span>
                                                                    @endif
                                                                </h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card" >
                            <div class="card-header">
<!--                                <h4 class="card-title d-inline-block">list of documents</h4> <a href="{{url('upload/document',[$pateintview->patientID])}}" class="btn btn-primary float-right"><i class="fa fa-plus"></i> Add file</a>-->
                                <h4 class="card-title d-inline-block">list of documents</h4> <a href="{{route('multiple.file.upload')}}" class="btn btn-primary float-right"><i class="fa fa-plus"></i> Add file</a>
                            </div>
                            <div class="card-block" style="padding: 10px 30px;">
                                <div class="content">
                                    <div class="card-box profile-header">
                                        <div class="row">
                                            <p>Picture File Formate</p><hr>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    @foreach($document as $doc)
                                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                                        <div class="card widget-profile pat-widget-profile">
                                                            <div class="card-body">
                                                                <div class="pro-widget-content">
                                                                    @foreach($doc->file as $file)
                                                                    <div class="profile-info-widget">
                                                                        <a href="patient-profile.html" class="booking-doc-img">
                                                                            <img src="{{asset('doctor/public/document/pateint/'.$file->document)}}" alt="User Image">
                                                                        </a>
                                                                        <div class="profile-det-info">
                                                                            <h3><a href="patient-profile.html">{{$doc->doctor_name}}</a></h3>
                                                                            <div class="patient-details">
                                                                                <a href="{{asset('doctor/public/document/pateint/'.$file->document)}}" target="_blank"class="btn btn-primary btn-sm"><i class="fa fa-eye"></i>view</a>
                                                                                <a href="{{route('user.delete.doc',[$doc->docID])}}" class="btn btn-danger btn-sm"><i class="fa fa-remove"></i>remove</a>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

</div>

@include('pateint.footer')
