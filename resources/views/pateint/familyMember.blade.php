@include('pateint.header')
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('pateint.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Family Member')}}</li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title">{{__('Family Member')}}</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->
<div class="content" style="transform: none; min-height: 239.6px;"  @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            <!-- Profile Sidebar -->
            @include('pateint.sidebar')
            <!-- / Profile Sidebar -->
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title d-inline-block">{{__('Family Member')}}</h4> <a href="{{route('pateint.addFamilyMemberView')}}" class="btn btn-primary float-right"><i class="fa fa-plus"></i> {{__('add family member')}}</a>
                            </div>
                            <div class="card-block">
                                <div class="table-responsive">
                                    <table class="table mb-0 new-patient-table">
                                        <thead>
                                        <th>{{__('Name')}}</th>
                                        <th>{{__('Relation Ship')}}</th>
                                        <th>{{__('Phone')}}</th>
                                        <th>{{__('birthday')}}</th>

                                        <th>{{__('Action')}}</th>
                                        </thead>
                                        <tbody>
                                            @foreach($pateint as $p)
                                            <tr>
                                                <td>
                                                    <img width="28" height="28" class="rounded-circle" src="{{asset('images/pateint/'.$p->p_photo)}}" alt="">
                                                    <h2>{{$p->p_name }}</h2>
                                                </td>
                                                <td>{{$p->p_Relation}} </td>
                                                <td>{{$p->p_phoneNo}} </td>
                                                <td>{{$p->birthday}} </td>

                                                <td>
                                                    <a href="{{route('pateint.familymamber.View', [$p->patientID]) }}" style="text-decoration: none;"><button class="btn btn-primary btn-primary-one "><i class="fa fa-eye"></i> {{__('View')}}</button></a>
                                                    <a href="{{route('pateint.familymamber.Edit', [$p->patientID]) }}" style="text-decoration: none;"><button class="btn btn-warning btn-primary-one "><i class="fa fa-eye"></i> {{__('Edit')}}</button></a>
                                                    <a href="javascript:void(0)" onclick="deletetfamilymember('{{$p->patientID}}')" style="text-decoration: none;"><button class="btn btn-danger btn-primary-one "><i class="fa fa-eye"></i> {{__('Delete')}}</button></a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <br>
                                    <div class="d-flex align-items-center justify-content-center h-100">{{$pateint->links()}}</div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>
@include('pateint.footer')
