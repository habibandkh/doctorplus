@include('pateint.header')
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('pateint.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('View Record')}}</li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title">{{__('Medical Records')}}</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<!-- Page Content -->
<div class="content" style="transform: none; min-height: 239.6px;"  @if(Request::segment(1)=='en') @else dir="rtl" @endif>
    <div class="container-fluid" style="transform: none;">
        <div class="row" style="transform: none;">
            @include('pateint.sidebar')

            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="card card-table">
                    <div class="card-header">
                        <h4 class="card-title d-inline-block">{{__('All List Of Medical Records')}}</h4> <a href="{{route('multiple.file.upload')}}" class="btn btn-primary float-right"><i class="fa fa-plus"></i> {{__('Add Medical Records')}}</a>
                    </div>
                    <div class="card-block">
                        <div class="card-body">
                            <!-- Invoice Table -->
                            <div class="table-responsive">
                                <table class="table table-hover table-center mb-0">
                                    <thead>
                                        <tr>
                                            <th>{{__('No')}}</th>
                                            <th>{{__('File Name')}}</th>
                                            <th>{{__('action')}}</th>

                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($document as $doc)
                                        <tr>
                                            <td>
                                                <a href="#">#{{$doc->file_id}}</a>
                                            </td>
                                            <td>
                                                <h2 class="table-avatar">
                                                    {{$doc->document}} 
                                                </h2>
                                            </td>
                                            <td class="text-right">
                                                <div class="table-action">
                                                    <a href="{{url('images/medicalrecords/'.$doc->document)}}" class="btn btn-sm bg-info-light">
                                                        {{__('View')}}
                                                    </a>
                                                    <a href="{{route('user.delete.doc',[$doc->file_id])}}" class="btn btn-sm bg-danger-light">
                                                         {{__('delete')}}
                                                    </a>
                                                  
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="d-flex align-items-center justify-content-center h-100">{{$document->links()}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>		
<!-- /Page Content -->
@include('pateint.footer')
