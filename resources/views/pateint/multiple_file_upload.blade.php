@include('pateint.header')
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>upload multi medical records</title>
    <link href="{{asset('assets/fileupload/boot45.css')}}" rel="stylesheet">
    <script src="{{asset('assets/fileupload/jquery35.js')}}"></script>
    <script src="{{asset('assets/fileupload/jqueryform.js')}}"></script> 
    <script src="{{asset('assets/fileupload/bootjava45.js')}}"></script>
</head>
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('pateint.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Medical Records')}}</li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title">{{__('Medical Records')}}</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->
<div class="content @if(Request::segment(1)=='en') @else text-right @endif"   @if(Request::segment(1)=='en') @else dir="rtl" @endif>
     <div class="container-fluid" style="transform: none;">
        <div class="row justify-content-center " style="transform: none;">
            @include('pateint.sidebar')
            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" >
                            <div class="card-header">
                                <h4 class="card-title d-inline-block">{{__('Add Medical Records')}}</h4> 
                            </div>
                            <div class="card-block" style="padding: 10px 30px;">
                                <div class="content">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <br />
                                            <form method="post" action="{{ route('upload.multi.file') }}" enctype="multipart/form-data" >
                                                @csrf
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="col-md-3" align="right"><h4>{{__('Select Images')}}</h4></div>
                                                        <div class="col-md-6">
                                                            <input required type="file" name="file[]" id="file"  />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>{{__('Doctor')}}</label>
                                                            <select required="" name="doctor_who" class="form-control  " data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                                <option value="">{{__('select doctor')}}</option>
                                                                @foreach($doctor_name as $doci)
                                                                <option value="{{$doci->doctorID}}">{{$doci->title}}{{$doci->doctor_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>{{__('Related to who?')}}</label>
                                                            <select required="" name="related_to_who" class="form-control  " data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                                <option value="">{{__('select type')}}</option>
                                                                <option value="0">{{__('My Self')}}</option>
                                                                @foreach($patient_name as $patient_n)
                                                                <option value="{{$patient_n->patientID}}">{{$patient_n->p_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>{{__('Records Type')}}</label>
                                                            <select required="" name="records_type" class="form-control  " data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                                <option value="">{{__('Select recorde type')}}</option>
                                                                <option value="Prescription">{{__('Prescription')}}</option>
                                                                <option value="Medical records">{{__('Medical records')}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <input type="submit" name="upload" value="{{__('save')}}" class="btn btn-success" />
                                                    </div>
                                                </div>
                                            </form>
                                            <br/>  
                                            <br/>
                                            <div class="progress">
                                                <div class="progress-bar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                                    0%
                                                </div>
                                            </div>
                                            <br/>
                                            <div id="success" class="row">

                                            </div>
                                            <br/>
                                        </div>
                                    </div>


                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

</div>
@include('pateint.footer')
<script>
$(document).ready(function () {
    $('form').ajaxForm({
        beforeSend: function () {
            $('#success').empty();
            $('.progress-bar').text('0%');
            $('.progress-bar').css('width', '0%');
        },
        uploadProgress: function (event, position, total, percentComplete) {
            $('.progress-bar').text(percentComplete + '0%');
            $('.progress-bar').css('width', percentComplete + '0%');
        },
        success: function (data)
        {
            if (data.success)
            {

                $('#success').html('<div class="text-success text-center"><b>' + data.success + '</b></div><br /><br />');
                $('#success').append(data.image);
                $('.progress-bar').text('Uploaded');
                $('.progress-bar').css('width', '100%');
                
                 window.location.href = "{{route('pateint.medical.records')}}";

            } else {
                $('#success').html('<div class="text-danger text-center"><b>' + data.error + '</b></div><br /><br />');

                $('.progress-bar').css('width', '0%');

            }

        }
    });
});
</script>



