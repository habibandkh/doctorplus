<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Doctor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor', function (Blueprint $table) {
            $table->bigIncrements('doctorID');
            $table->string('pmdc');
            $table->string('title');
            $table->string('city');
            $table->string('doctor_name');
            $table->string('doctor_email');
            $table->string('doctor_phoneNo');
            $table->string('doctor_password');
            $table->string('doctor_picture');
            $table->string('doctor_clinicAddress');
            $table->string('district');
            $table->tinyInteger('doctor_status');
            $table->timestamp('doctor_regDate')->useCurrent();
            $table->integer('doctor_fee');
            $table->string('doctor_gender');
            $table->string('doctor_last_name');
            $table->string('doctor_birthday');
            $table->string('doctor_father_name');
            $table->text('doctor_Short_Biography');
            $table->string('Expert');
            $table->string('experience');
            $table->tinyInteger('email_verification');
            $table->string('email_verification_token');
            $table->tinyInteger('professional_profile_status');
             $table->tinyInteger('under_review');
              $table->string('lang');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor');
    }
}
