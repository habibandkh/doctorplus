<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Appointment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('appID');
            $table->string('doctor_id');
            $table->string('pateint_id');
            $table->string('user_id');
            $table->string('clinic_address_id');
            $table->string('appointment_date');
            $table->string('appointment_time');
            $table->string('status');
            $table->string('feedback');
            $table->string('cancel_by_user');
            $table->string('cancel_by_doctor');
            $table->string('day');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment');
    }
}
