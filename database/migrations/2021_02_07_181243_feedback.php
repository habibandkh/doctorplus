<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Feedback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->bigIncrements('feedback_id');
            $table->string('feedback_doctor_id');
            $table->string('feedback_user_id');
            $table->string('feedback_patient_id');
            $table->string('overall_experience');
            $table->string('doctor_checkup');
            $table->string('staff_behavior');
            $table->string('clinic_environment');
            $table->string('comment');
            $table->string('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
