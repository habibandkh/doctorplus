<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Chat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat', function (Blueprint $table) {
            $table->bigIncrements('ChatID');
            $table->string('doctor_id');
            $table->string('user_id');
            $table->string('pateint_id');
            $table->string('is_read');
            $table->string('message');
            $table->string('date');
            $table->string('from');
            $table->string('to');
            $table->string('type');
            $table->string('file');
            $table->string('voice');
            $table->string('who_send');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat');
    }
}
