<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Document extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document', function (Blueprint $table) {
            $table->bigIncrements('docID');
            $table->string('doctor_id');
            $table->string('user_id');
            $table->string('pateint_id');
            $table->string('pateint_name');
            $table->string('doctor_name');
            $table->string('record_type');
            $table->string('date');
            $table->string('allow_share_records');
            $table->string('allow_share_all_records');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document');
    }
}
