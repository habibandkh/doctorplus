<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pateint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pateint', function (Blueprint $table) {
            $table->bigIncrements('patientID');
            $table->string('user_ID');
            $table->string('p_name');
            $table->string('p_photo');
            $table->string('p_relation');
            $table->string('p_phoneNo');
            $table->string('birthday');
            $table->string('gender');
            $table->string('bloodgroup');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pateint');
    }
}
