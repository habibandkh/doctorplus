<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Visitingtime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitingtime', function (Blueprint $table) {
            $table->bigIncrements('vtID');
            
             $table->string('doctorID');
              $table->string('day');
               $table->string('visit_time');
                $table->string('doctor_active');
                 $table->string('time_off_on');
                  $table->string('clinic_address_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitingtime');
    }
}
