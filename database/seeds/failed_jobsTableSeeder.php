<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
use Faker\factory as Faker;


class failed_jobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
           $faker =faker::create();
        foreach(range(1,300)as $index){

            DB::table('failed_jobs')->insert([
       		'connection'=>$faker->sentence(5),
       		'queue'=>$faker->sentence(5),
       		'payload'=>$faker->sentence(5),
       		'expection'=>$faker->sentence(5),
       		'failed_at'=>$faker->sentence(5),
       		
       		
 		
        ]);
        }
    }
}
