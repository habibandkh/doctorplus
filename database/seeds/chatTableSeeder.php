<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
use Faker\factory as Faker;

class chatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
           $faker =faker::create();
        foreach(range(1,300)as $index){

            DB::table('chat')->insert([
       		'doctor_id'=>$faker->sentence(5),
       		'user_id'=>$faker->sentence(5),
       		'pateint_id'=>$faker->sentence(5),
       		'is_read'=>$faker->sentence(5),
       		'message'=>$faker->sentence(5),
       		'date'=>$faker->sentence(5),
       		'from'=>$faker->sentence(5),
       		'to'=>$faker->sentence(5),
       		'type'=>$faker->sentence(5),
       		'file'=>$faker->sentence(5),
       		'voice'=>$faker->sentence(5),
       		'who_send'=>$faker->sentence(5),

       		
       		
        ]);
        }
    }
}
