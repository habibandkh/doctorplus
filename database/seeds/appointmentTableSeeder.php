<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
use Faker\factory as Faker;


class appointmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
            $faker =faker::create();
        foreach(range(1,300)as $index){

            DB::table('appointment')->insert([
       		'appID'=>$faker->sentence(5),
       		'doctor_id'=>$faker->sentence(5),
       		'pateint_id'=>$faker->sentence(5),
       		'user_id'=>$faker->sentence(5),
       		'clinic_address_id'=>$faker->sentence(5),
       		'appointment_date'=>$faker->sentence(5),
       		'status'=>$faker->sentence(5),
       		'feedback'=>$faker->sentence(5),
       		'cancel_by_user'=>$faker->sentence(5),
       		'cancel_by_doctor'=>$faker->sentence(5),
       		'day'=>$faker->sentence(5),
       		
       		
        ]);
        }
    }
}
