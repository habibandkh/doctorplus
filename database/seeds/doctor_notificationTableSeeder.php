<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
use Faker\factory as Faker;


class doctor_notificationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $faker =faker::create();
        foreach(range(1,300)as $index){

            DB::table('doctor_notification')->insert([
       		'doctor_id'=>$faker->sentence(5),
       		'user_id'=>$faker->sentence(5),
       		
       		'status'=>$faker->sentence(5),
       		'text'=>$faker->sentence(5),
       		'action'=>$faker->sentence(5),
       		'patient_id'=>$faker->sentence(5),
       		
       		
       		
       		
        ]);
        }
    }
}
