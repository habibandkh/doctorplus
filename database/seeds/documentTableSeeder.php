<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
use Faker\factory as Faker;


class documentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
              $faker =faker::create();
        foreach(range(1,300)as $index){

            DB::table('document')->insert([
       		'user_id'=>$faker->sentence(5),
       		'doctor_id'=>$faker->sentence(5),
       		'pateint_id'=>$faker->sentence(5),
       		'pateint_name'=>$faker->sentence(5),
       		'doctor_name'=>$faker->sentence(5),
       		'record_type'=>$faker->sentence(5),
       		'date'=>$faker->sentence(5),
       		'allow_share_records'=>$faker->sentence(5),
       		'allow_share_all_records'=>$faker->sentence(5),
       		
       		
 		
        ]);
        }
    }
}
