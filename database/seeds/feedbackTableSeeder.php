<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
use Faker\factory as Faker;


class feedbackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
          $faker =faker::create();
        foreach(range(1,300)as $index){

            DB::table('feedback')->insert([
       		'feedback_doctor_id'=>$faker->sentence(5),
       		'feedback_user_id'=>$faker->sentence(5),
       		'feedback_patient_id'=>$faker->sentence(5),
       		'overall_experience'=>$faker->sentence(5),
       		'doctor_checkup'=>$faker->sentence(5),
       		'staff_behavior'=>$faker->sentence(5),
       		'clinic_environment'=>$faker->sentence(5),
       		'comment'=>$faker->sentence(5),
       		'date'=>$faker->sentence(5),
       		
       		
       		
 		
        ]);
        }
    }
}
