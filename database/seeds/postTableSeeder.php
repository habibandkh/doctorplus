<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
use Faker\factory as Faker;


class postTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $faker =faker::create();
        foreach(range(1,300)as $index){

            DB::table('post')->insert([
       		'category_id'=>$faker->sentence(5),
       		'post_title'=>$faker->sentence(5),
       		'post_body'=>$faker->sentence(5),
       		'post_img'=>$faker->sentence(5),
       		'post_view'=>$faker->sentence(5),
       		'status'=>$faker->sentence(5),
       		'date'=>$faker->sentence(5),
       		'lang'=>$faker->sentence(5),
       		
       		
       		
       		
 		
        ]);
        }
    }
}
