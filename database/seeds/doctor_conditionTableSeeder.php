<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
//use Faker\factory as Faker;


class doctor_conditionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
           

            DB::table('doctor_condition')->insert([
[
'specialty_id' => '61', 
'name' => 'Amenorrhoea' 
 ],
[
'specialty_id' => '61', 
'name' => 'Aplastic Anemia' 
 ],
[
'specialty_id' => '61', 
'name' => 'Ectopic Pregnancy' 
 ],
[
'specialty_id' => '61', 
'name' => 'Endometriosis' 
 ],
[
'specialty_id' => '61', 
'name' => 'Female Infertility' 
 ],
[
'specialty_id' => '61', 
'name' => 'Female Urinary Problems' 
 ],
[
'specialty_id' => '61', 
'name' => 'Fibroids' 
 ],
[
'specialty_id' => '61', 
'name' => 'Gynaecological Tumors' 
 ],
[
'specialty_id' => '61', 
'name' => 'High Risk Pregnancy' 
 ],
[
'specialty_id' => '61', 
'name' => 'Infectious Diseases' 
 ],
[
'specialty_id' => '61', 
'name' => 'Menopause' 
 ],
[
'specialty_id' => '61', 
'name' => 'Menstruation Problems' 
 ],
[
'specialty_id' => '61', 
'name' => 'Miscarriage' 
 ],
[
'specialty_id' => '61', 
'name' => 'Ovarian Cyst' 
 ],
[
'specialty_id' => '61', 
'name' => 'Polycystic Ovary Syndrome (PCOS)' 
 ],
[
'specialty_id' => '61', 
'name' => 'Post Menopausal Bleeding' 
 ],
[
'specialty_id' => '61', 
'name' => 'Preeclampsia' 
 ],
[
'specialty_id' => '61', 
'name' => 'Pregnancy' 
 ],
[
'specialty_id' => '61', 
'name' => 'Urinary Tract Infections (UTI)' 
 ],
[
'specialty_id' => '61', 
'name' => 'Uro Gynae Cancer' 
 ],
[
'specialty_id' => '14', 
'name' => 'Emotional Outbursts' 
 ],
[
'specialty_id' => '18', 
'name' => 'Emotional Outbursts' 
 ],
[
'specialty_id' => '62', 
'name' => 'Erectile Dsyfunction' 
 ],
[
'specialty_id' => '62', 
'name' => 'Female Urinary Incontinence' 
 ],
[
'specialty_id' => '62', 
'name' => 'Haematuria' 
 ],
[
'specialty_id' => '62', 
'name' => 'Low Testosterone' 
 ],
[
'specialty_id' => '62', 
'name' => 'Male Infertility ' 
 ],
[
'specialty_id' => '62', 
'name' => 'Male Urinary Incontinence' 
 ],
[
'specialty_id' => '62', 
'name' => 'Peyronie’s Disease' 
 ],
[
'specialty_id' => '62', 
'name' => 'Premature Ejaculation' 
 ],
[
'specialty_id' => '62', 
'name' => 'Prostate Disease ' 
 ],
[
'specialty_id' => '62', 
'name' => 'Urinary Stones' 
 ],
[
'specialty_id' => '19', 
'name' => 'Balance Disorder' 
 ],
[
'specialty_id' => '19', 
'name' => 'Benign Paroxysmal Positional Vertigo (BPPV)' 
 ],
[
'specialty_id' => '19', 
'name' => 'Dizziness' 
 ],
[
'specialty_id' => '19', 
'name' => 'Hearing Loss' 
 ],
[
'specialty_id' => '2', 
'name' => 'Angina ' 
 ],
[
'specialty_id' => '2', 
'name' => 'Atherosclerosis' 
 ],
[
'specialty_id' => '2', 
'name' => 'Atrial Fibrillation' 
 ],
[
'specialty_id' => '2', 
'name' => 'Cardiac Arrest' 
 ],
[
'specialty_id' => '2', 
'name' => 'Cardiology Diseases' 
 ],
[
'specialty_id' => '2', 
'name' => 'Cardiovascular Diseases' 
 ],
[
'specialty_id' => '2', 
'name' => 'Congenital Diseases' 
 ],
[
'specialty_id' => '2', 
'name' => 'Heart Attack' 
 ],
[
'specialty_id' => '2', 
'name' => 'Heart Burn' 
 ],
[
'specialty_id' => '2', 
'name' => 'Heart Diseases' 
 ],
[
'specialty_id' => '2', 
'name' => 'Heart Failure' 
 ],
[
'specialty_id' => '2', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '2', 
'name' => 'Ischemic Heart Disease' 
 ],
[
'specialty_id' => '2', 
'name' => 'Myocardial Infarction (MI)' 
 ],
[
'specialty_id' => '2', 
'name' => 'Palpitations' 
 ],
[
'specialty_id' => '2', 
'name' => 'Pulmonary Edema' 
 ],
[
'specialty_id' => '2', 
'name' => 'Rheumatic Heart Disease' 
 ],
[
'specialty_id' => '2', 
'name' => 'Tachycardia' 
 ],
[
'specialty_id' => '3', 
'name' => 'Aorta Transaction' 
 ],
[
'specialty_id' => '3', 
'name' => 'Aortic Dissection (Bleeding in the Aorta Walls)' 
 ],
[
'specialty_id' => '3', 
'name' => 'Atrial Fibrillation' 
 ],
[
'specialty_id' => '3', 
'name' => 'Congestive Heart Failure (inffective heart muscle)' 
 ],
[
'specialty_id' => '3', 
'name' => 'Coronary Artery Disease (artery bloackage)' 
 ],
[
'specialty_id' => '3', 
'name' => 'Esophageal Cancer and Benign Conditions of the Eso' 
 ],
[
'specialty_id' => '3', 
'name' => 'Left Ventricular Aneurysm (Stretched heart muscle)' 
 ],
[
'specialty_id' => '3', 
'name' => 'Left Ventricular Aneurysm (Stretched heart muscle)' 
 ],
[
'specialty_id' => '3', 
'name' => 'Lungs Cancer' 
 ],
[
'specialty_id' => '3', 
'name' => 'Mediastinal Tumors' 
 ],
[
'specialty_id' => '3', 
'name' => 'Myocardial Infarction (Heart Attack)' 
 ],
[
'specialty_id' => '3', 
'name' => 'Thoracic Aortic Aneurysm (Streched Aorta in chest)' 
 ],
[
'specialty_id' => '3', 
'name' => 'Thoracoabdominal Aortic Aneurysm' 
 ],
[
'specialty_id' => '3', 
'name' => 'Tracheal Stenosis' 
 ],
[
'specialty_id' => '3', 
'name' => 'Tracheobronchial Cancer' 
 ],
[
'specialty_id' => '3', 
'name' => 'Valvular Insufficiency (Leaking or regurgitation)' 
 ],
[
'specialty_id' => '3', 
'name' => 'Valvular Stenosis (Blocked or Narrowe' 
 ],
[
'specialty_id' => '140', 
'name' => 'Anxiety ' 
 ],
[
'specialty_id' => '140', 
'name' => 'Back Pain' 
 ],
[
'specialty_id' => '140', 
'name' => 'Car Accident Injuries' 
 ],
[
'specialty_id' => '140', 
'name' => 'Chronic Fatigue' 
 ],
[
'specialty_id' => '140', 
'name' => 'Chronic Pain' 
 ],
[
'specialty_id' => '140', 
'name' => 'Dizziness' 
 ],
[
'specialty_id' => '140', 
'name' => 'Fibromyalgia' 
 ],
[
'specialty_id' => '140', 
'name' => 'Headaches' 
 ],
[
'specialty_id' => '140', 
'name' => 'Herniated Disc' 
 ],
[
'specialty_id' => '140', 
'name' => 'Hiatal Hernia' 
 ],
[
'specialty_id' => '140', 
'name' => 'Migraine Headache' 
 ],
[
'specialty_id' => '140', 
'name' => 'Musculoskeletal Problems' 
 ],
[
'specialty_id' => '140', 
'name' => 'Neck Pain' 
 ],
[
'specialty_id' => '140', 
'name' => 'Sciatica' 
 ],
[
'specialty_id' => '140', 
'name' => 'Sports Injury' 
 ],
[
'specialty_id' => '7', 
'name' => 'Canine Teeth' 
 ],
[
'specialty_id' => '7', 
'name' => 'Jaw Joint Problems' 
 ],
[
'specialty_id' => '7', 
'name' => 'Toothache' 
 ],
[
'specialty_id' => '42', 
'name' => 'Acne' 
 ],
[
'specialty_id' => '42', 
'name' => 'Acne Scars' 
 ],
[
'specialty_id' => '42', 
'name' => 'Benign Skin Tumors' 
 ],
[
'specialty_id' => '42', 
'name' => 'Eczema' 
 ],
[
'specialty_id' => '42', 
'name' => 'Facial Disfigurement' 
 ],
[
'specialty_id' => '42', 
'name' => 'Herpes' 
 ],
[
'specialty_id' => '42', 
'name' => 'Malignant Skin Tumors' 
 ],
[
'specialty_id' => '42', 
'name' => 'Moles' 
 ],
[
'specialty_id' => '42', 
'name' => 'Psoriasis' 
 ],
[
'specialty_id' => '42', 
'name' => 'Scabies' 
 ],
[
'specialty_id' => '42', 
'name' => 'Skin Allergy' 
 ],
[
'specialty_id' => '42', 
'name' => 'Skin Diseases' 
 ],
[
'specialty_id' => '42', 
'name' => 'Skin Fungal Infections' 
 ],
[
'specialty_id' => '42', 
'name' => 'Syphilis' 
 ],
[
'specialty_id' => '42', 
'name' => 'Vitiligo' 
 ],
[
'specialty_id' => '42', 
'name' => 'Warts' 
 ],
[
'specialty_id' => '13', 
'name' => 'Canine Teeth' 
 ],
[
'specialty_id' => '13', 
'name' => 'Cavities' 
 ],
[
'specialty_id' => '13', 
'name' => 'Dental Caries' 
 ],
[
'specialty_id' => '13', 
'name' => 'Dupuytren’s Contracture (Hypodontia)' 
 ],
[
'specialty_id' => '13', 
'name' => 'Gingivitis' 
 ],
[
'specialty_id' => '13', 
'name' => 'Gum Disease (Periodontal)' 
 ],
[
'specialty_id' => '13', 
'name' => 'Jaw Joint Problems' 
 ],
[
'specialty_id' => '13', 
'name' => 'Orofacial Pain' 
 ],
[
'specialty_id' => '13', 
'name' => 'Periodontitis' 
 ],
[
'specialty_id' => '13', 
'name' => 'TMJ Disorders' 
 ],
[
'specialty_id' => '13', 
'name' => 'Tooth Decay' 
 ],
[
'specialty_id' => '13', 
'name' => 'Toothache' 
 ],
[
'specialty_id' => '15', 
'name' => 'Acne' 
 ],
[
'specialty_id' => '15', 
'name' => 'Acne Scars' 
 ],
[
'specialty_id' => '15', 
'name' => 'Benign And Malignant Skin Tumors' 
 ],
[
'specialty_id' => '15', 
'name' => 'Eczema' 
 ],
[
'specialty_id' => '15', 
'name' => 'Herpes' 
 ],
[
'specialty_id' => '15', 
'name' => 'Moles' 
 ],
[
'specialty_id' => '15', 
'name' => 'Psoriasis' 
 ],
[
'specialty_id' => '15', 
'name' => 'Scabies' 
 ],
[
'specialty_id' => '15', 
'name' => 'Skin Allergy' 
 ],
[
'specialty_id' => '15', 
'name' => 'Skin Diseases' 
 ],
[
'specialty_id' => '15', 
'name' => 'Skin Fungal Infections' 
 ],
[
'specialty_id' => '15', 
'name' => 'Syphilis' 
 ],
[
'specialty_id' => '15', 
'name' => 'Vitiligo' 
 ],
[
'specialty_id' => '15', 
'name' => 'Warts' 
 ],
[
'specialty_id' => '17', 
'name' => 'Acne' 
 ],
[
'specialty_id' => '17', 
'name' => 'Acne Scars' 
 ],
[
'specialty_id' => '17', 
'name' => 'Benign And Malignant Skin Tumors' 
 ],
[
'specialty_id' => '17', 
'name' => 'Eczema' 
 ],
[
'specialty_id' => '17', 
'name' => 'Herpes' 
 ],
[
'specialty_id' => '17', 
'name' => 'Moles' 
 ],
[
'specialty_id' => '17', 
'name' => 'Psoriasis' 
 ],
[
'specialty_id' => '17', 
'name' => 'Scabies' 
 ],
[
'specialty_id' => '17', 
'name' => 'Skin Allergy' 
 ],
[
'specialty_id' => '17', 
'name' => 'Skin Diseases' 
 ],
[
'specialty_id' => '17', 
'name' => 'Skin Fungal Infections' 
 ],
[
'specialty_id' => '17', 
'name' => 'Syphilis' 
 ],
[
'specialty_id' => '17', 
'name' => 'Vitiligo' 
 ],
[
'specialty_id' => '17', 
'name' => 'Warts' 
 ],
[
'specialty_id' => '18', 
'name' => 'Acne' 
 ],
[
'specialty_id' => '18', 
'name' => 'Acne Scars' 
 ],
[
'specialty_id' => '18', 
'name' => 'Benign And Malignant Skin Tumors' 
 ],
[
'specialty_id' => '18', 
'name' => 'Eczema' 
 ],
[
'specialty_id' => '18', 
'name' => 'Herpes' 
 ],
[
'specialty_id' => '18', 
'name' => 'Moles' 
 ],
[
'specialty_id' => '18', 
'name' => 'Psoriasis' 
 ],
[
'specialty_id' => '18', 
'name' => 'Scabies' 
 ],
[
'specialty_id' => '18', 
'name' => 'Skin Allergy' 
 ],
[
'specialty_id' => '18', 
'name' => 'Skin Diseases' 
 ],
[
'specialty_id' => '18', 
'name' => 'Skin Fungal Infections' 
 ],
[
'specialty_id' => '18', 
'name' => 'Syphilis' 
 ],
[
'specialty_id' => '18', 
'name' => 'Vitiligo' 
 ],
[
'specialty_id' => '18', 
'name' => 'Warts' 
 ],
[
'specialty_id' => '71', 
'name' => 'Abdomen Pain' 
 ],
[
'specialty_id' => '71', 
'name' => 'Addison’s Disease' 
 ],
[
'specialty_id' => '71', 
'name' => 'Angina' 
 ],
[
'specialty_id' => '71', 
'name' => 'Arrythmias' 
 ],
[
'specialty_id' => '71', 
'name' => 'Atrial Fibrillation' 
 ],
[
'specialty_id' => '71', 
'name' => 'Cancers of the Endocrine Gland' 
 ],
[
'specialty_id' => '71', 
'name' => 'Cardiology Disease' 
 ],
[
'specialty_id' => '71', 
'name' => 'Cardiovascular Diseases' 
 ],
[
'specialty_id' => '71', 
'name' => 'Cholesterol Disorders' 
 ],
[
'specialty_id' => '71', 
'name' => 'Chronic Hepatitis' 
 ],
[
'specialty_id' => '71', 
'name' => 'Cushing’s Syndrome' 
 ],
[
'specialty_id' => '71', 
'name' => 'Diabetes Insipidus' 
 ],
[
'specialty_id' => '71', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '71', 
'name' => 'Diarrhea' 
 ],
[
'specialty_id' => '71', 
'name' => 'GERD (Gastroesophageal Reflux Disease)' 
 ],
[
'specialty_id' => '71', 
'name' => 'Goiter' 
 ],
[
'specialty_id' => '71', 
'name' => 'Heart Diseases' 
 ],
[
'specialty_id' => '71', 
'name' => 'Heart Failure' 
 ],
[
'specialty_id' => '71', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '71', 
'name' => 'Hyperthyroidism' 
 ],
[
'specialty_id' => '71', 
'name' => 'Hypoglycemia' 
 ],
[
'specialty_id' => '71', 
'name' => 'Hypothyroidism' 
 ],
[
'specialty_id' => '71', 
'name' => 'Infertility' 
 ],
[
'specialty_id' => '71', 
'name' => 'Ischemic Heart Disease' 
 ],
[
'specialty_id' => '71', 
'name' => 'Lack of Growth' 
 ],
[
'specialty_id' => '71', 
'name' => 'Menopause' 
 ],
[
'specialty_id' => '71', 
'name' => 'Metabolic Disorder' 
 ],
[
'specialty_id' => '71', 
'name' => 'Myocardial Infarction (MI)' 
 ],
[
'specialty_id' => '71', 
'name' => 'Osteoporosis' 
 ],
[
'specialty_id' => '71', 
'name' => 'Over or Under Production of Hormones' 
 ],
[
'specialty_id' => '71', 
'name' => 'Palpitations' 
 ],
[
'specialty_id' => '71', 
'name' => 'Pulmonary Edema' 
 ],
[
'specialty_id' => '71', 
'name' => 'Rheumatic Heart Disease' 
 ],
[
'specialty_id' => '71', 
'name' => 'Tachycardia' 
 ],
[
'specialty_id' => '71', 
'name' => 'Thyroid Disease' 
 ],
[
'specialty_id' => '71', 
'name' => 'Turner Syndrome' 
 ],
[
'specialty_id' => '183', 
'name' => 'Addison’s Disease' 
 ],
[
'specialty_id' => '183', 
'name' => 'Cholesterol Disorders' 
 ],
[
'specialty_id' => '183', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '183', 
'name' => 'Heart Diseases hypertension' 
 ],
[
'specialty_id' => '183', 
'name' => 'Hyperthyroidism' 
 ],
[
'specialty_id' => '183', 
'name' => 'Hypoglycemia' 
 ],
[
'specialty_id' => '183', 
'name' => 'Hypothyroidism' 
 ],
[
'specialty_id' => '183', 
'name' => 'Lack of Growth' 
 ],
[
'specialty_id' => '183', 
'name' => 'Metabolic Disorder' 
 ],
[
'specialty_id' => '183', 
'name' => 'Thyroid Disease' 
 ],
[
'specialty_id' => '182', 
'name' => 'Addison’s Disease' 
 ],
[
'specialty_id' => '182', 
'name' => 'Cancers of the Endocrine Gland' 
 ],
[
'specialty_id' => '182', 
'name' => 'Cholesterol Disorders' 
 ],
[
'specialty_id' => '182', 
'name' => 'Cushing’s syndrome' 
 ],
[
'specialty_id' => '182', 
'name' => 'Diabetes Insipidus' 
 ],
[
'specialty_id' => '182', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '182', 
'name' => 'Goiter hypertension' 
 ],
[
'specialty_id' => '182', 
'name' => 'Hyperthyroidism' 
 ],
[
'specialty_id' => '182', 
'name' => 'Hypoglycemia' 
 ],
[
'specialty_id' => '182', 
'name' => 'Hypothyroidism' 
 ],
[
'specialty_id' => '182', 
'name' => 'Infertility' 
 ],
[
'specialty_id' => '182', 
'name' => 'Lack of Growth' 
 ],
[
'specialty_id' => '182', 
'name' => 'Menopause' 
 ],
[
'specialty_id' => '182', 
'name' => 'Metabolic Disorder' 
 ],
[
'specialty_id' => '182', 
'name' => 'Osteoporosis' 
 ],
[
'specialty_id' => '182', 
'name' => 'Over or Under Production of Hormones' 
 ],
[
'specialty_id' => '182', 
'name' => 'Thyroid Disease' 
 ],
[
'specialty_id' => '182', 
'name' => 'Tumor syndrome.' 
 ],
[
'specialty_id' => '125', 
'name' => 'Addison’s Disease' 
 ],
[
'specialty_id' => '125', 
'name' => 'Cancers of the Endocrine Gland' 
 ],
[
'specialty_id' => '125', 
'name' => 'Cholesterol Disorders' 
 ],
[
'specialty_id' => '125', 
'name' => 'Cushing’s syndrome' 
 ],
[
'specialty_id' => '125', 
'name' => 'Diabetes Insipidus' 
 ],
[
'specialty_id' => '125', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '125', 
'name' => 'Goiter hypertension' 
 ],
[
'specialty_id' => '125', 
'name' => 'Hyperthyroidism' 
 ],
[
'specialty_id' => '125', 
'name' => 'Hypoglycemia' 
 ],
[
'specialty_id' => '125', 
'name' => 'Hypothyroidism' 
 ],
[
'specialty_id' => '125', 
'name' => 'Infertility' 
 ],
[
'specialty_id' => '125', 
'name' => 'Lack of Growth' 
 ],
[
'specialty_id' => '125', 
'name' => 'Menopause' 
 ],
[
'specialty_id' => '125', 
'name' => 'Metabolic Disorder' 
 ],
[
'specialty_id' => '125', 
'name' => 'Osteoporosis' 
 ],
[
'specialty_id' => '125', 
'name' => 'Over or Under Production of Hormones' 
 ],
[
'specialty_id' => '125', 
'name' => 'Thyroid Disease' 
 ],
[
'specialty_id' => '125', 
'name' => 'Tumor syndrome.' 
 ],
[
'specialty_id' => '72', 
'name' => 'Addison’s Disease' 
 ],
[
'specialty_id' => '72', 
'name' => 'Cancers of the Endocrine Gland' 
 ],
[
'specialty_id' => '72', 
'name' => 'Cholesterol Disorders' 
 ],
[
'specialty_id' => '72', 
'name' => 'Cushing’s syndrome' 
 ],
[
'specialty_id' => '72', 
'name' => 'Diabetes Insipidus' 
 ],
[
'specialty_id' => '72', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '72', 
'name' => 'Goiter hypertension' 
 ],
[
'specialty_id' => '72', 
'name' => 'Hyperthyroidism' 
 ],
[
'specialty_id' => '72', 
'name' => 'Hypoglycemia' 
 ],
[
'specialty_id' => '72', 
'name' => 'Hypothyroidism' 
 ],
[
'specialty_id' => '72', 
'name' => 'Infertility' 
 ],
[
'specialty_id' => '72', 
'name' => 'Lack of Growth' 
 ],
[
'specialty_id' => '72', 
'name' => 'Menopause' 
 ],
[
'specialty_id' => '72', 
'name' => 'Metabolic Disorder' 
 ],
[
'specialty_id' => '72', 
'name' => 'Osteoporosis' 
 ],
[
'specialty_id' => '72', 
'name' => 'Over or Under Production of Hormones' 
 ],
[
'specialty_id' => '72', 
'name' => 'Thyroid Disease' 
 ],
[
'specialty_id' => '72', 
'name' => 'Tumor syndrome.' 
 ],
[
'specialty_id' => '64', 
'name' => 'Addison’s Disease' 
 ],
[
'specialty_id' => '64', 
'name' => 'Cancers of the Endocrine Gland' 
 ],
[
'specialty_id' => '64', 
'name' => 'Cholesterol Disorders' 
 ],
[
'specialty_id' => '64', 
'name' => 'Cushing’s syndrome' 
 ],
[
'specialty_id' => '64', 
'name' => 'Diabetes Insipidus' 
 ],
[
'specialty_id' => '64', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '64', 
'name' => 'Goiter hypertension' 
 ],
[
'specialty_id' => '64', 
'name' => 'Hyperthyroidism' 
 ],
[
'specialty_id' => '64', 
'name' => 'Hypoglycemia' 
 ],
[
'specialty_id' => '64', 
'name' => 'Hypothyroidism' 
 ],
[
'specialty_id' => '64', 
'name' => 'Infertility' 
 ],
[
'specialty_id' => '64', 
'name' => 'Lack of Growth' 
 ],
[
'specialty_id' => '64', 
'name' => 'Menopause' 
 ],
[
'specialty_id' => '64', 
'name' => 'Metabolic Disorder' 
 ],
[
'specialty_id' => '64', 
'name' => 'Osteoporosis' 
 ],
[
'specialty_id' => '64', 
'name' => 'Over or Under Production of Hormones' 
 ],
[
'specialty_id' => '64', 
'name' => 'Thyroid Disease' 
 ],
[
'specialty_id' => '64', 
'name' => 'Tumor syndrome.' 
 ],
[
'specialty_id' => '20', 
'name' => 'Adenoids Problems (Children)' 
 ],
[
'specialty_id' => '20', 
'name' => 'Allergic Rhinitis' 
 ],
[
'specialty_id' => '20', 
'name' => 'Choking' 
 ],
[
'specialty_id' => '20', 
'name' => 'Cold and Influenza' 
 ],
[
'specialty_id' => '20', 
'name' => 'Deviated Nasal Septum' 
 ],
[
'specialty_id' => '20', 
'name' => 'Difficulty in Swallowing' 
 ],
[
'specialty_id' => '20', 
'name' => 'Diphtheria' 
 ],
[
'specialty_id' => '20', 
'name' => 'Dust Allergy' 
 ],
[
'specialty_id' => '20', 
'name' => 'Dysphagia' 
 ],
[
'specialty_id' => '20', 
'name' => 'Ear Drum Rupture' 
 ],
[
'specialty_id' => '20', 
'name' => 'Ear Pain' 
 ],
[
'specialty_id' => '20', 
'name' => 'Hay Fever' 
 ],
[
'specialty_id' => '20', 
'name' => 'Head and Neck Infections' 
 ],
[
'specialty_id' => '20', 
'name' => 'Nasal Injuries' 
 ],
[
'specialty_id' => '20', 
'name' => 'Nasal Obstruction' 
 ],
[
'specialty_id' => '20', 
'name' => 'Nasal Polyps' 
 ],
[
'specialty_id' => '20', 
'name' => 'Pollen Allergy' 
 ],
[
'specialty_id' => '20', 
'name' => 'Rhinitis' 
 ],
[
'specialty_id' => '20', 
'name' => 'Sinus Infection' 
 ],
[
'specialty_id' => '20', 
'name' => 'Sinusitis' 
 ],
[
'specialty_id' => '20', 
'name' => 'Snoring in Children' 
 ],
[
'specialty_id' => '20', 
'name' => 'Tonsillitis' 
 ],
[
'specialty_id' => '20', 
'name' => 'Vertigo (Dizziness)' 
 ],
[
'specialty_id' => '21', 
'name' => 'Adenoids Problems (Children)' 
 ],
[
'specialty_id' => '21', 
'name' => 'Allergic Rhinitis' 
 ],
[
'specialty_id' => '21', 
'name' => 'Choking' 
 ],
[
'specialty_id' => '21', 
'name' => 'Cold and Influenza' 
 ],
[
'specialty_id' => '21', 
'name' => 'Deviated Nasal Septum' 
 ],
[
'specialty_id' => '21', 
'name' => 'Difficulty in Swallowing' 
 ],
[
'specialty_id' => '21', 
'name' => 'Diphtheria' 
 ],
[
'specialty_id' => '21', 
'name' => 'Dust Allergy' 
 ],
[
'specialty_id' => '21', 
'name' => 'Dysphagia' 
 ],
[
'specialty_id' => '21', 
'name' => 'Ear Drum Rupture' 
 ],
[
'specialty_id' => '21', 
'name' => 'Ear Pain' 
 ],
[
'specialty_id' => '21', 
'name' => 'Hay Fever' 
 ],
[
'specialty_id' => '21', 
'name' => 'Head and Neck Infections' 
 ],
[
'specialty_id' => '21', 
'name' => 'Nasal Injuries' 
 ],
[
'specialty_id' => '21', 
'name' => 'Nasal Obstruction' 
 ],
[
'specialty_id' => '21', 
'name' => 'Nasal Polyps' 
 ],
[
'specialty_id' => '21', 
'name' => 'Pollen Allergy' 
 ],
[
'specialty_id' => '21', 
'name' => 'Rhinitis' 
 ],
[
'specialty_id' => '21', 
'name' => 'Sinus Infection' 
 ],
[
'specialty_id' => '21', 
'name' => 'Sinusitis' 
 ],
[
'specialty_id' => '21', 
'name' => 'Snoring in Children' 
 ],
[
'specialty_id' => '21', 
'name' => 'Tonsillitis' 
 ],
[
'specialty_id' => '21', 
'name' => 'Vertigo (Dizziness)' 
 ],
[
'specialty_id' => '22', 
'name' => 'Blepharitis' 
 ],
[
'specialty_id' => '22', 
'name' => 'Chalazion' 
 ],
[
'specialty_id' => '22', 
'name' => 'Conjunctivitis' 
 ],
[
'specialty_id' => '22', 
'name' => 'Diabetic Retinopathy' 
 ],
[
'specialty_id' => '22', 
'name' => 'Diplopia' 
 ],
[
'specialty_id' => '22', 
'name' => 'Dry Eye' 
 ],
[
'specialty_id' => '22', 
'name' => 'Eye Allergy' 
 ],
[
'specialty_id' => '22', 
'name' => 'Glaucoma' 
 ],
[
'specialty_id' => '22', 
'name' => 'Macular Hole' 
 ],
[
'specialty_id' => '22', 
'name' => 'Ocular Trauma' 
 ],
[
'specialty_id' => '22', 
'name' => 'Red Eye' 
 ],
[
'specialty_id' => '22', 
'name' => 'Retina Detachment' 
 ],
[
'specialty_id' => '22', 
'name' => 'Squint' 
 ],
[
'specialty_id' => '22', 
'name' => 'Strabismus' 
 ],
[
'specialty_id' => '22', 
'name' => 'Stye' 
 ],
[
'specialty_id' => '23', 
'name' => 'Blepharitis' 
 ],
[
'specialty_id' => '23', 
'name' => 'Chalazion' 
 ],
[
'specialty_id' => '23', 
'name' => 'Conjunctivitis' 
 ],
[
'specialty_id' => '23', 
'name' => 'Diabetic Retinopathy' 
 ],
[
'specialty_id' => '23', 
'name' => 'Diplopia' 
 ],
[
'specialty_id' => '23', 
'name' => 'Dry Eye' 
 ],
[
'specialty_id' => '23', 
'name' => 'Eye Allergy' 
 ],
[
'specialty_id' => '23', 
'name' => 'Glaucoma' 
 ],
[
'specialty_id' => '23', 
'name' => 'Macular Hole' 
 ],
[
'specialty_id' => '23', 
'name' => 'Ocular Trauma' 
 ],
[
'specialty_id' => '23', 
'name' => 'Red Eye' 
 ],
[
'specialty_id' => '23', 
'name' => 'Retina Detachment' 
 ],
[
'specialty_id' => '23', 
'name' => 'Squint' 
 ],
[
'specialty_id' => '23', 
'name' => 'Strabismus' 
 ],
[
'specialty_id' => '23', 
'name' => 'Stye' 
 ],
[
'specialty_id' => '24', 
'name' => 'Blepharitis' 
 ],
[
'specialty_id' => '24', 
'name' => 'Chalazion' 
 ],
[
'specialty_id' => '24', 
'name' => 'Conjunctivitis' 
 ],
[
'specialty_id' => '24', 
'name' => 'Diabetic Retinopathy' 
 ],
[
'specialty_id' => '24', 
'name' => 'Diplopia' 
 ],
[
'specialty_id' => '24', 
'name' => 'Dry Eye' 
 ],
[
'specialty_id' => '24', 
'name' => 'Eye Allergy' 
 ],
[
'specialty_id' => '24', 
'name' => 'Glaucoma' 
 ],
[
'specialty_id' => '24', 
'name' => 'Macular Hole' 
 ],
[
'specialty_id' => '24', 
'name' => 'Ocular Trauma' 
 ],
[
'specialty_id' => '24', 
'name' => 'Red Eye' 
 ],
[
'specialty_id' => '24', 
'name' => 'Retina Detachment' 
 ],
[
'specialty_id' => '24', 
'name' => 'Squint' 
 ],
[
'specialty_id' => '24', 
'name' => 'Strabismus' 
 ],
[
'specialty_id' => '24', 
'name' => 'Stye' 
 ],
[
'specialty_id' => '25', 
'name' => 'Blepharitis' 
 ],
[
'specialty_id' => '25', 
'name' => 'Chalazion' 
 ],
[
'specialty_id' => '25', 
'name' => 'Conjunctivitis' 
 ],
[
'specialty_id' => '25', 
'name' => 'Diabetic Retinopathy' 
 ],
[
'specialty_id' => '25', 
'name' => 'Diplopia' 
 ],
[
'specialty_id' => '25', 
'name' => 'Dry Eye' 
 ],
[
'specialty_id' => '25', 
'name' => 'Eye Allergy' 
 ],
[
'specialty_id' => '25', 
'name' => 'Glaucoma' 
 ],
[
'specialty_id' => '25', 
'name' => 'Macular Hole' 
 ],
[
'specialty_id' => '25', 
'name' => 'Ocular Trauma' 
 ],
[
'specialty_id' => '25', 
'name' => 'Red Eye' 
 ],
[
'specialty_id' => '25', 
'name' => 'Retina Detachment' 
 ],
[
'specialty_id' => '25', 
'name' => 'Squint' 
 ],
[
'specialty_id' => '25', 
'name' => 'Strabismus' 
 ],
[
'specialty_id' => '25', 
'name' => 'Stye' 
 ],
[
'specialty_id' => '26', 
'name' => 'Blepharitis' 
 ],
[
'specialty_id' => '26', 
'name' => 'Chalazion' 
 ],
[
'specialty_id' => '26', 
'name' => 'Conjunctivitis' 
 ],
[
'specialty_id' => '26', 
'name' => 'Diabetic Retinopathy' 
 ],
[
'specialty_id' => '26', 
'name' => 'Diplopia' 
 ],
[
'specialty_id' => '26', 
'name' => 'Dry Eye' 
 ],
[
'specialty_id' => '26', 
'name' => 'Eye Allergy' 
 ],
[
'specialty_id' => '26', 
'name' => 'Glaucoma' 
 ],
[
'specialty_id' => '26', 
'name' => 'Macular Hole' 
 ],
[
'specialty_id' => '26', 
'name' => 'Ocular Trauma' 
 ],
[
'specialty_id' => '26', 
'name' => 'Red Eye' 
 ],
[
'specialty_id' => '26', 
'name' => 'Retina Detachment' 
 ],
[
'specialty_id' => '26', 
'name' => 'Squint' 
 ],
[
'specialty_id' => '26', 
'name' => 'Strabismus' 
 ],
[
'specialty_id' => '26', 
'name' => 'Stye' 
 ],
[
'specialty_id' => '27', 
'name' => 'Blepharitis' 
 ],
[
'specialty_id' => '27', 
'name' => 'Chalazion' 
 ],
[
'specialty_id' => '27', 
'name' => 'Conjunctivitis' 
 ],
[
'specialty_id' => '27', 
'name' => 'Diabetic Retinopathy' 
 ],
[
'specialty_id' => '27', 
'name' => 'Diplopia' 
 ],
[
'specialty_id' => '27', 
'name' => 'Dry Eye' 
 ],
[
'specialty_id' => '27', 
'name' => 'Eye Allergy' 
 ],
[
'specialty_id' => '27', 
'name' => 'Glaucoma' 
 ],
[
'specialty_id' => '27', 
'name' => 'Macular Hole' 
 ],
[
'specialty_id' => '27', 
'name' => 'Ocular Trauma' 
 ],
[
'specialty_id' => '27', 
'name' => 'Red Eye' 
 ],
[
'specialty_id' => '27', 
'name' => 'Retina Detachment' 
 ],
[
'specialty_id' => '27', 
'name' => 'Squint' 
 ],
[
'specialty_id' => '27', 
'name' => 'Strabismus' 
 ],
[
'specialty_id' => '27', 
'name' => 'Stye' 
 ],
[
'specialty_id' => '28', 
'name' => 'Blepharitis' 
 ],
[
'specialty_id' => '28', 
'name' => 'Chalazion' 
 ],
[
'specialty_id' => '28', 
'name' => 'Conjunctivitis' 
 ],
[
'specialty_id' => '28', 
'name' => 'Diabetic Retinopathy' 
 ],
[
'specialty_id' => '28', 
'name' => 'Diplopia' 
 ],
[
'specialty_id' => '28', 
'name' => 'Dry Eye' 
 ],
[
'specialty_id' => '28', 
'name' => 'Eye Allergy' 
 ],
[
'specialty_id' => '28', 
'name' => 'Glaucoma' 
 ],
[
'specialty_id' => '28', 
'name' => 'Macular Hole' 
 ],
[
'specialty_id' => '28', 
'name' => 'Ocular Trauma' 
 ],
[
'specialty_id' => '28', 
'name' => 'Red Eye' 
 ],
[
'specialty_id' => '28', 
'name' => 'Retina Detachment' 
 ],
[
'specialty_id' => '28', 
'name' => 'Squint' 
 ],
[
'specialty_id' => '28', 
'name' => 'Strabismus' 
 ],
[
'specialty_id' => '28', 
'name' => 'Stye' 
 ],
[
'specialty_id' => '29', 
'name' => 'Blepharitis' 
 ],
[
'specialty_id' => '29', 
'name' => 'Chalazion' 
 ],
[
'specialty_id' => '29', 
'name' => 'Conjunctivitis' 
 ],
[
'specialty_id' => '29', 
'name' => 'Diabetic Retinopathy' 
 ],
[
'specialty_id' => '29', 
'name' => 'Diplopia' 
 ],
[
'specialty_id' => '29', 
'name' => 'Dry Eye' 
 ],
[
'specialty_id' => '29', 
'name' => 'Eye Allergy' 
 ],
[
'specialty_id' => '29', 
'name' => 'Glaucoma' 
 ],
[
'specialty_id' => '29', 
'name' => 'Macular Hole' 
 ],
[
'specialty_id' => '29', 
'name' => 'Ocular Trauma' 
 ],
[
'specialty_id' => '29', 
'name' => 'Red Eye' 
 ],
[
'specialty_id' => '29', 
'name' => 'Retina Detachment' 
 ],
[
'specialty_id' => '29', 
'name' => 'Squint' 
 ],
[
'specialty_id' => '29', 
'name' => 'Strabismus' 
 ],
[
'specialty_id' => '29', 
'name' => 'Stye' 
 ],
[
'specialty_id' => '30', 
'name' => 'Blepharitis' 
 ],
[
'specialty_id' => '30', 
'name' => 'Chalazion' 
 ],
[
'specialty_id' => '30', 
'name' => 'Conjunctivitis' 
 ],
[
'specialty_id' => '30', 
'name' => 'Diabetic Retinopathy' 
 ],
[
'specialty_id' => '30', 
'name' => 'Diplopia' 
 ],
[
'specialty_id' => '30', 
'name' => 'Dry Eye' 
 ],
[
'specialty_id' => '30', 
'name' => 'Eye Allergy' 
 ],
[
'specialty_id' => '30', 
'name' => 'Glaucoma' 
 ],
[
'specialty_id' => '30', 
'name' => 'Macular Hole' 
 ],
[
'specialty_id' => '30', 
'name' => 'Ocular Trauma' 
 ],
[
'specialty_id' => '30', 
'name' => 'Red Eye' 
 ],
[
'specialty_id' => '30', 
'name' => 'Retina Detachment' 
 ],
[
'specialty_id' => '30', 
'name' => 'Squint' 
 ],
[
'specialty_id' => '30', 
'name' => 'Strabismus' 
 ],
[
'specialty_id' => '30', 
'name' => 'Stye' 
 ],
[
'specialty_id' => '58', 
'name' => 'Erectile Dysfunction' 
 ],
[
'specialty_id' => '58', 
'name' => 'Female Infertility' 
 ],
[
'specialty_id' => '58', 
'name' => 'Male Infertility' 
 ],
[
'specialty_id' => '73', 
'name' => 'Abdomen Pain' 
 ],
[
'specialty_id' => '73', 
'name' => 'Abdominal Pain' 
 ],
[
'specialty_id' => '73', 
'name' => 'Acid Peptic Disease' 
 ],
[
'specialty_id' => '73', 
'name' => 'Anemia' 
 ],
[
'specialty_id' => '73', 
'name' => 'Angina' 
 ],
[
'specialty_id' => '73', 
'name' => 'Arrythmias' 
 ],
[
'specialty_id' => '73', 
'name' => 'Arthritis' 
 ],
[
'specialty_id' => '73', 
'name' => 'Atherosclerosis' 
 ],
[
'specialty_id' => '73', 
'name' => 'Atrial Fibrillation' 
 ],
[
'specialty_id' => '73', 
'name' => 'Back Ache' 
 ],
[
'specialty_id' => '73', 
'name' => 'Black Stools' 
 ],
[
'specialty_id' => '73', 
'name' => 'Bloating' 
 ],
[
'specialty_id' => '73', 
'name' => 'Blood Pressure' 
 ],
[
'specialty_id' => '73', 
'name' => 'Cardiac Arrest' 
 ],
[
'specialty_id' => '73', 
'name' => 'Cardiac Disease' 
 ],
[
'specialty_id' => '73', 
'name' => 'Cardiology Disease' 
 ],
[
'specialty_id' => '73', 
'name' => 'Cardiovascular Disease' 
 ],
[
'specialty_id' => '73', 
'name' => 'Chest Infections' 
 ],
[
'specialty_id' => '73', 
'name' => 'Chicken Hepatitis' 
 ],
[
'specialty_id' => '73', 
'name' => 'Chronic Hepatitis' 
 ],
[
'specialty_id' => '73', 
'name' => 'Cold And Influenza' 
 ],
[
'specialty_id' => '73', 
'name' => 'Common Cold' 
 ],
[
'specialty_id' => '73', 
'name' => 'Congenital Disease' 
 ],
[
'specialty_id' => '73', 
'name' => 'Cough ' 
 ],
[
'specialty_id' => '73', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '73', 
'name' => 'Diarrhea' 
 ],
[
'specialty_id' => '73', 
'name' => 'Dyspepsia' 
 ],
[
'specialty_id' => '73', 
'name' => 'Enteric Fever' 
 ],
[
'specialty_id' => '73', 
'name' => 'Fatigue' 
 ],
[
'specialty_id' => '73', 
'name' => 'Fever' 
 ],
[
'specialty_id' => '73', 
'name' => 'Fissures' 
 ],
[
'specialty_id' => '73', 
'name' => 'Flatulence' 
 ],
[
'specialty_id' => '73', 
'name' => 'Gas' 
 ],
[
'specialty_id' => '73', 
'name' => 'Gastric Ulcer' 
 ],
[
'specialty_id' => '73', 
'name' => 'Gastroenteritis' 
 ],
[
'specialty_id' => '73', 
'name' => 'GERD (Gastroesophageal Reflux Disease)' 
 ],
[
'specialty_id' => '73', 
'name' => 'Heart Attack' 
 ],
[
'specialty_id' => '73', 
'name' => 'Heart Burn' 
 ],
[
'specialty_id' => '73', 
'name' => 'Heart Disease' 
 ],
[
'specialty_id' => '73', 
'name' => 'Heart Failure' 
 ],
[
'specialty_id' => '73', 
'name' => 'Helicobacter Pylori Bacteria' 
 ],
[
'specialty_id' => '73', 
'name' => 'Hemorrhoids' 
 ],
[
'specialty_id' => '73', 
'name' => 'Hepatitis B' 
 ],
[
'specialty_id' => '73', 
'name' => 'Hepatitis C' 
 ],
[
'specialty_id' => '73', 
'name' => 'Hernia' 
 ],
[
'specialty_id' => '73', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '73', 
'name' => 'Infectious Diseases' 
 ],
[
'specialty_id' => '73', 
'name' => 'Ischemic Heart Disease' 
 ],
[
'specialty_id' => '73', 
'name' => 'Jaundice' 
 ],
[
'specialty_id' => '73', 
'name' => 'Liver Disease' 
 ],
[
'specialty_id' => '73', 
'name' => 'Metabolic Syndrome' 
 ],
[
'specialty_id' => '73', 
'name' => 'Muscle Aches' 
 ],
[
'specialty_id' => '126', 
'name' => 'Abdomen Pain' 
 ],
[
'specialty_id' => '126', 
'name' => 'Abdominal Pain' 
 ],
[
'specialty_id' => '126', 
'name' => 'Acid Peptic Disease' 
 ],
[
'specialty_id' => '126', 
'name' => 'Anemia' 
 ],
[
'specialty_id' => '126', 
'name' => 'Angina' 
 ],
[
'specialty_id' => '126', 
'name' => 'Arrythmias' 
 ],
[
'specialty_id' => '126', 
'name' => 'Arthritis' 
 ],
[
'specialty_id' => '126', 
'name' => 'Atherosclerosis' 
 ],
[
'specialty_id' => '126', 
'name' => 'Atrial Fibrillation' 
 ],
[
'specialty_id' => '126', 
'name' => 'Back Ache' 
 ],
[
'specialty_id' => '126', 
'name' => 'Black Stools' 
 ],
[
'specialty_id' => '126', 
'name' => 'Bloating' 
 ],
[
'specialty_id' => '126', 
'name' => 'Blood Pressure' 
 ],
[
'specialty_id' => '126', 
'name' => 'Cardiac Arrest' 
 ],
[
'specialty_id' => '126', 
'name' => 'Cardiac Disease' 
 ],
[
'specialty_id' => '126', 
'name' => 'Cardiology Disease' 
 ],
[
'specialty_id' => '126', 
'name' => 'Cardiovascular Disease' 
 ],
[
'specialty_id' => '126', 
'name' => 'Chest Infections' 
 ],
[
'specialty_id' => '126', 
'name' => 'Chicken Hepatitis' 
 ],
[
'specialty_id' => '126', 
'name' => 'Chronic Hepatitis' 
 ],
[
'specialty_id' => '126', 
'name' => 'Cold And Influenza' 
 ],
[
'specialty_id' => '126', 
'name' => 'Common Cold' 
 ],
[
'specialty_id' => '126', 
'name' => 'Congenital Disease' 
 ],
[
'specialty_id' => '126', 
'name' => 'Cough ' 
 ],
[
'specialty_id' => '126', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '126', 
'name' => 'Diarrhea' 
 ],
[
'specialty_id' => '126', 
'name' => 'Dyspepsia' 
 ],
[
'specialty_id' => '126', 
'name' => 'Enteric Fever' 
 ],
[
'specialty_id' => '126', 
'name' => 'Fatigue' 
 ],
[
'specialty_id' => '126', 
'name' => 'Fever' 
 ],
[
'specialty_id' => '126', 
'name' => 'Fissures' 
 ],
[
'specialty_id' => '126', 
'name' => 'Flatulence' 
 ],
[
'specialty_id' => '126', 
'name' => 'Gas' 
 ],
[
'specialty_id' => '126', 
'name' => 'Gastric Ulcer' 
 ],
[
'specialty_id' => '126', 
'name' => 'Gastroenteritis' 
 ],
[
'specialty_id' => '126', 
'name' => 'GERD (Gastroesophageal Reflux Disease)' 
 ],
[
'specialty_id' => '126', 
'name' => 'Heart Attack' 
 ],
[
'specialty_id' => '126', 
'name' => 'Heart Burn' 
 ],
[
'specialty_id' => '126', 
'name' => 'Heart Disease' 
 ],
[
'specialty_id' => '126', 
'name' => 'Heart Failure' 
 ],
[
'specialty_id' => '126', 
'name' => 'Helicobacter Pylori Bacteria' 
 ],
[
'specialty_id' => '126', 
'name' => 'Hemorrhoids' 
 ],
[
'specialty_id' => '126', 
'name' => 'Hepatitis B' 
 ],
[
'specialty_id' => '126', 
'name' => 'Hepatitis C' 
 ],
[
'specialty_id' => '126', 
'name' => 'Hernia' 
 ],
[
'specialty_id' => '126', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '126', 
'name' => 'Infectious Diseases' 
 ],
[
'specialty_id' => '126', 
'name' => 'Ischemic Heart Disease' 
 ],
[
'specialty_id' => '126', 
'name' => 'Jaundice' 
 ],
[
'specialty_id' => '126', 
'name' => 'Liver Disease' 
 ],
[
'specialty_id' => '126', 
'name' => 'Metabolic Syndrome' 
 ],
[
'specialty_id' => '126', 
'name' => 'Muscle Aches' 
 ],
[
'specialty_id' => '181', 
'name' => 'Abdomen Pain' 
 ],
[
'specialty_id' => '181', 
'name' => 'Abdominal Pain' 
 ],
[
'specialty_id' => '181', 
'name' => 'Acid Peptic Disease' 
 ],
[
'specialty_id' => '181', 
'name' => 'Anemia' 
 ],
[
'specialty_id' => '181', 
'name' => 'Angina' 
 ],
[
'specialty_id' => '181', 
'name' => 'Arrythmias' 
 ],
[
'specialty_id' => '181', 
'name' => 'Arthritis' 
 ],
[
'specialty_id' => '181', 
'name' => 'Atherosclerosis' 
 ],
[
'specialty_id' => '181', 
'name' => 'Atrial Fibrillation' 
 ],
[
'specialty_id' => '181', 
'name' => 'Back Ache' 
 ],
[
'specialty_id' => '181', 
'name' => 'Black Stools' 
 ],
[
'specialty_id' => '181', 
'name' => 'Bloating' 
 ],
[
'specialty_id' => '181', 
'name' => 'Blood Pressure' 
 ],
[
'specialty_id' => '181', 
'name' => 'Cardiac Arrest' 
 ],
[
'specialty_id' => '181', 
'name' => 'Cardiac Disease' 
 ],
[
'specialty_id' => '181', 
'name' => 'Cardiology Disease' 
 ],
[
'specialty_id' => '181', 
'name' => 'Cardiovascular Disease' 
 ],
[
'specialty_id' => '181', 
'name' => 'Chest Infections' 
 ],
[
'specialty_id' => '181', 
'name' => 'Chicken Hepatitis' 
 ],
[
'specialty_id' => '181', 
'name' => 'Chronic Hepatitis' 
 ],
[
'specialty_id' => '181', 
'name' => 'Cold And Influenza' 
 ],
[
'specialty_id' => '181', 
'name' => 'Common Cold' 
 ],
[
'specialty_id' => '181', 
'name' => 'Congenital Disease' 
 ],
[
'specialty_id' => '181', 
'name' => 'Cough ' 
 ],
[
'specialty_id' => '181', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '181', 
'name' => 'Diarrhea' 
 ],
[
'specialty_id' => '181', 
'name' => 'Dyspepsia' 
 ],
[
'specialty_id' => '181', 
'name' => 'Enteric Fever' 
 ],
[
'specialty_id' => '181', 
'name' => 'Fatigue' 
 ],
[
'specialty_id' => '181', 
'name' => 'Fever' 
 ],
[
'specialty_id' => '181', 
'name' => 'Fissures' 
 ],
[
'specialty_id' => '181', 
'name' => 'Flatulence' 
 ],
[
'specialty_id' => '181', 
'name' => 'Gas' 
 ],
[
'specialty_id' => '181', 
'name' => 'Gastric Ulcer' 
 ],
[
'specialty_id' => '181', 
'name' => 'Gastroenteritis' 
 ],
[
'specialty_id' => '181', 
'name' => 'GERD (Gastroesophageal Reflux Disease)' 
 ],
[
'specialty_id' => '181', 
'name' => 'Heart Attack' 
 ],
[
'specialty_id' => '181', 
'name' => 'Heart Burn' 
 ],
[
'specialty_id' => '181', 
'name' => 'Heart Disease' 
 ],
[
'specialty_id' => '181', 
'name' => 'Heart Failure' 
 ],
[
'specialty_id' => '181', 
'name' => 'Helicobacter Pylori Bacteria' 
 ],
[
'specialty_id' => '181', 
'name' => 'Hemorrhoids' 
 ],
[
'specialty_id' => '181', 
'name' => 'Hepatitis B' 
 ],
[
'specialty_id' => '181', 
'name' => 'Hepatitis C' 
 ],
[
'specialty_id' => '181', 
'name' => 'Hernia' 
 ],
[
'specialty_id' => '181', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '181', 
'name' => 'Infectious Diseases' 
 ],
[
'specialty_id' => '181', 
'name' => 'Ischemic Heart Disease' 
 ],
[
'specialty_id' => '181', 
'name' => 'Jaundice' 
 ],
[
'specialty_id' => '181', 
'name' => 'Liver Disease' 
 ],
[
'specialty_id' => '181', 
'name' => 'Metabolic Syndrome' 
 ],
[
'specialty_id' => '181', 
'name' => 'Muscle Aches' 
 ],
[
'specialty_id' => '45', 
'name' => 'Anal And Perianal Conditions' 
 ],
[
'specialty_id' => '45', 
'name' => 'Anal Fissure' 
 ],
[
'specialty_id' => '45', 
'name' => 'Burns' 
 ],
[
'specialty_id' => '45', 
'name' => 'Cyst' 
 ],
[
'specialty_id' => '45', 
'name' => 'Diabetic Foot' 
 ],
[
'specialty_id' => '45', 
'name' => 'Fissure' 
 ],
[
'specialty_id' => '45', 
'name' => 'Head Injury' 
 ],
[
'specialty_id' => '45', 
'name' => 'Ingrown Toe Nail' 
 ],
[
'specialty_id' => '45', 
'name' => 'Lumps And Bumps' 
 ],
[
'specialty_id' => '45', 
'name' => 'Piles' 
 ],
[
'specialty_id' => '45', 
'name' => 'Pilonidal Cysts' 
 ],
[
'specialty_id' => '45', 
'name' => 'Pilonidal Sinus' 
 ],
[
'specialty_id' => '45', 
'name' => 'Thoracic Outlet Syndrome' 
 ],
[
'specialty_id' => '45', 
'name' => 'Varicose Veins' 
 ],
[
'specialty_id' => '76', 
'name' => 'Biliary Stone Treatment' 
 ],
[
'specialty_id' => '76', 
'name' => 'Biopsy' 
 ],
[
'specialty_id' => '76', 
'name' => 'Cardiac CT Angiography' 
 ],
[
'specialty_id' => '76', 
'name' => 'Cardiac Patient Management' 
 ],
[
'specialty_id' => '76', 
'name' => 'Cardiology Medicine  (General)' 
 ],
[
'specialty_id' => '76', 
'name' => 'Coloscopy' 
 ],
[
'specialty_id' => '76', 
'name' => 'Constipation Treatment' 
 ],
[
'specialty_id' => '76', 
'name' => 'Corona-virus Treatment' 
 ],
[
'specialty_id' => '76', 
'name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '76', 
'name' => 'Diarrhea Treatment' 
 ],
[
'specialty_id' => '76', 
'name' => 'Digital Rectal Examintation' 
 ],
[
'specialty_id' => '76', 
'name' => 'Echocardiogram' 
 ],
[
'specialty_id' => '76', 
'name' => 'Echocardiography (ECHO)' 
 ],
[
'specialty_id' => '76', 
'name' => 'Echocardiography (ECG)' 
 ],
[
'specialty_id' => '76', 
'name' => 'Endoscopic Guided Ultrasound' 
 ],
[
'specialty_id' => '76', 
'name' => 'Endoscopic Surgeries' 
 ],
[
'specialty_id' => '76', 
'name' => 'Endoscopy' 
 ],
[
'specialty_id' => '76', 
'name' => 'ERCP (Endoscopic Retrograde Cholangiopancreatograp' 
 ],
[
'specialty_id' => '76', 
'name' => 'Exercise Tolerance Test (ETT)' 
 ],
[
'specialty_id' => '76', 
'name' => 'Family Medicine' 
 ],
[
'specialty_id' => '76', 
'name' => 'Gastric Ballon' 
 ],
[
'specialty_id' => '76', 
'name' => 'Gastric Band Ligation' 
 ],
[
'specialty_id' => '76', 
'name' => 'Gastroscopy' 
 ],
[
'specialty_id' => '76', 
'name' => 'Heart Attack Management' 
 ],
[
'specialty_id' => '76', 
'name' => 'Hepatitis A Treatment' 
 ],
[
'specialty_id' => '76', 
'name' => 'Hepatitis B Treatment' 
 ],
[
'specialty_id' => '76', 
'name' => 'Hepatitis C Treatment' 
 ],
[
'specialty_id' => '76', 
'name' => 'Holter Monitoring' 
 ],
[
'specialty_id' => '76', 
'name' => 'Infertility Treatment' 
 ],
[
'specialty_id' => '76', 
'name' => 'Manometry' 
 ],
[
'specialty_id' => '76', 
'name' => 'Metabolic Disorders Treatment' 
 ],
[
'specialty_id' => '76', 
'name' => 'MRCP (Magnetic Resonance Cholangiopancreatography)' 
 ],
[
'specialty_id' => '76', 
'name' => 'Myotomy' 
 ],
[
'specialty_id' => '76', 
'name' => 'Nutritional Advice' 
 ],
[
'specialty_id' => '76', 
'name' => 'Obesity Management' 
 ],
[
'specialty_id' => '76', 
'name' => 'Oesophagoscopy' 
 ],
[
'specialty_id' => '76', 
'name' => 'Osteoporosis Management' 
 ],
[
'specialty_id' => '76', 
'name' => 'Pain Management Services' 
 ],
[
'specialty_id' => '76', 
'name' => 'Peg Tube Placement' 
 ],
[
'specialty_id' => '76', 
'name' => 'Stenosis And Valvular Regurgitation' 
 ],
[
'specialty_id' => '76', 
'name' => 'Stress Echocardiography' 
 ],
[
'specialty_id' => '76', 
'name' => 'Thyroid Treatment' 
 ],
[
'specialty_id' => '76', 
'name' => 'Trans-thoracic Echocardiography' 
 ],
[
'specialty_id' => '76', 
'name' => 'Treatment for Heart Attack.' 
 ],
[
'specialty_id' => '6', 
'name' => 'Abdomen Pain' 
 ],
[
'specialty_id' => '6', 
'name' => 'Abdominal Pain' 
 ],
[
'specialty_id' => '6', 
'name' => 'Acid Peptic Disease' 
 ],
[
'specialty_id' => '6', 
'name' => 'Anemia ' 
 ],
[
'specialty_id' => '6', 
'name' => 'Angina' 
 ],
[
'specialty_id' => '6', 
'name' => 'Arrythmias' 
 ],
[
'specialty_id' => '6', 
'name' => 'Arthritis' 
 ],
[
'specialty_id' => '6', 
'name' => 'Atherosclerosis' 
 ],
[
'specialty_id' => '6', 
'name' => 'Atrial Fibrillation' 
 ],
[
'specialty_id' => '6', 
'name' => 'Back Ache' 
 ],
[
'specialty_id' => '6', 
'name' => 'Black Stools' 
 ],
[
'specialty_id' => '6', 
'name' => 'Bloating' 
 ],
[
'specialty_id' => '6', 
'name' => 'Blood Pressure' 
 ],
[
'specialty_id' => '6', 
'name' => 'Bronchial Asthma' 
 ],
[
'specialty_id' => '6', 
'name' => 'Bronchiectasis' 
 ],
[
'specialty_id' => '6', 
'name' => 'Cardiac Arrest' 
 ],
[
'specialty_id' => '6', 
'name' => 'Cardiac Diseases' 
 ],
[
'specialty_id' => '6', 
'name' => 'Cardiovascular Diseases' 
 ],
[
'specialty_id' => '6', 
'name' => 'Chest Infections' 
 ],
[
'specialty_id' => '6', 
'name' => 'Cholesterol Disorders' 
 ],
[
'specialty_id' => '6', 
'name' => 'Chronic Bronchitis' 
 ],
[
'specialty_id' => '6', 
'name' => 'Chronic Hepatitis' 
 ],
[
'specialty_id' => '6', 
'name' => 'Chronic Obstructive Pulmonary Diseases (COPD)' 
 ],
[
'specialty_id' => '6', 
'name' => 'Cold And Influenza' 
 ],
[
'specialty_id' => '6', 
'name' => 'Common Cold' 
 ],
[
'specialty_id' => '6', 
'name' => 'Congenital Diseases' 
 ],
[
'specialty_id' => '6', 
'name' => 'Cough' 
 ],
[
'specialty_id' => '6', 
'name' => 'Diabetes Insipidus' 
 ],
[
'specialty_id' => '6', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '6', 
'name' => 'Diarrhea' 
 ],
[
'specialty_id' => '6', 
'name' => 'Dyspepsia' 
 ],
[
'specialty_id' => '6', 
'name' => 'Embolism' 
 ],
[
'specialty_id' => '6', 
'name' => 'Emphysema' 
 ],
[
'specialty_id' => '6', 
'name' => 'Enteric Fever' 
 ],
[
'specialty_id' => '6', 
'name' => 'Fatigue' 
 ],
[
'specialty_id' => '6', 
'name' => 'Fever' 
 ],
[
'specialty_id' => '6', 
'name' => 'Fissures' 
 ],
[
'specialty_id' => '6', 
'name' => 'Flatulence' 
 ],
[
'specialty_id' => '6', 
'name' => 'Gas' 
 ],
[
'specialty_id' => '6', 
'name' => 'Gastric Ulcer' 
 ],
[
'specialty_id' => '6', 
'name' => 'Gastroenteritis' 
 ],
[
'specialty_id' => '6', 
'name' => 'GERD (Gastroesophageal Reflux Disease)' 
 ],
[
'specialty_id' => '6', 
'name' => 'Goiter' 
 ],
[
'specialty_id' => '6', 
'name' => 'Heart Attack' 
 ],
[
'specialty_id' => '6', 
'name' => 'Heart Burn' 
 ],
[
'specialty_id' => '6', 
'name' => 'Heart Diseases' 
 ],
[
'specialty_id' => '6', 
'name' => 'Heart Failure' 
 ],
[
'specialty_id' => '6', 
'name' => 'Helicobacter Pylori Bacteria' 
 ],
[
'specialty_id' => '6', 
'name' => 'Hemorrhoids' 
 ],
[
'specialty_id' => '6', 
'name' => 'Hepatitis B' 
 ],
[
'specialty_id' => '6', 
'name' => 'Hepatitis C' 
 ],
[
'specialty_id' => '6', 
'name' => 'Hernia' 
 ],
[
'specialty_id' => '6', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '6', 
'name' => 'Hyperthyroidism' 
 ],
[
'specialty_id' => '6', 
'name' => 'Hypoglycemia' 
 ],
[
'specialty_id' => '6', 
'name' => 'Hypothyroidism' 
 ],
[
'specialty_id' => '6', 
'name' => 'Infectious Diseases' 
 ],
[
'specialty_id' => '6', 
'name' => 'Interstitial Lung Disease (ILD)' 
 ],
[
'specialty_id' => '6', 
'name' => 'Ischemic Heart Disease' 
 ],
[
'specialty_id' => '6', 
'name' => 'Jaundice' 
 ],
[
'specialty_id' => '6', 
'name' => 'Lack Of Growth' 
 ],
[
'specialty_id' => '6', 
'name' => 'Liver Diseases' 
 ],
[
'specialty_id' => '6', 
'name' => 'Metabolic Syndrome' 
 ],
[
'specialty_id' => '6', 
'name' => 'Muscle Aches' 
 ],
[
'specialty_id' => '6', 
'name' => 'Myocardial Infarction (MI)' 
 ],
[
'specialty_id' => '6', 
'name' => 'Nausea ' 
 ],
[
'specialty_id' => '6', 
'name' => 'Osteoporosis' 
 ],
[
'specialty_id' => '6', 
'name' => 'Palpitations' 
 ],
[
'specialty_id' => '6', 
'name' => 'Pancreas Disease' 
 ],
[
'specialty_id' => '6', 
'name' => 'Pneumonia' 
 ],
[
'specialty_id' => '6', 
'name' => 'Pulmonary Edema' 
 ],
[
'specialty_id' => '6', 
'name' => 'Pulmonary Hypertension' 
 ],
[
'specialty_id' => '6', 
'name' => 'Respiratory Diseases' 
 ],
[
'specialty_id' => '6', 
'name' => 'Rheumatic Heart Disease' 
 ],
[
'specialty_id' => '6', 
'name' => 'Sleep Disorders' 
 ],
[
'specialty_id' => '6', 
'name' => 'Stomach Pain' 
 ],
[
'specialty_id' => '6', 
'name' => 'Tachycardia' 
 ],
[
'specialty_id' => '6', 
'name' => 'Thyroid Disease' 
 ],
[
'specialty_id' => '6', 
'name' => 'Tuberculosis' 
 ],
[
'specialty_id' => '6', 
'name' => 'Typhoid Fever' 
 ],
[
'specialty_id' => '49', 
'name' => 'Abdomen Pain' 
 ],
[
'specialty_id' => '49', 
'name' => 'Abscess' 
 ],
[
'specialty_id' => '49', 
'name' => 'Anal And Perianal Conditions' 
 ],
[
'specialty_id' => '49', 
'name' => 'Anal Fissure' 
 ],
[
'specialty_id' => '49', 
'name' => 'Anemia' 
 ],
[
'specialty_id' => '49', 
'name' => 'Arthritis' 
 ],
[
'specialty_id' => '49', 
'name' => 'Back Ache' 
 ],
[
'specialty_id' => '49', 
'name' => 'Blood Pressure' 
 ],
[
'specialty_id' => '49', 
'name' => 'Bruise' 
 ],
[
'specialty_id' => '49', 
'name' => 'Burns' 
 ],
[
'specialty_id' => '49', 
'name' => 'Cardiac Diseases' 
 ],
[
'specialty_id' => '49', 
'name' => 'Chest Infections' 
 ],
[
'specialty_id' => '49', 
'name' => 'Chicken Pox' 
 ],
[
'specialty_id' => '49', 
'name' => 'Cold And Influenza' 
 ],
[
'specialty_id' => '49', 
'name' => 'Common Cold ' 
 ],
[
'specialty_id' => '49', 
'name' => 'Constipation' 
 ],
[
'specialty_id' => '49', 
'name' => 'Cough' 
 ],
[
'specialty_id' => '49', 
'name' => 'Cyst' 
 ],
[
'specialty_id' => '49', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '49', 
'name' => 'Diabetic Foot' 
 ],
[
'specialty_id' => '49', 
'name' => 'Diarrhea' 
 ],
[
'specialty_id' => '49', 
'name' => 'Fatigue' 
 ],
[
'specialty_id' => '49', 
'name' => 'Fever' 
 ],
[
'specialty_id' => '49', 
'name' => 'Fissure' 
 ],
[
'specialty_id' => '49', 
'name' => 'Flu' 
 ],
[
'specialty_id' => '49', 
'name' => 'General Urology' 
 ],
[
'specialty_id' => '49', 
'name' => 'Head Injury' 
 ],
[
'specialty_id' => '49', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '49', 
'name' => 'Infectious Disease' 
 ],
[
'specialty_id' => '49', 
'name' => 'Ingrown Toe Nail' 
 ],
[
'specialty_id' => '49', 
'name' => 'Lumps And Bumps' 
 ],
[
'specialty_id' => '49', 
'name' => 'Muscle Aches' 
 ],
[
'specialty_id' => '49', 
'name' => 'Nausea' 
 ],
[
'specialty_id' => '49', 
'name' => 'Piles' 
 ],
[
'specialty_id' => '49', 
'name' => 'Pilonidal Cysts' 
 ],
[
'specialty_id' => '49', 
'name' => 'Pilonidal Sinus' 
 ],
[
'specialty_id' => '49', 
'name' => 'Pneumonia' 
 ],
[
'specialty_id' => '49', 
'name' => 'Respiratory Diseases' 
 ],
[
'specialty_id' => '49', 
'name' => 'Sinusitis' 
 ],
[
'specialty_id' => '49', 
'name' => 'Skin Diseases' 
 ],
[
'specialty_id' => '49', 
'name' => 'Sleep Disorders' 
 ],
[
'specialty_id' => '49', 
'name' => 'Stomach Pain' 
 ],
[
'specialty_id' => '49', 
'name' => 'Thoracic Outlet Syndrome' 
 ],
[
'specialty_id' => '49', 
'name' => 'Urine Problems' 
 ],
[
'specialty_id' => '49', 
'name' => 'Varicose Viens' 
 ],
[
'specialty_id' => '49', 
'name' => 'Vomiting' 
 ],
[
'specialty_id' => '163', 
'name' => 'Acne' 
 ],
[
'specialty_id' => '163', 
'name' => 'Acne Scars' 
 ],
[
'specialty_id' => '163', 
'name' => 'Benign And Malignant Skin Tumors' 
 ],
[
'specialty_id' => '163', 
'name' => 'Benign Skin Tumors' 
 ],
[
'specialty_id' => '163', 
'name' => 'Canine Teeth' 
 ],
[
'specialty_id' => '163', 
'name' => 'Cavities' 
 ],
[
'specialty_id' => '163', 
'name' => 'Dental Caries' 
 ],
[
'specialty_id' => '163', 
'name' => 'Eczema' 
 ],
[
'specialty_id' => '163', 
'name' => 'Facial Disfigurement' 
 ],
[
'specialty_id' => '163', 
'name' => 'Gingivitis' 
 ],
[
'specialty_id' => '163', 
'name' => 'Gum Disease (Periodontal)' 
 ],
[
'specialty_id' => '163', 
'name' => 'Herpes' 
 ],
[
'specialty_id' => '163', 
'name' => 'Jaw Joint Problems' 
 ],
[
'specialty_id' => '163', 
'name' => 'Moles' 
 ],
[
'specialty_id' => '163', 
'name' => 'Orofacial Pain' 
 ],
[
'specialty_id' => '163', 
'name' => 'Periodontitis' 
 ],
[
'specialty_id' => '163', 
'name' => 'Psoriasis' 
 ],
[
'specialty_id' => '163', 
'name' => 'Scabies' 
 ],
[
'specialty_id' => '163', 
'name' => 'Skin Allergy' 
 ],
[
'specialty_id' => '163', 
'name' => 'Skin Disease' 
 ],
[
'specialty_id' => '163', 
'name' => 'Skin Fungal Infections' 
 ],
[
'specialty_id' => '163', 
'name' => 'Syphilis' 
 ],
[
'specialty_id' => '163', 
'name' => 'TMJ Disorders' 
 ],
[
'specialty_id' => '163', 
'name' => 'Tooth Decay' 
 ],
[
'specialty_id' => '163', 
'name' => 'Toothache' 
 ],
[
'specialty_id' => '163', 
'name' => 'Vitiligo' 
 ],
[
'specialty_id' => '163', 
'name' => 'Warts' 
 ],
[
'specialty_id' => '120', 
'name' => 'Abdomen Disease In Children' 
 ],
[
'specialty_id' => '120', 
'name' => 'Allergy In Children' 
 ],
[
'specialty_id' => '120', 
'name' => 'Asthma In Children' 
 ],
[
'specialty_id' => '120', 
'name' => 'Bile Duct Stones' 
 ],
[
'specialty_id' => '120', 
'name' => 'Birth Defects' 
 ],
[
'specialty_id' => '120', 
'name' => 'Cerebral Palsy (CP)' 
 ],
[
'specialty_id' => '120', 
'name' => 'Chest Disease in Children' 
 ],
[
'specialty_id' => '120', 
'name' => 'Chest Infection in Children' 
 ],
[
'specialty_id' => '120', 
'name' => 'Chicken Pox' 
 ],
[
'specialty_id' => '120', 
'name' => 'Childhood Cancer' 
 ],
[
'specialty_id' => '120', 
'name' => 'Cirrhosis' 
 ],
[
'specialty_id' => '120', 
'name' => 'Cleft Lip And Palate' 
 ],
[
'specialty_id' => '120', 
'name' => 'Diarrhea' 
 ],
[
'specialty_id' => '120', 
'name' => 'Down Syndrome' 
 ],
[
'specialty_id' => '120', 
'name' => 'Eating Problems in Children' 
 ],
[
'specialty_id' => '120', 
'name' => 'Fever in Children' 
 ],
[
'specialty_id' => '120', 
'name' => 'Growth Problem in Children' 
 ],
[
'specialty_id' => '120', 
'name' => 'Hepatobiliary Disease' 
 ],
[
'specialty_id' => '120', 
'name' => 'Hypothyroidism' 
 ],
[
'specialty_id' => '120', 
'name' => 'Kawasakis Disease' 
 ],
[
'specialty_id' => '120', 
'name' => 'Kernicterus' 
 ],
[
'specialty_id' => '120', 
'name' => 'Kidney Problems in Children' 
 ],
[
'specialty_id' => '120', 
'name' => 'Measles' 
 ],
[
'specialty_id' => '120', 
'name' => 'Meconium Aspiration Syndrome' 
 ],
[
'specialty_id' => '120', 
'name' => 'Meningitis' 
 ],
[
'specialty_id' => '120', 
'name' => 'Mumps' 
 ],
[
'specialty_id' => '120', 
'name' => 'Neonatal Jaundice' 
 ],
[
'specialty_id' => '120', 
'name' => 'Neonatal Sepsis' 
 ],
[
'specialty_id' => '120', 
'name' => 'Pneumonia in Children' 
 ],
[
'specialty_id' => '120', 
'name' => 'Respiratory Disease In Children' 
 ],
[
'specialty_id' => '120', 
'name' => 'Respiratory Distress Syndrome' 
 ],
[
'specialty_id' => '120', 
'name' => 'Respiratory Tract Infection (RTI)' 
 ],
[
'specialty_id' => '120', 
'name' => 'Rubella' 
 ],
[
'specialty_id' => '120', 
'name' => 'Scarlet Fever' 
 ],
[
'specialty_id' => '120', 
'name' => 'Sturge Weber Syndrome' 
 ],
[
'specialty_id' => '120', 
'name' => 'Teething Issue' 
 ],
[
'specialty_id' => '120', 
'name' => 'Tonsillitis' 
 ],
[
'specialty_id' => '120', 
'name' => 'Tuberous Sclerosis' 
 ],
[
'specialty_id' => '120', 
'name' => 'Whooping Cough' 
 ],
[
'specialty_id' => '129', 
'name' => 'Acute Kidney Injury' 
 ],
[
'specialty_id' => '129', 
'name' => 'Acute Renal Failure (ARF)' 
 ],
[
'specialty_id' => '129', 
'name' => 'Blood Pressure' 
 ],
[
'specialty_id' => '129', 
'name' => 'Chronic Kidney Disease' 
 ],
[
'specialty_id' => '129', 
'name' => 'Chronic Renal Failure (CRF)' 
 ],
[
'specialty_id' => '129', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '129', 
'name' => 'Electrolyte Imbalance' 
 ],
[
'specialty_id' => '129', 
'name' => 'Glomerular Diseases' 
 ],
[
'specialty_id' => '129', 
'name' => 'Hemolytic Uremic Syndrome (HUS)' 
 ],
[
'specialty_id' => '129', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '129', 
'name' => 'Hyponatremia' 
 ],
[
'specialty_id' => '129', 
'name' => 'Kidney Disease' 
 ],
[
'specialty_id' => '129', 
'name' => 'Kidney Failure' 
 ],
[
'specialty_id' => '129', 
'name' => 'Kidney Infections' 
 ],
[
'specialty_id' => '129', 
'name' => 'Kidney Stones' 
 ],
[
'specialty_id' => '129', 
'name' => 'Nephrotic Syndrome' 
 ],
[
'specialty_id' => '129', 
'name' => 'Orthostatic Hypotension' 
 ],
[
'specialty_id' => '129', 
'name' => 'Polycystic Kidney Disease (PKD)' 
 ],
[
'specialty_id' => '129', 
'name' => 'Pyelonephritis' 
 ],
[
'specialty_id' => '129', 
'name' => 'Urinary Tract infections' 
 ],
[
'specialty_id' => '172', 
'name' => 'Acute Kidney Injury' 
 ],
[
'specialty_id' => '172', 
'name' => 'Acute Renal Failure (ARF)' 
 ],
[
'specialty_id' => '172', 
'name' => 'Blood Pressure' 
 ],
[
'specialty_id' => '172', 
'name' => 'Chronic Kidney Disease' 
 ],
[
'specialty_id' => '172', 
'name' => 'Chronic Renal Failure (CRF)' 
 ],
[
'specialty_id' => '172', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '172', 
'name' => 'Electrolyte Imbalance' 
 ],
[
'specialty_id' => '172', 
'name' => 'Glomerular Diseases' 
 ],
[
'specialty_id' => '172', 
'name' => 'Hemolytic Uremic Syndrome (HUS)' 
 ],
[
'specialty_id' => '172', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '172', 
'name' => 'Hyponatremia' 
 ],
[
'specialty_id' => '172', 
'name' => 'Kidney Disease' 
 ],
[
'specialty_id' => '172', 
'name' => 'Kidney Failure' 
 ],
[
'specialty_id' => '172', 
'name' => 'Kidney Infections' 
 ],
[
'specialty_id' => '172', 
'name' => 'Kidney Stones' 
 ],
[
'specialty_id' => '172', 
'name' => 'Nephrotic Syndrome' 
 ],
[
'specialty_id' => '172', 
'name' => 'Orthostatic Hypotension' 
 ],
[
'specialty_id' => '172', 
'name' => 'Polycystic Kidney Disease (PKD)' 
 ],
[
'specialty_id' => '172', 
'name' => 'Pyelonephritis' 
 ],
[
'specialty_id' => '172', 
'name' => 'Urinary Tract infections' 
 ],
[
'specialty_id' => '89', 
'name' => 'Aneurysm' 
 ],
[
'specialty_id' => '89', 
'name' => 'Back Ache' 
 ],
[
'specialty_id' => '89', 
'name' => 'Back Pain' 
 ],
[
'specialty_id' => '89', 
'name' => 'Brain Hemorrhage' 
 ],
[
'specialty_id' => '89', 
'name' => 'Brain Tumor' 
 ],
[
'specialty_id' => '89', 
'name' => 'Cerebal Abscess' 
 ],
[
'specialty_id' => '89', 
'name' => 'Cerebal Herniation' 
 ],
[
'specialty_id' => '89', 
'name' => 'Degenerative Spine Diseases' 
 ],
[
'specialty_id' => '89', 
'name' => 'Disc Rupture' 
 ],
[
'specialty_id' => '89', 
'name' => 'Epilepsy' 
 ],
[
'specialty_id' => '89', 
'name' => 'Facial Pain' 
 ],
[
'specialty_id' => '89', 
'name' => 'Fits' 
 ],
[
'specialty_id' => '89', 
'name' => 'Head Trauma ' 
 ],
[
'specialty_id' => '89', 
'name' => 'Headache' 
 ],
[
'specialty_id' => '89', 
'name' => 'Hydrocephalus' 
 ],
[
'specialty_id' => '89', 
'name' => 'Memory Loss' 
 ],
[
'specialty_id' => '89', 
'name' => 'Meningitis' 
 ],
[
'specialty_id' => '89', 
'name' => 'Migraine' 
 ],
[
'specialty_id' => '89', 
'name' => 'Multiple Sclerosis (MS)' 
 ],
[
'specialty_id' => '89', 
'name' => 'Neck Pain' 
 ],
[
'specialty_id' => '89', 
'name' => 'Pituitary Gland Tumor' 
 ],
[
'specialty_id' => '89', 
'name' => 'Raised Intracranial Pressure' 
 ],
[
'specialty_id' => '89', 
'name' => 'Sciatica' 
 ],
[
'specialty_id' => '89', 
'name' => 'Seizures' 
 ],
[
'specialty_id' => '89', 
'name' => 'Slip Disk' 
 ],
[
'specialty_id' => '89', 
'name' => 'Slip Disk' 
 ],
[
'specialty_id' => '89', 
'name' => 'Spina Bifida' 
 ],
[
'specialty_id' => '89', 
'name' => 'Spinal Cord Injury' 
 ],
[
'specialty_id' => '89', 
'name' => 'Stroke' 
 ],
[
'specialty_id' => '89', 
'name' => 'Transient Ischemic Attack (TIA)' 
 ],
[
'specialty_id' => '89', 
'name' => 'Vertigo' 
 ],
[
'specialty_id' => '84', 
'name' => 'ALS' 
 ],
[
'specialty_id' => '84', 
'name' => 'Autism' 
 ],
[
'specialty_id' => '84', 
'name' => 'Dementia' 
 ],
[
'specialty_id' => '84', 
'name' => 'Dizziness' 
 ],
[
'specialty_id' => '84', 
'name' => 'Epilepsy' 
 ],
[
'specialty_id' => '84', 
'name' => 'Facial Pain' 
 ],
[
'specialty_id' => '84', 
'name' => 'Headache' 
 ],
[
'specialty_id' => '84', 
'name' => 'Imbalance' 
 ],
[
'specialty_id' => '84', 
'name' => 'Memory Loss' 
 ],
[
'specialty_id' => '84', 
'name' => 'Meningitis' 
 ],
[
'specialty_id' => '84', 
'name' => 'Migraine' 
 ],
[
'specialty_id' => '84', 
'name' => 'Movement Disorders' 
 ],
[
'specialty_id' => '84', 
'name' => 'Multiple Sclerosis (MS)' 
 ],
[
'specialty_id' => '84', 
'name' => 'Neck Pain' 
 ],
[
'specialty_id' => '84', 
'name' => 'Neuro Psychiatric Disorders' 
 ],
[
'specialty_id' => '84', 
'name' => 'Neurological Diseases' 
 ],
[
'specialty_id' => '84', 
'name' => 'Neurological Issues' 
 ],
[
'specialty_id' => '84', 
'name' => 'Neuromuscular Disorders' 
 ],
[
'specialty_id' => '84', 
'name' => 'Parkinson’s Disease' 
 ],
[
'specialty_id' => '84', 
'name' => 'Progressive Supranuclear Palsy (PSP)' 
 ],
[
'specialty_id' => '84', 
'name' => 'Seizures' 
 ],
[
'specialty_id' => '84', 
'name' => 'Sleep Disorders' 
 ],
[
'specialty_id' => '84', 
'name' => 'Spinal Cord Issues' 
 ],
[
'specialty_id' => '84', 
'name' => 'Stroke ' 
 ],
[
'specialty_id' => '84', 
'name' => 'Vertigo' 
 ],
[
'specialty_id' => '88', 
'name' => 'ALS' 
 ],
[
'specialty_id' => '88', 
'name' => 'Autism' 
 ],
[
'specialty_id' => '88', 
'name' => 'Dementia' 
 ],
[
'specialty_id' => '88', 
'name' => 'Dizziness' 
 ],
[
'specialty_id' => '88', 
'name' => 'Epilepsy' 
 ],
[
'specialty_id' => '88', 
'name' => 'Facial Pain' 
 ],
[
'specialty_id' => '88', 
'name' => 'Headache' 
 ],
[
'specialty_id' => '88', 
'name' => 'Imbalance' 
 ],
[
'specialty_id' => '88', 
'name' => 'Memory Loss' 
 ],
[
'specialty_id' => '88', 
'name' => 'Meningitis' 
 ],
[
'specialty_id' => '88', 
'name' => 'Migraine' 
 ],
[
'specialty_id' => '88', 
'name' => 'Movement Disorders' 
 ],
[
'specialty_id' => '88', 
'name' => 'Multiple Sclerosis (MS)' 
 ],
[
'specialty_id' => '88', 
'name' => 'Neck Pain' 
 ],
[
'specialty_id' => '88', 
'name' => 'Neuro Psychiatric Disorders' 
 ],
[
'specialty_id' => '88', 
'name' => 'Neurological Diseases' 
 ],
[
'specialty_id' => '88', 
'name' => 'Neurological Issues' 
 ],
[
'specialty_id' => '88', 
'name' => 'Neuromuscular Disorders' 
 ],
[
'specialty_id' => '88', 
'name' => 'Parkinson’s Disease' 
 ],
[
'specialty_id' => '88', 
'name' => 'Progressive Supranuclear Palsy (PSP)' 
 ],
[
'specialty_id' => '88', 
'name' => 'Seizures' 
 ],
[
'specialty_id' => '88', 
'name' => 'Sleep Disorders' 
 ],
[
'specialty_id' => '88', 
'name' => 'Spinal Cord Issues' 
 ],
[
'specialty_id' => '88', 
'name' => 'Stroke ' 
 ],
[
'specialty_id' => '88', 
'name' => 'Vertigo' 
 ],
[
'specialty_id' => '93', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '93', 
'name' => 'Eating Disorders' 
 ],
[
'specialty_id' => '93', 
'name' => 'Obesity' 
 ],
[
'specialty_id' => '63', 
'name' => 'Amenorrhoea' 
 ],
[
'specialty_id' => '63', 
'name' => 'Aplastic Anemia' 
 ],
[
'specialty_id' => '63', 
'name' => 'Blood Pressure' 
 ],
[
'specialty_id' => '63', 
'name' => 'Ectopic Pregnancy' 
 ],
[
'specialty_id' => '63', 
'name' => 'Endometriosis' 
 ],
[
'specialty_id' => '63', 
'name' => 'Female Infertility' 
 ],
[
'specialty_id' => '63', 
'name' => 'Female Urinary Problems' 
 ],
[
'specialty_id' => '63', 
'name' => 'Fibroids' 
 ],
[
'specialty_id' => '63', 
'name' => 'Gynaecological Tumors' 
 ],
[
'specialty_id' => '63', 
'name' => 'High Risk Pregnancy' 
 ],
[
'specialty_id' => '63', 
'name' => 'Infectious Diseases' 
 ],
[
'specialty_id' => '63', 
'name' => 'Menopause' 
 ],
[
'specialty_id' => '63', 
'name' => 'Menstruation Problems' 
 ],
[
'specialty_id' => '63', 
'name' => 'Miscarriage' 
 ],
[
'specialty_id' => '63', 
'name' => 'Ovarian Cyst' 
 ],
[
'specialty_id' => '63', 
'name' => 'Polycystic Ovary Syndrome (PCOS)' 
 ],
[
'specialty_id' => '63', 
'name' => 'Post Menopausal Bleeding' 
 ],
[
'specialty_id' => '63', 
'name' => 'PreEclampsia' 
 ],
[
'specialty_id' => '63', 
'name' => 'Pregnancy' 
 ],
[
'specialty_id' => '63', 
'name' => 'Urinary Tract Infections (UTI)' 
 ],
[
'specialty_id' => '63', 
'name' => 'Uro Gynea Cancer' 
 ],
[
'specialty_id' => '77', 
'name' => 'Basal Cell Carcinoma' 
 ],
[
'specialty_id' => '77', 
'name' => 'Blood Malignancy' 
 ],
[
'specialty_id' => '77', 
'name' => 'Bone Cancer' 
 ],
[
'specialty_id' => '77', 
'name' => 'Brain Tumor' 
 ],
[
'specialty_id' => '77', 
'name' => 'Breast Cancer' 
 ],
[
'specialty_id' => '77', 
'name' => 'Cervical Cancer' 
 ],
[
'specialty_id' => '77', 
'name' => 'Gastrointestinal Tumors' 
 ],
[
'specialty_id' => '77', 
'name' => 'Genito Urinary Tumor' 
 ],
[
'specialty_id' => '77', 
'name' => 'Intestinal Cancer' 
 ],
[
'specialty_id' => '77', 
'name' => 'Leukemia (Blood Cancer)' 
 ],
[
'specialty_id' => '77', 
'name' => 'Liver Cancer' 
 ],
[
'specialty_id' => '77', 
'name' => 'Lung Cancer' 
 ],
[
'specialty_id' => '77', 
'name' => 'Lung Carcinomas and Carcinoid Tumors' 
 ],
[
'specialty_id' => '77', 
'name' => 'Lung Lymphoma ' 
 ],
[
'specialty_id' => '77', 
'name' => 'Lymphomas' 
 ],
[
'specialty_id' => '77', 
'name' => 'Myeloma' 
 ],
[
'specialty_id' => '77', 
'name' => 'Ovarian Cancer' 
 ],
[
'specialty_id' => '77', 
'name' => 'Ovarian Tumor' 
 ],
[
'specialty_id' => '77', 
'name' => 'Pancreatic Cancer' 
 ],
[
'specialty_id' => '77', 
'name' => 'Prostate Cancer' 
 ],
[
'specialty_id' => '77', 
'name' => 'Renal (Kidney) Cancer' 
 ],
[
'specialty_id' => '77', 
'name' => 'Skin Cancer' 
 ],
[
'specialty_id' => '77', 
'name' => 'Squamous Cell Carcinoma' 
 ],
[
'specialty_id' => '95', 
'name' => 'Basal Cell Carcinoma' 
 ],
[
'specialty_id' => '95', 
'name' => 'Blood Malignancy' 
 ],
[
'specialty_id' => '95', 
'name' => 'Bone Cancer' 
 ],
[
'specialty_id' => '95', 
'name' => 'Brain Tumor' 
 ],
[
'specialty_id' => '95', 
'name' => 'Breast Cancer' 
 ],
[
'specialty_id' => '95', 
'name' => 'Cervical Cancer' 
 ],
[
'specialty_id' => '95', 
'name' => 'Gastrointestinal Tumors' 
 ],
[
'specialty_id' => '95', 
'name' => 'Genito Urinary Tumor' 
 ],
[
'specialty_id' => '95', 
'name' => 'Intestinal Cancer' 
 ],
[
'specialty_id' => '95', 
'name' => 'Leukemia (Blood Cancer)' 
 ],
[
'specialty_id' => '95', 
'name' => 'Liver Cancer' 
 ],
[
'specialty_id' => '95', 
'name' => 'Lung Cancer' 
 ],
[
'specialty_id' => '95', 
'name' => 'Lung Carcinomas and Carcinoid Tumors' 
 ],
[
'specialty_id' => '95', 
'name' => 'Lung Lymphoma ' 
 ],
[
'specialty_id' => '95', 
'name' => 'Lymphomas' 
 ],
[
'specialty_id' => '95', 
'name' => 'Myeloma' 
 ],
[
'specialty_id' => '95', 
'name' => 'Ovarian Cancer' 
 ],
[
'specialty_id' => '95', 
'name' => 'Ovarian Tumor' 
 ],
[
'specialty_id' => '95', 
'name' => 'Pancreatic Cancer' 
 ],
[
'specialty_id' => '95', 
'name' => 'Prostate Cancer' 
 ],
[
'specialty_id' => '95', 
'name' => 'Renal (Kidney) Cancer' 
 ],
[
'specialty_id' => '95', 
'name' => 'Skin Cancer' 
 ],
[
'specialty_id' => '95', 
'name' => 'Squamous Cell Carcinoma' 
 ],
[
'specialty_id' => '127', 
'name' => 'Basal Cell Carcinoma' 
 ],
[
'specialty_id' => '127', 
'name' => 'Blood Malignancy' 
 ],
[
'specialty_id' => '127', 
'name' => 'Bone Cancer' 
 ],
[
'specialty_id' => '127', 
'name' => 'Brain Tumor' 
 ],
[
'specialty_id' => '127', 
'name' => 'Breast Cancer' 
 ],
[
'specialty_id' => '127', 
'name' => 'Cervical Cancer' 
 ],
[
'specialty_id' => '127', 
'name' => 'Gastrointestinal Tumors' 
 ],
[
'specialty_id' => '127', 
'name' => 'Genito Urinary Tumor' 
 ],
[
'specialty_id' => '127', 
'name' => 'Intestinal Cancer' 
 ],
[
'specialty_id' => '127', 
'name' => 'Leukemia (Blood Cancer)' 
 ],
[
'specialty_id' => '127', 
'name' => 'Liver Cancer' 
 ],
[
'specialty_id' => '127', 
'name' => 'Lung Cancer' 
 ],
[
'specialty_id' => '127', 
'name' => 'Lung Carcinomas and Carcinoid Tumors' 
 ],
[
'specialty_id' => '127', 
'name' => 'Lung Lymphoma ' 
 ],
[
'specialty_id' => '127', 
'name' => 'Lymphomas' 
 ],
[
'specialty_id' => '127', 
'name' => 'Myeloma' 
 ],
[
'specialty_id' => '127', 
'name' => 'Ovarian Cancer' 
 ],
[
'specialty_id' => '127', 
'name' => 'Ovarian Tumor' 
 ],
[
'specialty_id' => '127', 
'name' => 'Pancreatic Cancer' 
 ],
[
'specialty_id' => '127', 
'name' => 'Prostate Cancer' 
 ],
[
'specialty_id' => '127', 
'name' => 'Renal (Kidney) Cancer' 
 ],
[
'specialty_id' => '127', 
'name' => 'Skin Cancer' 
 ],
[
'specialty_id' => '127', 
'name' => 'Squamous Cell Carcinoma' 
 ],
[
'specialty_id' => '9', 
'name' => 'Canine Teeth' 
 ],
[
'specialty_id' => '9', 
'name' => 'Cavities' 
 ],
[
'specialty_id' => '9', 
'name' => 'Dental Caries' 
 ],
[
'specialty_id' => '9', 
'name' => 'Dupuytren’s Contracture (Hypodontia)' 
 ],
[
'specialty_id' => '9', 
'name' => 'Gingivitis' 
 ],
[
'specialty_id' => '9', 
'name' => 'Gum Disease (Periodontal)' 
 ],
[
'specialty_id' => '9', 
'name' => 'Jaw Joint Problems' 
 ],
[
'specialty_id' => '9', 
'name' => 'Orofacial Pain' 
 ],
[
'specialty_id' => '9', 
'name' => 'Periodontitis' 
 ],
[
'specialty_id' => '9', 
'name' => 'TMJ Disorders' 
 ],
[
'specialty_id' => '9', 
'name' => 'Tooth Decay' 
 ],
[
'specialty_id' => '9', 
'name' => 'Toothache' 
 ],
[
'specialty_id' => '50', 
'name' => 'Canine Teeth' 
 ],
[
'specialty_id' => '50', 
'name' => 'Cavities' 
 ],
[
'specialty_id' => '50', 
'name' => 'Dental Caries' 
 ],
[
'specialty_id' => '50', 
'name' => 'Dupuytren’s Contracture (Hypodontia)' 
 ],
[
'specialty_id' => '50', 
'name' => 'Gingivitis' 
 ],
[
'specialty_id' => '50', 
'name' => 'Gum Disease (Periodontal)' 
 ],
[
'specialty_id' => '50', 
'name' => 'Jaw Joint Problems' 
 ],
[
'specialty_id' => '50', 
'name' => 'Orofacial Pain' 
 ],
[
'specialty_id' => '50', 
'name' => 'Periodontitis' 
 ],
[
'specialty_id' => '50', 
'name' => 'TMJ Disorders' 
 ],
[
'specialty_id' => '50', 
'name' => 'Tooth Decay' 
 ],
[
'specialty_id' => '50', 
'name' => 'Toothache' 
 ],
[
'specialty_id' => '10', 
'name' => 'Canine Teeth' 
 ],
[
'specialty_id' => '10', 
'name' => 'Cavities' 
 ],
[
'specialty_id' => '10', 
'name' => 'Dental Caries' 
 ],
[
'specialty_id' => '10', 
'name' => 'Dupuytren’s Contracture (Hypodontia)' 
 ],
[
'specialty_id' => '10', 
'name' => 'Gingivitis' 
 ],
[
'specialty_id' => '10', 
'name' => 'Gum Disease (Periodontal)' 
 ],
[
'specialty_id' => '10', 
'name' => 'Jaw Joint Problems' 
 ],
[
'specialty_id' => '10', 
'name' => 'Orofacial Pain' 
 ],
[
'specialty_id' => '10', 
'name' => 'Periodontitis' 
 ],
[
'specialty_id' => '10', 
'name' => 'TMJ Disorders' 
 ],
[
'specialty_id' => '10', 
'name' => 'Tooth Decay' 
 ],
[
'specialty_id' => '10', 
'name' => 'Toothache' 
 ],
[
'specialty_id' => '96', 
'name' => 'Arthritis Management' 
 ],
[
'specialty_id' => '96', 
'name' => 'Back Ache' 
 ],
[
'specialty_id' => '96', 
'name' => 'Bone Fracture' 
 ],
[
'specialty_id' => '96', 
'name' => 'Bone Trauma' 
 ],
[
'specialty_id' => '96', 
'name' => 'Childhood Joints Deformity' 
 ],
[
'specialty_id' => '96', 
'name' => 'Fracture' 
 ],
[
'specialty_id' => '96', 
'name' => 'Knees Pain' 
 ],
[
'specialty_id' => '96', 
'name' => 'Ligament Tear' 
 ],
[
'specialty_id' => '96', 
'name' => 'Lower Back Pain' 
 ],
[
'specialty_id' => '96', 
'name' => 'Musculoskeletal Infection' 
 ],
[
'specialty_id' => '96', 
'name' => 'Oesteoprosis' 
 ],
[
'specialty_id' => '96', 
'name' => 'Paget’s Disease' 
 ],
[
'specialty_id' => '96', 
'name' => 'Road Traffic Accident' 
 ],
[
'specialty_id' => '96', 
'name' => 'Scoliosis' 
 ],
[
'specialty_id' => '96', 
'name' => 'Sports Injury' 
 ],
[
'specialty_id' => '96', 
'name' => 'Trauma Pain' 
 ],
[
'specialty_id' => '101', 
'name' => 'Arthritis Management' 
 ],
[
'specialty_id' => '101', 
'name' => 'Back Ache' 
 ],
[
'specialty_id' => '101', 
'name' => 'Bone Fracture' 
 ],
[
'specialty_id' => '101', 
'name' => 'Bone Trauma' 
 ],
[
'specialty_id' => '101', 
'name' => 'Childhood Joints Deformity' 
 ],
[
'specialty_id' => '101', 
'name' => 'Fracture' 
 ],
[
'specialty_id' => '101', 
'name' => 'Knees Pain' 
 ],
[
'specialty_id' => '101', 
'name' => 'Ligament Tear' 
 ],
[
'specialty_id' => '101', 
'name' => 'Lower Back Pain' 
 ],
[
'specialty_id' => '101', 
'name' => 'Musculoskeletal Infection' 
 ],
[
'specialty_id' => '101', 
'name' => 'Oesteoprosis' 
 ],
[
'specialty_id' => '101', 
'name' => 'Paget’s Disease' 
 ],
[
'specialty_id' => '101', 
'name' => 'Road Traffic Accident' 
 ],
[
'specialty_id' => '101', 
'name' => 'Scoliosis' 
 ],
[
'specialty_id' => '101', 
'name' => 'Sports Injury' 
 ],
[
'specialty_id' => '101', 
'name' => 'Trauma Pain' 
 ],
[
'specialty_id' => '123', 
'name' => 'Angina ' 
 ],
[
'specialty_id' => '123', 
'name' => 'Arrythmias' 
 ],
[
'specialty_id' => '123', 
'name' => 'Atherosclerosis' 
 ],
[
'specialty_id' => '123', 
'name' => 'Atrial Fibrillation' 
 ],
[
'specialty_id' => '123', 
'name' => 'Cardiac Arrest' 
 ],
[
'specialty_id' => '123', 
'name' => 'Cardiology Diseases' 
 ],
[
'specialty_id' => '123', 
'name' => 'Cardiovascular Diseases' 
 ],
[
'specialty_id' => '123', 
'name' => 'Congenital Diseases' 
 ],
[
'specialty_id' => '123', 
'name' => 'Congenital Heart Diseases' 
 ],
[
'specialty_id' => '123', 
'name' => 'Heart Diseases' 
 ],
[
'specialty_id' => '123', 
'name' => 'Heart Failure' 
 ],
[
'specialty_id' => '123', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '123', 
'name' => 'Ischemic Heart Disease ' 
 ],
[
'specialty_id' => '123', 
'name' => 'Myocardial Infarction (MI)' 
 ],
[
'specialty_id' => '123', 
'name' => 'Palpitations' 
 ],
[
'specialty_id' => '123', 
'name' => 'Pulmonary Edema' 
 ],
[
'specialty_id' => '123', 
'name' => 'Rheumatic Heart Disease' 
 ],
[
'specialty_id' => '123', 
'name' => 'Tachycardia' 
 ],
[
'specialty_id' => '123', 
'name' => 'Tetralogy of Fallot' 
 ],
[
'specialty_id' => '123', 
'name' => 'Ventricular Septal Defect (VSD)' 
 ],
[
'specialty_id' => '101', 
'name' => 'Bone Fracture' 
 ],
[
'specialty_id' => '101', 
'name' => 'Childhood Joints Deformity' 
 ],
[
'specialty_id' => '101', 
'name' => 'Ligament Tear' 
 ],
[
'specialty_id' => '101', 
'name' => 'Road Traffic Accident' 
 ],
[
'specialty_id' => '51', 
'name' => 'Chest Deformity' 
 ],
[
'specialty_id' => '51', 
'name' => 'Cleft Lip and Palate' 
 ],
[
'specialty_id' => '51', 
'name' => 'Conjoined Twins' 
 ],
[
'specialty_id' => '51', 
'name' => 'Pediatric Hernia' 
 ],
[
'specialty_id' => '51', 
'name' => 'Retinopathy of Permaturity' 
 ],
[
'specialty_id' => '51', 
'name' => 'Undescended Testicles' 
 ],
[
'specialty_id' => '122', 
'name' => 'Abdomen Diseases in Children' 
 ],
[
'specialty_id' => '122', 
'name' => 'Abinisim' 
 ],
[
'specialty_id' => '122', 
'name' => 'Allergy in children' 
 ],
[
'specialty_id' => '122', 
'name' => 'Asthma in Children' 
 ],
[
'specialty_id' => '122', 
'name' => 'Bile Duct Stones' 
 ],
[
'specialty_id' => '122', 
'name' => 'Birth Defects' 
 ],
[
'specialty_id' => '122', 
'name' => 'Cerebal Palsy (CP)' 
 ],
[
'specialty_id' => '122', 
'name' => 'Chest Disease in Children' 
 ],
[
'specialty_id' => '122', 
'name' => 'Chest infection in Children' 
 ],
[
'specialty_id' => '122', 
'name' => 'Chicken Pox' 
 ],
[
'specialty_id' => '122', 
'name' => 'Childhood Cancer' 
 ],
[
'specialty_id' => '122', 
'name' => 'Childhood Joints Deformity' 
 ],
[
'specialty_id' => '122', 
'name' => 'Cholestasis' 
 ],
[
'specialty_id' => '122', 
'name' => 'Cirrhosis' 
 ],
[
'specialty_id' => '122', 
'name' => 'Cleft lip and Palate' 
 ],
[
'specialty_id' => '122', 
'name' => 'Diarrhea' 
 ],
[
'specialty_id' => '122', 
'name' => 'Down Syndrome' 
 ],
[
'specialty_id' => '122', 
'name' => 'Eating Problems in Children' 
 ],
[
'specialty_id' => '122', 
'name' => 'Fever in Children' 
 ],
[
'specialty_id' => '122', 
'name' => 'Growth Problem in Children' 
 ],
[
'specialty_id' => '122', 
'name' => 'Hepatitis A ' 
 ],
[
'specialty_id' => '122', 
'name' => 'Hepatitis B' 
 ],
[
'specialty_id' => '122', 
'name' => 'Hepatitis C' 
 ],
[
'specialty_id' => '122', 
'name' => 'Hepatobiliary Disease' 
 ],
[
'specialty_id' => '122', 
'name' => 'Hypothyroidism' 
 ],
[
'specialty_id' => '122', 
'name' => 'Kawasakis Disease' 
 ],
[
'specialty_id' => '122', 
'name' => 'Kidney Problems in Children' 
 ],
[
'specialty_id' => '122', 
'name' => 'Liver Disease' 
 ],
[
'specialty_id' => '122', 
'name' => 'Measles' 
 ],
[
'specialty_id' => '122', 
'name' => 'Meningitis' 
 ],
[
'specialty_id' => '122', 
'name' => 'Mumps' 
 ],
[
'specialty_id' => '122', 
'name' => 'Pediatric hernia' 
 ],
[
'specialty_id' => '122', 
'name' => 'Pneumonia in Children' 
 ],
[
'specialty_id' => '122', 
'name' => 'Respiratory Disease In Children' 
 ],
[
'specialty_id' => '122', 
'name' => 'Respiratory Tract Infection (RTI)' 
 ],
[
'specialty_id' => '122', 
'name' => 'Rubella' 
 ],
[
'specialty_id' => '122', 
'name' => 'Scarlet Fever' 
 ],
[
'specialty_id' => '122', 
'name' => 'Sturge Weber Syndrome' 
 ],
[
'specialty_id' => '122', 
'name' => 'Teething Issue' 
 ],
[
'specialty_id' => '122', 
'name' => 'Tonsillitis' 
 ],
[
'specialty_id' => '122', 
'name' => 'Tuberous Sclerosis' 
 ],
[
'specialty_id' => '134', 
'name' => 'Arthritis' 
 ],
[
'specialty_id' => '134', 
'name' => 'Back Pain' 
 ],
[
'specialty_id' => '134', 
'name' => 'Carpal Tunnel Syndrome' 
 ],
[
'specialty_id' => '134', 
'name' => 'Chronic Pain Syndrome' 
 ],
[
'specialty_id' => '134', 
'name' => 'Concussion' 
 ],
[
'specialty_id' => '134', 
'name' => 'Dizziness, Vertigo, and Imbalance' 
 ],
[
'specialty_id' => '134', 
'name' => 'Frozen Shoulder' 
 ],
[
'specialty_id' => '134', 
'name' => 'Golfer’s Elbow' 
 ],
[
'specialty_id' => '134', 
'name' => 'Heel and Foot Pain' 
 ],
[
'specialty_id' => '134', 
'name' => 'Low Back Pain' 
 ],
[
'specialty_id' => '134', 
'name' => 'Motor Vehicle Accident Injuries' 
 ],
[
'specialty_id' => '134', 
'name' => 'Neck Pain' 
 ],
[
'specialty_id' => '134', 
'name' => 'Pelvic Floor Conditions' 
 ],
[
'specialty_id' => '134', 
'name' => 'Post surgery' 
 ],
[
'specialty_id' => '134', 
'name' => 'Repetitive Strain' 
 ],
[
'specialty_id' => '134', 
'name' => 'Rotator cull Injury' 
 ],
[
'specialty_id' => '134', 
'name' => 'Running Injuries' 
 ],
[
'specialty_id' => '134', 
'name' => 'Sciatica' 
 ],
[
'specialty_id' => '134', 
'name' => 'Shoulder Pain' 
 ],
[
'specialty_id' => '134', 
'name' => 'Sports Injuries' 
 ],
[
'specialty_id' => '134', 
'name' => 'Sprains and Strains' 
 ],
[
'specialty_id' => '134', 
'name' => 'Tendonitis' 
 ],
[
'specialty_id' => '134', 
'name' => 'Tennis Elbow' 
 ],
[
'specialty_id' => '134', 
'name' => 'TMJ Dysfunction' 
 ],
[
'specialty_id' => '134', 
'name' => 'Whilplash' 
 ],
[
'specialty_id' => '27', 
'name' => 'Acne' 
 ],
[
'specialty_id' => '27', 
'name' => 'Acne Scars' 
 ],
[
'specialty_id' => '27', 
'name' => 'Benign and Malignant Skin Tumors' 
 ],
[
'specialty_id' => '27', 
'name' => 'Benign skin Tumors' 
 ],
[
'specialty_id' => '27', 
'name' => 'Facial Disfigurement' 
 ],
[
'specialty_id' => '27', 
'name' => 'Malignant skin Tumors' 
 ],
[
'specialty_id' => '27', 
'name' => 'Moles' 
 ],
[
'specialty_id' => '27', 
'name' => 'Skin Allergy' 
 ],
[
'specialty_id' => '27', 
'name' => 'Skin Diseases' 
 ],
[
'specialty_id' => '27', 
'name' => 'Skin Fungal Infections' 
 ],
[
'specialty_id' => '27', 
'name' => 'Vitiligo' 
 ],
[
'specialty_id' => '27', 
'name' => 'Warts' 
 ],
[
'specialty_id' => '152', 
'name' => 'Addiction' 
 ],
[
'specialty_id' => '152', 
'name' => 'Anxiety Disorder' 
 ],
[
'specialty_id' => '152', 
'name' => 'Aphasia' 
 ],
[
'specialty_id' => '152', 
'name' => 'Attention Deficit Hyperactivity Disorder (ADHD)' 
 ],
[
'specialty_id' => '152', 
'name' => 'Bipolar Disorder' 
 ],
[
'specialty_id' => '152', 
'name' => 'Depression' 
 ],
[
'specialty_id' => '152', 
'name' => 'Drug Addiction' 
 ],
[
'specialty_id' => '152', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '152', 
'name' => 'Learning Difficulty' 
 ],
[
'specialty_id' => '152', 
'name' => 'Obsessive Compulsive Disorder' 
 ],
[
'specialty_id' => '152', 
'name' => 'Panic Disorder' 
 ],
[
'specialty_id' => '152', 
'name' => 'Phobias' 
 ],
[
'specialty_id' => '152', 
'name' => 'Post Traumatic Stress Disorder (PTSD)' 
 ],
[
'specialty_id' => '152', 
'name' => 'Schizophrenia' 
 ],
[
'specialty_id' => '152', 
'name' => 'Sleep Disorder' 
 ],
[
'specialty_id' => '152', 
'name' => 'Social phobia' 
 ],
[
'specialty_id' => '153', 
'name' => 'Addiction' 
 ],
[
'specialty_id' => '153', 
'name' => 'Anxiety Disorder' 
 ],
[
'specialty_id' => '153', 
'name' => 'Bruxism' 
 ],
[
'specialty_id' => '153', 
'name' => 'Concentration Problems' 
 ],
[
'specialty_id' => '153', 
'name' => 'Depression' 
 ],
[
'specialty_id' => '153', 
'name' => 'Drug Addiction' 
 ],
[
'specialty_id' => '153', 
'name' => 'Early Parenting Issues' 
 ],
[
'specialty_id' => '153', 
'name' => 'Emotional Outbursts' 
 ],
[
'specialty_id' => '153', 
'name' => 'Family Problems' 
 ],
[
'specialty_id' => '153', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '153', 
'name' => 'Marital Conflict' 
 ],
[
'specialty_id' => '153', 
'name' => 'Panic Disorder' 
 ],
[
'specialty_id' => '153', 
'name' => 'Phobias' 
 ],
[
'specialty_id' => '153', 
'name' => 'Sleep Disorder' 
 ],
[
'specialty_id' => '153', 
'name' => 'Social Phobia ' 
 ],
[
'specialty_id' => '153', 
'name' => 'Stammering' 
 ],
[
'specialty_id' => '148', 
'name' => 'Addiction' 
 ],
[
'specialty_id' => '148', 
'name' => 'Anxiety Disorder' 
 ],
[
'specialty_id' => '148', 
'name' => 'Bruxism' 
 ],
[
'specialty_id' => '148', 
'name' => 'Concentration Problems' 
 ],
[
'specialty_id' => '148', 
'name' => 'Depression' 
 ],
[
'specialty_id' => '148', 
'name' => 'Drug Addiction' 
 ],
[
'specialty_id' => '148', 
'name' => 'Early Parenting Issues' 
 ],
[
'specialty_id' => '148', 
'name' => 'Emotional Outbursts' 
 ],
[
'specialty_id' => '148', 
'name' => 'Family Problems' 
 ],
[
'specialty_id' => '148', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '148', 
'name' => 'Marital Conflict' 
 ],
[
'specialty_id' => '148', 
'name' => 'Panic Disorder' 
 ],
[
'specialty_id' => '148', 
'name' => 'Phobias' 
 ],
[
'specialty_id' => '148', 
'name' => 'Sleep Disorder' 
 ],
[
'specialty_id' => '148', 
'name' => 'Social Phobia ' 
 ],
[
'specialty_id' => '148', 
'name' => 'Stammering' 
 ],
[
'specialty_id' => '149', 
'name' => 'Addiction' 
 ],
[
'specialty_id' => '149', 
'name' => 'Anxiety Disorder' 
 ],
[
'specialty_id' => '149', 
'name' => 'Bruxism' 
 ],
[
'specialty_id' => '149', 
'name' => 'Concentration Problems' 
 ],
[
'specialty_id' => '149', 
'name' => 'Depression' 
 ],
[
'specialty_id' => '149', 
'name' => 'Drug Addiction' 
 ],
[
'specialty_id' => '149', 
'name' => 'Early Parenting Issues' 
 ],
[
'specialty_id' => '149', 
'name' => 'Emotional Outbursts' 
 ],
[
'specialty_id' => '149', 
'name' => 'Family Problems' 
 ],
[
'specialty_id' => '149', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '149', 
'name' => 'Marital Conflict' 
 ],
[
'specialty_id' => '149', 
'name' => 'Panic Disorder' 
 ],
[
'specialty_id' => '149', 
'name' => 'Phobias' 
 ],
[
'specialty_id' => '149', 
'name' => 'Sleep Disorder' 
 ],
[
'specialty_id' => '149', 
'name' => 'Social Phobia ' 
 ],
[
'specialty_id' => '149', 
'name' => 'Stammering' 
 ],
[
'specialty_id' => '130', 
'name' => 'Acute Respiratory Distress Syndrome' 
 ],
[
'specialty_id' => '130', 
'name' => 'Asthma ' 
 ],
[
'specialty_id' => '130', 
'name' => 'Atelectasis' 
 ],
[
'specialty_id' => '130', 
'name' => 'Bronchial Asthma' 
 ],
[
'specialty_id' => '130', 
'name' => 'Bronchiectasis' 
 ],
[
'specialty_id' => '130', 
'name' => 'Bronchitis' 
 ],
[
'specialty_id' => '130', 
'name' => 'Chronic Bronchitis' 
 ],
[
'specialty_id' => '130', 
'name' => 'Chronic Obstructive Pulmonary Diseases (COPD)' 
 ],
[
'specialty_id' => '130', 
'name' => 'Cystic Fibrosis' 
 ],
[
'specialty_id' => '130', 
'name' => 'Embolism' 
 ],
[
'specialty_id' => '130', 
'name' => 'Emphysema' 
 ],
[
'specialty_id' => '130', 
'name' => 'Hypoxia' 
 ],
[
'specialty_id' => '130', 
'name' => 'Interstitial Lung Disease (ILD)' 
 ],
[
'specialty_id' => '130', 
'name' => 'Lung Cancer' 
 ],
[
'specialty_id' => '130', 
'name' => 'Lung Lymphoma' 
 ],
[
'specialty_id' => '130', 
'name' => 'Mucus Plug' 
 ],
[
'specialty_id' => '130', 
'name' => 'Phneumonia' 
 ],
[
'specialty_id' => '130', 
'name' => 'Pulmonary Hypertension' 
 ],
[
'specialty_id' => '130', 
'name' => 'Sarcoidosis' 
 ],
[
'specialty_id' => '130', 
'name' => 'Tuberculosis' 
 ],
[
'specialty_id' => '130', 
'name' => 'Tuberous sclerosis' 
 ],
[
'specialty_id' => '136', 
'name' => 'Back Pain' 
 ],
[
'specialty_id' => '136', 
'name' => 'Chronic Fatigue' 
 ],
[
'specialty_id' => '136', 
'name' => 'Fibromyalgia' 
 ],
[
'specialty_id' => '136', 
'name' => 'High Blood Pressure' 
 ],
[
'specialty_id' => '136', 
'name' => 'Musculoskeletal Problems' 
 ],
[
'specialty_id' => '136', 
'name' => 'Sciatica' 
 ],
[
'specialty_id' => '136', 
'name' => 'TMJ' 
 ],
[
'specialty_id' => '139', 
'name' => 'Back Pain' 
 ],
[
'specialty_id' => '139', 
'name' => 'Chronic Fatigue' 
 ],
[
'specialty_id' => '139', 
'name' => 'Fibromyalgia' 
 ],
[
'specialty_id' => '139', 
'name' => 'High Blood Pressure' 
 ],
[
'specialty_id' => '139', 
'name' => 'Musculoskeletal Problems' 
 ],
[
'specialty_id' => '139', 
'name' => 'Sciatica' 
 ],
[
'specialty_id' => '139', 
'name' => 'TMJ' 
 ],
[
'specialty_id' => '79', 
'name' => 'Abdominal Pain' 
 ],
[
'specialty_id' => '79', 
'name' => 'Acid Peptic Disease' 
 ],
[
'specialty_id' => '79', 
'name' => 'Anemia' 
 ],
[
'specialty_id' => '79', 
'name' => 'Angina' 
 ],
[
'specialty_id' => '79', 
'name' => 'Ankylosing Spondylitis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Arrythmias' 
 ],
[
'specialty_id' => '79', 
'name' => 'Arthritis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Asthma' 
 ],
[
'specialty_id' => '79', 
'name' => 'Back Ache' 
 ],
[
'specialty_id' => '79', 
'name' => 'Behcet’s Disease' 
 ],
[
'specialty_id' => '79', 
'name' => 'Black Stools' 
 ],
[
'specialty_id' => '79', 
'name' => 'Bloating' 
 ],
[
'specialty_id' => '79', 
'name' => 'Blood Pressure' 
 ],
[
'specialty_id' => '79', 
'name' => 'Bursitis/ Tendinitis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Cardiac Diseases' 
 ],
[
'specialty_id' => '79', 
'name' => 'Cardiovascular Diseases' 
 ],
[
'specialty_id' => '79', 
'name' => 'Chest Infections' 
 ],
[
'specialty_id' => '79', 
'name' => 'Chronic Bronchitis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Chronic Hepatitis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Chronic Obstructive Pulmonary Diseases (COPD)' 
 ],
[
'specialty_id' => '79', 
'name' => 'Churg- Strauss syndrome' 
 ],
[
'specialty_id' => '79', 
'name' => 'Cold And Influenza ' 
 ],
[
'specialty_id' => '79', 
'name' => 'Common Cold' 
 ],
[
'specialty_id' => '79', 
'name' => 'Corticosteroid-Induced Osteoporosis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Cough' 
 ],
[
'specialty_id' => '79', 
'name' => 'Diabetes Mellitus' 
 ],
[
'specialty_id' => '79', 
'name' => 'Diarrhea' 
 ],
[
'specialty_id' => '79', 
'name' => 'Dyspepsia' 
 ],
[
'specialty_id' => '79', 
'name' => 'Enteric Fever' 
 ],
[
'specialty_id' => '79', 
'name' => 'Fatigue' 
 ],
[
'specialty_id' => '79', 
'name' => 'Fever' 
 ],
[
'specialty_id' => '79', 
'name' => 'Fibromyalgia' 
 ],
[
'specialty_id' => '79', 
'name' => 'Flatulence' 
 ],
[
'specialty_id' => '79', 
'name' => 'Gas' 
 ],
[
'specialty_id' => '79', 
'name' => 'Gastric Ulcer' 
 ],
[
'specialty_id' => '79', 
'name' => 'Gastroenteritis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Gaint Cell Arteritis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Gout' 
 ],
[
'specialty_id' => '79', 
'name' => 'Heart Failure' 
 ],
[
'specialty_id' => '79', 
'name' => 'Helicobacter Pylori Bacteria' 
 ],
[
'specialty_id' => '79', 
'name' => 'Henoch-Schonlein Purpura' 
 ],
[
'specialty_id' => '79', 
'name' => 'Hepatitis B' 
 ],
[
'specialty_id' => '79', 
'name' => 'Hepatitis C' 
 ],
[
'specialty_id' => '79', 
'name' => 'Hypersensitively Vasculitis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '79', 
'name' => 'Hyperthyroidism' 
 ],
[
'specialty_id' => '79', 
'name' => 'Hypoglycemia' 
 ],
[
'specialty_id' => '79', 
'name' => 'Hypothyroidism' 
 ],
[
'specialty_id' => '79', 
'name' => 'Infectious Diseases' 
 ],
[
'specialty_id' => '79', 
'name' => 'Interstitial Lung Diseases (IDL)' 
 ],
[
'specialty_id' => '79', 
'name' => 'Ischemic Heart Disease' 
 ],
[
'specialty_id' => '79', 
'name' => 'Jaundice' 
 ],
[
'specialty_id' => '79', 
'name' => 'Juvenile Rheumatoid Arthritis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Kawasaki Syndrome' 
 ],
[
'specialty_id' => '79', 
'name' => 'Liver Diseases' 
 ],
[
'specialty_id' => '79', 
'name' => 'Lupus' 
 ],
[
'specialty_id' => '79', 
'name' => 'Lupus Nephritis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Lyme Disease' 
 ],
[
'specialty_id' => '79', 
'name' => 'Metabolic Syndrome' 
 ],
[
'specialty_id' => '79', 
'name' => 'Mixed Connective Tissue Disease' 
 ],
[
'specialty_id' => '79', 
'name' => 'Myositis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Nausea' 
 ],
[
'specialty_id' => '79', 
'name' => 'Osteoarthritis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Pancreas Disease' 
 ],
[
'specialty_id' => '79', 
'name' => 'Pneumonia' 
 ],
[
'specialty_id' => '79', 
'name' => 'Polyarteritis Nodosa' 
 ],
[
'specialty_id' => '79', 
'name' => 'Polymyalgia Rheumatica' 
 ],
[
'specialty_id' => '79', 
'name' => 'Polymyositis/ Dermatomyositis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Primary Antiphospholipid Syndrome' 
 ],
[
'specialty_id' => '79', 
'name' => 'Psoriatic Arthritis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Raynaud’s phenomenon' 
 ],
[
'specialty_id' => '79', 
'name' => 'Reactive Arthritis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Respiratory Diseases' 
 ],
[
'specialty_id' => '79', 
'name' => 'Rheumatoid arthritis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Scleroderma' 
 ],
[
'specialty_id' => '79', 
'name' => 'Sjogren’s Syndrome' 
 ],
[
'specialty_id' => '79', 
'name' => 'Takayasu’s Arthritis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Thyroid Disease' 
 ],
[
'specialty_id' => '79', 
'name' => 'Tuberculosis' 
 ],
[
'specialty_id' => '79', 
'name' => 'Typhoid Fever' 
 ],
[
'specialty_id' => '79', 
'name' => 'undifferentiated Spondyloarthropathy' 
 ],
[
'specialty_id' => '79', 
'name' => 'vasculitis' 
 ],
[
'specialty_id' => '79', 
'name' => 'wegener’s Granulomatosis' 
 ],
[
'specialty_id' => '65', 
'name' => 'Acne' 
 ],
[
'specialty_id' => '65', 
'name' => 'Acne Scars' 
 ],
[
'specialty_id' => '65', 
'name' => 'Benign and Malignant Skin Tumors' 
 ],
[
'specialty_id' => '65', 
'name' => 'Erectile Dysfunction' 
 ],
[
'specialty_id' => '65', 
'name' => 'Fissure' 
 ],
[
'specialty_id' => '65', 
'name' => 'Herpes' 
 ],
[
'specialty_id' => '65', 
'name' => 'Hydrocele' 
 ],
[
'specialty_id' => '65', 
'name' => 'Moles' 
 ],
[
'specialty_id' => '65', 
'name' => 'Night Fall' 
 ],
[
'specialty_id' => '65', 
'name' => 'Premature Ejaculation' 
 ],
[
'specialty_id' => '65', 
'name' => 'Psoriasis' 
 ],
[
'specialty_id' => '65', 
'name' => 'Scabies' 
 ],
[
'specialty_id' => '65', 
'name' => 'Skin Diseases' 
 ],
[
'specialty_id' => '65', 
'name' => 'Syphilis' 
 ],
[
'specialty_id' => '65', 
'name' => 'Vitiligo' 
 ],
[
'specialty_id' => '65', 
'name' => 'Warts' 
 ],
[
'specialty_id' => '80', 
'name' => 'Acute Respiratory Distress Syndrome' 
 ],
[
'specialty_id' => '80', 
'name' => 'Asthma' 
 ],
[
'specialty_id' => '80', 
'name' => 'Atelectasis' 
 ],
[
'specialty_id' => '80', 
'name' => 'Bronchial Asthma' 
 ],
[
'specialty_id' => '80', 
'name' => 'Bronchiectasis' 
 ],
[
'specialty_id' => '80', 
'name' => 'Bronchitis' 
 ],
[
'specialty_id' => '80', 
'name' => 'Chronic Bronchitis' 
 ],
[
'specialty_id' => '80', 
'name' => 'Chronic Obstructive Pulmonary Diseases (COPD)' 
 ],
[
'specialty_id' => '80', 
'name' => 'Embolism' 
 ],
[
'specialty_id' => '80', 
'name' => 'Emphysema' 
 ],
[
'specialty_id' => '80', 
'name' => 'Hypoxia' 
 ],
[
'specialty_id' => '80', 
'name' => 'Interstitial Lung Disease (ILD)' 
 ],
[
'specialty_id' => '80', 
'name' => 'Lung Cancer' 
 ],
[
'specialty_id' => '80', 
'name' => 'Mucus Plug' 
 ],
[
'specialty_id' => '80', 
'name' => 'Pneumonia' 
 ],
[
'specialty_id' => '80', 
'name' => 'Pulmonary Hypertension' 
 ],
[
'specialty_id' => '80', 
'name' => 'Sarcoidosis' 
 ],
[
'specialty_id' => '80', 
'name' => 'tuberulosis' 
 ],
[
'specialty_id' => '168', 
'name' => 'Amenorrhoea' 
 ],
[
'specialty_id' => '168', 
'name' => 'Aplastic Anemia' 
 ],
[
'specialty_id' => '168', 
'name' => 'Ectopic Pregnancy' 
 ],
[
'specialty_id' => '168', 
'name' => 'Female Urinary Problems' 
 ],
[
'specialty_id' => '168', 
'name' => 'Fibroids' 
 ],
[
'specialty_id' => '168', 
'name' => 'High Risk Pregnancy' 
 ],
[
'specialty_id' => '168', 
'name' => 'Infectious Diseases' 
 ],
[
'specialty_id' => '168', 
'name' => 'Menopause' 
 ],
[
'specialty_id' => '168', 
'name' => 'Menstruation Problems' 
 ],
[
'specialty_id' => '168', 
'name' => 'Miscarriage' 
 ],
[
'specialty_id' => '168', 
'name' => 'Ovarian Cyst' 
 ],
[
'specialty_id' => '168', 
'name' => 'Pregnancy' 
 ],
[
'specialty_id' => '168', 
'name' => 'Uro Gynae Cancer' 
 ],
[
'specialty_id' => '142', 
'name' => 'Anxiety Disorder' 
 ],
[
'specialty_id' => '142', 
'name' => 'Concentration Problems' 
 ],
[
'specialty_id' => '142', 
'name' => 'Depression' 
 ],
[
'specialty_id' => '142', 
'name' => 'Early Parenting issues' 
 ],
[
'specialty_id' => '142', 
'name' => 'Family Problems' 
 ],
[
'specialty_id' => '142', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '142', 
'name' => 'Marital Conflict' 
 ],
[
'specialty_id' => '142', 
'name' => 'Phobias' 
 ],
[
'specialty_id' => '142', 
'name' => 'Sleep Disorder' 
 ],
[
'specialty_id' => '142', 
'name' => 'Social Phobia' 
 ],
[
'specialty_id' => '142', 
'name' => 'Stammering' 
 ],
[
'specialty_id' => '142', 
'name' => 'Anxiety Disorder' 
 ],
[
'specialty_id' => '142', 
'name' => 'Concentration Problems' 
 ],
[
'specialty_id' => '44', 
'name' => 'Abdominal Aortic Aneurysm (AAA)' 
 ],
[
'specialty_id' => '44', 
'name' => 'Aortic Arch Conditions' 
 ],
[
'specialty_id' => '44', 
'name' => 'Aortoiliac Occlusive Disease' 
 ],
[
'specialty_id' => '44', 
'name' => 'Arm Artery Disease' 
 ],
[
'specialty_id' => '44', 
'name' => 'Carotid Artery Disease' 
 ],
[
'specialty_id' => '44', 
'name' => 'Chronic Pelvic Pain' 
 ],
[
'specialty_id' => '44', 
'name' => 'Chronic Ulcer of the Leg/foot' 
 ],
[
'specialty_id' => '44', 
'name' => 'Chronic Venous Insufficiency' 
 ],
[
'specialty_id' => '44', 
'name' => 'Chronic Venous Thrombosis' 
 ],
[
'specialty_id' => '44', 
'name' => 'Deep Vein Thrombosis (DVT)' 
 ],
[
'specialty_id' => '44', 
'name' => 'Diabetic Vascular Disease' 
 ],
[
'specialty_id' => '44', 
'name' => 'Digital Artery Conditions' 
 ],
[
'specialty_id' => '44', 
'name' => 'Hyperlipidemia' 
 ],
[
'specialty_id' => '44', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '44', 
'name' => 'Leg Artery Disease' 
 ],
[
'specialty_id' => '44', 
'name' => 'Liver Disease' 
 ],
[
'specialty_id' => '44', 
'name' => 'Lymphedema' 
 ],
[
'specialty_id' => '44', 
'name' => 'Pelvic Venous Incompetence' 
 ],
[
'specialty_id' => '44', 
'name' => 'Peripheral Artery Disease (PAD)' 
 ],
[
'specialty_id' => '44', 
'name' => 'Portal hypertension' 
 ],
[
'specialty_id' => '44', 
'name' => 'Pulmonary Embolism' 
 ],
[
'specialty_id' => '44', 
'name' => 'Renal Failure' 
 ],
[
'specialty_id' => '44', 
'name' => 'Renovascular Conditions' 
 ],
[
'specialty_id' => '44', 
'name' => 'Stroke ' 
 ],
[
'specialty_id' => '44', 
'name' => 'Thoracic Aneurysm' 
 ],
[
'specialty_id' => '44', 
'name' => 'Thoracic Outlet Syndrome' 
 ],
[
'specialty_id' => '44', 
'name' => 'Thoracic Outlet Syndrome' 
 ],
[
'specialty_id' => '44', 
'name' => 'Varicose Malformations' 
 ],
[
'specialty_id' => '44', 
'name' => 'Venous Ulcerations' 
 ],
[
'specialty_id' => '44', 
'name' => 'Visceral Artery Conditions' 
 ],
[
'specialty_id' => '57', 
'name' => 'Abdominal Aortic Aneurysm (AAA)' 
 ],
[
'specialty_id' => '57', 
'name' => 'Aortic Arch Conditions' 
 ],
[
'specialty_id' => '57', 
'name' => 'Aortoiliac Occlusive Disease' 
 ],
[
'specialty_id' => '57', 
'name' => 'Arm Artery Disease' 
 ],
[
'specialty_id' => '57', 
'name' => 'Carotid Artery Disease' 
 ],
[
'specialty_id' => '57', 
'name' => 'Chronic Pelvic Pain' 
 ],
[
'specialty_id' => '57', 
'name' => 'Chronic Ulcer of the Leg/foot' 
 ],
[
'specialty_id' => '57', 
'name' => 'Chronic Venous Insufficiency' 
 ],
[
'specialty_id' => '57', 
'name' => 'Chronic Venous Thrombosis' 
 ],
[
'specialty_id' => '57', 
'name' => 'Deep Vein Thrombosis (DVT)' 
 ],
[
'specialty_id' => '57', 
'name' => 'Diabetic Vascular Disease' 
 ],
[
'specialty_id' => '57', 
'name' => 'Digital Artery Conditions' 
 ],
[
'specialty_id' => '57', 
'name' => 'Hyperlipidemia' 
 ],
[
'specialty_id' => '57', 
'name' => 'Hypertension' 
 ],
[
'specialty_id' => '57', 
'name' => 'Leg Artery Disease' 
 ],
[
'specialty_id' => '57', 
'name' => 'Liver Disease' 
 ],
[
'specialty_id' => '57', 
'name' => 'Lymphedema' 
 ],
[
'specialty_id' => '57', 
'name' => 'Pelvic Venous Incompetence' 
 ],
[
'specialty_id' => '57', 
'name' => 'Peripheral Artery Disease (PAD)' 
 ],
[
'specialty_id' => '57', 
'name' => 'Portal hypertension' 
 ],
[
'specialty_id' => '57', 
'name' => 'Pulmonary Embolism' 
 ],
[
'specialty_id' => '57', 
'name' => 'Renal Failure' 
 ],
[
'specialty_id' => '57', 
'name' => 'Renovascular Conditions' 
 ],
[
'specialty_id' => '57', 
'name' => 'Stroke ' 
 ],
[
'specialty_id' => '57', 
'name' => 'Thoracic Aneurysm' 
 ],
[
'specialty_id' => '57', 
'name' => 'Thoracic Outlet Syndrome' 
 ],
[
'specialty_id' => '57', 
'name' => 'Thoracic Outlet Syndrome' 
 ],
[
'specialty_id' => '57', 
'name' => 'Varicose Malformations' 
 ],
[
'specialty_id' => '57', 
'name' => 'Venous Ulcerations' 
 ],
[
'specialty_id' => '57', 
'name' => 'Visceral Artery Conditions' 
 ],
[
'specialty_id' => '174', 
'name' => 'Abdomen Diseases in children' 
 ],
[
'specialty_id' => '174', 
'name' => 'Abdomen Pain' 
 ],
[
'specialty_id' => '174', 
'name' => 'Absecess' 
 ],
[
'specialty_id' => '174', 
'name' => 'Anal And percoanal conditions' 
 ],
[
'specialty_id' => '174', 
'name' => 'Anl Fissure' 
 ],
[
'specialty_id' => '174', 
'name' => 'Bedwetting' 
 ],
[
'specialty_id' => '174', 
'name' => 'Bladder diverticula' 
 ],
[
'specialty_id' => '174', 
'name' => 'Bedder Diverticula' 
 ],
[
'specialty_id' => '174', 
'name' => 'Bladder Propase' 
 ],
[
'specialty_id' => '174', 
'name' => 'Blood In Urin' 
 ],
[
'specialty_id' => '174', 
'name' => 'BPH' 
 ],
[
'specialty_id' => '174', 
'name' => 'Burning Micturition' 
 ],
[
'specialty_id' => '174', 
'name' => 'Constipation' 
 ],
[
'specialty_id' => '174', 
'name' => 'Cyst' 
 ],
[
'specialty_id' => '174', 
'name' => 'Enuresis' 
 ],
[
'specialty_id' => '174', 
'name' => 'Epidiasis' 
 ],
[
'specialty_id' => '174', 
'name' => 'Erect Dysfuction' 
 ],
[
'specialty_id' => '174', 
'name' => 'Female Urinary Inocontinence' 
 ],
[
'specialty_id' => '174', 
'name' => 'Fissure ' 
 ],
[
'specialty_id' => '174', 
'name' => 'Gynaecomastia' 
 ],
[
'specialty_id' => '174', 
'name' => 'Haematuria' 
 ],
[
'specialty_id' => '174', 
'name' => 'Hypospadias' 
 ],
[
'specialty_id' => '174', 
'name' => 'Ingrown Toe Nail' 
 ],
[
'specialty_id' => '174', 
'name' => 'Kidney Infection' 
 ],
[
'specialty_id' => '174', 
'name' => 'Kidney Stone' 
 ],
[
'specialty_id' => '174', 
'name' => 'Kidney Tumor' 
 ],
[
'specialty_id' => '174', 
'name' => 'Kaw Testosterone' 
 ],
[
'specialty_id' => '174', 
'name' => 'Kumps and Bumps' 
 ],
[
'specialty_id' => '174', 
'name' => 'Male Infertility' 
 ],
[
'specialty_id' => '174', 
'name' => 'Male Sexual Dysfuntion' 
 ],
[
'specialty_id' => '174', 
'name' => 'Male urinary incontinence' 
 ],
[
'specialty_id' => '174', 
'name' => 'Pediatric hernia' 
 ],
[
'specialty_id' => '174', 
'name' => 'Penile venous leadage' 
 ],
[
'specialty_id' => '174', 
'name' => 'Perineal Pain' 
 ],
[
'specialty_id' => '174', 
'name' => 'Peyronie’s Disease' 
 ],
[
'specialty_id' => '174', 
'name' => 'Premature Ejaculation' 
 ],
[
'specialty_id' => '174', 
'name' => 'Prostate Cancer' 
 ],
[
'specialty_id' => '174', 
'name' => 'Prostate Enlargement' 
 ],
[
'specialty_id' => '174', 
'name' => 'Prostratitis' 
 ],
[
'specialty_id' => '174', 
'name' => 'Testicular Torsin' 
 ],
[
'specialty_id' => '174', 
'name' => 'Tumors of the Tests' 
 ],
[
'specialty_id' => '174', 
'name' => 'Undescended TesticlesUre' 
 ],
[
'specialty_id' => '174', 
'name' => 'ter Stone' 
 ],
[
'specialty_id' => '174', 
'name' => 'Urethral stricture' 
 ],
[
'specialty_id' => '174', 
'name' => 'Urinary Bladder Cancer' 
 ],
[
'specialty_id' => '174', 
'name' => 'Urinary Bladder Stone' 
 ],
[
'specialty_id' => '174', 
'name' => 'Urinary Incontinence' 
 ],
[
'specialty_id' => '174', 
'name' => 'Urinary Problems' 
 ],
[
'specialty_id' => '174', 
'name' => 'Urinary Stones' 
 ],
[
'specialty_id' => '174', 
'name' => 'Urinary Tract Infection' 
 ],
[
'specialty_id' => '174', 
'name' => 'Urine Blockage' 
 ],
[
'specialty_id' => '174', 
'name' => 'varicocele' 
 ],


        ]);
        }
    
}
