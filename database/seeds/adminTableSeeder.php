<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
use Faker\factory as Faker;


class adminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
           $faker =faker::create();
        foreach(range(1,300)as $index){

            DB::table('admins')->insert([
       		'name'=>$faker->sentence(5),
       		'pic'=>$faker->sentence(5),
       		'email'=>$faker->sentence(5),
       		'password'=>$faker->sentence(5),
       		'phone'=>$faker->sentence(5),
       		'age'=>$faker->sentence(5),
       		'gender'=>$faker->sentence(5),
       		'type'=>$faker->sentence(5),
       		
        ]);
        }
    }
}
