<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       
          $this->call(specialityTableSeed::class);

          $this->call(cityTableSeeder::class);
          $this->call(speciality_classTableseed::class);
//           $this->call(addressTableSeed::class);
//           $this->call(address_doctor_viewTableSeeder::class);
//           $this->call(adminTableSeeder::class);

//          $this->call(appointmentTableSeeder::class);

//           $this->call(categoriesTableSeeder::class);

//           $this->call(chatTableSeeder::class);

//           $this->call(CommentTableSeeder::class);

//           $this->call(districtTableSeeder::class);

           $this->call(doctorTableSeeder::class);

//           $this->call(doctorspecialityTableSeeder::class);

         $this->call(doctor_conditionTableSeeder::class);

//           $this->call(doctor_condition_viewTableSeeder::class);

//           $this->call(doctor_educationTableSeeder::class);

//   $this->call(doctor_experienceTableSeeder::class);

// $this->call(doctor_notificationTableSeeder::class);

         $this->call(doctor_servicesTableSeeder::class);

//           $this->call(doctor_services_viewTableSeeder::class);

//           $this->call(doctor_viewTableSeeder::class);

//           $this->call(documentTableSeeder::class);

//          $this->call(failed_jobsTableSeeder::class);
         

//            $this->call(favorite_doctorTableSeeder::class);

//           $this->call(feedbackTableSeeder::class);

//            $this->call(messageTableSeeder::class);


//            $this->call(patientTableSeeder::class);
//            $this->call(paymentTableSeeder::class);
//            $this->call(phone_verificationTableSeeder::class);
//            $this->call(postTableSeeder::class);
//            $this->call(socialmediaTableSeeder::class);
//            $this->call(postTableSeeder::class);
    }
}
