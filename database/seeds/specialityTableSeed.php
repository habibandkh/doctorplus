<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
//use Faker\factory as Faker;


class specialityTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        

            DB::table('speciality')->insert([
[
'class_spa_id' => '11', 
'specialityName' => 'Advanced Heart failure and Transplant Cardiologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '11', 
'specialityName' => 'Cardiac Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '11', 
'specialityName' => 'Cardiothoracic Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '11', 
'specialityName' => 'Cardiovascular Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '11', 
'specialityName' => 'Clinical Cardiac Electrophysiologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '11', 
'specialityName' => 'Interventional Cardiologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '17', 
'specialityName' => 'Cosmetic Dentist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '17', 
'specialityName' => 'Endodontist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '17', 
'specialityName' => 'Oral and Maxillofacial Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '17', 
'specialityName' => 'Orthodontist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '17', 
'specialityName' => 'Orthotist and Prosthetist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '17', 
'specialityName' => 'Periodontist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '17', 
'specialityName' => 'Restorative Dentist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '17', 
'specialityName' => 'Special in operative Dentistry', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '2', 
'specialityName' => 'Dermatologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '2', 
'specialityName' => 'Laser specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '2', 
'specialityName' => 'Pediatric Dermatologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '2', 
'specialityName' => 'Procedural Dermatologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '8', 
'specialityName' => 'Audiologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '8', 
'specialityName' => 'ENT Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '8', 
'specialityName' => 'ENT Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '13', 
'specialityName' => 'Anterior segment/Cornea specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '13', 
'specialityName' => 'Glaucoma specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '13', 
'specialityName' => 'Neuro-ophthalmologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '13', 
'specialityName' => 'Ocular Oncology', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '13', 
'specialityName' => 'Oculoplastic / Orbit specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '13', 
'specialityName' => 'Ophthalmic Plastic and Reconstructive Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '13', 
'specialityName' => 'Strabismus/Pediatric ophthalmologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '13', 
'specialityName' => 'Optometrist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '13', 
'specialityName' => 'Vitero-Retina Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Bariatric Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Breast Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Burns Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Cancer Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Colorectal Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Cosmetic Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Endoscopic Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Endovascular Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'General Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Gynecological Oncology Specialist Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Hepatobiliary and Liver Transplant Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Kidney Transplant Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Laparoscopic Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Oral and Maxillofacial Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Pediatric Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '9', 
'specialityName' => 'Vascular Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '1', 
'specialityName' => 'Fertility Consultant', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '1', 
'specialityName' => 'Female Pelvic Medicine and reconstructive surgeon (Fistula specialist)', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '1', 
'specialityName' => 'Gynecologic Oncology specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '1', 
'specialityName' => 'Gynecologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '1', 
'specialityName' => 'Male Sexual Health Specialist (Andrologist)', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '1', 
'specialityName' => 'Maternal-Fetal Medicine specialist (Obstetrician) ', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '1', 
'specialityName' => 'Reproductive Endocrinologist and Infertility Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '1', 
'specialityName' => 'Sexologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Aesthetic Medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Allergy and Immunology Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Community Medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Consultant Physician', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Critical Care Medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Diabetes Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Endocrinologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Gastroenterologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Geriatric Medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Infectious Diseases Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Internal Medicine Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Oncologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Pediatric Internal Medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Rheumatologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Sleep medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Sports medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Transplant Hepatology specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '4', 
'specialityName' => 'Brain Injury Medicine Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '4', 
'specialityName' => 'Child Neurologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '4', 
'specialityName' => 'Endovascular Surgical Neuroradiologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '4', 
'specialityName' => 'Hospice and Palliative Medicine Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '4', 
'specialityName' => 'Neurodevelopmental Disabilities Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '4', 
'specialityName' => 'Neurologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '4', 
'specialityName' => 'Neurosurgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '4', 
'specialityName' => 'Neuromuscular Medicine Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '4', 
'specialityName' => 'Pain Medicine Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '4', 
'specialityName' => 'Vascular Neurologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '7', 
'specialityName' => 'NUTRITIONIST', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '19', 
'specialityName' => 'Onco-surgeon (cancer surgeon)', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '19', 
'specialityName' => 'Oncologist (cancer medicine specialist)', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '5', 
'specialityName' => 'Adult reconstructive orthopedic surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '5', 
'specialityName' => 'Orthopedic specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '5', 
'specialityName' => 'Orthopedic sports medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '5', 
'specialityName' => 'Orthopedic Surgery of the Spine', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '5', 
'specialityName' => 'Orthopedic Trauma specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '5', 
'specialityName' => 'Pediatric Orthopedic surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Anatomical Pathologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Blood Banking and Transfusion medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Chemical Pathologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Clinical Pathologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Cytopathologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Forensic Pathologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Genetic Pathologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Hematologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Immunopathologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Medical Microbiologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Molecular Pathologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Neuropathologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Pediatric Pathologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '15', 
'specialityName' => 'Histopathologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Adolescent Medicine Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Autism Consultant', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Child abuse pediatric specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Developmental-behavioral pediatrician', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Neonatologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Neonatal -perinatal medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Pediatrician', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Pediatric Cardiologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Pediatric Critical Care medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Pediatric Endocrinologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Pediatric Gastroenterologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Pediatric Hematologist and oncologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Pediatric infectious Diseases specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Pediatric Nephrologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Pediatric Pulmonologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Pediatric Rheumatology specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Pediatric Sports medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '3', 
'specialityName' => 'Pediatric Transplant Hepatology specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '16', 
'specialityName' => 'PHYSIOTHERAPIST', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '18', 
'specialityName' => 'Brain injury medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '18', 
'specialityName' => 'Drug Addict rehabilitation specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '18', 
'specialityName' => 'Pediatric rehabilitation medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '18', 
'specialityName' => 'Spinal Cord injury medicine specialist (CHIROPRACTOR)', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '18', 
'specialityName' => 'Speech and Language Pathologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '14', 
'specialityName' => 'Addiction Psychiatrist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '14', 
'specialityName' => 'Autism Consultant', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '14', 
'specialityName' => 'Child and adolescent Psychiatrist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '14', 
'specialityName' => 'Consultation/Liaison Psychiatrist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '14', 
'specialityName' => 'Counselor', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '14', 
'specialityName' => 'Emergency Psychologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '14', 
'specialityName' => 'Geriatric Psychologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '14', 
'specialityName' => 'Mental retardation psychiatrist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '14', 
'specialityName' => 'Psychiatrist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '14', 
'specialityName' => 'Psychologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '12', 
'specialityName' => 'Lung Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'Breast Imaging (Mammography / ultrasonography)', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'Chest radiology (X-ray)', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'CT Scan', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'Echocardiography', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'Emergency Radiology', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'Endoscopy', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'Genitourinary radiology (KUB)', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'Interventional and Vascular radiology', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'Laser Specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'MRI Scan', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'Neuroradiology', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'Nuclear medicine Radiology', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'Radiation oncology specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '20', 
'specialityName' => 'Ultrasonography (Sonologist)', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '6', 
'specialityName' => 'Kidney Transplant Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '6', 
'specialityName' => 'Male Infertility specialist ', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '6', 
'specialityName' => 'Neuro-urologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '6', 
'specialityName' => 'Pediatric Nephrologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '6', 
'specialityName' => 'Pediatric Urologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '6', 
'specialityName' => 'Urologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Pain Medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '10', 
'specialityName' => 'Hospice and Palliative Medicine specialist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '21', 
'specialityName' => 'Craniofacial Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '21', 
'specialityName' => 'Hair Transplant Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '21', 
'specialityName' => 'Hand Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '21', 
'specialityName' => 'Reconstructive Surgeon', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '22', 
'specialityName' => 'Gastroenterologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '23', 
'specialityName' => 'Endocrinologist', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],
[
'class_spa_id' => '24', 
'specialityName' => 'Diabetes', 
'farsi_name' => '', 
'pashto_name' => '', 
'photo' => '' 
 ],


            
        ]);
        }
         
    
}
