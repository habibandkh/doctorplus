<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
use Faker\factory as Faker;


class doctor_educationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
           $faker =faker::create();
        foreach(range(1,300)as $index){

            DB::table('doctor_education')->insert([
       		'D_FID'=>$faker->sentence(5),
       		'school_name'=>$faker->sentence(5),
       		'title_of_study'=>$faker->sentence(5),
       		'start_date'=>$faker->sentence(5),
       		'end_date'=>$faker->sentence(5),
       		
       		
       		
       		
        ]);
        }
    }
}
