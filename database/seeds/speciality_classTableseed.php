<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
//use Faker\factory as Faker;

class speciality_classTableseed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
      

            DB::table('speciality_class')->insert([

[
'name' => 'GYNECOLOGIST', 
'farsi_name' => 'متخصص نسائ ولادی', 
'pashto_name' => 'متخصص نسائ ولادی', 
'photo' => '1.png' 
],	
[
'name' => 'SKIN SPECIALIST', 
'farsi_name' => 'متخصص جلدی زهروی', 
'pashto_name' => 'متخصص جلدی زهروی
', 
'photo' => '2.png' 
],	
[
'name' => 'CHILD SPECIALIST', 
'farsi_name' => 'متخصص اطفال', 
'pashto_name' => 'متخصص اطفال
', 
'photo' => '3.png' 
],	
[
'name' => 'NEUROLOGIST', 
'farsi_name' => 'متخصص عقلی و عصبی', 
'pashto_name' => 'متخصص عقلی و عصبی
', 
'photo' => '4.png' 
],	
[
'name' => 'ORTHOPEDIC SURGEON', 
'farsi_name' => 'جراحی اورتوپیدی', 
'pashto_name' => 'جراحی اورتوپیدی', 
'photo' => '5.png' 
],	
[
'name' => 'UROLOGIST', 
'farsi_name' => 'متخصص بولی و تناسلی', 
'pashto_name' => 'متخصص بولی و تناسلی', 
'photo' => '6.png' 
],	
[
'name' => 'NUTRITIONIST', 
'farsi_name' => 'متخصص تغذیه', 
'pashto_name' => 'متخصص تغذیه', 
'photo' => '7.png' 
],	
[
'name' => 'ENT SPECIALIST', 
'farsi_name' => 'متخصص جراحی گوش و گلو', 
'pashto_name' => 'متخصص جراحی گوش و گلو', 
'photo' => '8.png' 
],	
[
'name' => 'GENERAL SURGEON', 
'farsi_name' => 'جراحی عمومی', 
'pashto_name' => 'جراحی عمومی', 
'photo' => '9.png' 
],	
[
'name' => 'GENERAL MEDICINE', 
'farsi_name' => 'داخله عمومی', 
'pashto_name' => 'داخله عمومی
', 
'photo' => '10.png' 
],	
[
'name' => 'HEART SPECIALIST', 
'farsi_name' => 'متخصص امراض قلب', 
'pashto_name' => 'متخصص امراض قلب', 
'photo' => '11.png' 
],	
[
'name' => 'CHEST SPECIALIST', 
'farsi_name' => 'متخصص امراض صدری', 
'pashto_name' => 'متخصص امراض صدری', 
'photo' => '12.png' 
],	
[
'name' => 'EYE SPECIALIST', 
'farsi_name' => 'متخصص چشم', 
'pashto_name' => 'متخصص چشم
', 
'photo' => '13.png' 
],	
[
'name' => 'PSYCHIATRIST', 
'farsi_name' => 'روانپزشک', 
'pashto_name' => 'روانپزشک', 
'photo' => '14.png' 
],	
[
'name' => 'PATHOLOGIST', 
'farsi_name' => 'آسیب شناس', 
'pashto_name' => 'آسیب شناس
', 
'photo' => '15.png' 
],	
[
'name' => 'PHYSIOTHERAPIST', 
'farsi_name' => 'فیزیوتراپیست', 
'pashto_name' => 'فیزیوتراپیست', 
'photo' => '16.png' 
],	
[
'name' => 'DENTIST', 
'farsi_name' => 'داکتر دندان', 
'pashto_name' => 'داکتر دندان
', 
'photo' => '17.png' 
],	
[
'name' => 'REHABILITATION', 
'farsi_name' => 'متخصص توانبخشی', 
'pashto_name' => 'متخصص توانبخشی', 
'photo' => '18.png' 
],	
[
'name' => 'ONCOLOGIST', 
'farsi_name' => 'انکولوژیست', 
'pashto_name' => 'انکولوژیست
', 
'photo' => '19.png' 
],	
[
'name' => 'RADIOLOGIST', 
'farsi_name' => 'متخصص معاینات تلویزویونی', 
'pashto_name' => 'متخصص معاینات تلویزویونی
', 
'photo' => '20.png' 
],	
[
'name' => 'Plastic Surgeon', 
'farsi_name' => 'جراح پلاستیک', 
'pashto_name' => 'جراح پلاستیک', 
'photo' => '21.png' 
],	
[
'name' => 'Gastroenterologist', 
'farsi_name' => 'متخصص گوارش', 
'pashto_name' => 'متخصص گوارش', 
'photo' => '22.png' 
],	
[
'name' => 'Endocrinologist ', 
'farsi_name' => 'متخصص غدد', 
'pashto_name' => 'متخصص غدد', 
'photo' => '23.png' 
],	
[
'name' => 'Diabetes ', 
'farsi_name' => 'دیابت', 
'pashto_name' => 'دیابت', 
'photo' => '24.png' 
],	
          
        ]);
      
    }
}
