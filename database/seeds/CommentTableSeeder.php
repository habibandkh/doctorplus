<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
use Faker\factory as Faker;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
           $faker =faker::create();
        foreach(range(1,300)as $index){

            DB::table('comment')->insert([
       		'com_name'=>$faker->sentence(5),
       		'com_email'=>$faker->sentence(5),
       		'comment'=>$faker->sentence(5),
       		'postid'=>$faker->sentence(5),
       		
        ]);
        }
    }
}
