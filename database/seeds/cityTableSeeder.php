<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
//use Faker\factory as Faker;

class cityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
DB::table('city')->insert([
['f_name' => 'بدخشان', 'p_name' => 'بدخشان', 'e_name' => 'badakhshan' ],		
['f_name' => 'بادغیس', 'p_name' => 'بادغیس', 'e_name' => 'badghiz' ],		
['f_name' => 'بغلان', 'p_name' => 'بغلان', 'e_name' => 'baghlan' ],		
['f_name' => 'بلخ', 'p_name' => 'بلخ', 'e_name' => 'balkh' ],		
['f_name' => 'بامیان', 'p_name' => 'بامیان', 'e_name' => 'bamiyan' ],		
['f_name' => 'دایکندی', 'p_name' => 'دایکندي', 'e_name' => 'daikundi' ],		
['f_name' => 'فاریاب', 'p_name' => 'فاریاب', 'e_name' => 'faryab' ],		
['f_name' => 'غزنی', 'p_name' => 'غزني', 'e_name' => 'ghazni' ],		
['f_name' => 'غور', 'p_name' => 'غور', 'e_name' => 'ghor' ],		
['f_name' => 'هیلمند', 'p_name' => 'هیلمند', 'e_name' => 'helmand' ],		
['f_name' => 'هرات', 'p_name' => 'هرات', 'e_name' => 'herat' ],		
['f_name' => 'جوزجان', 'p_name' => 'جوزجان', 'e_name' => 'jowzjan' ],		
['f_name' => 'کابل', 'p_name' => 'کابل', 'e_name' => 'kabul' ],		
['f_name' => 'کندهار', 'p_name' => 'کندهار', 'e_name' => 'kandahar' ],		
['f_name' => 'کاپیسا', 'p_name' => 'کاپیسا', 'e_name' => 'kapisa' ],		
['f_name' => 'خوست', 'p_name' => 'خوست', 'e_name' => 'khost' ],		
['f_name' => 'کونر', 'p_name' => 'کونر', 'e_name' => 'kunar' ],		
['f_name' => 'کندوز', 'p_name' => 'کندوز', 'e_name' => 'kunduz' ],		
['f_name' => 'لغمان', 'p_name' => 'لغمان', 'e_name' => 'laghman' ],		
['f_name' => 'لوگر', 'p_name' => 'لوګر', 'e_name' => 'logar' ],		
['f_name' => ' میدان وردک', 'p_name' => 'میډن ورډک', 'e_name' => 'maidan wardak' ],		
['f_name' => 'ننگرهار', 'p_name' => 'ننګرهار', 'e_name' => 'nangarhar' ],		
['f_name' => 'پکتیا', 'p_name' => 'پکټیا', 'e_name' => 'paktia' ],		
['f_name' => 'پکتیکا', 'p_name' => 'پکتيکا', 'e_name' => 'paktika' ],		
['f_name' => 'پنجشیر', 'p_name' => 'پنجشیر', 'e_name' => 'panjshir' ],		
['f_name' => 'پروان', 'p_name' => 'پروان', 'e_name' => 'parwan' ],		
['f_name' => 'سمنگان', 'p_name' => 'سمنګان', 'e_name' => 'samangaan' ],		
['f_name' => 'سرپل', 'p_name' => 'سرپل', 'e_name' => 'sar-e-pul' ],		
['f_name' => 'تخار', 'p_name' => 'تخار', 'e_name' => 'takhar' ],		
['f_name' => 'زابل', 'p_name' => 'زابل', 'e_name' => 'zabul' ],		
['f_name' => 'نیمروز', 'p_name' => 'نیمروز', 'e_name' => 'nimroz' ],		
['f_name' => 'فراه', 'p_name' => 'فراه', 'e_name' => 'farah' ],		
['f_name' => 'نورستان', 'p_name' => 'نورستان', 'e_name' => 'nuristan' ],		
['f_name' => 'اروزگان', 'p_name' => 'اروزگان', 'e_name' => 'uruzgan' ],
        ]);
        }
    
}
