<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
use Faker\factory as Faker;

class doctorspecialityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
           $faker =faker::create();
        foreach(range(1,300)as $index){

            DB::table('doctorspeciality')->insert([
       		'F_specialityID'=>$faker->sentence(5),
       		'F_doctorID'=>$faker->sentence(5),
       		'type'=>$faker->sentence(5),
       		
       		
        ]);
        }
    }
}
