<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
use Faker\factory as Faker;


class messageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
          $faker =faker::create();
        foreach(range(1,300)as $index){

            DB::table('message')->insert([
       		'name'=>$faker->sentence(5),
       		'phone'=>$faker->sentence(5),
       		'message'=>$faker->sentence(5),
       		'status'=>$faker->sentence(5),
       		
       		
       		
 		
        ]);
        }
    }
}
