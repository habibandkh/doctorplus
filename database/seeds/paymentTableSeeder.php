<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
use Faker\factory as Faker;


class paymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
          $faker =faker::create();
        foreach(range(1,300)as $index){

            DB::table('payment')->insert([
       		'payDate'=>$faker->sentence(5),
       		'receivedPay'=>$faker->sentence(5),
       		'amount'=>$faker->sentence(5),
       		'balance'=>$faker->sentence(5),
       		'doctorID'=>$faker->sentence(5),
       		'adminID'=>$faker->sentence(5),
       		
       		
       		
 		
        ]);
        }
    }
}
