<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;
//use Faker\factory as Faker;


class doctor_servicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
          

            DB::table('doctor_services')->insert([
[
'specialty_id' => '61', 
'service_name' => 'Antenatal Care' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Antenatal Checkup' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Caesarean (C-Section)' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Clinical breast Examination' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Contraceptive Advice' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Cosmetic And Reconstruction Surgery' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Diagnostic Laparascopy' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Endoscopic Surgery' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Genetic Testing' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Gynaecological Surgeries' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Gynaecological Tumors' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Hysterectomy' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Hysterosalpingogram' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Hysteroscopic Procedures' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Hysteroscopy' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Infertility Treatment' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'IUI' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'IVF' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Laparoscopic Surgery' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Laparotomy' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Maternal Care' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Myomectomy' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Normal Delivery' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Obesity Management' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Obstetrical Ultrasound' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Caesarean (C-Section)' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Clinical breast Examination' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Contraceptive Advice' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Cosmetic And Reconstruction Surgery' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Diagnostic Laparascopy' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Endoscopic Surgery' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Genetic Testing' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Spontaneous Vaginal Delivery (SVD)' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Test Tube Baby' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Triplet Deliveries' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Tubal Ligation' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Tubectomy' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'UV Prolapse' 
 ],
[
'specialty_id' => '61', 
'service_name' => 'Spontaneous Vaginal Delivery (SVD)' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Anger Management' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Autism Treatment' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Career Counselling' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Cognitive Behaviour Therapy (CBT)' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Mental Health Treatment' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Neurospshychological and Psychological Assessment' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Social Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Stress Management' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Anger Management' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Autism Treatment' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Career Counselling' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Cognitive Behaviour Therapy (CBT)' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Mental Health Treatment' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Neurospshychological and Psychological Assessment' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Social Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Stress Management' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'Artificial Teeth' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'Cosmetic Dentistry' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'Dental Crown' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'Dental Hygiene' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'Dental Implants' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'Fillings' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'General Dentistry' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'Gums Treatment' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'Root Canal' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'Routine Tooth Extractions' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'Scaling and Polishing' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'Teeth Whitening ' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'Tooth Extraction' 
 ],
[
'specialty_id' => '7', 
'service_name' => 'Wisdom Teeth Removal' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Acne Treatment' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Alopecia' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Antihistamine Treatment' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Boil' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Chemical Peel' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Cosmetology' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Cryotherapy & Electrocautery for Removal or warts' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Dermal Fillers' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Dermatology' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Head Lice' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Intense Pulsed Light (IPL)' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Intrauterine insemination (IUI)' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Iintophoresis ' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Laser for Skin' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Laser Hair Removal' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Melasma' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Meso Therapy' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Microdermabrasion' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Molluscum Contagoisum Treatment' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Phototherapy' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Pimples' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Procedural Dermatology' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'PRP' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Skin Cancer Surgery' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Skin Care Consultation' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Skin Peeling ' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Skin Tightening' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Skin Toning' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'UVB Light Therapy For Vitiligo' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Venereology' 
 ],
[
'specialty_id' => '42', 
'service_name' => 'Xanthelasmas' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Aesthetic Crown And Bridges' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Artificial Teeth' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Braces' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Cancer Surgeries' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Ceramic Braces' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Ceramic Crowns ' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Cosmetic Dentistry' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Dental Crown' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Dental Curettage' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Dental Hygiene' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Dental Implants' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Dental Treatments Including Borees' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Dentoalveolar Surgery' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Disimpactions' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Facial Deformities' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Facial Jaw Bone Fracture Treatment' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Fillings' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Fluoride Application Treatment' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'General Dentistry' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Gums Treatment' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Implantology' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Implants' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Jewel Teeth' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Maxilla Surgery' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Monitoring of The Dental Eruption' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Oral And Maxillofacial Surgery' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Orthodontics' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Preventive Dental Care' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Prosthodontics' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Root Canal' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Routine Tooth Extractions' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Scaling and Polishing' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Teeth Straightening' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Teeth Whitening' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Tooth Extraction' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Trauma' 
 ],
[
'specialty_id' => '13', 
'service_name' => 'Wisdom Teeth Removal' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Aesthetic Crown and Bridge' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Artificial Teeth' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Braces' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Cancer Surgeries' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Ceramic Braces' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Ceramic Crowns' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Cosmetic Dentistry' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Dental Crown' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Dental Curettage' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Dental Hygiene' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Dental Implants' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Dental Treatments Including Bores' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Dentoalveolar Surgery' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Disimpactions' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Facial Deformities' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Facial Jaw Bone Fracture Treatment' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Fillings' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Fluoride Application Treatment' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'General Dentistry' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Gums Treatment' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Implantology' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Implants' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Jewel Teeth' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Maxilla Surgery' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Monitoring of the Dental Eruption' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Oral And Maxillofacial Surgery' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Orthodontics' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Preventive Dental Care' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Prosthodontics' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Root Canal' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Routine Tooth Extractions' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Scaling and Polishing' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Teeth Cleaning' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Teeth Straightening' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Teeth Whitening' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Tooth Extraction' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Trauma' 
 ],
[
'specialty_id' => '14', 
'service_name' => 'Wisdom Teeth Removal' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Acne Treatment' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Alopecia' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Antihistamine Treatment' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Boil' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Chemical Peel' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Cosmetology' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Cryotherapy and Electrocautery For Removal of Warts' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Dermal Fillers' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Dermatology' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Head lice' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Intense Pulsed Light (IPL)' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Intrauterine Insemination (IUI)' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Iiontiphoresis' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Laser for Skin' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Laser Hair Removal' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Melasma' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Meso Therapy' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Microdermabrasion' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Microdermabrasion' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Molluscum contagoisum Treatment' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Phototherapy' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Pimples' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Procedural Dermatology' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'PRP' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Skin Cancer Consulation' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Skin Care Consulation' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Skin Peeling' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Skin Tightening' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Skin Toning' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'UVB Light Therapy For Vitiligo' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Venereology' 
 ],
[
'specialty_id' => '15', 
'service_name' => 'Xanthelasmas' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Acne Treatment' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Alopecia' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Antihistamine Treatment' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Boil' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Chemical Peel' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Cosmetology' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Cryotherapy and Electrocautery For Removal of Warts' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Dermal Fillers' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Dermatology' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Head lice' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Intense Pulsed Light (IPL)' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Intrauterine Insemination (IUI)' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Iiontiphoresis' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Laser for Skin' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Laser Hair Removal' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Melasma' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Meso Therapy' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Microdermabrasion' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Microdermabrasion' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Molluscum contagoisum Treatment' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Phototherapy' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Pimples' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Procedural Dermatology' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'PRP' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Skin Cancer Consulation' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Skin Care Consulation' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Skin Peeling' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Skin Tightening' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Skin Toning' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'UVB Light Therapy For Vitiligo' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Venereology' 
 ],
[
'specialty_id' => '17', 
'service_name' => 'Xanthelasmas' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Acne Treatment' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Alopecia' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Antihistamine Treatment' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Boil' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Chemical Peel' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Cosmetology' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Cryotherapy and Electrocautery For Removal of Warts' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Dermal Fillers' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Dermatology' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Head lice' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Intense Pulsed Light (IPL)' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Intrauterine Insemination (IUI)' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Iiontiphoresis' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Laser for Skin' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Laser Hair Removal' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Melasma' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Meso Therapy' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Microdermabrasion' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Microdermabrasion' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Molluscum contagoisum Treatment' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Phototherapy' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Pimples' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Procedural Dermatology' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'PRP' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Skin Cancer Consulation' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Skin Care Consulation' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Skin Peeling' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Skin Tightening' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Skin Toning' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'UVB Light Therapy For Vitiligo' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Venereology' 
 ],
[
'specialty_id' => '18', 
'service_name' => 'Xanthelasmas' 
 ],
[
'specialty_id' => '71', 
'service_name' => 'Cardiology Medicine (General)' 
 ],
[
'specialty_id' => '71', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '71', 
'service_name' => 'Echocardiogram' 
 ],
[
'specialty_id' => '71', 
'service_name' => 'Echocardiography (ECHO)' 
 ],
[
'specialty_id' => '71', 
'service_name' => 'Electrocardiography (ECG)' 
 ],
[
'specialty_id' => '71', 
'service_name' => 'Exercise Tolerance Test (ETT)' 
 ],
[
'specialty_id' => '71', 
'service_name' => 'Infertility Treatment' 
 ],
[
'specialty_id' => '71', 
'service_name' => 'Metabolic Disorders Treatment' 
 ],
[
'specialty_id' => '71', 
'service_name' => 'Osteoporosis Management' 
 ],
[
'specialty_id' => '71', 
'service_name' => 'Stress Echocardiography' 
 ],
[
'specialty_id' => '71', 
'service_name' => 'Thyroid Treatment' 
 ],
[
'specialty_id' => '183', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '183', 
'service_name' => 'Metabolic Disorders Treatment' 
 ],
[
'specialty_id' => '183', 
'service_name' => 'Thyroid Treatment' 
 ],
[
'specialty_id' => '64', 
'service_name' => 'Acute Medicine' 
 ],
[
'specialty_id' => '64', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '64', 
'service_name' => 'Family Medicine' 
 ],
[
'specialty_id' => '64', 
'service_name' => 'Infertility Treatment' 
 ],
[
'specialty_id' => '64', 
'service_name' => 'Metabolic Disorders Treatment' 
 ],
[
'specialty_id' => '64', 
'service_name' => 'Osteoporosis Management' 
 ],
[
'specialty_id' => '64', 
'service_name' => 'Pain Management Services' 
 ],
[
'specialty_id' => '64', 
'service_name' => 'Thyroid Treatment' 
 ],
[
'specialty_id' => '72', 
'service_name' => 'Acute Medicine' 
 ],
[
'specialty_id' => '72', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '72', 
'service_name' => 'Family Medicine' 
 ],
[
'specialty_id' => '72', 
'service_name' => 'Infertility Treatment' 
 ],
[
'specialty_id' => '72', 
'service_name' => 'Metabolic Disorders Treatment' 
 ],
[
'specialty_id' => '72', 
'service_name' => 'Osteoporosis Management' 
 ],
[
'specialty_id' => '72', 
'service_name' => 'Pain Management Services' 
 ],
[
'specialty_id' => '72', 
'service_name' => 'Thyroid Treatment' 
 ],
[
'specialty_id' => '125', 
'service_name' => 'Acute Medicine' 
 ],
[
'specialty_id' => '125', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '125', 
'service_name' => 'Family Medicine' 
 ],
[
'specialty_id' => '125', 
'service_name' => 'Infertility Treatment' 
 ],
[
'specialty_id' => '125', 
'service_name' => 'Metabolic Disorders Treatment' 
 ],
[
'specialty_id' => '125', 
'service_name' => 'Osteoporosis Management' 
 ],
[
'specialty_id' => '125', 
'service_name' => 'Pain Management Services' 
 ],
[
'specialty_id' => '125', 
'service_name' => 'Thyroid Treatment' 
 ],
[
'specialty_id' => '182', 
'service_name' => 'Acute Medicine' 
 ],
[
'specialty_id' => '182', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '182', 
'service_name' => 'Family Medicine' 
 ],
[
'specialty_id' => '182', 
'service_name' => 'Infertility Treatment' 
 ],
[
'specialty_id' => '182', 
'service_name' => 'Metabolic Disorders Treatment' 
 ],
[
'specialty_id' => '182', 
'service_name' => 'Osteoporosis Management' 
 ],
[
'specialty_id' => '182', 
'service_name' => 'Pain Management Services' 
 ],
[
'specialty_id' => '182', 
'service_name' => 'Thyroid Treatment' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Allergy Treatment' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Coblation' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Diagnostic Endoscopy of Nose and Throat' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Ear Cleaning' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Ear Lobe Repairs' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Ear Wax Removal' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Endoscopic Surgery' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'ENT Surgery' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Foreign Body Impaction' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Hearing Evaluation And Management' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Laryngoscopy' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Laser Surgery' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Oncological Surgery' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Rhinoplasty (Cosmetic Nose Surgery)' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Septoplasty' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Septoplasty (Septal Surgery)' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Tinnitus Treatment' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Tonsillectomy' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Tonsillitis Treatment' 
 ],
[
'specialty_id' => '20', 
'service_name' => 'Tracheostomy' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Allergy Treatment' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Coblation' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Diagnostic Endoscopy of Nose and Throat' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Ear Cleaning' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Ear Lobe Repairs' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Ear Wax Removal' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Endoscopic Surgery' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'ENT Surgery' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Foreign Body Impaction' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Hearing Evaluation And Management' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Laryngoscopy' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Laser Surgery' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Oncological Surgery' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Rhinoplasty (Cosmetic Nose Surgery)' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Septoplasty' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Septoplasty (Septal Surgery)' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Tinnitus Treatment' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Tonsillectomy' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Tonsillitis Treatment' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Myringotomy ' 
 ],
[
'specialty_id' => '21', 
'service_name' => 'Tracheostomy' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Cataract Eye Surgery' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Color Blind Test' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Comprehensive Eye Care' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Corneal Surgery' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Corneal Transplant' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Correction or Eye Bags' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Diabetic Eye Care' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Diagnostic Ophthalmology' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Excimer Laser' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Eye Removal ' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Eye Surgery' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Eyelid Lift' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Glasses Removal Surgery' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Glaucoma Surgery' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Keratoplasty' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Laser Cataract Surgery' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Laser Eye Surgery' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'LASIK Eye Surgery' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Lens Implant' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Management of Complicated Traumatic Aphakia' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Orbital Surgery' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Phaco Surgery' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Phacoemulsification (Phaco Cataract Surgery)' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Photo Refractive Keratectomy (PRK)' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Photo Therapeutic Keratectomy (PTK)' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Refractive Surgery' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Retina Surgery' 
 ],
[
'specialty_id' => '22', 
'service_name' => 'Vitreoretinal Surgery' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Cataract Eye Surgery' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Color Blind Test' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Comprehensive Eye Care' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Corneal Surgery' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Corneal Transplant' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Correction or Eye Bags' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Diabetic Eye Care' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Diagnostic Ophthalmology' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Excimer Laser' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Eye Removal ' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Eye Surgery' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Eyelid Lift' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Glasses Removal Surgery' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Glaucoma Surgery' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Keratoplasty' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Laser Cataract Surgery' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Laser Eye Surgery' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'LASIK Eye Surgery' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Lens Implant' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Management of Complicated Traumatic Aphakia' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Orbital Surgery' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Phaco Surgery' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Phacoemulsification (Phaco Cataract Surgery)' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Photo Refractive Keratectomy (PRK)' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Photo Therapeutic Keratectomy (PTK)' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Refractive Surgery' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Retina Surgery' 
 ],
[
'specialty_id' => '23', 
'service_name' => 'Vitreoretinal Surgery' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Cataract Eye Surgery' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Color Blind Test' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Comprehensive Eye Care' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Corneal Surgery' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Corneal Transplant' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Correction or Eye Bags' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Diabetic Eye Care' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Diagnostic Ophthalmology' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Excimer Laser' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Eye Removal ' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Eye Surgery' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Eyelid Lift' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Glasses Removal Surgery' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Glaucoma Surgery' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Keratoplasty' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Laser Cataract Surgery' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Laser Eye Surgery' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'LASIK Eye Surgery' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Lens Implant' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Management of Complicated Traumatic Aphakia' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Orbital Surgery' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Phaco Surgery' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Phacoemulsification (Phaco Cataract Surgery)' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Photo Refractive Keratectomy (PRK)' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Photo Therapeutic Keratectomy (PTK)' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Refractive Surgery' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Retina Surgery' 
 ],
[
'specialty_id' => '24', 
'service_name' => 'Vitreoretinal Surgery' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Cataract Eye Surgery' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Color Blind Test' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Comprehensive Eye Care' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Corneal Surgery' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Corneal Transplant' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Correction or Eye Bags' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Diabetic Eye Care' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Diagnostic Ophthalmology' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Excimer Laser' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Eye Removal ' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Eye Surgery' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Eyelid Lift' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Glasses Removal Surgery' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Glaucoma Surgery' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Keratoplasty' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Laser Cataract Surgery' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Laser Eye Surgery' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'LASIK Eye Surgery' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Lens Implant' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Management of Complicated Traumatic Aphakia' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Orbital Surgery' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Phaco Surgery' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Phacoemulsification (Phaco Cataract Surgery)' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Photo Refractive Keratectomy (PRK)' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Photo Therapeutic Keratectomy (PTK)' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Refractive Surgery' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Retina Surgery' 
 ],
[
'specialty_id' => '25', 
'service_name' => 'Vitreoretinal Surgery' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Cataract Eye Surgery' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Color Blind Test' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Comprehensive Eye Care' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Corneal Surgery' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Corneal Transplant' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Correction or Eye Bags' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Diabetic Eye Care' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Diagnostic Ophthalmology' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Excimer Laser' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Eye Removal ' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Eye Surgery' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Eyelid Lift' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Glasses Removal Surgery' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Glaucoma Surgery' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Keratoplasty' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Laser Cataract Surgery' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Laser Eye Surgery' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'LASIK Eye Surgery' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Lens Implant' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Management of Complicated Traumatic Aphakia' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Orbital Surgery' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Phaco Surgery' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Phacoemulsification (Phaco Cataract Surgery)' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Photo Refractive Keratectomy (PRK)' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Photo Therapeutic Keratectomy (PTK)' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Refractive Surgery' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Retina Surgery' 
 ],
[
'specialty_id' => '26', 
'service_name' => 'Vitreoretinal Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Cataract Eye Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Color Blind Test' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Comprehensive Eye Care' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Corneal Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Corneal Transplant' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Correction or Eye Bags' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Diabetic Eye Care' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Diagnostic Ophthalmology' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Excimer Laser' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Eye Removal ' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Eye Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Eyelid Lift' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Glasses Removal Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Glaucoma Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Keratoplasty' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Laser Cataract Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Laser Eye Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'LASIK Eye Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Lens Implant' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Management of Complicated Traumatic Aphakia' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Orbital Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Phaco Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Phacoemulsification (Phaco Cataract Surgery)' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Photo Refractive Keratectomy (PRK)' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Photo Therapeutic Keratectomy (PTK)' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Refractive Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Retina Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Vitreoretinal Surgery' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Cataract Eye Surgery' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Color Blind Test' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Comprehensive Eye Care' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Corneal Surgery' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Corneal Transplant' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Correction or Eye Bags' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Diabetic Eye Care' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Diagnostic Ophthalmology' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Excimer Laser' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Eye Removal ' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Eye Surgery' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Eyelid Lift' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Glasses Removal Surgery' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Glaucoma Surgery' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Keratoplasty' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Laser Cataract Surgery' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Laser Eye Surgery' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'LASIK Eye Surgery' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Lens Implant' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Management of Complicated Traumatic Aphakia' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Orbital Surgery' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Phaco Surgery' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Phacoemulsification (Phaco Cataract Surgery)' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Photo Refractive Keratectomy (PRK)' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Photo Therapeutic Keratectomy (PTK)' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Refractive Surgery' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Retina Surgery' 
 ],
[
'specialty_id' => '28', 
'service_name' => 'Vitreoretinal Surgery' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Cataract Eye Surgery' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Color Blind Test' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Comprehensive Eye Care' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Corneal Surgery' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Corneal Transplant' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Correction or Eye Bags' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Diabetic Eye Care' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Diagnostic Ophthalmology' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Excimer Laser' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Eye Removal ' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Eye Surgery' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Eyelid Lift' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Glasses Removal Surgery' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Glaucoma Surgery' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Keratoplasty' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Laser Cataract Surgery' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Laser Eye Surgery' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'LASIK Eye Surgery' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Lens Implant' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Management of Complicated Traumatic Aphakia' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Orbital Surgery' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Phaco Surgery' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Phacoemulsification (Phaco Cataract Surgery)' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Photo Refractive Keratectomy (PRK)' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Photo Therapeutic Keratectomy (PTK)' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Refractive Surgery' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Retina Surgery' 
 ],
[
'specialty_id' => '29', 
'service_name' => 'Vitreoretinal Surgery' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Cataract Eye Surgery' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Color Blind Test' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Comprehensive Eye Care' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Corneal Surgery' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Corneal Transplant' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Correction or Eye Bags' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Diabetic Eye Care' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Diagnostic Ophthalmology' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Excimer Laser' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Eye Removal ' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Eye Surgery' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Eyelid Lift' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Glasses Removal Surgery' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Glaucoma Surgery' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Keratoplasty' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Laser Cataract Surgery' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Laser Eye Surgery' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'LASIK Eye Surgery' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Lens Implant' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Management of Complicated Traumatic Aphakia' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Orbital Surgery' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Phaco Surgery' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Phacoemulsification (Phaco Cataract Surgery)' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Photo Refractive Keratectomy (PRK)' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Photo Therapeutic Keratectomy (PTK)' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Refractive Surgery' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Retina Surgery' 
 ],
[
'specialty_id' => '30', 
'service_name' => 'Vitreoretinal Surgery' 
 ],
[
'specialty_id' => '58', 
'service_name' => 'Infertility Treatment' 
 ],
[
'specialty_id' => '58', 
'service_name' => 'IUI' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Biliary Stone Treatment' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Biopsy' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Cardiac CT Angiography' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Cardiac Patient Management' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Cardiology Medicine (General )' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Colonoscopy ' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Constipation Treatment' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Diarrhea Treatment' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Digital Rectal Examination' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Echocardiography (ECHO)' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Electrocardiography (ECG)' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Endoscopic Guided Ultrasound' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Endoscopic Surgeries' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Endoscopy' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'ERCP (Endoscopic Retrograde Cholangiopancreatography)' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Family Medicine' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Gastric Ballon' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Gastric Band Ligation' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Gastroscopy' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Heart Attack Management' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Hepatitis A Treatment' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Hepatitis B Treatment' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Hepatitis C Treatment' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Manometry' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'MRCP (Magnetic Resonance Cholangiopancreatography)' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Myotomy' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Nutritional Advice' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Obesity Management' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Oesophagoscopy' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Peg Tube Placement' 
 ],
[
'specialty_id' => '73', 
'service_name' => 'Treatment For Heart Attack' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Biliary Stone Treatment' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Biopsy' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Cardiac CT Angiography' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Cardiac Patient Management' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Cardiology Medicine (General )' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Colonoscopy ' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Constipation Treatment' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Diarrhea Treatment' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Digital Rectal Examination' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Echocardiography (ECHO)' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Electrocardiography (ECG)' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Endoscopic Guided Ultrasound' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Endoscopic Surgeries' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Endoscopy' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'ERCP (Endoscopic Retrograde Cholangiopancreatography)' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Family Medicine' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Gastric Ballon' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Gastric Band Ligation' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Gastroscopy' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Heart Attack Management' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Hepatitis A Treatment' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Hepatitis B Treatment' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Hepatitis C Treatment' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Manometry' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'MRCP (Magnetic Resonance Cholangiopancreatography)' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Myotomy' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Nutritional Advice' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Obesity Management' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Oesophagoscopy' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Peg Tube Placement' 
 ],
[
'specialty_id' => '126', 
'service_name' => 'Treatment For Heart Attack' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Biliary Stone Treatment' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Biopsy' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Cardiac CT Angiography' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Cardiac Patient Management' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Cardiology Medicine (General )' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Colonoscopy ' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Constipation Treatment' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Diarrhea Treatment' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Digital Rectal Examination' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Echocardiography (ECHO)' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Electrocardiography (ECG)' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Endoscopic Guided Ultrasound' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Endoscopic Surgeries' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Endoscopy' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'ERCP (Endoscopic Retrograde Cholangiopancreatography)' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Family Medicine' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Gastric Ballon' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Gastric Band Ligation' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Gastroscopy' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Heart Attack Management' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Hepatitis A Treatment' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Hepatitis B Treatment' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Hepatitis C Treatment' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Manometry' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'MRCP (Magnetic Resonance Cholangiopancreatography)' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Myotomy' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Nutritional Advice' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Obesity Management' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Oesophagoscopy' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Peg Tube Placement' 
 ],
[
'specialty_id' => '181', 
'service_name' => 'Treatment For Heart Attack' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Abdomen Colorectal Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Abdominal Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Appendectomy' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Biopsy' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Breast Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Cardiothoracic Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Cerebral Herniation' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Cholecystectomy' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Circumcision' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Colostomy' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Colposcopy' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Cosmetic Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Emergency Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Fistula' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'General Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Head And Neck Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Hepatobiliary Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Hernia And Perianal Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Hernia Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Intussusception' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Laparoscopic Appendectomy' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Laparoscopic Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Laparoscopic Chole' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Mastectomy' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Oncological Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Operation of Gall Bladder' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Stitches' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Thyroid Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Trauma Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Varicose Vein Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Vascular Surgery' 
 ],
[
'specialty_id' => '45', 
'service_name' => 'Vasectomy' 
 ],
[
'specialty_id' => '109', 
'service_name' => 'Hemophilias' 
 ],
[
'specialty_id' => '109', 
'service_name' => 'Hodgkin’s Lymphoma' 
 ],
[
'specialty_id' => '109', 
'service_name' => 'Indiopathic Thrombocytopenic Purpura (ITP)' 
 ],
[
'specialty_id' => '109', 
'service_name' => 'Leukemia ' 
 ],
[
'specialty_id' => '109', 
'service_name' => 'Lymphoma' 
 ],
[
'specialty_id' => '109', 
'service_name' => 'Platelets disorder' 
 ],
[
'specialty_id' => '109', 
'service_name' => 'Thalassemia' 
 ],
[
'specialty_id' => '109', 
'service_name' => 'Toxopasmosis' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Acute Hives' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Allergic Conjunctivitis' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Allergy Middle Ear Dysfunction' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Allergic Nasal Polyposis' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Allergy Rhinitis (Hay Fever)' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Allergic Rhinosinusitis' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Allergy To Medication' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Anaphylaxis' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Angioedema' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Asthma' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Atopic Dermatitis (Eczema)' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Chronic Cough' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Chug Strauss Syndrome' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Contact Dermtitis' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'DRESS- Drug Rash Eosinophilia Systemic Syndrome' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Drug Allergy' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Eosinophilic Esophagitis' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Eosinophilic Gastroenteritis' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Exercise-induced Bronchospasm' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Food Allergy' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Food Protein Enterocolitis (EPIES)' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Food Protein Proctocolitis' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Hiener’s Syndrome' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Histamine Toxicity' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Hives' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Hypersensitive Pneumonitis (HP)' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Immunodeficiency Disorders ' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Insect Venom Allergy' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Itching' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Lactose Intolerance' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Latex Allergy' 
 ],
[
'specialty_id' => '67', 
'service_name' => ' Nose / Sinus Allergy' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Penicillin Allergy' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Pollen Allergy' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Reactive Airway Disease' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Respiratory Allergy' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Skeeter Syndrome' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Skin Allergy ' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Skin Rashes' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Steven Johnson Syndrome' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Systemic Mastocytosis' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Toxic Epidermal Necrolysis' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Vaccine Allergy' 
 ],
[
'specialty_id' => '67', 
'service_name' => 'Vocal Cord Dysfunction' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Acute Hives' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Allergic Conjunctivitis' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Allergy Middle Ear Dysfunction' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Allergic Nasal Polyposis' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Allergy Rhinitis (Hay Fever)' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Allergic Rhinosinusitis' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Allergy To Medication' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Anaphylaxis' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Angioedema' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Asthma' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Atopic Dermatitis (Eczema)' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Chronic Cough' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Chug Strauss Syndrome' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Contact Dermtitis' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'DRESS- Drug Rash Eosinophilia Systemic Syndrome' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Drug Allergy' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Eosinophilic Esophagitis' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Eosinophilic Gastroenteritis' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Exercise-induced Bronchospasm' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Food Allergy' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Food Protein Enterocolitis (EPIES)' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Food Protein Proctocolitis' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Hiener’s Syndrome' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Histamine Toxicity' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Hives' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Hypersensitive Pneumonitis (HP)' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Immunodeficiency Disorders ' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Insect Venom Allergy' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Itching' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Lactose Intolerance' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Latex Allergy' 
 ],
[
'specialty_id' => '110', 
'service_name' => ' Nose / Sinus Allergy' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Penicillin Allergy' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Pollen Allergy' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Reactive Airway Disease' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Respiratory Allergy' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Skeeter Syndrome' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Skin Allergy ' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Skin Rashes' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Steven Johnson Syndrome' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Systemic Mastocytosis' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Toxic Epidermal Necrolysis' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Vaccine Allergy' 
 ],
[
'specialty_id' => '110', 
'service_name' => 'Vocal Cord Dysfunction' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Biliary Stone Treatment' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Biopsy' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Cardiac CT Angiography' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Cardiac Patient Management' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Cardiology Medicine (General)' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Colonoscopy ' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Constipation Treatment' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Corona-virus Treatment' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Diarrhea Treatment' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Digital Rectal Examination' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Echocardiogram' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Echocardiography (ECHO)' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Electrocardiography (ECG)' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Endoscopic Guided Ultrasound' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Endoscopic Surgeries' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Endoscopy' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'ERCP (Endoscopic Retrograde Cholangiopancreatography)' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Exercise Tolerance Test (ETT)' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Family Medicine' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Gastric Ballon' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Gastric Band Ligation' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Gastroscopy' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Heart Attack Management' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Hepatitis A Treatment' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Hepatitis B Treatment' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Hepatitis C Treatment' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Holter Monitoring' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Infertility Treatment' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Manometry' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Metabolic Disorders Treatment' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'MRCP (Magnetic Resonance Cholangiopancreatography)' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Myotomy' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Nutritional Advice' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Obesity Management' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Oesophagoscopy' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Osteoporosis Management' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Pain Management Services' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Peg Tube Placement' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Stenosis And Valvular Regurgitation' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Stress Echocardiography' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Thyroid Treatment' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Trans-thoracic Echocardiography' 
 ],
[
'specialty_id' => '76', 
'service_name' => 'Treatment for Heart Attack' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Angiography' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Angioplasty' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Balloon Valvotomy' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Cardiac Catheterization' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Cardiac CT Angiography' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Cardiac Patient Management' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Cardiology Medicine (General)' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Echocardiography (ECHO)' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Electrocardiography (ECG)' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Electrophysiology' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Exercise Tolerance Test (ETT)' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Heart Attack Management' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Holter Monitoring' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Interventional Cardiolog' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Radiofrequency Ablation' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Stenting' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Stress Echocardiography' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Trans-thoracic Echocardiography' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Treatment for Heart Attack' 
 ],
[
'specialty_id' => '6', 
'service_name' => 'Ventriculo Perotoneal (VP) Shunt' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Abdomen Colorectal Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Abdominal Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Appendectomy' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Biopsy' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Blood Tests' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Breast Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Cardiothoracic Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Cerebral Herniation' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Cholecystectomy' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Circumcision' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Colostomy' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Colposcopy' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Cosmetic Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'ECG' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Emergency Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Family Medicine' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Fistula' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'General Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Head And Neck Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Hepatobiliary Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Hernia And Perianal Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Hernia Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Intussusception' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Laparoscopic Appendectomy' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Laparoscopic Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Laparoscopic chole' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Mastectomy' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Nutritional Advice' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Obesity management' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Oncological Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Operation of Gall Bladder' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Sports Medicine' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Stitches' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Thyroid Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Varicose Vein Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Vascular Surgery' 
 ],
[
'specialty_id' => '49', 
'service_name' => 'Vasectomy' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Acne Treatment' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Aesthetic Crown And Bridges' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Alopecia' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Antihistamine Treatment' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Artificial Teeth' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Boil' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Braces' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Cancer Surgeries' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Ceramic Braces' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Ceramic Crowns' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Chemical Peel' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Cosmetic Dentistry' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Cosmetology' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Cryotherapy And Electrocautery For Removal Of Warts' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Dental Crown' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Dental Curettage' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Dental Hygiene' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Dental Implants' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Dentoalveolar Surgery' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Dermal Fillers' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Dermatology' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Disimpactions' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Fillings' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Fluoride Application Treatment' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'General Dentistry' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Gums Treatment' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Head Lice' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Implantology' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Implants' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Intense Pulsed Light (IPL)' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Iontophoresis' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Jewel Teeth' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'LASER For Skin' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Laser Hair Removal' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Maxillo Surgery' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Melasma' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Meso Therapy' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Microdermabrasion' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Molluscum Contagoisum Treatment' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Monitoring of the Dental Eruption' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Oral And Maxillofacial Surgery' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Orthodontics' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Phototherapy' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Pimples' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Preventive Dental Care' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Procedural Dermatology' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Prosthodontics' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'PRP' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Root Canal' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Routine Tooth Extractions' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Scaling And Polishing' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Skin Cancer Surgery' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Skin Care Consultation' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Skin Peeling' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Skin Tightening' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Skin Toning' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Teeth Straightening' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Teeth Whitening' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Tooth Extraction' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Trauma' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'UVB Light Therapy For Vitiligo' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Venereology' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Wisdom Teeth Removal' 
 ],
[
'specialty_id' => '163', 
'service_name' => 'Xanthelasmas' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Adolescent Medicine' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Child And Adolescent Psychiatry' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Child Dietary Consultation' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Circumcision' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Diabetic Child Management' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Emergency Care' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Indoor Neonatal Care' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'New Born Examination' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Nutritional Assessment' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Pediatric Consultation' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Pediatric ICU' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Phototherapy' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Treatment of Infections' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Treatment of Tonsillitis' 
 ],
[
'specialty_id' => '120', 
'service_name' => 'Vaccination' 
 ],
[
'specialty_id' => '129', 
'service_name' => 'Chronic Ambulatory Peritoneal Dialysis' 
 ],
[
'specialty_id' => '129', 
'service_name' => 'Hemodialysis' 
 ],
[
'specialty_id' => '129', 
'service_name' => 'ICU Dialysis' 
 ],
[
'specialty_id' => '129', 
'service_name' => 'Interventional Nephrology' 
 ],
[
'specialty_id' => '129', 
'service_name' => 'Kidney Biopsy' 
 ],
[
'specialty_id' => '129', 
'service_name' => 'Lithotripsy' 
 ],
[
'specialty_id' => '129', 
'service_name' => 'Peritoneal Dialysis' 
 ],
[
'specialty_id' => '129', 
'service_name' => 'Pyelolithotomy' 
 ],
[
'specialty_id' => '129', 
'service_name' => 'Pyeloplasty' 
 ],
[
'specialty_id' => '129', 
'service_name' => 'Renal (Kidney) Transplant' 
 ],
[
'specialty_id' => '129', 
'service_name' => 'Renal Replacement Therapy' 
 ],
[
'specialty_id' => '172', 
'service_name' => 'Chronic Ambulatory Peritoneal Dialysis' 
 ],
[
'specialty_id' => '172', 
'service_name' => 'Hemodialysis' 
 ],
[
'specialty_id' => '172', 
'service_name' => 'ICU Dialysis' 
 ],
[
'specialty_id' => '172', 
'service_name' => 'Interventional Nephrology' 
 ],
[
'specialty_id' => '172', 
'service_name' => 'Kidney Biopsy' 
 ],
[
'specialty_id' => '172', 
'service_name' => 'Lithotripsy' 
 ],
[
'specialty_id' => '172', 
'service_name' => 'Peritoneal Dialysis' 
 ],
[
'specialty_id' => '172', 
'service_name' => 'Pyelolithotomy' 
 ],
[
'specialty_id' => '172', 
'service_name' => 'Pyeloplasty' 
 ],
[
'specialty_id' => '172', 
'service_name' => 'Renal (Kidney) Transplant' 
 ],
[
'specialty_id' => '172', 
'service_name' => 'Renal Replacement Therapy' 
 ],
[
'specialty_id' => '89', 
'service_name' => 'Epilepsy Treatment' 
 ],
[
'specialty_id' => '89', 
'service_name' => 'Genetic Testing' 
 ],
[
'specialty_id' => '89', 
'service_name' => 'Migraine Treatment' 
 ],
[
'specialty_id' => '89', 
'service_name' => 'Neuro Rehabilitation' 
 ],
[
'specialty_id' => '89', 
'service_name' => 'Sleep Studies' 
 ],
[
'specialty_id' => '89', 
'service_name' => 'Transcranial Magnetic Stimulation (TMS)' 
 ],
[
'specialty_id' => '89', 
'service_name' => 'Tremor Analysis' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'ALS Treatment' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Autonomic Testing' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Carotid and Transcranial Doppler (TCD)' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Electroencephalography (EEG)' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Electroencephalography (EMG)' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Electronystagmography (ENG)' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Epilepsy Treatment' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Evoked Potential ' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Genetic Testing' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'ICP Monitoring' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Migraine Treatment' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Neuro Rehabilitation' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Skin Biopsies' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Sleep Studies' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Transcranial Magnetic Stimulation (TMS)' 
 ],
[
'specialty_id' => '84', 
'service_name' => 'Tremor Analysis' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'ALS Treatment' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Autonomic Testing' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Carotid and Transcranial Doppler (TCD)' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Electroencephalography (EEG)' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Electroencephalography (EMG)' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Electronystagmography (ENG)' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Epilepsy Treatment' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Evoked Potential ' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Genetic Testing' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'ICP Monitoring' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Migraine Treatment' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Neuro Rehabilitation' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Skin Biopsies' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Sleep Studies' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Transcranial Magnetic Stimulation (TMS)' 
 ],
[
'specialty_id' => '88', 
'service_name' => 'Tremor Analysis' 
 ],
[
'specialty_id' => '93', 
'service_name' => 'Obesity Management' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Antenatal Care' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Antenatal Checkup' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Caesarean (C-Section)' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Clinical Breast Examination' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Contraceptive Advice' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Cosmetic And Reconstruction Surgery' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Diagnostic Laparascopy' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Endoscopic Surgery' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Genetic Testing' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Gynaecological Surgeries' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Gyneacological Tumors' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Hysterectomy' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Hysterosalpingogram' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Hysteroscopic Procedures' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Hysteroscopy' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Infertility Treatment' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'IUI' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'IVF' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Laparoscopic Surgery' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Laparotomy' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Maternal Care' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Myomectomy' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Normal Delivery' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Obesity Management' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Obstetrical Ultrasound' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Spontaneous Vaginal Delivery (SVD)' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Test Tube Baby' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Triplet Deliveries' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Tubal Ligation' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'Tubectomy' 
 ],
[
'specialty_id' => '63', 
'service_name' => 'UV Prolapse' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Biopsy' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Blood Cancer Treatment' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Bone Cancer Treatment' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Brain Cancer Treatment' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Breast Cancer Treatment' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Cancer Treatment' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Chemotherapy' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Gastrointestinal Tumors Management' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Intestinal Cancer Treatment' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Liver Cancer Treatment' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Lung Cancer Treatment' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Oncology Medicine' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Ovarian Cancer Treatment' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Pancreatic Cancer Treatment' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Prostate Cancer Treatment' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Radiation Therapy' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Renal (Kidney) Cancer Treatment' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Targeted Cell Therapies' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'TNM Staging' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Tumor Markers' 
 ],
[
'specialty_id' => '77', 
'service_name' => 'Tumor Removal Surgery' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Biopsy' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Blood Cancer Treatment' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Bone Cancer Treatment' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Brain Cancer Treatment' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Breast Cancer Treatment' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Cancer Treatment' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Chemotherapy' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Gastrointestinal Tumors Management' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Intestinal Cancer Treatment' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Liver Cancer Treatment' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Lung Cancer Treatment' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Oncology Medicine' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Ovarian Cancer Treatment' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Pancreatic Cancer Treatment' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Prostate Cancer Treatment' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Radiation Therapy' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Renal (Kidney) Cancer Treatment' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Targeted Cell Therapies' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'TNM Staging' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Tumor Markers' 
 ],
[
'specialty_id' => '95', 
'service_name' => 'Tumor Removal Surgery' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Biopsy' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Blood Cancer Treatment' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Bone Cancer Treatment' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Brain Cancer Treatment' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Breast Cancer Treatment' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Cancer Treatment' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Chemotherapy' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Gastrointestinal Tumors Management' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Intestinal Cancer Treatment' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Liver Cancer Treatment' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Lung Cancer Treatment' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Oncology Medicine' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Ovarian Cancer Treatment' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Pancreatic Cancer Treatment' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Prostate Cancer Treatment' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Radiation Therapy' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Renal (Kidney) Cancer Treatment' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Targeted Cell Therapies' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'TNM Staging' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Tumor Markers' 
 ],
[
'specialty_id' => '127', 
'service_name' => 'Tumor Removal Surgery' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Aesthetic Crown and Bridges' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Artificial Teeth' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Braces' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Cancer Surgeries' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Ceramic Braces' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Ceramic Crowns' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Cosmetic Dentistry' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Dental Crown' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Dental Curettage' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Dental Hygiene' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Dental Implants' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Dental Treatments Including Borees' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Dentoalveolar Surger' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Disimpactions' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Facial Deformities' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Facial Jaw Bone Fracture Treatment' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Fillings' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Fluoride Application Treatment' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'General Dentistry' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Gums Treatment' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Implantology' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Implants' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Jewel Teeth' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Maxilla Surgery' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Monitoring Of the Dental Eruption' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Oral and Maxillofacial Surgery' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Orthodontics' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Preventive Dental Care' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Prosthodontics' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Root Canal' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Routine Tooth Extractions' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Scaling and Polishing' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Teeth Straightening' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Tooth Extraction' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Trauma' 
 ],
[
'specialty_id' => '9', 
'service_name' => 'Wisdom Teeth Removal' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Aesthetic Crown and Bridges' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Artificial Teeth' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Braces' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Cancer Surgeries' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Ceramic Braces' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Ceramic Crowns' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Cosmetic Dentistry' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Dental Crown' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Dental Curettage' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Dental Hygiene' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Dental Implants' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Dental Treatments Including Borees' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Dentoalveolar Surger' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Disimpactions' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Facial Deformities' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Facial Jaw Bone Fracture Treatment' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Fillings' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Fluoride Application Treatment' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'General Dentistry' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Gums Treatment' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Implantology' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Implants' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Jewel Teeth' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Maxilla Surgery' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Monitoring Of the Dental Eruption' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Oral and Maxillofacial Surgery' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Orthodontics' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Preventive Dental Care' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Prosthodontics' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Root Canal' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Routine Tooth Extractions' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Scaling and Polishing' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Teeth Straightening' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Tooth Extraction' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Trauma' 
 ],
[
'specialty_id' => '50', 
'service_name' => 'Wisdom Teeth Removal' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Aesthetic Crown and Bridges' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Artificial Teeth' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Braces' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Cancer Surgeries' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Ceramic Braces' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Ceramic Crowns' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Cosmetic Dentistry' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Dental Crown' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Dental Curettage' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Dental Hygiene' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Dental Implants' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Dental Treatments Including Borees' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Dentoalveolar Surger' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Disimpactions' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Facial Deformities' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Facial Jaw Bone Fracture Treatment' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Fillings' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Fluoride Application Treatment' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'General Dentistry' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Gums Treatment' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Implantology' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Implants' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Jewel Teeth' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Maxilla Surgery' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Monitoring Of the Dental Eruption' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Oral and Maxillofacial Surgery' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Orthodontics' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Preventive Dental Care' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Prosthodontics' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Root Canal' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Routine Tooth Extractions' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Scaling and Polishing' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Teeth Straightening' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Tooth Extraction' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Trauma' 
 ],
[
'specialty_id' => '10', 
'service_name' => 'Wisdom Teeth Removal' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Angiography' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Angioplasty' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Balloon Valvotomy' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Beating Heart Surgery' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Cardiac Bypass Surgery' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Cardiac Catheterization' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Cardiac CT Angiography' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Cardiac Patient Management' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Cardiology Medicine (General)' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Coronary Artery Bypass Surgery (CABG)' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Echocardiogram' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Echocardiography (ECHO)' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Electrocardiography (ECG)' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Electrophysiology' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Endoscopic Third Ventriculostomy (ETV)' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Exercise Tolerance Test (ETT)' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Heart Attack Management' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Heart Transplant' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Holter Monitoring' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Interventional Cardiology' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Open Heart Surgery' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Radiofrequency Ablation' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Stenosis And Valvular Regurgitation' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Stenting' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Stress Echocardiography' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Trans-thoracic Echocardiography' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Trans-esophageal Echocardiography' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Treatment for Heart Attack' 
 ],
[
'specialty_id' => '123', 
'service_name' => 'Ventriculo Peritoneal (VP) Shunt' 
 ],
[
'specialty_id' => '51', 
'service_name' => 'Hernia Surgery' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Adolesent Medicine' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Allergy Treatment' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Biliary Stone Treatment' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Biopsy' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Child and Adolescent Psychiatry' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Child Dietary Consultation' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Circumcision' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Diabetic Child management' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Emergency Care' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Hepatitis A Treatment' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Hepatitis B Treatment' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Hepatitis C Treatmen' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Hernia Surgery' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Indoor Neonatal Care' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'New Born Examination' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Nutritional Assessment' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Pediatric Consultation' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Pediatric ICU' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Treatment of Infections' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Treatment Of Tonsillitis' 
 ],
[
'specialty_id' => '122', 
'service_name' => 'Vaccination' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Acne Treatment' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Alopecia' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Boil' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Chemical Peel' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Cosmetology' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Cryotherapy and Electrocautery for Removal of Warts' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Dermal Fillers' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Dermatology' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Intense Pulsed light (IPL)' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Iontophoresis' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'LASER for Skin' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Laser Hair Removal' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Melasma' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Meso Therapy' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Microdermabrasion' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Phototherapy' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Pimples' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Procedural Dermatology' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'PRP' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Skin Cancer Surgery' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Skin Care Consulation' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Skin Peeling' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Skin Tightening' 
 ],
[
'specialty_id' => '27', 
'service_name' => 'Skin Toning' 
 ],
[
'specialty_id' => '152', 
'service_name' => 'ADHD Treatment' 
 ],
[
'specialty_id' => '152', 
'service_name' => 'Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '152', 
'service_name' => 'Bipolar Disorder Treatment' 
 ],
[
'specialty_id' => '152', 
'service_name' => 'Depression Treatment' 
 ],
[
'specialty_id' => '152', 
'service_name' => 'Electroconvulsive Therapy (ECT)' 
 ],
[
'specialty_id' => '152', 
'service_name' => 'Mental Health Treatment' 
 ],
[
'specialty_id' => '152', 
'service_name' => 'Psychiatric Consultation and Management' 
 ],
[
'specialty_id' => '152', 
'service_name' => 'Social Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '152', 
'service_name' => 'Stress Management' 
 ],
[
'specialty_id' => '153', 
'service_name' => 'Anger Management' 
 ],
[
'specialty_id' => '153', 
'service_name' => 'Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '153', 
'service_name' => 'Autism Treatment' 
 ],
[
'specialty_id' => '153', 
'service_name' => 'Career Counselling' 
 ],
[
'specialty_id' => '153', 
'service_name' => 'Cognitive Behaviour Therapy (CBT)' 
 ],
[
'specialty_id' => '153', 
'service_name' => 'Depression Treatment' 
 ],
[
'specialty_id' => '153', 
'service_name' => 'Mental Health Treatment' 
 ],
[
'specialty_id' => '153', 
'service_name' => 'Neuropsychological and Psychological Assessment' 
 ],
[
'specialty_id' => '153', 
'service_name' => 'Social Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '153', 
'service_name' => 'Speech Therapy' 
 ],
[
'specialty_id' => '153', 
'service_name' => 'Stress Management' 
 ],
[
'specialty_id' => '148', 
'service_name' => 'Anger Management' 
 ],
[
'specialty_id' => '148', 
'service_name' => 'Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '148', 
'service_name' => 'Autism Treatment' 
 ],
[
'specialty_id' => '148', 
'service_name' => 'Career Counselling' 
 ],
[
'specialty_id' => '148', 
'service_name' => 'Cognitive Behaviour Therapy (CBT)' 
 ],
[
'specialty_id' => '148', 
'service_name' => 'Depression Treatment' 
 ],
[
'specialty_id' => '148', 
'service_name' => 'Mental Health Treatment' 
 ],
[
'specialty_id' => '148', 
'service_name' => 'Neuropsychological and Psychological Assessment' 
 ],
[
'specialty_id' => '148', 
'service_name' => 'Social Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '148', 
'service_name' => 'Speech Therapy' 
 ],
[
'specialty_id' => '148', 
'service_name' => 'Stress Management' 
 ],
[
'specialty_id' => '149', 
'service_name' => 'Anger Management' 
 ],
[
'specialty_id' => '149', 
'service_name' => 'Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '149', 
'service_name' => 'Autism Treatment' 
 ],
[
'specialty_id' => '149', 
'service_name' => 'Career Counselling' 
 ],
[
'specialty_id' => '149', 
'service_name' => 'Cognitive Behaviour Therapy (CBT)' 
 ],
[
'specialty_id' => '149', 
'service_name' => 'Depression Treatment' 
 ],
[
'specialty_id' => '149', 
'service_name' => 'Mental Health Treatment' 
 ],
[
'specialty_id' => '149', 
'service_name' => 'Neuropsychological and Psychological Assessment' 
 ],
[
'specialty_id' => '149', 
'service_name' => 'Social Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '149', 
'service_name' => 'Speech Therapy' 
 ],
[
'specialty_id' => '149', 
'service_name' => 'Stress Management' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'Blood Gas Analysis' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'Bronchial Biopsy' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'Bronchial Thermoplasty (Severe Asthma Treatment)' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'Bronchoscopy' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'Diagnostic Testing' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'General Pulmonary And Critical Care' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'Intensive Care Management' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'Lung Biopsy' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'Lung Transplant' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'Pediatric Pulmonary Care' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'Pulmonary Rehabilitation' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'Spirometry' 
 ],
[
'specialty_id' => '130', 
'service_name' => 'Thyroid Treatment' 
 ],
[
'specialty_id' => '136', 
'service_name' => 'Pain Management Services' 
 ],
[
'specialty_id' => '139', 
'service_name' => 'Pain Management Services' 
 ],
[
'specialty_id' => '79', 
'service_name' => 'Cardiology Medicine (General)' 
 ],
[
'specialty_id' => '79', 
'service_name' => 'Constipation Treatment' 
 ],
[
'specialty_id' => '79', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '79', 
'service_name' => 'Diarrhea Treatment' 
 ],
[
'specialty_id' => '79', 
'service_name' => 'General Pulmonary And Critical Care' 
 ],
[
'specialty_id' => '79', 
'service_name' => 'Hepatitis A Treatment' 
 ],
[
'specialty_id' => '79', 
'service_name' => 'Hepatitis B Treatment' 
 ],
[
'specialty_id' => '79', 
'service_name' => 'Hepatitis C Treatment' 
 ],
[
'specialty_id' => '79', 
'service_name' => 'Obesity Management' 
 ],
[
'specialty_id' => '79', 
'service_name' => 'Thyroid Treatment' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Acne Treatment' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Alopecia' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Antihistamine Treatment' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Chemical Peel' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Cosmetology' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Cryotherapy and Electrocautery For Removal Of Warts' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Dermal Fillers' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Diabetes Management' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Head Lice' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Intense Pulsed Light (IPL)' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Iontophoresis' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'LASER for Skin' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Melasma' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Meso Therapy' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Metabolic Disorders Treatment' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Microdermabrasion' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Molluscum Contagoisum Treatment' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Phototherapy' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Pimples' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'PRP' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Skin Care Consultation' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Skin Peeling' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Skin Tightening' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Skin Toning' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'UVB Light Therapy for Vitiligo' 
 ],
[
'specialty_id' => '65', 
'service_name' => 'Venereology' 
 ],
[
'specialty_id' => '80', 
'service_name' => 'Bronchial Biopsy' 
 ],
[
'specialty_id' => '80', 
'service_name' => 'Bronchoscopy' 
 ],
[
'specialty_id' => '80', 
'service_name' => 'Diagnostic Testing' 
 ],
[
'specialty_id' => '80', 
'service_name' => 'General Pulmonary and Critical Care' 
 ],
[
'specialty_id' => '80', 
'service_name' => 'Lung Biopsy' 
 ],
[
'specialty_id' => '80', 
'service_name' => 'Spirometry' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Abdomen Ultrasound' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Antenatal Care' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Antenatal Checkup' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Caesarean (C-Section)' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Contraceptive Advice' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Diagnostic Laparascopy' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Doppler Ultrasound' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Dyneacological surgeries' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Infertility Treatment' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'KUB Ultrasound' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Maternal Care' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Normal Delivery' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Obstetric Ultrasound' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Pelvic Ultrasound' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Soft Tissue Ultrasound' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Triplet Deliveries' 
 ],
[
'specialty_id' => '168', 
'service_name' => 'Ultrasound' 
 ],
[
'specialty_id' => '142', 
'service_name' => 'Anger Management' 
 ],
[
'specialty_id' => '142', 
'service_name' => 'Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '142', 
'service_name' => 'Autism Treatment' 
 ],
[
'specialty_id' => '142', 
'service_name' => 'Career Counselling' 
 ],
[
'specialty_id' => '142', 
'service_name' => 'Cognitive Behaviour Therapy (CBT)' 
 ],
[
'specialty_id' => '142', 
'service_name' => 'Depression Treatment' 
 ],
[
'specialty_id' => '142', 
'service_name' => 'Mental Health Treatment' 
 ],
[
'specialty_id' => '142', 
'service_name' => 'Neuropsychological And Psychological Assessment' 
 ],
[
'specialty_id' => '142', 
'service_name' => 'Social Anxiety Disorders Treatment' 
 ],
[
'specialty_id' => '142', 
'service_name' => 'Speech Therapy' 
 ],
[
'specialty_id' => '142', 
'service_name' => 'Stress Management' 
 ],
[
'specialty_id' => '81', 
'service_name' => 'Pain Management Services' 
 ],
[
'specialty_id' => '44', 
'service_name' => 'Corona-Virus Treatment' 
 ],
[
'specialty_id' => '57', 
'service_name' => 'Corona-Virus Treatment' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Abdomen Colorectal Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Abdominal Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Appendectomy' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Breast Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Cholecystectomy' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Circumcision' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Colostomy' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Cosmetic Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'cystectomy' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Emergency Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Endocrology And Laser Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Extracorporeal Shockwave Lithotripsy (ESWL)' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Fistula ' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Fistulae Slings' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'General Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Head and Neck Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Hepatobiliary Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Hernia and Perianal Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Hernia Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Intussusception' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Kidney Cancer' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Kidney Failure' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Kidney Transplantation' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Laparoscopic Appendectomy' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Laparoscopic Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Laparoscopic Chole' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Laparoscopic nephrectomy' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Lithotripsy' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Oncologist Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Operation of Gall Bladder' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Panile Implants' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Percutaneous Nephrolithotomy (PCNL)' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Prostatectomy' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Pyeloithotomy' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Radical Prostectomy' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Thyroid surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'TURP' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Oncological Surgery' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Operation of Gall Bladder' 
 ],
[
'specialty_id' => '174', 
'service_name' => 'Venereology' 
 ],
          
        ]);
        }
    
}
