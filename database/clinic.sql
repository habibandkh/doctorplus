-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2020 at 05:49 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clinic`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pic` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `gender` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `pic`, `email`, `password`, `phone`, `age`, `gender`) VALUES
(1, 'habib', 'admin-1573032037.jpg', 'habibandkh@gmail.com', '000', '0705931096', 25, 1);

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `appID` int(8) NOT NULL,
  `doctor_id` int(8) DEFAULT NULL,
  `pateint_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `appointment_date` date DEFAULT NULL,
  `appointment_time` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `Status` tinyint(4) DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `feedback` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`appID`, `doctor_id`, `pateint_id`, `user_id`, `appointment_date`, `appointment_time`, `created_at`, `Status`, `reason`, `feedback`) VALUES
(787, 110, 23, 19, '2019-11-20', '13:00:00', '2019-11-24 10:37:51', 1, 'fgjdfh', 1),
(788, 110, 23, 19, '2019-11-20', '13:00:00', '2019-11-24 10:37:51', 1, 'fgjdfh', 1),
(789, 110, 23, 19, '2019-11-20', '13:00:00', '2019-11-24 10:37:51', 1, 'fgjdfh', 1),
(790, 110, 23, 19, '2019-11-20', '13:00:00', '2019-11-24 10:37:51', 1, 'fgjdfh', 0),
(791, 110, 23, 19, '2019-11-20', '13:00:00', '2019-11-24 10:37:51', 1, 'fgjdfh', 1),
(792, 110, 23, 19, '2019-11-20', '13:00:00', '2019-11-24 10:37:51', 1, 'fgjdfh', 0),
(793, 110, 23, 19, '2019-11-20', '13:00:00', '2019-11-24 10:37:51', 1, 'fgjdfh', 0),
(794, 110, 23, 19, '2019-11-20', '13:00:00', '2019-11-24 10:37:51', 1, 'fgjdfh', 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `name`) VALUES
(1, 'AAAA'),
(2, 'B'),
(3, 'C'),
(4, 'D'),
(9, 'Ah'),
(12, 'baby'),
(13, 'hihi hi'),
(14, 'hi'),
(15, 'f'),
(16, 'ff'),
(17, 'g'),
(18, 'k'),
(19, 'jhf');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `com_id` int(11) NOT NULL,
  `com_name` varchar(50) NOT NULL,
  `com_email` varchar(50) NOT NULL,
  `comment` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `postid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`com_id`, `com_name`, `com_email`, `comment`, `date`, `postid`) VALUES
(3, 'reza', 'habibkh028@gmail.com', 'dsfghfgjhjtghkfkj', '2019-11-11 05:15:31', 20),
(4, 'djgh', 'habibandkh@gmail.com', 'fgjh', '2019-11-11 05:18:31', 20),
(5, 'l;', 'habibakh028@gmail.com', ';llllllllllllll\'', '2019-11-11 05:18:48', 20),
(6, 'reza', 'habibandkh@gmail.com', 'YURTU', '2019-12-07 06:56:33', 22);

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `doctorID` int(8) NOT NULL,
  `doctor_name` varchar(50) DEFAULT NULL,
  `doctor_email` varchar(50) DEFAULT NULL,
  `doctor_phoneNo` varchar(10) DEFAULT NULL,
  `doctor_password` varchar(50) DEFAULT NULL,
  `doctor_picture` varchar(50) DEFAULT NULL,
  `doctor_clinicAddress` varchar(255) DEFAULT NULL,
  `doctor_status` tinyint(4) DEFAULT NULL,
  `doctor_regDate` varchar(50) NOT NULL,
  `doctor_fee` int(10) DEFAULT NULL,
  `doctor_gender` varchar(11) NOT NULL,
  `doctor_birthday` varchar(50) NOT NULL,
  `doctor_last_name` varchar(50) NOT NULL,
  `doctor_father_name` varchar(50) NOT NULL,
  `doctor_Short_Biography` text NOT NULL,
  `Internist` varchar(50) DEFAULT NULL,
  `Expert` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`doctorID`, `doctor_name`, `doctor_email`, `doctor_phoneNo`, `doctor_password`, `doctor_picture`, `doctor_clinicAddress`, `doctor_status`, `doctor_regDate`, `doctor_fee`, `doctor_gender`, `doctor_birthday`, `doctor_last_name`, `doctor_father_name`, `doctor_Short_Biography`, `Internist`, `Expert`) VALUES
(110, 'Mariam', 'mariam@gmail.com', '0705931096', 'Mariam', 'doctor-1572324497.jpg', 'taimani', 1, '2019', 2000, 'female', '1990-05-10', 'Rahmani', 'Reza', 'George A. Sample, MD, FCCP, is a graduate of George Washington University Medical School. He trained in internal medicine at the University of Oregon Health Sciences (UOHS) and pulmonary critical care medicine at UOHS and the University of Southern California.\r\n\r\nSample has been practicing since 1976, the last 12 years in Washington, D.C., at the Washington Hospital Center, selected as one of the top 100 hospitals in the United States. Sample\'s practice specializes in the care of critically ill and injured postoperative patients. Sample\'s interests also include end-of-life issues, advanced directives, and furthering organ procurement. Sample is also a clinical associate professor of medicine at George Washington University Medical School and is a fellow in the College of Chest Physicians.\r\n\r\nSample recently received the Distinguished Service Award from the Society of Critical Care Medicine (an international society comprised of critical care practioners) for his role in patient and physician advocacy.\r\n\r\nSample is board certified in internal medicine, pulmonary medicine, and critical care medicine.', 'Primary care', 'Dermatology'),
(111, 'Roya', 'Rahmani@gmail.com', '0705931096', 'Rahmani', 'doctor-1572325132.jpg', 'karte - chaher', 1, '2019', 500, 'female', '1990-05-10', 'Rahmani', 'dady', 'George A. Sample, MD, FCCP, is a graduate of George Washington University Medical School. He trained in internal medicine at the University of Oregon Health Sciences (UOHS) and pulmonary critical care medicine at UOHS and the University of Southern California.\r\n\r\nSample has been practicing since 1976, the last 12 years in Washington, D.C., at the Washington Hospital Center, selected as one of the top 100 hospitals in the United States. Sample\'s practice specializes in the care of critically ill and injured postoperative patients. Sample\'s interests also include end-of-life issues, advanced directives, and furthering organ procurement. Sample is also a clinical associate professor of medicine at George Washington University Medical School and is a fellow in the College of Chest Physicians.\r\n\r\nSample recently received the Distinguished Service Award from the Society of Critical Care Medicine (an international society comprised of critical care practioners) for his role in patient and physician advocacy.\r\n\r\nSample is board certified in internal medicine, pulmonary medicine, and critical care medicine.', '', 'psycologi'),
(112, 'Rona', 'habibandkh@gmail.com', '0705931096', 'aaa', 'doctor-1572325167.jpg', 'taimani', 1, '2019', 200, 'male', '1990-05-10', 'dsfg', 'dsfg', 'ghllkjThe issue was actually dghllkjThe issue was actually due to the very top image (image of the clouds) being set with the position absolute and a high z-index. Thank you all for your help.ue to the very top image (image of the clouds) being set with the position absolute and a high z-index. Thank you all for your help.', '', 'Dermatology');

-- --------------------------------------------------------

--
-- Table structure for table `doctorspeciality`
--

CREATE TABLE `doctorspeciality` (
  `id_s_d` int(11) NOT NULL,
  `F_specialityID` int(8) DEFAULT NULL,
  `F_doctorID` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctorspeciality`
--

INSERT INTO `doctorspeciality` (`id_s_d`, `F_specialityID`, `F_doctorID`) VALUES
(13, 63, 110),
(15, 64, 110),
(19, 64, 111),
(16, 65, 110),
(20, 66, 110);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_education`
--

CREATE TABLE `doctor_education` (
  `D_E_ID` int(11) NOT NULL,
  `D_FID` int(11) NOT NULL,
  `school_name` varchar(50) NOT NULL,
  `title_of_study` varchar(50) NOT NULL,
  `start_Date` varchar(50) NOT NULL,
  `end_Date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_education`
--

INSERT INTO `doctor_education` (`D_E_ID`, `D_FID`, `school_name`, `title_of_study`, `start_Date`, `end_Date`) VALUES
(8, 110, 'Comsats University Islamabad', 'MBA', '2010', '2016');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_experience`
--

CREATE TABLE `doctor_experience` (
  `experience_id` int(11) NOT NULL,
  `doctor_FID` int(11) NOT NULL,
  `experience_name` varchar(50) NOT NULL,
  `start` varchar(10) NOT NULL,
  `end` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_experience`
--

INSERT INTO `doctor_experience` (`experience_id`, `doctor_FID`, `experience_name`, `start`, `end`) VALUES
(5, 110, 'Shafa hospital doctor assistant', '2015', '2016');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_services`
--

CREATE TABLE `doctor_services` (
  `services_id` int(11) NOT NULL,
  `doctor_s_id` int(11) NOT NULL,
  `service_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_services`
--

INSERT INTO `doctor_services` (`services_id`, `doctor_s_id`, `service_name`) VALUES
(26, 110, 'laser therapy'),
(27, 110, 'light therap');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_view`
--

CREATE TABLE `doctor_view` (
  `view_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_view`
--

INSERT INTO `doctor_view` (`view_id`, `doctor_id`, `year`, `month`, `created_at`) VALUES
(3, 111, 2020, 6, '2019-12-04 06:45:51'),
(4, 111, 2020, 6, '2019-12-04 06:46:11'),
(5, 111, 2020, 6, '2019-12-04 06:46:19'),
(6, 110, 2019, 12, '2019-12-04 06:51:03'),
(7, 110, 2019, 12, '2019-12-04 06:58:45'),
(8, 110, 2019, 12, '2019-12-04 07:02:39'),
(9, 110, 2019, 12, '2019-12-04 07:05:30'),
(10, 110, 2019, 12, '2019-12-04 07:16:03'),
(11, 110, 2019, 12, '2019-12-04 07:18:38'),
(12, 110, 2019, 12, '2019-12-04 07:19:55'),
(13, 110, 2019, 12, '2019-12-04 07:20:14'),
(14, 110, 2019, 12, '2019-12-04 07:20:54'),
(15, 110, 2019, 12, '2019-12-04 07:21:54'),
(16, 110, 2019, 12, '2019-12-04 07:23:20'),
(17, 110, 2019, 12, '2019-12-04 07:24:14'),
(18, 110, 2019, 12, '2019-12-04 07:32:02'),
(19, 110, 2019, 12, '2019-12-04 10:06:23'),
(20, 110, 2019, 12, '2019-12-04 10:11:12'),
(21, 110, 2019, 12, '2019-12-04 10:11:35'),
(22, 110, 2019, 12, '2019-12-04 10:13:29'),
(23, 110, 2019, 12, '2019-12-04 10:18:14'),
(24, 110, 2019, 12, '2019-12-04 10:22:40'),
(25, 110, 2019, 12, '2019-12-04 10:22:51'),
(26, 110, 2019, 12, '2019-12-04 10:25:37'),
(27, 110, 2019, 12, '2019-12-05 05:32:22'),
(28, 110, 2019, 12, '2019-12-05 05:32:27'),
(29, 110, 2019, 12, '2019-12-05 05:32:38'),
(30, 110, 2019, 12, '2019-12-05 05:32:52'),
(31, 110, 2019, 12, '2019-12-05 05:33:41'),
(32, 110, 2019, 12, '2019-12-10 09:06:23'),
(33, 111, 2019, 12, '2019-12-10 09:06:47'),
(34, 110, 2019, 12, '2019-12-10 09:06:53'),
(35, 111, 2019, 12, '2019-12-10 09:07:00'),
(36, 112, 2019, 12, '2019-12-10 16:04:16'),
(37, 112, 2019, 12, '2019-12-10 16:04:24'),
(38, 112, 2019, 12, '2019-12-10 16:04:37');

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `docID` int(11) NOT NULL,
  `pateint_id` int(11) DEFAULT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_nopad_ci DEFAULT NULL,
  `document` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`docID`, `pateint_id`, `details`, `document`) VALUES
(63, 22, 'aa', '1971432887.png'),
(64, 22, 'اتم', '624851930.pdf'),
(65, 23, 'te', '1103035147.pdf'),
(66, 22, 'سیبل', '1524231951.jpg'),
(67, 22, 'ghj', '1638089155.jpg'),
(68, 23, 'fghdfgh', '2099985468.png'),
(69, 23, 'dfghdf', '599133834.png'),
(70, 23, 'ncvn', '1262294485.pdf'),
(71, 23, 'hhh', '1505993999.jpg'),
(72, 23, 'hhhhh', '322260067.pdf'),
(78, 22, 'sags', '2138895742.png');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `feedback_id` int(11) NOT NULL,
  `feedback_doctor_id` int(11) NOT NULL,
  `feedback_user_id` int(11) NOT NULL,
  `rate` tinyint(1) NOT NULL,
  `comment` text DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`feedback_id`, `feedback_doctor_id`, `feedback_user_id`, `rate`, `comment`, `date`) VALUES
(76, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(77, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(78, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(79, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(80, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(81, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(82, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(83, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(84, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(85, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(86, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(87, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(88, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(89, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(90, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(91, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(92, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(93, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(94, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(95, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(96, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(97, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(98, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(99, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(100, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(101, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(102, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(103, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(104, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(105, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(106, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(107, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(108, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(109, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(110, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(111, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(112, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(113, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(114, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(115, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(116, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(117, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(118, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(119, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(120, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(121, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(122, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(123, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(124, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(125, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(126, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(127, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(128, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(129, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(130, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(131, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(132, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(133, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(134, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(135, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(136, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(137, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(138, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(139, 111, 19, 1, NULL, '0000-00-00 00:00:00'),
(140, 112, 19, 1, NULL, '0000-00-00 00:00:00'),
(141, 111, 19, 1, NULL, '0000-00-00 00:00:00'),
(142, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(143, 112, 19, 1, NULL, '0000-00-00 00:00:00'),
(144, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(145, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(146, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(147, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(148, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(149, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(150, 111, 19, 1, NULL, '0000-00-00 00:00:00'),
(151, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(152, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(153, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(154, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(155, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(156, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(157, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(158, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(159, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(160, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(161, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(162, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(163, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(164, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(165, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(166, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(167, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(168, 110, 19, 3, NULL, '0000-00-00 00:00:00'),
(169, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(170, 110, 19, 3, NULL, '0000-00-00 00:00:00'),
(171, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(172, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(173, 110, 19, 3, NULL, '0000-00-00 00:00:00'),
(174, 110, 19, 3, NULL, '0000-00-00 00:00:00'),
(175, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(176, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(177, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(178, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(179, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(180, 110, 19, 4, NULL, '0000-00-00 00:00:00'),
(181, 110, 19, 4, NULL, '0000-00-00 00:00:00'),
(182, 110, 19, 2, NULL, '0000-00-00 00:00:00'),
(183, 110, 19, 2, NULL, '0000-00-00 00:00:00'),
(184, 110, 19, 2, NULL, '0000-00-00 00:00:00'),
(185, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(186, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(187, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(188, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(189, 110, 19, 2, NULL, '0000-00-00 00:00:00'),
(190, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(191, 110, 19, 2, NULL, '0000-00-00 00:00:00'),
(192, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(193, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(194, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(195, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(196, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(197, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(198, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(199, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(200, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(201, 110, 19, 5, NULL, '0000-00-00 00:00:00'),
(202, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(203, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(204, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(205, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(206, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(207, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(208, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(209, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(210, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(211, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(212, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(213, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(214, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(215, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(216, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(217, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(218, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(219, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(220, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(221, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(222, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(223, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(224, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(225, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(226, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(227, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(228, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(229, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(230, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(231, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(232, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(233, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(234, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(235, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(236, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(237, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(238, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(239, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(240, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(241, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(242, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(243, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(244, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(245, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(246, 110, 19, 1, NULL, '0000-00-00 00:00:00'),
(247, 110, 19, 5, NULL, '2019-11-05 11:37:32'),
(248, 110, 19, 2, NULL, '2019-11-05 11:38:14'),
(249, 110, 19, 5, NULL, '2019-11-23 07:55:49'),
(250, 110, 19, 4, NULL, '2019-11-23 07:55:53'),
(251, 110, 19, 5, NULL, '2019-11-23 07:55:56'),
(252, 110, 19, 4, NULL, '2019-11-23 07:56:03'),
(253, 110, 19, 1, NULL, '2019-11-23 07:57:38'),
(254, 110, 19, 2, NULL, '2019-11-23 07:58:10'),
(255, 110, 19, 5, NULL, '2019-11-23 08:06:01'),
(256, 110, 19, 4, NULL, '2019-11-23 08:06:05'),
(257, 110, 19, 5, NULL, '2019-11-23 08:06:17'),
(258, 110, 19, 4, NULL, '2019-11-23 08:06:23'),
(259, 110, 19, 3, NULL, '2019-11-23 08:06:26'),
(260, 111, 19, 5, NULL, '2019-11-23 09:42:41'),
(261, 110, 19, 5, NULL, '2019-11-24 10:25:04'),
(262, 111, 19, 5, NULL, '2019-11-24 10:25:06'),
(263, 111, 19, 5, NULL, '2019-11-24 10:25:07'),
(264, 110, 19, 5, NULL, '2019-11-24 10:25:08'),
(265, 110, 19, 5, NULL, '2019-11-24 10:25:08'),
(266, 110, 19, 5, NULL, '2019-11-24 10:25:08'),
(267, 110, 19, 5, NULL, '2019-11-24 10:25:15'),
(268, 110, 19, 5, NULL, '2019-11-24 10:25:19'),
(269, 110, 19, 5, NULL, '2019-11-24 11:02:28'),
(270, 110, 19, 5, NULL, '2019-11-24 11:02:30'),
(271, 110, 19, 5, NULL, '2019-11-24 11:02:32'),
(272, 110, 19, 5, NULL, '2019-11-24 11:02:33'),
(273, 110, 19, 5, NULL, '2019-11-24 11:02:33'),
(274, 110, 19, 5, NULL, '2019-11-24 11:02:34'),
(275, 110, 19, 5, NULL, '2019-11-24 11:02:36'),
(276, 110, 19, 5, NULL, '2019-11-24 11:02:36'),
(277, 110, 19, 5, NULL, '2019-11-24 11:02:37'),
(278, 110, 19, 5, NULL, '2019-11-24 11:02:37'),
(279, 110, 19, 5, NULL, '2019-11-24 11:02:37'),
(280, 110, 19, 5, NULL, '2019-11-24 11:02:37'),
(281, 110, 19, 5, NULL, '2019-11-24 11:02:38'),
(282, 110, 19, 5, NULL, '2019-11-24 11:02:39'),
(283, 110, 19, 4, NULL, '2019-11-25 05:53:58'),
(284, 110, 19, 4, NULL, '2019-11-25 05:54:01'),
(285, 110, 19, 4, NULL, '2019-11-25 05:54:02'),
(286, 110, 19, 4, NULL, '2019-11-25 05:54:02'),
(287, 110, 19, 4, NULL, '2019-11-25 05:54:02'),
(288, 110, 19, 4, NULL, '2019-11-25 05:54:03'),
(289, 110, 19, 4, NULL, '2019-11-25 05:54:03'),
(290, 110, 19, 4, NULL, '2019-11-25 05:54:03'),
(291, 110, 19, 4, NULL, '2019-11-25 05:54:03'),
(292, 110, 19, 4, NULL, '2019-11-25 05:54:04'),
(293, 110, 19, 4, NULL, '2019-11-25 05:54:04'),
(294, 110, 19, 4, NULL, '2019-11-25 05:54:04'),
(295, 110, 19, 4, NULL, '2019-11-25 05:54:04'),
(296, 110, 19, 4, NULL, '2019-11-25 05:54:05'),
(297, 110, 19, 4, NULL, '2019-11-25 05:56:05'),
(298, 110, 19, 3, NULL, '2019-11-25 06:00:31'),
(299, 110, 19, 4, NULL, '2019-11-25 06:05:49'),
(300, 110, 19, 4, NULL, '2019-11-25 06:05:53'),
(301, 110, 19, 3, NULL, '2019-11-25 06:14:16');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `message` text NOT NULL,
  `status` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `name`, `phone`, `message`, `status`) VALUES
(123, 'reza', '0705931096', 'ghfdhhd', 1),
(124, 'reza', '2345345345', 'sdgsdfgf', 1),
(126, 'mohammad reza', '2345345345', 'To do that we\'d need to add a jQuery method name for each DOM element method name. And of course that method would do nothing for non-media elements so it doesn\'t seem like it would be worth the extra bytes it would take.\r\nTo do that we\'d need to add a jQuery method name for each DOM element method name. And of course that method would do nothing for non-media elements so it doesn\'t seem like it would be worth the extra bytes it would take.', 1),
(127, 'habib', '0705931096', 'To do that we\'d need to add a jQuery method name for each DOM element method name. And of course that method would do nothing for non-media elements so it doesn\'t seem like it would be worth the extra bytes it would take.', 1),
(128, 'sail', '0705931096', 'i want to be doctor', 1),
(129, 'hfg', '0705931096', 'ghfdghggf', 0),
(130, 'hfg', '0705931096', 'ghfdghggf', 0),
(131, 'habib', '0705931096', 'hi hi', 0),
(132, 'reza', '0705931096', 's', 0),
(133, 'reza', '0705931096', 'kkk', 0),
(134, 'fida', '0705931096', 'im want to be a docotr', 1),
(135, 'jhdfhg', '0705931096', 'gfjg', 0),
(136, 'kh', '0775694589', 'need to join', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_11_19_061112_create_notifications_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pateint`
--

CREATE TABLE `pateint` (
  `patientID` int(8) NOT NULL,
  `user_ID` int(11) NOT NULL,
  `p_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_photo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_fathername` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_phoneNo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(11) NOT NULL,
  `gender` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pateint`
--

INSERT INTO `pateint` (`patientID`, `user_ID`, `p_name`, `p_photo`, `p_fathername`, `p_phoneNo`, `age`, `gender`) VALUES
(22, 19, 'Rona', 'pateint-1570959959.png', 'ali', '07055931096', 15, 1),
(23, 19, 'khatereh', 'female.png', 'ali', '0705939393', 20, 0),
(25, 22, 'ahmad', 'pateint-1570952504.png', 'reza', '35', 33, 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `payID` int(8) NOT NULL,
  `payDate` date DEFAULT NULL,
  `receivedPay` int(10) DEFAULT NULL,
  `amount` int(10) DEFAULT NULL,
  `balance` int(10) DEFAULT NULL,
  `doctorID` int(8) DEFAULT NULL,
  `adminID` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`payID`, `payDate`, `receivedPay`, `amount`, `balance`, `doctorID`, `adminID`) VALUES
(1, '2019-04-03', 200, 3000, 2800, 1, 1),
(2, '2019-04-02', 300, 2000, 1700, 2, 1),
(3, '2019-04-03', 200, 3000, 2800, 1, 1),
(4, '2019-04-02', 300, 2000, 1700, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `post_title` text NOT NULL,
  `post_body` text NOT NULL,
  `post_img` varchar(50) NOT NULL,
  `post_view` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`post_id`, `category_id`, `post_title`, `post_body`, `post_img`, `post_view`, `status`, `date`) VALUES
(20, 2, 'A study plan is an effective way to help you navigate through your college education, ', 'A study plan is an effective way to help you navigate through your college education, and hold you accountable for your own learning outcomes. Time management can be challenging. Besides your classes, you likely have other commitments like extracurricular activities, work, and social engagements.\r\n\r\nA study plan is an effective way to help you navigate through your college education, and hold you accountable for your own learning outcomes. Time management can be challenging. Besides your classes, you likely have other commitments like extracurricular activities, work, and social engagements.\r\n\r\nA study plan is an effective way to help you navigate through your college education, and hold you accountable for your own learning outcomes. Time management can be challenging. Besides your classes, you likely have other commitments like extracurricular activities, work, and social engagements.', 'blog-1573371711.jpg', 0, 1, '2019-11-10 09:51:46'),
(21, 9, 'fdgjhrujhdj', 'fdghdfjhjdfghdhd', 'blog-1573371720.jpg', 0, 1, '2019-11-10 07:42:00'),
(22, 2, 'dfh', 'dfghdfjghjyjghjhfghjkjkhjfghfdfh', 'blog-1573371711.jpg', 0, 1, '2019-11-10 07:41:51');

-- --------------------------------------------------------

--
-- Table structure for table `speciality`
--

CREATE TABLE `speciality` (
  `specialityID` int(8) NOT NULL,
  `specialityName` varchar(255) DEFAULT NULL,
  `detail` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `speciality`
--

INSERT INTO `speciality` (`specialityID`, `specialityName`, `detail`) VALUES
(62, 'Gynecologist', NULL),
(63, 'Dermatologist', NULL),
(64, 'Neurologist', NULL),
(65, 'Pediatrician', NULL),
(66, 'habib', NULL),
(67, 'rahmad', NULL),
(68, 'khanom', NULL),
(69, 'rahat', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pic` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(1) NOT NULL,
  `gender` int(1) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `f_name`, `pic`, `age`, `gender`, `email`, `phone`, `password`, `created_at`, `updated_at`) VALUES
(19, 'habib', 'ali', 'pateint-1572341692.png', 56, 1, '', '0705931099', '0000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visitingtime`
--

CREATE TABLE `visitingtime` (
  `vtID` int(8) NOT NULL,
  `doctorID` int(8) DEFAULT NULL,
  `day` varchar(11) NOT NULL,
  `visit_time` time DEFAULT NULL,
  `doctor_active` tinyint(4) DEFAULT NULL,
  `time_off_on` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visitingtime`
--

INSERT INTO `visitingtime` (`vtID`, `doctorID`, `day`, `visit_time`, `doctor_active`, `time_off_on`) VALUES
(3975, 110, 'Monday', '10:00:00', 1, 0),
(3976, 110, 'Tuesday', '10:00:00', 1, 1),
(3977, 110, 'Wednesday', '10:00:00', 1, 1),
(3978, 110, 'Thursday', '10:00:00', 1, 1),
(3979, 110, 'Friday', '10:00:00', 1, 1),
(3980, 110, 'Saturday', '10:00:00', 1, 1),
(3981, 110, 'Sunday', '10:00:00', 1, 1),
(3982, 110, 'Monday', '10:30:00', 1, 1),
(3983, 110, 'Tuesday', '10:30:00', 1, 1),
(3984, 110, 'Wednesday', '10:30:00', 1, 1),
(3985, 110, 'Thursday', '10:30:00', 1, 1),
(3986, 110, 'Friday', '10:30:00', 1, 1),
(3987, 110, 'Saturday', '10:30:00', 1, 1),
(3988, 110, 'Sunday', '10:30:00', 1, 1),
(3989, 110, 'Monday', '11:00:00', 1, 0),
(3990, 110, 'Tuesday', '11:00:00', 1, 1),
(3991, 110, 'Wednesday', '11:00:00', 1, 1),
(3992, 110, 'Thursday', '11:00:00', 1, 1),
(3993, 110, 'Friday', '11:00:00', 1, 1),
(3994, 110, 'Saturday', '11:00:00', 1, 1),
(3995, 110, 'Sunday', '11:00:00', 1, 1),
(3996, 110, 'Monday', '11:30:00', 1, 0),
(3997, 110, 'Tuesday', '11:30:00', 1, 1),
(3998, 110, 'Wednesday', '11:30:00', 1, 1),
(3999, 110, 'Thursday', '11:30:00', 1, 1),
(4000, 110, 'Friday', '11:30:00', 1, 1),
(4001, 110, 'Saturday', '11:30:00', 1, 1),
(4002, 110, 'Sunday', '11:30:00', 1, 1),
(4003, 110, 'Monday', '12:00:00', 1, 1),
(4004, 110, 'Tuesday', '12:00:00', 1, 1),
(4005, 110, 'Wednesday', '12:00:00', 1, 1),
(4006, 110, 'Thursday', '12:00:00', 1, 1),
(4007, 110, 'Friday', '12:00:00', 1, 1),
(4008, 110, 'Saturday', '12:00:00', 1, 1),
(4009, 110, 'Sunday', '12:00:00', 1, 1),
(4010, 110, 'Monday', '12:30:00', 1, 0),
(4011, 110, 'Tuesday', '12:30:00', 1, 1),
(4012, 110, 'Wednesday', '12:30:00', 1, 1),
(4013, 110, 'Thursday', '12:30:00', 1, 1),
(4014, 110, 'Friday', '12:30:00', 1, 1),
(4015, 110, 'Saturday', '12:30:00', 1, 1),
(4016, 110, 'Sunday', '12:30:00', 1, 1),
(4017, 110, 'Monday', '13:00:00', 1, 1),
(4018, 110, 'Tuesday', '13:00:00', 1, 1),
(4019, 110, 'Wednesday', '13:00:00', 1, 1),
(4020, 110, 'Thursday', '13:00:00', 1, 1),
(4021, 110, 'Friday', '13:00:00', 1, 1),
(4022, 110, 'Saturday', '13:00:00', 1, 1),
(4023, 110, 'Sunday', '13:00:00', 1, 1),
(4024, 110, 'Monday', '13:30:00', 1, 1),
(4025, 110, 'Tuesday', '13:30:00', 1, 1),
(4026, 110, 'Wednesday', '13:30:00', 1, 1),
(4027, 110, 'Thursday', '13:30:00', 1, 1),
(4028, 110, 'Friday', '13:30:00', 1, 1),
(4029, 110, 'Saturday', '13:30:00', 1, 1),
(4030, 110, 'Sunday', '13:30:00', 1, 1),
(4031, 110, 'Monday', '14:00:00', 1, 1),
(4032, 110, 'Tuesday', '14:00:00', 1, 1),
(4033, 110, 'Wednesday', '14:00:00', 1, 1),
(4034, 110, 'Thursday', '14:00:00', 1, 1),
(4035, 110, 'Friday', '14:00:00', 1, 1),
(4036, 110, 'Saturday', '14:00:00', 1, 1),
(4037, 110, 'Sunday', '14:00:00', 1, 1),
(4038, 110, 'Monday', '14:30:00', 1, 0),
(4039, 110, 'Tuesday', '14:30:00', 1, 1),
(4040, 110, 'Wednesday', '14:30:00', 1, 1),
(4041, 110, 'Thursday', '14:30:00', 1, 1),
(4042, 110, 'Friday', '14:30:00', 1, 1),
(4043, 110, 'Saturday', '14:30:00', 1, 1),
(4044, 110, 'Sunday', '14:30:00', 1, 1),
(4045, 110, 'Monday', '15:00:00', 1, 0),
(4046, 110, 'Tuesday', '15:00:00', 1, 1),
(4047, 110, 'Wednesday', '15:00:00', 1, 1),
(4048, 110, 'Thursday', '15:00:00', 1, 1),
(4049, 110, 'Friday', '15:00:00', 1, 1),
(4050, 110, 'Saturday', '15:00:00', 1, 1),
(4051, 110, 'Sunday', '15:00:00', 1, 1),
(4052, 110, 'Monday', '15:30:00', 1, 1),
(4053, 110, 'Tuesday', '15:30:00', 1, 1),
(4054, 110, 'Wednesday', '15:30:00', 1, 1),
(4055, 110, 'Thursday', '15:30:00', 1, 1),
(4056, 110, 'Friday', '15:30:00', 1, 1),
(4057, 110, 'Saturday', '15:30:00', 1, 1),
(4058, 110, 'Sunday', '15:30:00', 1, 1),
(4059, 110, 'Monday', '16:00:00', 1, 1),
(4060, 110, 'Tuesday', '16:00:00', 1, 1),
(4061, 110, 'Wednesday', '16:00:00', 1, 1),
(4062, 110, 'Thursday', '16:00:00', 1, 1),
(4063, 110, 'Friday', '16:00:00', 1, 1),
(4064, 110, 'Saturday', '16:00:00', 1, 1),
(4065, 110, 'Sunday', '16:00:00', 1, 1),
(4066, 110, 'Monday', '16:30:00', 1, 1),
(4067, 110, 'Tuesday', '16:30:00', 1, 1),
(4068, 110, 'Wednesday', '16:30:00', 1, 1),
(4069, 110, 'Thursday', '16:30:00', 1, 1),
(4070, 110, 'Friday', '16:30:00', 1, 1),
(4071, 110, 'Saturday', '16:30:00', 1, 1),
(4072, 110, 'Sunday', '16:30:00', 1, 1),
(4073, 110, 'Monday', '17:00:00', 1, 1),
(4074, 110, 'Tuesday', '17:00:00', 1, 1),
(4075, 110, 'Wednesday', '17:00:00', 1, 1),
(4076, 110, 'Thursday', '17:00:00', 1, 1),
(4077, 110, 'Friday', '17:00:00', 1, 1),
(4078, 110, 'Saturday', '17:00:00', 1, 1),
(4079, 110, 'Sunday', '17:00:00', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`appID`),
  ADD KEY `FK` (`doctor_id`),
  ADD KEY `patient_id` (`pateint_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`com_id`),
  ADD KEY `postid` (`postid`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`doctorID`);

--
-- Indexes for table `doctorspeciality`
--
ALTER TABLE `doctorspeciality`
  ADD PRIMARY KEY (`id_s_d`),
  ADD KEY `FK` (`F_specialityID`,`F_doctorID`),
  ADD KEY `F_doctorID` (`F_doctorID`);

--
-- Indexes for table `doctor_education`
--
ALTER TABLE `doctor_education`
  ADD PRIMARY KEY (`D_E_ID`),
  ADD KEY `D_FID` (`D_FID`);

--
-- Indexes for table `doctor_experience`
--
ALTER TABLE `doctor_experience`
  ADD PRIMARY KEY (`experience_id`),
  ADD KEY `doctor_FID` (`doctor_FID`);

--
-- Indexes for table `doctor_services`
--
ALTER TABLE `doctor_services`
  ADD PRIMARY KEY (`services_id`),
  ADD KEY `doctor_s_id` (`doctor_s_id`);

--
-- Indexes for table `doctor_view`
--
ALTER TABLE `doctor_view`
  ADD PRIMARY KEY (`view_id`),
  ADD KEY `doctor_id` (`doctor_id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`docID`),
  ADD KEY `FK` (`pateint_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`feedback_id`),
  ADD KEY `feedback_doctor_id` (`feedback_doctor_id`,`feedback_user_id`),
  ADD KEY `feedback_user_id` (`feedback_user_id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pateint`
--
ALTER TABLE `pateint`
  ADD PRIMARY KEY (`patientID`),
  ADD KEY `user_ID` (`user_ID`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payID`),
  ADD KEY `FK` (`doctorID`,`adminID`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `speciality`
--
ALTER TABLE `speciality`
  ADD PRIMARY KEY (`specialityID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitingtime`
--
ALTER TABLE `visitingtime`
  ADD PRIMARY KEY (`vtID`),
  ADD KEY `doctorID` (`doctorID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `appID` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=795;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `com_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `doctorID` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `doctorspeciality`
--
ALTER TABLE `doctorspeciality`
  MODIFY `id_s_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `doctor_education`
--
ALTER TABLE `doctor_education`
  MODIFY `D_E_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `doctor_experience`
--
ALTER TABLE `doctor_experience`
  MODIFY `experience_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `doctor_services`
--
ALTER TABLE `doctor_services`
  MODIFY `services_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `doctor_view`
--
ALTER TABLE `doctor_view`
  MODIFY `view_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `docID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `feedback_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=302;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pateint`
--
ALTER TABLE `pateint`
  MODIFY `patientID` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payID` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `speciality`
--
ALTER TABLE `speciality`
  MODIFY `specialityID` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `visitingtime`
--
ALTER TABLE `visitingtime`
  MODIFY `vtID` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4080;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `appointment_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`doctorID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `appointment_ibfk_2` FOREIGN KEY (`pateint_id`) REFERENCES `pateint` (`patientID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `appointment_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`postid`) REFERENCES `post` (`post_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `doctorspeciality`
--
ALTER TABLE `doctorspeciality`
  ADD CONSTRAINT `doctorspeciality_ibfk_1` FOREIGN KEY (`F_doctorID`) REFERENCES `doctor` (`doctorID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `doctorspeciality_ibfk_2` FOREIGN KEY (`F_specialityID`) REFERENCES `speciality` (`specialityID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `doctor_education`
--
ALTER TABLE `doctor_education`
  ADD CONSTRAINT `doctor_education_ibfk_1` FOREIGN KEY (`D_FID`) REFERENCES `doctor` (`doctorID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `doctor_experience`
--
ALTER TABLE `doctor_experience`
  ADD CONSTRAINT `doctor_experience_ibfk_1` FOREIGN KEY (`doctor_FID`) REFERENCES `doctor` (`doctorID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `doctor_services`
--
ALTER TABLE `doctor_services`
  ADD CONSTRAINT `doctor_services_ibfk_1` FOREIGN KEY (`doctor_s_id`) REFERENCES `doctor` (`doctorID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `doctor_view`
--
ALTER TABLE `doctor_view`
  ADD CONSTRAINT `doctor_view_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`doctorID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `document_ibfk_1` FOREIGN KEY (`pateint_id`) REFERENCES `pateint` (`patientID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`feedback_doctor_id`) REFERENCES `doctor` (`doctorID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `feedback_ibfk_2` FOREIGN KEY (`feedback_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`cat_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `visitingtime`
--
ALTER TABLE `visitingtime`
  ADD CONSTRAINT `visitingtime_ibfk_1` FOREIGN KEY (`doctorID`) REFERENCES `doctor` (`doctorID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
